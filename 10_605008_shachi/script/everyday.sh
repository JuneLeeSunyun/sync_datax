#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "15 day ago" +%Y-%m-%d`
else
    s_day=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
else
    e_day=$ed
fi

Location='/opt/sydata/datax/bin'

cd $Location;
# python datax.py ../job/ms2my/s2m_item_class.json;
python datax.py ../job/ms2my/s2m_category.json;
python datax.py ../job/ms2my/s2m_size.json;
python datax.py ../job/ms2my/s2m_color.json;
python datax.py ../job/ms2my/s2m_sea.json;
python datax.py ../job/ms2my/s2m_item.json;
python datax.py ../job/ms2my/s2m_barcode.json;
#python datax.py ../job/ms2my/s2m_cup.json;
python datax.py ../job/ms2my/s2m_itemcolor.json;
python datax.py ../job/ms2my/s2m_itemsize.json;
python datax.py ../job/ms2my/s2m_shop.json;
# python datax.py ../job/ms2my/s2m_shop_type.json;
# python datax.py ../job/ms2my/s2m_typeshop.json;
#python datax.py ../job/ms2my/s2m_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/ms2my/s2m_staff_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
#python datax.py ../job/ms2my/s2m_shop_map.json;
python datax.py ../job/ms2my/s2m_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/s2m_stock.json;
# python datax.py ../job/ms2my/s2m_instock.json;
python datax.py ../job/ms2my/s2m_staff.json;
# python datax.py ../job/ms2my/s2m_plani.json;
cd /opt/sydata/lsl_sync_c;python3 lsl_sync_c.py
