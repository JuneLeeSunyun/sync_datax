<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
//decode(aa,null, '当空时要填充的内容 '， '非空时内容 ', '默认内容 ')

$config['cid'] = 10;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['shop_count'] = 20;
$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
    'table' => 'CUST',
    'columns' => array(
        'vip_no' => 'rtrim(SYS_PRSNL.PRSNL_CODE)',
        'cust_name' => "SYS_PRSNL.FULL_NAME",
        'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
        'cust_sex' => "(case when SYS_PRSNL.GENDER ='F' then 2 else 1 end)",
        'age' => "' '",
        'born_date' => "convert(varchar(10), CUST.BIRTHDAY, 120)",
        'register_date' => "convert(varchar(10), CUST.REG_DATE, 120)",
        'vip_level' => "isnull(CUST.CUST_GRD,' ')",
        'mobile' => "isnull(SYS_PRSNL.MOBILE_NUM,' ')",
        'address' => "isnull(SYS_PRSNL.ADDRESS,' ')",
        'staff_id' => "' '",
        'vip_state' => "(case when CUST.CUST_STATUS='A' then 1 else 2 end)",
        'active_money' => "0",
        'vip_points' => "CUST.RATING_PNT"
    ),
    'join' => array('SYS_PRSNL' => 'SYS_PRSNL.PRSNL_ID =CUST.CUST_ID', 'SYS_UNIT' => 'CUST.SV_UNIT_ID =SYS_UNIT.UNIT_ID'),
    'where' => "SYS_PRSNL.PRSNL_CODE is not null and SYS_PRSNL.PRSNL_CODE <> ''",
    'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
    'date_key' => '',
    'sy_key' => array('vip_no'),
    'duplicate' => array('cust_name', 'shop_code', 'cust_sex', 'age', 'born_date', 'register_date', 'vip_level', 'mobile', 'address', 'staff_id', 'vip_state', 'active_money'),    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'by_shop' => false,
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
    'table' => 'SYS_CODE_DTL',
    'columns' => array(
        'ic_id' => 'CODE',
        'ic_name' => "DESCRIPTION",
        'ic_code' => "CODE",
        'ic_prop' => "'2'",
        'ic_order' => '999'//客户表没有这个字段
    ),
    'join' => array(),
    'where' => "CODE_TYPE='MKT_TYPE'",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('ic_id'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('ic_name')
);


$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
    'table' => 'SPEC_SCP_DTL',
    'columns' => array(
        'size_group' => 'SPEC_SCP_DTL.SPEC_SCP_ID',
        'size_id' => 'SPEC.SPEC_NUM',
        'size_desc' => 'SPEC.SPEC_NAME',
        'size_order' => 'SPEC.SPEC_ID'
    ),
    'join' => array('SPEC' => 'SPEC_SCP_DTL.SPEC_ID=SPEC.SPEC_ID'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('size_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
    'table' => '',
    'columns' => array(
// 		'id' => 'id',
        'brand_id' => '',
        'brand_name' => '',
        'sort_order' => "' '",
        'active' => '1'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('brand_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
    'table' => 'PRODUCT',
    'columns' => array(
        'bar_no' => 'PRODUCT.PROD_CODE',    //有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
        'item_id' => 'PROD_CLS.PROD_CLS_CODE',
        'color_id' => 'COLOR.COLOR_CODE',
        'size_id' => 'SPEC.SPEC_NAME',
        'color_name' => 'COLOR.COLOR_NAME'
    ),
    'join' => array('PROD_CLS' => 'PRODUCT.PROD_CLS_ID=PROD_CLS.PROD_CLS_ID', 'COLOR' => 'PRODUCT.COLOR_ID =COLOR.COLOR_ID', 'SPEC' => 'PRODUCT.SPEC_ID=SPEC.SPEC_ID'),//表的关联
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'by_limit' => true,
    'shop_code' => '',
    'sy_key' => array('bar_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
    'table' => 'COLOR',
    'columns' => array(
        'color_id' => 'COLOR_CODE',
        'color_desc' => 'COLOR_NAME'
    ),
    'join' => array(),
    'where' => '',
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
    'table' => 'PROD_CLS',
    'columns' => array(
        'item_id' => 'rtrim(PROD_CLS_CODE)',
        'size_group' => 'SPEC_SCP_ID'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'by_limit' => true,
    'shop_code' => '',
    'sy_key' => array('item_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
    'table' => 'PRODUCT',
    'columns' => array(
        'item_id' => 'PROD_CLS.PROD_CLS_CODE',
        'color_id' => 'COLOR.COLOR_CODE',
        'item_picture' => "rtrim(PROD_CLS.PROD_CLS_CODE)+COLOR.COLOR_CODE+'.jpg'"
    ),
    'join' => array('PROD_CLS' => 'PRODUCT.PROD_CLS_ID=PROD_CLS.PROD_CLS_ID', 'COLOR' => 'PRODUCT.COLOR_ID =COLOR.COLOR_ID'),
    'where' => '',
    'date_key' => '',
    'group_by' => 'PROD_CLS.PROD_CLS_CODE, COLOR.COLOR_CODE',
    'by_shop' => false,
    'by_limit' => true,
    'shop_code' => '',
    'sy_key' => array('item_id', 'color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'SYS_CODE_DTL',
    'columns' => array(
        'sea_id'=> 'CODE',
        'sea_name' =>'DESCRIPTION',
        'sea_desc'=>'DESCRIPTION',
        's_mon' => "(case CODE
				when '01' then '02'
				when '07' then '02'
				when '18' then '02'
				when '05' then '05'
				when '03' then '08'
				when '06' then '08'
				when '02' then '11'
				else '01' end
			)",
        'e_mon' => "(case CODE
				when '01' then '04'
				when '07' then '07'
				when '18' then '04'
				when '05' then '07'
				when '03' then '10'
				when '06' then '01'
				when '02' then '01'
				else '12' end
			)",
        'is_span_sea' => "(case CODE
			when '02' then '1'
			when '06' then '1'
			else '0' end)",
		'sea_order' => '999'
    ),
    'join' => array(),
    'where' => "CODE_TYPE='SEASON'",
    'date_key' => '',
    //'group_by' => array(''),
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('sea_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//商品资料
$config['sea_span'] = "'02' or sea_id = '06' or sea_id = '08' or sea_id = '09' or sea_id = '10' or sea_id = '11' or sea_id = '12' or sea_id = '13' or sea_id = '14' or sea_id = '15' or sea_id = '16' or sea_id = '17'";        //跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');    //跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(        //季度具体时间段
    "'01'" => array('from' => '02', 'to' => '04'),
    "'07'" => array('from' => '02', 'to' => '07'),
    "'18'" => array('from' => '02', 'to' => '04'),
    "'05'" => array('from' => '05', 'to' => '07'),
    "'03'" => array('from' => '08', 'to' => '10'),
    "'06'" => array('from' => '08', 'to' => '01'),
    "'02'" => array('from' => '11', 'to' => '01'),
    "'08'" => array('from' => '01', 'to' => '12'),
    "'09'" => array('from' => '01', 'to' => '12'),
    "'10'" => array('from' => '01', 'to' => '12'),
    "'11'" => array('from' => '01', 'to' => '12'),
    "'12'" => array('from' => '01', 'to' => '12'),
    "'13'" => array('from' => '01', 'to' => '12'),
    "'14'" => array('from' => '01', 'to' => '12'),
    "'15'" => array('from' => '01', 'to' => '12'),
    "'16'" => array('from' => '01', 'to' => '12'),
    "'17'" => array('from' => '01', 'to' => '12'),
    "'04'" => array('from' => '01', 'to' => '12')
);

//商品表
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
    'table' => 'PROD_CLS',
    'columns' => array(
        'item_id' => 'rtrim(PROD_CLS.PROD_CLS_CODE)',
        'brand_id' => "' '",
        'year_id' => 'PROD_CLS.YEAR_VAL',
        'sea_id' => 'PROD_CLS.SEASON',
        'sea_name' => 'SEASON.DESCRIPTION',
        'ic_id' => 'PROD_CLS.MKT_TYPE',
        'ic_name' =>'MKT_TYPE.DESCRIPTION',
        'is_acc' => "'N'",
        'sale_price' => '0',
        'item_desc' => 'PROD_NAME',
        'sales_point' => "' '",
        'item_file' => "rtrim(PROD_CLS.PROD_CLS_CODE)+'.jpg'",
        'info_url' => "' '",
        'style_id' => "' '",
        's_date' => "' '",
        'e_date' => "' '",
    ),
    'join' => array("(select CODE,DESCRIPTION from SYS_CODE_DTL where CODE_TYPE='SEASON') as SEASON " => 'PROD_CLS.SEASON = SEASON.CODE', "(select CODE,DESCRIPTION from SYS_CODE_DTL where CODE_TYPE='MKT_TYPE') as MKT_TYPE" => 'PROD_CLS.MKT_TYPE = MKT_TYPE.CODE'),
    'where' => "",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id'),
    'duplicate' => array('sale_price', 'ic_id', 'ic_name', 's_date', 'e_date'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
    'table' => 'SHOP',
    'columns' => array(
        'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
        'shop_name' => 'SYS_UNIT.UNIT_NAME',
        'model_no' => "' '",
        'branch_id' => "' '",
        'open_date' => "convert(varchar(10), SHOP.OPEN_DATE, 120)",
        'shop_area' => "' '",
        'ref_shopcode' => "' '",//参考代号
        'shop_level' => "' '",
        'stand_staffs' => "' '",
        'real_staff' => "' '",
        'max_sku' => "' '",
        'min_sku' => "' '",
        'max_qtys' => "' '",
        'min_qtys' => "' '",
        'tel_no' => 'SYS_UNIT.TEL_NUM',
        'addr' => 'SYS_UNIT.ADDRESS',
        'contact' => "' '",
        'shop_state' => "1"
    ),
    'join' => array(
    	'SYS_UNIT' => 'SHOP.SHOP_ID=SYS_UNIT.UNIT_ID'),
    'where' => "SYS_UNIT.UNIT_STATUS='A'",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),
    'duplicate' => array('shop_name', 'open_date', 'tel_no', 'addr', 'shop_state'),
    'is_multi' => true //多数据库情况下是否这个数据库这个表要抽
);

$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
    'table' => 'SHOP_EMP',
    'columns' => array(
        'staff_id' => 'rtrim(SYS_PRSNL.PRSNL_CODE)',
        'staff_name' => 'SYS_PRSNL.FULL_NAME',
        'staff_shop_code' => "SYS_UNIT.UNIT_CODE",
        'staff_shop_name' => "UNIT_NAME"
    ),
    'join' => array(
    	'SYS_UNIT' => 'SHOP_EMP.SHOP_ID =SYS_UNIT.UNIT_ID',
    	'SYS_PRSNL' => 'SHOP_EMP.EMPL_ID =SYS_PRSNL.PRSNL_ID'),
    'where' => "",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('staff_id'),
    'duplicate' => array('staff_name', 'staff_shop_code', 'staff_shop_name'),
    'is_multi' => true //多数据库情况下是否这个数据库这个表要抽
);

$sales_info = 'sales_info';
$config[$sales_info] = array(
    'table' => 'RLB',
    'columns' => array(
        'inv_no' => "cast(SYS_UNIT.UNIT_CODE as varchar)+'-'+cast(RLB.RLB_NUM as varchar)",
        'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
        'sale_date' => "convert(varchar(10), RLB.FSCL_DATE, 120)",
        'vip_state' => "(case when len(RLB.CUST_ID)>0 then 1 else 0 end)",
        'staff_id' => 'SYS_PRSNL.PRSNL_CODE',
        'vip_no' => "(select PRSNL_CODE from SYS_PRSNL where PRSNL_ID=RLB.CUST_ID)",
        'inv_money' => 'RLB.TTL_PAY_VAL',
        'inv_qtys' => 'RLB.TTL_QTY',
        'tag_money' => '0'
    ),
    'join' => array(
        'SYS_UNIT' => 'RLB.SHOP_ID=SYS_UNIT.UNIT_ID',
        ' (select RLB_DTL.SHOP_ID, RLB_DTL.RLB_NUM, max(RLB_DTL.SELLER_ID) as SELLER_ID from RLB_DTL group by RLB_DTL.SHOP_ID, RLB_DTL.RLB_NUM )  d ' => 'd.SHOP_ID = RLB.SHOP_ID and d.RLB_NUM = RLB.RLB_NUM ',
        'SYS_PRSNL' => 'd.SELLER_ID=SYS_PRSNL.PRSNL_ID'
    ),
    'where' => '',
    'date_key' => "RLB.FSCL_DATE",
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
    'table' => 'RLB_DTL',
    'columns' => array(
        'inv_no' => "cast(SYS_UNIT.UNIT_CODE as varchar)+'-'+cast(RLB_DTL.RLB_NUM as varchar)",
        'item_id' => 'PROD_CLS.PROD_CLS_CODE',
        'color_id' => 'COLOR.COLOR_CODE',
        'size_id' => 'SPEC.SPEC_NAME',
        'inv_qtys' => 'RLB_DTL.QTY',
        'inv_money' => 'RLB_DTL.VAL',
        'tag_price' => 'RLB_DTL.UNIT_PRICE'
    ),
    'join' => array(
    	'RLB' => 'RLB_DTL.SHOP_ID = RLB.SHOP_ID and RLB_DTL.RLB_NUM = RLB.RLB_NUM',
        'SYS_UNIT' => 'RLB.SHOP_ID=SYS_UNIT.UNIT_ID',
        'PRODUCT' => 'RLB_DTL.PROD_ID=PRODUCT.PROD_ID',
        'PROD_CLS' => 'PRODUCT.PROD_CLS_ID=PROD_CLS.PROD_CLS_ID',
        'COLOR' => 'PRODUCT.COLOR_ID =COLOR.COLOR_ID',
        'SPEC' => 'PRODUCT.SPEC_ID=SPEC.SPEC_ID'
    ),
    'where' => '',
    'date_key' => "RLB.FSCL_DATE",
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => 'rtrim(SYS_UNIT.UNIT_CODE)',
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//销售目标
$plani_type_one_month_a_line = 'plani_with_type1';    //数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';        //数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';    //数据源表中一行记录代表的时间为某个时间段的情况'plan_date' =>'C_STOREMARKITEM.MONTHDATE',
$sy_v_plani = '';
$config[$sy_v_plani] = array(
    'table' => 'C_STOREMARKITEM',//C_STOREMARKITEM
    'columns' => array(
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'plan_date' =>  array(
            'year' => '', 	//年度字段名
            'month' => '', 	//月份字段名
            'day' => "to_char(to_date(C_STOREMARKITEM.MONTHDATE,'yyyy-mm-dd'),'yyyy-mm-dd')"),
        'plan_amt' => 'C_STOREMARKITEM.TOT_AMT_MARK'
    ),
    'join' => array('C_STOREMARK' => 'C_STOREMARKITEM.C_STOREMARK_ID=C_STOREMARK.ID','C_STORE'=>'C_STOREMARK.C_STORE_ID =C_STORE.ID'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => true,
    'shop_code' => 'rtrim(C_STORE.CODE)',
    'sy_key' => array('shop_code', 'plan_date'),
    'duplicate' => array('plan_amt'),
    'is_multi' => false,    //多数据库情况下是否这个数据库这个表要抽
    'type' => $plani_type_one_day_a_line
);


$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
    'table' => 'WAREH_STK',
    'columns' => array(
        'shop_code' => 'rtrim(UNIT_CODE)',
        'item_id' => 'rtrim(PROD_CLS.PROD_CLS_CODE)',
        'color_id' => 'COLOR.COLOR_CODE',
        'size_id' => 'SPEC.SPEC_NAME',
        'stock_qtys' => '(WAREH_STK.STK_ON_HAND-WAREH_STK.QTY_CMTD)'
    ),
    'join' => array(
        'SYS_UNIT' => 'WAREH_STK.WAREH_ID=SYS_UNIT.UNIT_ID',
        'PRODUCT' => 'WAREH_STK.PROD_ID=PRODUCT.PROD_ID',
        'PROD_CLS' => 'PRODUCT.PROD_CLS_ID=PROD_CLS.PROD_CLS_ID',
        'COLOR' => 'PRODUCT.COLOR_ID =COLOR.COLOR_ID',
		'SPEC' => 'PRODUCT.SPEC_ID=SPEC.SPEC_ID'
    ),
    'where' => "",
    'group_by' => array(),
    'date_key' => '',
    'by_shop' => false,
    'by_limit' => true,
    'shop_code' => 'rtrim(UNIT_CODE)',
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => 'GRN',
	'columns' => array(
		'stock_date' => "convert(varchar(10), STB.DOC_DATE, 120)",
		'shop_code' => 'SYS_UNIT.UNIT_CODE',
		'item_id' => 'PROD_CLS.PROD_CLS_CODE',
		'color_id' => 'COLOR.COLOR_CODE',
		'size_id' => 'SPEC.SPEC_NUM',
		'stock_qtys' => 'sum(STB_DTL.QTY)'
	),
	'join' => array(
                    'STB' => 'GRN.GRN_NUM=STB.STB_NUM and GRN.UNIT_ID=STB.UNIT_ID',
					'STB_DTL' => 'STB_DTL.UNIT_ID=STB.UNIT_ID and STB_DTL.STB_NUM=STB.STB_NUM',
					'SYS_UNIT' => 'STB.WAREH_ID=SYS_UNIT.UNIT_ID',
					'PRODUCT ' => 'STB_DTL.PROD_ID=PRODUCT.PROD_ID',
					'PROD_CLS ' => 'PRODUCT.PROD_CLS_ID=PROD_CLS.PROD_CLS_ID',
					'COLOR ' => 'PRODUCT.COLOR_ID =COLOR.COLOR_ID',
					'SPEC ' => 'SPEC.SPEC_ID=PRODUCT.SPEC_ID'),
	'where' => "STB.SRC_DOC_NUM not in ('*')
and GRN.PROGRESS in ('RD','SI','SD')",
	'group_by' => array('STB.DOC_DATE','SYS_UNIT.UNIT_CODE','PROD_CLS.PROD_CLS_CODE','COLOR.COLOR_CODE','SPEC.SPEC_NUM'),
	'date_key' => "convert(varchar(10), STB.DOC_DATE, 120)",
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'),
	'by_limit' => true,
	'duplicate' => '',//array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
/* End of file config.php */
/* Location: ./application/config/config.php */
