<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

/**
	数据库与配置文件对应关系，结构为
	$config['database_resource'] = array(
		'a' => array('b' => 'c'),
		...
	);
	其中，
		a为自定义（需要与报表数据库中sy_v_data_resource表config_key对应）
		b为database.config中抽数数据源库对应的item
		c为数据库b对应的配置文件文件名，文件内容为客户数据源库与三云报表数据库的表字段匹配信息
		
*/
$config['database_resource'] = array(
	'all' => array(
		'crosslink' => 'config_report_crosslink_v3'
	),
	'crosslink' => array('crosslink' => 'config_report_crosslink_v3')
);


$config['delete_sale_first'] = false;
$config['delete_group_first'] = false;


//计算相关语句
$config['sales_t_info_insert_method'] = '';
$config['sy_v_sales_insert_method'] = '';


//基础同步结束后，在基础服务器数据库中执行query_after_sync，为空时不执行
$config['query_after_sync'] = "";

$config['query_before_sync'] = "";


$config['com_no'] = '605008';//零售链授权号
$config['lsl_admin_ip'] = 'lsla.sunyunsoft.cn';
$config['lsl_admin_port'] = 80;
//建模服务器到零售链服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_lsl'] = true;
//建模服务器到零售链服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_lsl'] = array(
// 	'lsla_title' => 0,
// 	'lsla_dept' => 0,
	'lsla_groupshop' => 0,
	'lsla_shop' => 1,
	'lsla_group' => 0,
 	'lsla_category' => 1,
	'lsla_item' => 1,
	'lsla_color' => 1,
	'lsla_size' => 1,
	'lsla_itemcolor' => 1,
// 	'lsla_config' => 0,
// 	'lsla_user' => 0,
// 	'lsla_user_leader' => 0,
	'lsla_sea' => 1
);

$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';

$group_id = '9999';
$group_name = '未分组';
//基础同步结束后，在基础服务器数据库中执行query_after_sync，为空时不执行
$config['query_after_sync'] = "insert into `sy_group`(`group_id`, `group_name`, `group_type`, `is_ok`, `order_seq`) values ('$group_id', '$group_name', '2', '1', '999'); 
delete from sy_shopgroup where group_id = '$group_id';
insert into sy_shopgroup(`shop_code`, `group_id`, `order_seq`) (
select shop_code, '$group_id' as group_id, '999' as order_seq from sy_shop 
where shop_code not in (
select shop_code from sy_shopgroup
)
)";

$config['query_after_sync_to_lsl'] = "insert into `lsla_group`(`group_code`, `group_name`, `group_level`, `parent_group_code`, `group_state`, `group_order_seq`) values ('$group_id', '$group_name', '1', '$group_id', '1', '999'); 
delete from lsla_groupshop where group_code = '$group_id';
insert into lsla_groupshop(`shop_code`, `group_code`, `gs_order_seq`) (
select shop_code, '$group_id' as group_code, '999' as gs_order_seq from lsla_shop 
where shop_code not in (
select shop_code from lsla_groupshop
)
)";
/* End of file config.php */
/* Location: ./application/config/config.php */
