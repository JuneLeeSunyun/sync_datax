#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
    s_day=`date -d "1 day ago" +%Y-%m-%d`
    s_mday=`date +%Y-%m-01`
else
    s_day=$sd
    s_mday=$sd
fi

if [ ! -n "$ed" ]; then
    e_day=`date +%Y-%m-%d`
    e_mday=`date -d "1 month" +%Y-%m-01`
else
    e_day=$ed
    e_mday=$ed
fi

Location='/opt/sydata/datax/bin'

cd $Location;

python datax.py ../job/my2my/m2m_brand.json;
python datax.py ../job/my2my/m2m_item_class.json;
# python datax.py ../job/my2my/m2m_item_style.json;
python datax.py ../job/my2my/m2m_category.json;
python datax.py ../job/my2my/m2m_size.json;
python datax.py ../job/my2my/m2m_color.json;
python datax.py ../job/my2my/m2m_sea.json;
python datax.py ../job/my2my/m2m_item.json;
python datax.py ../job/my2my/m2m_barcode.json;
python datax.py ../job/my2my/m2m_itemcolor.json;
python datax.py ../job/my2my/m2m_itemsize.json;
python datax.py ../job/my2my/m2m_shop.json;
# python datax.py ../job/my2my/m2m_shop_type.json;
# python datax.py ../job/my2my/m2m_typeshop.json;
python datax.py ../job/my2my/m2m_vip.json;
# python datax.py ../job/my2my/m2m_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_staff_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
#python datax.py ../job/my2my/m2m_shop_map.json;
# python datax.py ../job/my2my/m2m_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
# python datax.py ../job/my2my/m2m_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_stock.json;
# python datax.py ../job/my2my/m2m_instock.json;
# python datax.py ../job/my2my/m2m_staff.json;
python datax.py ../job/my2my/m2m_plani_d.json -p "-Ds_day=$s_mday -De_day=$e_mday"
