<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
本库字头‘A’，编号‘01’，名称‘AMASS’
2020-02-29 取消字头
*/
$head_code = "'01'";
$head_character = '';//"'A'";
$head_name = "'AMASS'";

$config['cid'] = 4;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;
$config['shop_count'] = 30;

// $config['query_before_sync_to_1'] = "INSERT IGNORE INTO `sy_v_item_class` (`class_code`, `class_id`, `class_name`, `class_order`, `class_prop`, `class_state`) VALUES ($head_code, $head_code, $head_name, 999, '2', 1)";
$config['query_before_sync_to_1'] = '';//"INSERT IGNORE INTO `sy_v_item_class` (`class_code`, `class_id`, `class_name`, `class_order`, `class_prop`, `class_state`) VALUES ('9999', '9999', '未分类', 999, '2', 1);";
$config['query_before_sync_to_2'] = '';
$config['query_before_sync_to_3'] = '';
$config['query_before_sync_to_4'] = '';
$config['query_before_sync_to_5'] = '';
$config['query_before_sync_to_6'] = '';
$config['query_before_sync_to_7'] = '';
$config['query_before_sync_to_8'] = '';

//$config['query_after_sync_to_1'] = 'insert IGNORE into sy_v_color (color_id, color_desc) SELECT CONCAT(color_id, cup_id) as color_id, CONCAT(color_desc, cup_desc) as color_desc FROM data_sy_v_color, sy_v_cup;'; //insert IGNORE into sy_v_color (color_id, color_desc) SELECT CONCAT(color_id, cup_id) as color_id, CONCAT(color_desc, cup_desc) as color_desc FROM data_sy_v_color, sy_v_cup
$config['query_after_sync_to_1'] = '';//"INSERT IGNORE INTO `sy_v_category` (`ic_code`, `ic_id`, `ic_name`, `ic_order`, `ic_prop`, `ic_state`, `class_id`) VALUES ('9999', '9999', '未分类', 999, '2', 1, '9999');";
$config['query_after_sync_to_2'] = '';
$config['query_after_sync_to_3'] = '';
$config['query_after_sync_to_4'] = '';
$config['query_after_sync_to_5'] = '';
$config['query_after_sync_to_6'] = '';
$config['query_after_sync_to_7'] = '';
$config['query_after_sync_to_8'] = '';

$config['update_item_date'] = 'update_item_date';

//销售主表
$sales_info = 'sales_info';         //固定值，勿动
$config[$sales_info]['table'] = 'TF_RTL_VCHR_DLVR';   //客户数据源表名
$config[$sales_info]['columns'] = array(        //各字段对应
    'inv_no' => 'VCHR_ID',
    'shop_code' => 'SUBSTRING(VCHR_ID,8,6)',
    'sale_date' => 'convert(varchar(10), VCHR_DATE, 120)',
    'vip_state' => "(case ISNULL(CUST_MOBILE,0) when '0' then 0 when '' then 0 else 1 end)",
    'staff_id' => "SALE_STAFF1",
    'vip_no' => "(case ISNULL(CUST_MOBILE, '') when '' then '' else left(SUBSTRING(VCHR_ID,8,6), 4)+CUST_MOBILE end)",
    'inv_money' => 'AMT_REAL',
    'inv_qtys' => 'QTY',
    'tag_money' => 'AMT'
);
$config[$sales_info]['join'] = array();
$config[$sales_info]['where'] = "VCHR_STATUS ='20' and BIZ_TYPE ='00'";
$config[$sales_info]['date_key'] = 'convert(varchar(10), VCHR_DATE, 120)';        //表内是否有时间戳（用于抽数时时间约束）	//抽数方式，选填，参照文件开头 $operation_ 部分
$config[$sales_info]['shop_code'] = 'SUBSTRING(VCHR_ID,8,6)';
$config[$sales_info]['by_shop'] = false;    //是否根据店铺抽取，其值为true或false
$config[$sales_info]['sy_key'] = array('inv_no');
$config[$sales_info]['is_multi'] = true;
$config[$sales_info]['by_limit']=true;//分页


//销售明细
$sales_detail = 'sales_detail';
$config[$sales_detail]['table'] = 'TF_RTL_VCHR_DLVR_DETA';
$config[$sales_detail]['columns'] = array(
    'inv_no' => 'TF_RTL_VCHR_DLVR_DETA.VCHR_ID',
    'item_id' => "$head_character+rtrim(TF_RTL_VCHR_DLVR_DETA.GOODS_CODE)",
    'color_id' => 'TF_RTL_VCHR_DLVR_DETA.COLOR',
    'size_id' => 'TF_RTL_VCHR_DLVR_DETA.SIZE',
    'inv_qtys' => 'sum(TF_RTL_VCHR_DLVR_DETA.qty)',
    'inv_money' => 'sum(TF_RTL_VCHR_DLVR_DETA.AMT_REAL)',
    'tag_price' => "TF_RTL_VCHR_DLVR_DETA.PRC_R"

);
$config[$sales_detail]['join'] = array(
    'TF_RTL_VCHR_DLVR' => 'TF_RTL_VCHR_DLVR.VCHR_ID = TF_RTL_VCHR_DLVR_DETA.VCHR_ID');
$config[$sales_detail]['where'] = "";
$config[$sales_detail]['date_key'] = 'convert(varchar(10), VCHR_DATE, 120)';//'convert(varchar(10), vn_Check.CheckDate, 120)';
$config[$sales_detail]['shop_code'] = 'SUBSTRING(TF_RTL_VCHR_DLVR.VCHR_ID,8,6)';
$config[$sales_detail]['group_by'] = array('TF_RTL_VCHR_DLVR_DETA.VCHR_ID', "$head_character+rtrim(TF_RTL_VCHR_DLVR_DETA.GOODS_CODE)", 'TF_RTL_VCHR_DLVR_DETA.COLOR', 'TF_RTL_VCHR_DLVR_DETA.SIZE', 'TF_RTL_VCHR_DLVR_DETA.PRC_R');
$config[$sales_detail]['update'] = 'sales_update';
$config[$sales_detail]['by_shop'] = false;
$config[$sales_detail]['sy_key'] = array('inv_no');
$config[$sales_detail]['is_multi'] = true;
$config[$sales_detail]['by_limit']=true;//分页

//导购销售表
$sales_info = 'sy_v_staff_salesdet';         //固定值，勿动
$config[$sales_info]['table'] = "(select VCHR_ID, VCHR_DATE, CUST_MOBILE,QTY, AMT_REAL,AMT,SALE_STAFF1,SALE_STAFF2
from TF_RTL_VCHR_DLVR where VCHR_STATUS ='20' and BIZ_TYPE ='00') as AAA
UNPIVOT 
 (SALE_STAFF_value for staff_id IN (SALE_STAFF1,SALE_STAFF2)) AS BBB";   //客户数据源表名
$config[$sales_info]['columns'] = array(        //各字段对应
    'inv_no' => 'VCHR_ID',
    'shop_code' => 'SUBSTRING(VCHR_ID,8,6)',
    'sale_date' => 'convert(varchar(10), VCHR_DATE, 120)',
    'vip_state' => "(case ISNULL(CUST_MOBILE,0) when '0' then 0 when '' then 0 else 1 end)",
	'staff_state' =>"case when staff_id='SALE_STAFF1' then 1 else 0 end",
    'staff_id' => "SALE_STAFF_value",
    'vip_no' => "(case ISNULL(CUST_MOBILE, '') when '' then '' else left(SUBSTRING(VCHR_ID,8,6), 4)+CUST_MOBILE end)",
    'sale_money' => "AMT_REAL/2",
    'inv_qtys' => 'QTY',
    'inv_money' => 'AMT_REAL'
);
$config[$sales_info]['join'] = array();
$config[$sales_info]['where'] = "";
$config[$sales_info]['date_key'] = 'convert(varchar(10), VCHR_DATE, 120)';        //表内是否有时间戳（用于抽数时时间约束）	//抽数方式，选填，参照文件开头 $operation_ 部分
$config[$sales_info]['shop_code'] = 'SUBSTRING(VCHR_ID,8,6)';
$config[$sales_info]['by_shop'] = false;    //是否根据店铺抽取，其值为true或false
$config[$sales_info]['sy_key'] = array('inv_no');
$config[$sales_info]['is_multi'] = true;
$config[$sales_info]['by_limit']=true;//分页

/*
//条码
$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode]['table'] = 'TF_BAS_GOODS';
$config[$sy_v_barcode]['columns'] = array(
    'bar_no' => "$head_character+BARCODE_C+TD_BAS_SIZEGROUP_SIZE.SIZE_BAR_CODE",    //有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
    'item_id' => "$head_character+GOODS_CODE",
    'color_id' => "'NA'",
    'size_id' => 'TD_BAS_SIZEGROUP_SIZE.size_code',
    'color_name' => "'未定义'"
);
$config[$sy_v_barcode]['join'] = array(
    'TD_BAS_SIZEGROUP_SIZE' => 'TD_BAS_SIZEGROUP_SIZE.SIZE_GRP_CODE = TF_BAS_GOODS.SIZE_GRP_CODE'
);
$config[$sy_v_barcode]['where'] = "BARCODE_C<>''";
$config[$sy_v_barcode]['by_shop'] = false;
$config[$sy_v_barcode]['sy_key'] = array('bar_no');    //report_db中的目标表主键
$config[$sy_v_barcode]['is_multi'] = true;
$config[$sy_v_barcode]['by_limit']=true;//分页
$config[$sy_v_barcode]['date_key']='';
$config[$sy_v_barcode]['duplicate'] = array('item_id', 'color_id', 'size_id', 'color_name');


//商品资料
$config['sea_span'] = "'02' or sea_id = '06'";        //跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');    //跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(        //季度具体时间段
    "'03'" => array('from' => '02', 'to' => '04'),
    "'04'" => array('from' => '05', 'to' => '07'),
    "'05'" => array('from' => '08', 'to' => '10'),
    "'06'" => array('from' => '11', 'to' => '01'),
    "'01'" => array('from' => '02', 'to' => '07'),
    "'02'" => array('from' => '08', 'to' => '01')
);

$sy_v_item = 'sy_v_item';
$config[$sy_v_item]['table'] = 'TF_BAS_GOODS';
$config[$sy_v_item]['columns'] = array(
    'item_id' => "$head_character+rtrim(GOODS_CODE)",
    'item_key' => "$head_character+FASHION",
    'brand_id' => "''",
    'year_id' => "(case when ISNUMERIC(left(SERIES,2))=0 then '2010' else cast(20 as varchar)+cast(left(SERIES,2) as varchar) end)",
    'sea_id' => "(case when substring(SERIES,3,2)='SS' then '01' when substring(SERIES,3,2)='AW' then '02' else '07' end)",
    'sea_name' => "(case when substring(SERIES,3,2)='SS' then '春夏' when substring(SERIES,3,2)='AW' then '秋冬' else '无季节' end)",
    'ic_id' => "isnull(ic_new_id, '9999')",//"$head_character+(case when FAS_FL='' then '9999' else TD_BAS_PARAM.ITEM_CODE end)",
    'ic_name' => "isnull(REPLACE(REPLACE(ltrim(rtrim(ic_new_name)), CHAR(13), ''), CHAR(10), ''), '未分类')",//"$head_character+(case when FAS_FL='' then '未分类' else FAS_FL end)",
    'class_id' => "isnull(TD_BAS_CATG_GOODS.CATE_CODE, '9999')",//"$head_code",
    'class_name' => "isnull(CATE_NAME, '未分类')",//"$head_name",
    'is_acc' => "'N'",
    'sale_price' => 'PRC_R',
    'item_desc' => 'FAS_FL',
    'sales_point' => "''",
    'item_file' => "rtrim(GOODS_CODE)+'.jpg'",
    's_date' => "''",        //固定值，勿动，根据季度配置自动生成
    'e_date' => "''",        //固定值，勿动，根据季度配置自动生成
    'pub_date' => 'DISP_DATE'//"convert(varchar(10), newold, 120)"
);
$config[$sy_v_item]['join'] = array(
    'TD_BAS_PARAM' => 'TD_BAS_PARAM.ITEM_NAME = TF_BAS_GOODS.FAS_FL',
    'ERP..A_CATEGORY' => 'ERP..A_CATEGORY.ic_old_code = TD_BAS_PARAM.ITEM_CODE',
    'TD_BAS_CATG_GOODS' => 'ERP..A_CATEGORY.class_id = TD_BAS_CATG_GOODS.CATE_CODE'
);
$config[$sy_v_item]['where'] = "TD_BAS_PARAM.GRP_CODE ='30000003'";
$config[$sy_v_item]['date_key'] = '';
$config[$sy_v_item]['by_shop'] = false;
$config[$sy_v_item]['sy_key'] = array('item_id');
$config[$sy_v_item]['is_multi'] = true;
$config[$sy_v_item]['by_limit'] = false;//分页
$config[$sy_v_item]['duplicate'] = array('item_key', 'year_id', 'sea_id', 'sea_name', 'ic_id', 'ic_name', 'class_id', 'class_name', 'sale_price', 'item_desc', 'item_file', 's_date', 'e_date', 'pub_date');


//库存信息
$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock]['table'] = 'TF_INV_INVENTORY';
$config[$sy_v_stock]['columns'] = array(
    'shop_code' => 'TD_BAS_CHANNEL.CHNL_CODE+TD_BAS_STORE.ST_CODE',
    'item_id' => "$head_character+rtrim(TF_INV_INVENTORY.GOODS_CODE)",
    'color_id' => 'COLOR',
    'size_id' => 'TD_BAS_SIZEGROUP_SIZE.size_code',
    'stock_qtys' => 'sum(QTY_HAND)'    //多条记录算总值时，记得sum
);
$config[$sy_v_stock]['join'] = array(
    'TD_BAS_CHANNEL' => 'TD_BAS_CHANNEL.CHNL_ID = TF_INV_INVENTORY.CHNL_ID ',
    'TD_BAS_STORE' => 'TD_BAS_STORE.WH_ID = TF_INV_INVENTORY.WH_ID ',
    'TD_BAS_SIZEGROUP_SIZE' => 'TD_BAS_SIZEGROUP_SIZE.size_code = TF_INV_INVENTORY.SIZE'
);
$config[$sy_v_stock]['where'] = "QTY_HAND <> 0 and TD_BAS_CHANNEL.CHNL_CODE is not null and TD_BAS_STORE.ST_CODE is not null";
$config[$sy_v_stock]['shop_code'] = 'TD_BAS_CHANNEL.CHNL_CODE+TD_BAS_STORE.ST_CODE';
$config[$sy_v_stock]['date_key'] = '';
$config[$sy_v_stock]['group_by'] = array('TD_BAS_CHANNEL.CHNL_CODE+TD_BAS_STORE.ST_CODE', 'rtrim(TF_INV_INVENTORY.GOODS_CODE)', 'COLOR', 'TD_BAS_SIZEGROUP_SIZE.size_code');
$config[$sy_v_stock]['by_shop'] = false;
$config[$sy_v_stock]['sy_key'] = array('shop_code');    //本地表存在联合主键，所以多了这个，固定值，勿动
$config[$sy_v_stock]['is_multi'] = true;
$config[$sy_v_stock]['by_limit'] = true;

$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
	'table' => 'TD_BAS_CATG_GOODS',
	'columns' => array(
		'class_id' => 'CATE_CODE',
		'class_name' => "CATE_NAME",
		'class_code' => "CATE_CODE",
		'class_prop' => "'2'",
		'class_state' => "CATE_LEVEL",
		'class_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('class_id'),
	// 'duplicate' => array('class_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//品类
$sy_v_category = 'sy_v_category';
$config[$sy_v_category]['table'] = 'ERP..A_CATEGORY';//'TD_BAS_PARAM';
$config[$sy_v_category]['columns'] = array(
    'ic_id' => 'ic_new_id',//"$head_character+ITEM_CODE",
    'ic_name' => "REPLACE(REPLACE(ltrim(rtrim(ic_new_name)), CHAR(13), ''), CHAR(10), '')",//"$head_character+ITEM_NAME",
    'ic_code' => 'ic_new_id',//"$head_character+ITEM_CODE",
    'class_id' => "isnull(class_id,'9999')",//"$head_code",
    'ic_prop' => 'ic_prop',//"'2'",
    'ic_order' => 'ic_new_id',//"SORT_CODE",
    'ic_state' => 'ic_state'//'eff_tag'
);
$config[$sy_v_category]['join'] = array();
$config[$sy_v_category]['where'] = '';//"GRP_CODE ='30000003'";
$config[$sy_v_category]['duplicate'] = array('class_id', 'ic_name', 'ic_prop', 'ic_order', 'ic_state');    //由于抽数方式为operation_insert_duplicate（无则插入，有则更新），所以多了这一项，表明本地更新情况下，更新哪些字段
$config[$sy_v_category]['by_shop'] = false;
$config[$sy_v_category]['sy_key'] = array('ic_id');
$config[$sy_v_category]['is_multi'] = true;
$config[$sy_v_category]['by_limit'] = false;//分页
$config[$sy_v_category]['date_key']='';

//店铺(单独抽数成功)
$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop]['table'] = 'TD_BAS_STORE';
$config[$sy_v_shop]['columns'] = array(
    'shop_code' => 'TD_BAS_CHANNEL.CHNL_CODE+TD_BAS_STORE.st_code',
    'shop_name' => 'ST_NAME',
    'model_no' => "''",
    'shop_level' => "''",
    'open_date' => "isnull(convert(varchar(10), OPEN_DATE, 120), '')",
    'close_date'=> "''",
    'shop_area' => 'AREA',
    'stand_staffs' => '0',
    'real_staff' => '0',
    'max_sku' => "''",
    'min_sku' => "''",
    'max_qtys' => "''",
    'min_qtys' => "''",
    'tel_no' => 'PHONE',
    'addr' => 'address',
    'contact' => 'CTCT_MOBILE',
    'shop_state' => 'TD_BAS_STORE.STATUS'
);
$config[$sy_v_shop]['join'] = array(
  'TD_BAS_CHANNEL' => 'TD_BAS_STORE.CHNL_ID = TD_BAS_CHANNEL.CHNL_ID'
);
$config[$sy_v_shop]['where'] = '';
$config[$sy_v_shop]['date_key'] = '';
$config[$sy_v_shop]['by_shop'] = false;
$config[$sy_v_shop]['duplicate'] = array('shop_name', 'shop_level', 'shop_area', 'tel_no', 'addr','jm_code','close_date','open_date');//指定要更新的字段
$config[$sy_v_shop]['sy_key'] = array('shop_code');
$config[$sy_v_shop]['is_multi'] = true;

//销售目标(没有这个表,需要手动维护)
$plani_type_one_month_a_line = 'plani_with_type1';    //数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';        //数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';    //数据源表中一行记录代表的时间为某个时间段的情况
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani]['table'] = '';
$config[$sy_v_plani]['columns'] = array(    //此字段匹配结构还未最终确定，暂时这样
    'shop_code' => '',
    'plan_date' => array(
        'year' => '',    //年度字段名
        'month' => '',    //月份字段名
        'day' => ''),
    'plan_amt' => ''
);
$config[$sy_v_plani]['join'] = array();
$config[$sy_v_plani]['where'] = '';
$config[$sy_v_plani]['type'] = $plani_type_one_day_a_line;    //根据数据源表结构选填，参照上方 plani_type_ 部分
// $config[$sy_v_plani]['operation'] = $operation_insert_duplicate;
$config[$sy_v_plani]['date_key'] = '';
$config[$sy_v_plani]['by_shop'] = false;
// $config[$sy_v_plani]['duplicate'] = array('shop_code', 'plan_date');
$config[$sy_v_plani]['is_multi'] = false;
$config[$sy_v_plani]['by_limit']=true;//分页

//尺码
$sy_v_size = 'sy_v_size';
$config[$sy_v_size]['table'] = '';
$config[$sy_v_size]['columns'] = array(
    'size_group' => 'Size_Class',
    'size_id' => 'Size',
    'size_desc' => 'Size',
    'size_order' => 'DisplayIndex'
);
$config[$sy_v_size]['join'] = array();
$config[$sy_v_size]['where'] = '';
$config[$sy_v_size]['sy_key'] = array('size_group', 'size_id');
$config[$sy_v_size]['by_shop'] = false;
$config[$sy_v_size]['is_multi'] = true;
$config[$sy_v_size]['date_key']='';

//颜色
$sy_v_color = 'sy_v_color';
$config[$sy_v_color]['table'] = 'TD_BAS_COLOR';
$config[$sy_v_color]['columns'] = array(
    'color_id' => 'COLOR_CODE',
    'color_desc' => 'color_name'
);
$config[$sy_v_color]['join'] = array();
$config[$sy_v_color]['where'] = '';
$config[$sy_v_color]['sy_key'] = array('color_id');
$config[$sy_v_color]['by_shop'] = false;
$config[$sy_v_color]['is_multi'] = true;
$config[$sy_v_color]['date_key']='';


//商品尺码对照
$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize]['table'] = '';
$config[$sy_v_itemsize]['columns'] = array(
    'item_id' => 'rtrim(Goods_no)',
    'size_group' => 'Size_class'
);
$config[$sy_v_itemsize]['join'] = array();
$config[$sy_v_itemsize]['where'] = '';
$config[$sy_v_itemsize]['group_by'] = '';
$config[$sy_v_itemsize]['sy_key'] = array('item_id', 'size_group');
$config[$sy_v_itemsize]['by_shop'] = false;
$config[$sy_v_itemsize]['is_multi'] = false;
$config[$sy_v_itemsize]['date_key']='';


//商品颜色对照
$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor]['table'] = 'TF_BAS_GOODS';
$config[$sy_v_itemcolor]['columns'] = array(
    'color_id' => "'NA'",
    'item_id' => "$head_character+GOODS_CODE",
    'item_picture' => "''"
);
$config[$sy_v_itemcolor]['join'] = array();
$config[$sy_v_itemcolor]['where'] = '';
$config[$sy_v_itemcolor]['sy_key'] = array('item_id');//, 'color_id');
$config[$sy_v_itemcolor]['duplicate'] = array('item_picture');
$config[$sy_v_itemcolor]['by_shop'] = false;
$config[$sy_v_itemcolor]['is_multi'] = true;
$config[$sy_v_itemcolor]['date_key']='';


//品牌资料，无则各项内容为空
$sy_v_brand = 'sy_brand';
$config[$sy_v_brand]['table'] = '';
$config[$sy_v_brand]['columns'] = array(
    'brand_id' => 'BrandCode',
    'brand_name' => 'Brand',
    'sort_order' => "''",
    'active' => '1'
);
$config[$sy_v_brand]['join'] = array();
$config[$sy_v_brand]['where'] = '';
$config[$sy_v_brand]['sy_key'] = array('brand_id');
$config[$sy_v_brand]['by_shop'] = false;
$config[$sy_v_brand]['is_multi'] = false;
$config[$sy_v_brand]['date_key']='';
*/

$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
    'table' => 'TF_INV_VCHR_TRANS_RCPT',
    'columns' => array(
        'stock_date' => 'convert(varchar(10), TF_INV_VCHR_TRANS_RCPT.CRET_DATE, 120)',//"convert(varchar(10), ShopTin.tin_date, 120)",
        'shop_code' => 'D.shop_code',
        'item_id' => "$head_character+TF_INV_VCHR_TRANS_RCPT_DETA.GOODS_CODE",
        'color_id' => 'TF_INV_VCHR_TRANS_RCPT_DETA.COLOR',
        'size_id' => 'TD_BAS_SIZEGROUP_SIZE.size_code',
        'stock_qtys' => 'TF_INV_VCHR_TRANS_RCPT_DETA.QTY'
    ),
    'join' => array(
                    'TF_INV_VCHR_TRANS_RCPT_DETA' => 'TF_INV_VCHR_TRANS_RCPT.VCHR_ID = TF_INV_VCHR_TRANS_RCPT_DETA.VCHR_ID',
          '(select b.CHNL_CODE+a.st_code as shop_code,a.WH_ID from TD_BAS_STORE a left join
TD_BAS_CHANNEL b on a.CHNL_ID =b.CHNL_ID) D' => 'TF_INV_VCHR_TRANS_RCPT.TGT_WH_ID = D.WH_ID',
                    'TD_BAS_SIZEGROUP_SIZE' => 'TF_INV_VCHR_TRANS_RCPT_DETA.SIZE = TD_BAS_SIZEGROUP_SIZE.size_code'
        ),
    'where' => "D.shop_code is not null and D.shop_code <> ''",
    // 'group_by' => '',
    'date_key' => "TF_INV_VCHR_TRANS_RCPT.CRET_DATE",
    'by_shop' => false,
    'shop_code' => 'D.shop_code',
    'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'),
    'by_limit' => true,
    // 'duplicate' => '',//array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
/*
//员工信息
$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
    'table' => '',
    'columns' => array(
        'staff_id' => "rtrim(a.CustomerID)+'('+rtrim(a.BuisnessManID)+')'",
        'staff_name' => "a.Name",
        'staff_phone' => "isnull(a.Tel1,'')",
        'staff_shop_code' => "rtrim(a.CustomerID)",
        'staff_shop_name' => "b.customer_na"
    ),
    'join' => array(
        'Customer b' => 'a.CustomerID=b.Customer_ID'
        ),
    'where' => "a.CustomerID is not null and a.CustomerID <> ''",
    'shop_code' => '',
    'date_key' => '',
    'sy_key' => array('staff_id'),
    'by_shop' => false,
    'by_limit' => true,
    'is_multi' => false,
);

//季度
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'TD_BAS_PARAM',
    'columns' => array(
        'sea_id' => "ITEM_CODE",
        'sea_name' => "ITEM_NAME",
        's_mon' => "(case ITEM_CODE when '01' then '02' when '02' then '08' when '03' then '02' when '04' then '05' when '05' then '08' when '06' then '11' else '01' end)",
        'e_mon' => "(case ITEM_CODE when '01' then '07' when '02' then '01' when '03' then '04' when '04' then '07' when '05' then '10' when '06' then '01' else '12' end)",
        'sea_desc' => "ITEM_NAME",
        'sea_order' => "SORT_CODE",
        'sea_erp_code' => "ITEM_CODE",
        'sea_erp_name' => "ITEM_NAME",
        'is_span_sea' => "(case ITEM_CODE when '02' then 1 when '06' then 1 else 0 end)"
    ),
    'join' => array(),
    'where' => "GRP_CODE ='30000001'",
    'shop_code' => '',
    'date_key' => '',
    'sy_key' => array('sea_id'),
    'by_shop' => false,
    'by_limit' => true,
    'is_multi' => true,
);*/
/* End of file config.php */
/* Location: ./application/config/config.php */
