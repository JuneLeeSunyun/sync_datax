<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$db = "'1722'";
$config['cid'] = 1;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;
$config['query_before_sync_to_6'] = "update sy_v_vip set vip_level = '睡眠';
update sy_v_vip v, (
select vip_no, sum(inv_qtys)
from sales_info
where sale_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -180 day), '%Y-%m-%d')
group by vip_no
having sum(inv_qtys) BETWEEN 1 and 4) sale set vip_level = '白卡' where v.vip_no = sale.vip_no;
update sy_v_vip v, (
select vip_no, sum(inv_qtys)
from sales_info
where sale_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -180 day), '%Y-%m-%d')
group by vip_no
having sum(inv_qtys) BETWEEN 5 and 11) sale set vip_level = '黄卡' where v.vip_no = sale.vip_no;
update sy_v_vip v, (
select vip_no, sum(inv_qtys)
from sales_info
where sale_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -180 day), '%Y-%m-%d')
group by vip_no
having sum(inv_qtys) > 11) sale set vip_level = '蓝卡' where v.vip_no = sale.vip_no;
update sy_v_vip v, (
select vip_no, max(sale_date) as d from sales_info
group by vip_no
having MAX(sale_date) < DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -180 day), '%Y-%m-%d') and MAX(sale_date) >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -210 day), '%Y-%m-%d')
) sale set vip_level = '预冻结' where v.vip_no = sale.vip_no;
update sy_v_vip v, (
select vip_no, max(sale_date) as d from sales_info
group by vip_no
having MAX(sale_date) < DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -210 day), '%Y-%m-%d') and MAX(sale_date) >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -365 day), '%Y-%m-%d')
) sale set vip_level = '红卡' where v.vip_no = sale.vip_no;";

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip]['table'] = 'TF_F_CUSTOMER a';
$config[$sy_v_vip]['columns'] = array(
    'vip_no' => "$db+b.CUST_MOBILE",
    'cust_name' => "isnull(b.CUST_NAME,'')",
    'shop_code' => "$db+right(d.DEPT_CODE,2)",
    'cust_sex' => "b.CUST_SEX",
    'age' => "''",
    'born_date' => "convert(varchar(10), CAST(isnull(b.BIRTH_YEAR,'1900')AS VARCHAR)+'-'+CAST(b.BIRTH_MONTH AS VARCHAR)+'-'+CAST(b.BIRTH_DAY AS VARCHAR), 120)",            //格式转换，暂时需要加上
    'register_date' => 'convert(varchar(10), a.CRE_TIME, 120)',    //格式转换，暂时需要加上
    'vip_level' => "'睡眠'",//"c.LVL_NAME",
    'mobile' => "b.CUST_MOBILE",
    'address' => "isnull(a.POST_ADDR,'')",
    'staff_id' => "isnull(e.STAFF_CODE,'')",
    'vip_state' => "(case when A.CUST_STATUS =40 then 2 else 1 end)",
    'active_money' => '0'
);
$config[$sy_v_vip]['join'] = array(
  'TF_F_CUSTOMER_IND b' => 'a.CUST_ID = b.CUST_ID',
  'TD_B_CUST_LEVEL c' => 'b.CUST_LEVEL = c.LVL_CODE',
  'TD_M_DEPARTMENT d' => 'a.SERV_DEPT = d.DEPT_ID',
  'TD_M_STAFF e' => 'a.SERV_STAFF = e.STAFF_ID'
);
$config[$sy_v_vip]['where'] = "a.CUST_TYPE <>'1' and ISNUMERIC(right(d.DEPT_CODE,2))=1  and c.LVL_NAME <> '粉卡'";
$config[$sy_v_vip]['shop_code'] = "$db+right(d.DEPT_CODE,2)";
$config[$sy_v_vip]['date_key'] = '';
$config[$sy_v_vip]['sy_key'] = array('vip_no');
$config[$sy_v_vip]['by_shop'] = false;
$config[$sy_v_vip]['is_multi'] = true;
// $config[$sy_v_vip]['duplicate'] = array('mobile', 'vip_state');
/*
//销售主表
$sales_info = 'sales_info';         //固定值，勿动
$config[$sales_info]['table'] = 'TF_F_CUST_CONSUME_DETA';   //客户数据源表名
$config[$sales_info]['columns'] = array(        //各字段对应
    'inv_no' => 'TF_F_CUST_CONSUME_DETA.BILL_ID',
    'shop_code' => "''",
    'sale_date' => "''",
    'vip_state' => "1",
    'staff_id' => "''",
    'vip_no' => "$db+TF_F_CUSTOMER_IND.CUST_MOBILE",
    'inv_money' => "''",
    'inv_qtys' => "''",
    'tag_money' => "''"
);
$config[$sales_info]['join'] = array('TF_F_CUSTOMER_IND' => 'TF_F_CUSTOMER_IND.CUST_ID = TF_F_CUST_CONSUME_DETA.CUST_ID');
$config[$sales_info]['where'] = "";
$config[$sales_info]['group_by'] = array('BILL_ID','TF_F_CUSTOMER_IND.CUST_MOBILE');
$config[$sales_info]['date_key'] = 'convert(varchar(10), BILL_DATE, 120)';        //表内是否有时间戳（用于抽数时时间约束）	//抽数方式，选填，参照文件开头 $operation_ 部分
$config[$sales_info]['shop_code'] = '';
$config[$sales_info]['by_shop'] = false;    //是否根据店铺抽取，其值为true或false
$config[$sales_info]['sy_key'] = array('inv_no');
$config[$sales_info]['is_multi'] = true;
$config[$sales_info]['by_limit'] = true;//分页
$config[$sales_info]['duplicate'] = array('vip_state', 'vip_no');
*/
$config['query_after_sync_to_5'] = "update sales_info i, sales_info_crm crm set i.vip_state = crm.vip_state, i.vip_no = crm.vip_no where i.inv_no = crm.inv_no;";

$sales_info_crm = 'sales_info_crm';         //固定值，勿动
$config[$sales_info_crm]['table'] = 'TF_F_CUST_CONSUME_DETA';   //客户数据源表名
$config[$sales_info_crm]['columns'] = array(        //各字段对应
  'inv_no' => 'TF_F_CUST_CONSUME_DETA.BILL_ID',
  'shop_code' => "''",
  'sale_date' => "convert(varchar(10), BILL_DATE, 120)",
  'vip_state' => "1",
  'staff_id' => "''",
  'vip_no' => "$db+TF_F_CUSTOMER_IND.CUST_MOBILE",
  'inv_money' => "''",
  'inv_qtys' => "''",
  'tag_money' => "''"
);
$config[$sales_info_crm]['join'] = array('TF_F_CUSTOMER_IND' => 'TF_F_CUSTOMER_IND.CUST_ID = TF_F_CUST_CONSUME_DETA.CUST_ID');
$config[$sales_info_crm]['where'] = '';//"BILL_DATE >= '2019-01-01 00:00:00.000'";
$config[$sales_info_crm]['group_by'] = array('BILL_ID','TF_F_CUSTOMER_IND.CUST_MOBILE', 'BILL_DATE');
$config[$sales_info_crm]['date_key'] = 'convert(varchar(10), BILL_DATE, 120)';        //表内是否有时间戳（用于抽数时时间约束）	//抽数方式，选填，参照文件开头 $operation_ 部分
$config[$sales_info_crm]['shop_code'] = '';
$config[$sales_info_crm]['by_shop'] = false;    //是否根据店铺抽取，其值为true或false
$config[$sales_info_crm]['sy_key'] = array('inv_no');
$config[$sales_info_crm]['is_multi'] = true;
$config[$sales_info_crm]['by_limit'] = true;//分页
$config[$sales_info_crm]['duplicate'] = array('vip_state', 'vip_no');
