<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$config['page_limit'] = 100000;

//库存信息
$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock]['table'] = 'TF_INV_INVENTORY';
$config[$sy_v_stock]['columns'] = array(
    'shop_code' => "(case WH_ID when '17120811401200000005' then '170700' when '17120809561100000002' then '171700' else '172200' end)",
    'item_id' => "rtrim(TF_INV_INVENTORY.GOODS_CODE)",
    'color_id' => 'COLOR',
    'size_id' => 'size',
    'stock_qtys' => 'sum(QTY_HAND)'    //多条记录算总值时，记得sum
);
$config[$sy_v_stock]['join'] = array();
$config[$sy_v_stock]['where'] = "QTY_HAND <> 0 and QTY_HAND <>0 AND WH_LOCATION ='10' and WH_ID in ('17120811401200000005', '17120809561100000002', '180827101642200')";
$config[$sy_v_stock]['shop_code'] = '';
$config[$sy_v_stock]['date_key'] = '';
$config[$sy_v_stock]['group_by'] = array('WH_ID', 'rtrim(TF_INV_INVENTORY.GOODS_CODE)', 'COLOR', 'size');
$config[$sy_v_stock]['by_shop'] = false;
$config[$sy_v_stock]['sy_key'] = array('shop_code');    //本地表存在联合主键，所以多了这个，固定值，勿动
$config[$sy_v_stock]['is_multi'] = true;
$config[$sy_v_stock]['by_limit'] = true;

/* End of file config.php */
/* Location: ./application/config/config.php */
