<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

/**
 * 数据库与配置文件对应关系，结构为
 * $config['database_resource'] = array(
 * 'a' => array('b' => 'c'),
 * ...
 * );
 * 其中，
 * a为自定义（需要与报表数据库中sy_v_data_resource表config_key对应）
 * b为database.php中抽数数据源库对应的item
 * c为数据库b对应的配置文件文件名，文件内容为客户数据源库与三云报表数据库的表字段匹配信息
 */
$config['database_resource'] = array(
    'all' => array(
        'yake_ndp_acct_G' => 'config_yake_ndp_acct_G',
        'yake_crm_acc_G1707' => 'config_yake_crm_acc_G1707',
        'yake_crm_acc_G1717' => 'config_yake_crm_acc_G1717',
        'yake_crm_acc_G1722' => 'config_yake_crm_acc_G1722'
    ),
    'history' => array(
        'yake_ndp_acct_G_history' => 'config_yake_ndp_acct_G_history'
    ),
    'base' => array(
      'yake_ndp_acct_G' => 'config_yake_ndp_acct_G',
        'yake_ndp_acct_G_stock' => 'config_yake_ndp_acct_G_stock',
        'yake_crm_acc_G1707' => 'config_yake_crm_acc_G1707',
        'yake_crm_acc_G1717' => 'config_yake_crm_acc_G1717',
        'yake_crm_acc_G1722' => 'config_yake_crm_acc_G1722'
    ),
    'vip' => array(
      'yake_crm_acc_G1707' => 'config_yake_crm_acc_G1707',
      'yake_crm_acc_G1717' => 'config_yake_crm_acc_G1717',
      'yake_crm_acc_G1722' => 'config_yake_crm_acc_G1722'
    ),
    'crm' => array(
      'yake_crm_acc_G1707' => 'config_yake_crm_acc_G1707',
      'yake_crm_acc_G1717' => 'config_yake_crm_acc_G1717',
      'yake_crm_acc_G1722' => 'config_yake_crm_acc_G1722'
    ),
    'yake_ndp_acct_G' => array('yake_ndp_acct_G' => 'config_yake_ndp_acct_G'),
    'yake_crm_acc_G1707' => array('yake_crm_acc_G1707' => 'config_yake_crm_acc_G1707'),
    'yake_crm_acc_G1717' => array('yake_crm_acc_G1717' => 'config_yake_crm_acc_G1717'),
    'yake_crm_acc_G1722' => array('yake_crm_acc_G1722' => 'config_yake_crm_acc_G1722')
);

$config['delete_sale_first'] = true;//在抽数的时候,会把相应时间端的数据先删掉

$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';
$config['update_item_acc_by_category'] = 'N';

$config['com_no'] = '2';//零售链cid
$config['lsl_admin_ip'] = 'ztsjla.amass-zj.com';
$config['lsl_admin_port'] = 80;
//建模服务器到零售链服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_lsl'] = true;
//建模服务器到零售链服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_lsl'] = array(
// 	'lsla_title' => 0,
// 	'lsla_dept' => 0,
    'lsla_groupshop' => 1,
    'lsla_shop' => 1,
    'lsla_group' => 0,
    'lsla_category' => 1,
    'lsla_item' => 1,
    'lsla_color' => 1,
    'lsla_size' => 1,
    'lsla_itemcolor' => 0,
// 	'lsla_config' => 0,
// 	'lsla_user' => 0,
// 	'lsla_user_leader' => 0,
    'lsla_sea' => 1
);


    $group_id = '9999';
    $group_name = '未分组';
    //基础同步结束后，在基础服务器数据库中执行query_after_sync，为空时不执行


    $config['query_after_sync_to_lsl'] = "insert into `lsla_group`(`group_code`, `group_name`, `group_level`, `parent_group_code`, `group_state`, `group_order_seq`) values ('$group_id', '$group_name', '1', '$group_id', '1', '999');
    delete from lsla_groupshop where group_code = '$group_id';
    insert into lsla_groupshop(`shop_code`, `group_code`, `gs_order_seq`) (
    select shop_code, '$group_id' as group_code, '999' as gs_order_seq from lsla_shop
    where shop_code not in (
    select shop_code from lsla_groupshop
    )
    )";

    /*vip销售计算，
    vip_sale_1 为销售日期小于注册日期不计入。
    vip_sale_2 销售日期等于注册日期，第一单，不计入；销售日期等于注册日期，第一单除外部分，计入
    vip_sale_1_2 综合以上两种
    vip_sale_3 为销售日期小于等于注册日期不计入。
    */
    $config['vip_sale'] = 'vip_sale_3';
/* End of file config.php */
/* Location: ./application/config/config.php */
