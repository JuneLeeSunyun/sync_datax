<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$db = "'8538'";
$config['cid'] = 1;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip]['table'] = 'TF_F_CUSTOMER a';
$config[$sy_v_vip]['columns'] = array(
    'vip_no' => "$db+b.CUST_MOBILE",
    'cust_name' => "isnull(b.CUST_NAME,'')",
    'shop_code' => "$db+right(d.DEPT_NAME,2)",
    'cust_sex' => "b.CUST_SEX",
    'age' => "''",
    'born_date' => "convert(varchar(10), CAST(b.BIRTH_YEAR AS VARCHAR)+'-'+CAST(b.BIRTH_MONTH AS VARCHAR)+'-'+CAST(b.BIRTH_DAY AS VARCHAR), 120)",            //格式转换，暂时需要加上
    'register_date' => 'convert(varchar(10), a.CRE_TIME, 120)',    //格式转换，暂时需要加上
    'vip_level' => "c.LVL_NAME",
    'mobile' => "b.CUST_MOBILE",
    'address' => "isnull(a.POST_ADDR,'')",
    'staff_id' => "isnull(e.STAFF_CODE,'')",
    'vip_state' => "(case when A.CUST_STATUS =40 then 2 else 1 end)",
    'active_money' => '0'
);
$config[$sy_v_vip]['join'] = array(
  'TF_F_CUSTOMER_IND b' => 'a.CUST_ID = b.CUST_ID',
  'TD_B_CUST_LEVEL c' => 'b.CUST_LEVEL = c.LVL_CODE',
  'TD_M_DEPARTMENT d' => 'a.SERV_DEPT = d.DEPT_ID',
  'TD_M_STAFF e' => 'a.SERV_STAFF = e.STAFF_ID'
);
$config[$sy_v_vip]['where'] = "a.CUST_TYPE <>'1' and ISNUMERIC(right(d.DEPT_NAME,2))=1 and c.LVL_NAME <> '粉卡'";
$config[$sy_v_vip]['shop_code'] = "$db+right(d.DEPT_NAME,2)";
$config[$sy_v_vip]['date_key'] = '';
$config[$sy_v_vip]['sy_key'] = array('vip_no');
$config[$sy_v_vip]['by_shop'] = false;
$config[$sy_v_vip]['is_multi'] = true;
// $config[$sy_v_vip]['duplicate'] = array('mobile', 'vip_state');
