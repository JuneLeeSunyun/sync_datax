<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
$config['cid'] = 24;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;
$config['shop_count'] = 10;
$config['update_item_date'] = 'update_item_date';

$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
	'table' => 'CANGKU',
	'columns' => array(
		'shop_code' => "CKDM",
		'shop_name' => "CKMC",
		'model_no' => "' '",
		'ref_shopcode' => "' '",
		'jm_code' => "' '",
		'shop_level' => "' '",
		'branch_id' => "' '",
		'open_date' => "''",
		'close_date' => "' '",
		'shop_area' => "0",
		'stand_staffs' => "0",
		'real_staff' => "0",
		'max_sku' => "0",
		'min_sku' => "0",
		'max_qtys' => "0",
		'min_qtys' => "0",
		'tel_no' => "0",
		'addr' => "' '",
		'contact' => "' '",
		'shop_state' => '1'
	),
	'join' => array(),
	'where' => "CKDM in ('000','002','008')",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code'),
	'duplicate' => array('shop_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
	'table' => 'SPKCB',
	'columns' => array(
		'shop_code' => 'SPKCB.CKDM',
		'item_id' => 'SPKCB.SPDM',
		'color_id' => 'SPKCB.GG1DM',
		'size_id' => 'GUIGE2.GGMC',
		'stock_qtys' => 'sum(SPKCB.SL)'
	),
	'join' => array('GUIGE2'  => 'SPKCB.GG2DM=GUIGE2.GGDM'),
	'where' => "SPKCB.CKDM in ('000','002','008') and sl<>0",
	'group_by' => array('SPKCB.CKDM', 'SPKCB.SPDM', 'SPKCB.GG1DM', 'GUIGE2.GGMC'),
	'date_key' => "",
	'by_shop' => true,
	'shop_code' => 'SPKCB.CKDM',
	'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/* End of file config.php */
/* Location: ./application/config/config.php */
