'''
Author: philipdeng
Date: 2021-03-23 10:17:04
LastEditors: Please set LastEditors
LastEditTime: 2021-04-30 00:07:07
Description: 登录系统
'''
import requests,json

from libs.message_logs import MessageLogs
from libs.read_conf import ReadConf


class LoginSys():
    '''
    登录系统，获取Token
    '''

    def __init__(self):
        self.logs = MessageLogs()

    def get_server_info(self):
        '''
        获取企业服务器信息
        '''
        config = ReadConf().read_conf()
        com_no = config.get("SYNC_PARAM","COM_NO")
        secure_code = config.get("SYNC_PARAM","SECURE_CODE")
        server_url = config.get("SYNC_PARAM","SERVER_URL")
        url = f"{server_url}/v1/lsls_company"
        headers = {'Content-Type':'application/json'}
        data = {
            'com_no': com_no,
            'secure_code': secure_code
        }
        try:
            get_data = requests.post(url=url, data=json.dumps(data),headers=headers)
            data_list = get_data.json()
            if get_data.status_code == 200:
                return data_list
            else:
                log_txt = f"{data_list.get('error_code')},{data_list.get('msg')},{data_list.get('request')}"
                self.logs.message_logs(log_txt,'lsl_sync_c')
                raise Exception (log_txt)
        except Exception as e:
            log_txt = e
            self.logs.message_logs(log_txt,'lsl_sync_c')
            raise log_txt


    def login_lsl(self, com_no, com_id, secure_code, db_name, api_url):
        '''
        登录系统，获取Token
        '''
        url = f"{api_url}/v1/token"
        headers = {'Content-Type':'application/json'}
        data = {
            'com_no': com_no,
            'com_id': com_id,
            'secure_code':secure_code,
            'db_name':db_name
        }
        try:
            get_data = requests.post(url=url, data=json.dumps(data),headers=headers)

            data_list = get_data.json()
            if get_data.status_code == 200:
                return data_list
            else:
                log_txt = f"{data_list.get('error_code')},{data_list.get('msg')},{data_list.get('request')}"
                self.logs.message_logs(log_txt,'lsl_sync_c')
                raise Exception (log_txt)
        except Exception as e:
            log_txt = e
            self.logs.message_logs(log_txt,'lsl_sync_c')
            raise log_txt

    def login_info(self):
        com_info = self.get_server_info()
        api_url = com_info.get('sync_url')
        com_id = com_info.get('com_id')
        login_info = self.login_lsl(com_info.get('com_no'),com_id,com_info.get('secure_code'),
                                   com_info.get('company_db'),api_url)
        token = login_info.get('token')
        return {'api_url':api_url, 'com_id':com_id, 'token':token}


    