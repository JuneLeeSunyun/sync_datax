#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "32 day ago" +%Y-%m-%d`
	s_mday=`date +%Y-%m-01`
else
	s_day=$sd
	s_mday=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
	e_mday=`date -d "1 month" +%Y-%m-01`
else
	e_day=$ed
	e_mday=$ed
fi

Location='/opt/sydata/datax/bin'

cd $Location;
python datax.py ../job/o2my/o2m_item_class.json;
python datax.py ../job/o2my/o2m_category.json;
python datax.py ../job/o2my/o2m_item.json;
python datax.py ../job/o2my/o2m_barcode.json;
python datax.py ../job/o2my/o2m_shop.json;
#python datax.py ../job/o2my/o2m_group.json;
#考虑到客户脚本更新比较频繁，为方便更换脚本，调整为先取分组店铺，再根据分组店铺数据汇总分组信息
python datax.py ../job/o2my/o2m_groupshop.json;
python datax.py ../job/o2my/o2m_group.json;
python datax.py ../job/o2my/o2m_staff.json;
python datax.py ../job/o2my/o2m_stock.json;
python datax.py ../job/o2my/o2m_instock.json;
python datax.py ../job/o2my/o2m_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_plani_d.json -p "-Ds_day=$s_mday -De_day=$e_mday";
python datax.py ../job/o2my/o2m_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_vip_saledate_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2my/o2m_vip_level.json;
python datax.py ../job/o2my/o2m_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";

cd /opt/sydata/lsl_sync_c;python3 lsl_sync_c.py
