每天9点取最近32天数据，9点到23点每半个钟取一次库存、当天销售数据及新增会员

木九十取数规则：
2022-11-23 
分组脚本调整为：
SELECT 门店编码,门店名称,品牌名称,大区,渠道属性,渠道属性2,管辖区域,门店类型,加盟老板,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称 ELSE 品牌名称||'-'||大区 END 一级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟'
            ELSE 品牌名称||'-'||大区||渠道属性2
       END 二级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟'||加盟老板
            ELSE 品牌名称||'-'||管辖区域
       END 三级
  FROM (SELECT T.SHOP_NO     门店编码,
               T.SHOP_NAME   门店名称,
               CASE WHEN T.DEPARTMENT_NO='1125' THEN '奥莱'
                    WHEN T.DEPARTMENT_NO='1121' THEN '木九十'
                    WHEN T.DEPARTMENT_NO='1122' THEN 'aojo'
                    WHEN T.DEPARTMENT_NO='1123' THEN 'WAKEUP' END 品牌名称,
               T.SHOP_AREA   大区,
               T.SHOP_TYPE29 渠道属性,
               T.SHOP_TYPE02 渠道属性2,
               SHOP_BRAN     门店类型,
               T.SHOP_TYPE17   管辖区域,
               T.SHOP_ENDATE 关店时间,
               T.SHOP_CLOSE  最终撤店日期,
               SHOP_TYPE04   经营状态,
               CASE WHEN T.SHOP_TYPE29='加盟' THEN T.SHOP_TYPE15 ELSE '' END 加盟老板
          FROM POSBI.MD_SHOP_DETAIL T
         WHERE T.SHOP_NO NOT LIKE '1%'
           AND T.DEPARTMENT_NO IN ('1121', '1122', '1123', '1125')
           AND REGEXP_LIKE(T.SHOP_NO, '^[0-9]')
           AND T.SHOP_AREA<>'总部'
         )

2022-10-25
为了对数方便，店铺分组抽数规则调整，具体看groupshop文档和group文档，sh文件需调整两个数据的取数顺序。
另，原始脚本修改为：
SELECT 门店编码,门店名称,品牌名称,大区,渠道属性,渠道属性2,管辖区域,门店类型,加盟老板,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称 ELSE 品牌名称||'-'||大区 END 一级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟'
            ELSE 品牌名称||'-'||大区||渠道属性2
       END 二级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟'||加盟老板
            ELSE 品牌名称||'-'||管辖区域
       END 三级
  FROM (SELECT T.SHOP_NO     门店编码,
               T.SHOP_NAME   门店名称,
               CASE WHEN T.SHOP_NO IN ( '6641','6687','6194','6710','6715','6522','6768','6688','6195','6197','6169','6115') THEN '奥莱'
                    WHEN T.DEPARTMENT_NO='1121' THEN '木九十'
                    WHEN T.DEPARTMENT_NO='1122' THEN 'aojo'
                    WHEN T.DEPARTMENT_NO='1123' THEN 'WAKEUP' END 品牌名称,
               T.SHOP_AREA   大区,
               T.SHOP_TYPE29 渠道属性,
               T.SHOP_TYPE02 渠道属性2,
               SHOP_BRAN     门店类型,
               T.SHOP_TYPE17   管辖区域,
               T.SHOP_ENDATE 关店时间,
               T.SHOP_CLOSE  最终撤店日期,
               SHOP_TYPE04   经营状态,
               CASE WHEN T.SHOP_TYPE29='加盟' THEN T.SHOP_TYPE15 ELSE '' END 加盟老板,
               case when t.shop_no in ( '6641','6687','6194','6710','6715','6522','6768','6688',
'6195','6197','6169','6115')then '奥莱' else NULL END 奥莱
          FROM POSBI.MD_SHOP_DETAIL T
         WHERE T.SHOP_NO NOT LIKE '1%'
           AND T.DEPARTMENT_NO IN ('1121', '1122', '1123')
           AND REGEXP_LIKE(T.SHOP_NO, '^[0-9]')
           AND T.SHOP_AREA<>'总部'
         )

2022-10-18
shop表中ref_shopcode保存木九十新店编。
销售单据中店 根据shop表中shop_code与ref_shopcode关系挂到新店编下，

2022-10-14
分组增加“奥莱”组数据，相关脚本：
SELECT 门店编码,门店名称,品牌名称,大区,渠道属性,渠道属性2,管辖区域,门店类型,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称 ELSE 品牌名称||'-'||大区 END 一级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟'
            ELSE 品牌名称||'-'||大区||渠道属性2
       END 二级,
       CASE WHEN 品牌名称='奥莱' THEN 品牌名称
            WHEN 管辖区域 LIKE '%加盟%' THEN NULL
            ELSE 品牌名称||'-'||管辖区域
       END 三级
  FROM (SELECT T.SHOP_NO     门店编码,
               T.SHOP_NAME   门店名称,
               CASE WHEN T.SHOP_NO IN ( '6641', '6687', '6194', '6710', '6715', '6522', '6768', '6688', '6195', '6197', '6169', '6115') THEN '奥莱'
     WHEN T.DEPARTMENT_NO='1121' THEN '木九十'
     WHEN T.DEPARTMENT_NO='1122' THEN 'aojo'
     WHEN T.DEPARTMENT_NO='1123' THEN 'WAKEUP' END 品牌名称,
               T.SHOP_AREA   大区,
               T.SHOP_TYPE29 渠道属性,
               T.SHOP_TYPE02 渠道属性2,
               SHOP_BRAN     门店类型,
               T.SHOP_TYPE17   管辖区域,
               T.SHOP_ENDATE 关店时间,
               T.SHOP_CLOSE  最终撤店日期,
               SHOP_TYPE04   经营状态,
               case when t.shop_no in ( '6641', '6687', '6194', '6710', '6715', '6522', '6768', '6688', '6195',  '6197', '6169', '6115')then '奥莱' else NULL END 奥莱
          FROM POSBI.MD_SHOP_DETAIL T
         WHERE T.SHOP_NO NOT LIKE '1%'
           AND T.DEPARTMENT_NO IN ('1121', '1122', '1123')
           AND REGEXP_LIKE(T.SHOP_NO, '^[0-9]')
           AND T.SHOP_AREA<>'总部'
         )

2022-09-19
店铺分组增加ACMAL（木九十-奥莱）、ACAAL（aojo-奥莱）两个分组，代号固定，没有父子分组，分组规则不变情况下，另外将店铺类型为“奥莱店”的自动归到这两个分组下。
Ps：相关脚本如下
SELECT 门店编码,门店名称,品牌名称,大区,渠道属性,渠道属性2,管辖区域,门店类型,
       品牌名称||'-'||大区 一级,
       CASE WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟' ELSE
            品牌名称||'-'||大区||渠道属性2 END 二级,
       CASE WHEN 管辖区域 LIKE '%加盟%' THEN NULL ELSE 品牌名称||'-'||管辖区域 END 三级
  FROM (SELECT T.SHOP_NO     门店编码,
               T.SHOP_NAME   门店名称,
               CASE WHEN T.DEPARTMENT_NO='1121' THEN '木九十'
     WHEN T.DEPARTMENT_NO='1122' THEN 'aojo'
     WHEN T.DEPARTMENT_NO='1123' THEN 'WAKEUP' END 品牌名称,
               T.SHOP_AREA   大区,
               T.SHOP_TYPE29 渠道属性,
               T.SHOP_TYPE02 渠道属性2,
               SHOP_BRAN     门店类型,
               T.SHOP_TYPE17   管辖区域,
               T.SHOP_ENDATE 关店时间,
               T.SHOP_CLOSE  最终撤店日期,
               SHOP_TYPE04   经营状态
          FROM POSBI.MD_SHOP_DETAIL T
         WHERE T.SHOP_NO NOT LIKE '1%'
           AND T.DEPARTMENT_NO IN ('1121', '1122', '1123')
           AND REGEXP_LIKE(T.SHOP_NO, '^[0-9]')
           AND T.SHOP_AREA<>'总部'
         )

2022-09-16
店铺状态从POSBI.MD_SHOP_DETAIL表拿。修改shop文件。

2022-09-14
1、11太阳镜，12镜架，13镜片，23防蓝光镜，24老视成镜，25隐形眼镜，80券及服务，除这些商品外，其他的不计入一件单统计，同时单据明细不删除。此需求确定为修改info表的数量。修改sales文件。
2、所有的连带率，用11太阳镜，12镜架，13镜片，23防蓝光镜，24老视成镜，25隐形眼镜，80券及服务下的商品计算。此需求确定为修改sy_v_item中的is_add属性。修改item文件。

2022-09-09
折扣率的计算（订单金额/吊牌额）中，吊牌额剔除款号为'WX160SE006','EW160C011','HY156RG002','EM160C001','EW160C008','EW156C002','KD156SF003'且销售金额为0的吊牌。此需求确定为修改info表中的tag_money。修改sales文件。

2022-08-31
SELECT 门店编码,门店名称,品牌名称,大区,渠道属性,渠道属性2,管辖区域,
       品牌名称||'-'||大区 一级,
       CASE WHEN 管辖区域 LIKE '%加盟%' THEN 品牌名称||'-'||大区||'加盟' ELSE
            品牌名称||'-'||大区||渠道属性2 END 二级,
       CASE WHEN 管辖区域 LIKE '%加盟%' THEN NULL ELSE 品牌名称||'-'||管辖区域 END 三级
  FROM (SELECT T.SHOP_NO     门店编码,
               T.SHOP_NAME   门店名称,
               CASE WHEN T.DEPARTMENT_NO='1121' THEN '木九十'
     WHEN T.DEPARTMENT_NO='1122' THEN 'aojo'
     WHEN T.DEPARTMENT_NO='1123' THEN 'WAKEUP' END 品牌名称,
               T.SHOP_AREA   大区,
               T.SHOP_TYPE29 渠道属性,
               T.SHOP_TYPE02 渠道属性2,
               T.SHOP_TYPE17   管辖区域,
               T.SHOP_ENDATE 关店时间,
               T.SHOP_CLOSE  最终撤店日期,
               SHOP_TYPE04   经营状态
          FROM POSBI.MD_SHOP_DETAIL T
         WHERE T.SHOP_NO NOT LIKE '1%'
           AND T.DEPARTMENT_NO IN ('1121', '1122', '1123')
           AND REGEXP_LIKE(T.SHOP_NO, '^[0-9]')
           AND T.SHOP_AREA<>'总部'
         )
1、按此脚本同步一级、二级、三级为分组（WAKEUP的不取），并把店铺归到对应分组下，遇到名字为空或“无”的分组则分组不取，其下店铺归到父级分组下。
2、分组代号按拼音首字母生成（具体看代码）。
3、分组排序按东南西北，先直营后加盟再城市排序。
4、店铺分组排序按店铺代号排序。
5、店铺分组遇到三云系统下店铺状态为非1的，店铺分组明细删除，若分组下的店铺为空，则分组也删。
根据以上需求新增group、groupshop文件。

2022-08-30
增加类别23、24、45、80的抽取，修改category、item_class文件

2022-08-18
增加新、旧客销售占比计算及镜片、镜架转化率等，修改至sales_d文件，同时增加vip_saledate表计算

2022-08-15
品类“其他-其他”的ic_prop改为3，ic_prop为3的商品，从销售主表的数量中剔除，金额保留，修改sales_detail文件

2021-12-29
除太阳镜、镜片、镜架、隐形眼镜、周边商品外，剩下的大类归到一个叫“其他”的大类，下面的品类也归到“其他-其他”，品类属性调整为配件。
