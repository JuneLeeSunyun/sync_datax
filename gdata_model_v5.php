<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
 支持多数据库多配置

﻿销售单据总表(sales_info)
字段名	数据类型	描述
inv_type	tinyint(4)	单据类型（1销售 2退货）
pay_type	tinyint(4)	收款类型（1全款 2订金 3余款）
inv_source	varchar(200)	单据来源（为下订金的销售单号）
rec_money	decimal(12,2)	应收金额
arr_money	decimal(12,2)	未收金额

﻿﻿销售单据明细表(sales_detail)
字段名	数据类型	描述
rec_money	decimal(12,2)	应收金额
arr_money	decimal(12,2)	未收金额
 */

class Gdata_model_v5 extends CI_Model
{
    public $db_resource;      //数据源库
    public $db_sy;            //三云目标数据库
    public $arr_table;        //查询表 当前操作相关表，有可能为所有，有可能仅销售数据相关，由controller传入
    public $arr_data_table;   //备份表 当前操作相关表，有可能为所有，有可能仅销售数据相关，由controller传入

    const PAGE_LIMIT = 100000;  //数据超过100000条，就自动分页处理
    const REPORT_CONFIG_LAST_TIME_TYPE = 1;

    public $deal_with_data_from_date;
    public $deal_with_data_to_date;
    public $deal_with_data_with_limited_date;

    public $deal_with_resource_data_from_date;
    public $deal_with_resource_data_to_date;

    public $new_deal_with_data_from_date;
    public $new_deal_with_data_to_date;

    public $job_s_date;
    public $job_e_date;
    public $job_user_id;
    public $cid;

    private $update_item_acc_by_category;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Asia/Shanghai");

        $this->load->config('config_report_v3');

        $this->db_sy = $this->load->database('report_db', true);

        data_log_message('debug', "---------- sy db name: ". $this->db_sy->database);

        $this->update_item_acc_by_category = TRUE;
    }


    /*
     * 获取：任务表
    DROP TABLE IF EXISTS data_tables;
    CREATE TABLE IF NOT EXISTS `data_tables` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
      `t_name` varchar(200) DEFAULT NULL COMMENT '表名',
      `t_desc` varchar(200) DEFAULT NULL COMMENT '说明',
      `t_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '类别：1、基础信息 2、VIP资料 3、库存 4、销售目标 5、销售明细 6、数据计算',
      `order_seq` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序号',
      `t_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否抽取或计算：0.否；1.是',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抽数计算表';

    INSERT INTO `data_tables` (`t_name`, `t_desc`, `t_type`, `order_seq`, `t_state`) VALUES
      ('sy_v_category', '品类资料', 1, 1, 1),
      ('sy_v_sea', '季度资料', 1, 2, 1),
      ('sy_v_item', '商品资料', 1, 3, 1),
      ('sy_v_barcode', '条码资料', 1, 4, 1),
      ('sy_v_color', '颜色资料', 1, 5, 1),
      ('sy_v_size', '尺码资料', 1, 6, 1),
      ('sy_v_itemcolor', '商品颜色', 1, 7, 1),
      ('sy_v_itemsize', '商品尺码', 1, 8, 1),
      ('sy_v_staff', '导购资料', 1, 9, 1),
      ('sy_v_shop', '店铺资料', 1, 10, 1),
      ('sy_v_shoparea', '店铺面积', 1, 11, 1),
      ('sy_v_shop_config', '店铺配置', 1, 12, 1),
      ('sy_group', '分组资料', 1, 13, 0),
      ('sy_shopgroup', '分组店铺', 1, 14, 0),
      ('sy_v_shop_type', '店铺类型', 1, 15, 0),
      ('sy_v_typeshop', '店铺类型明细', 1, 16, 0),
      ('sy_v_vip', 'VIP数据', 2, 1, 1),
      ('sy_v_vip_level', 'VIP数据', 2, 2, 1),
      ('sy_v_stock', '库存数据', 3, 1, 1),
      ('sy_v_plani', '目标数据', 4, 1, 1),
      ('sales_info', '销售主表', 5, 1, 1),
      ('sales_detail', '销售明细', 5, 2, 1),
      ('sy_v_staff_sales', '导购销售明细', 5, 3, 0),
      ('sales_info2', '销售主表2', 5, 1, 0),
      ('sales_detail2', '销售明细2', 5, 2, 0),
      ('sy_v_sales', '店铺日销售主表', 6, 1, 1),
      ('sy_v_salesdet', '店铺日销售明细', 6, 2, 1),
      ('sy_v_shopstock', '店铺商品调拨表', 8, 1, 0);
     */
    public function getDataTable($data_type)
    {
        $arr_table = array();

        if ($data_type > 0) {
            $query_all = $this->db_sy->select('t_name, t_type')
                ->where('t_type', $data_type)
                ->where('t_state', 1)
                ->order_by('order_seq', 'asc')
                ->get('data_tables');

            if ($query_all->num_rows() > 0) {
                foreach ($query_all->result_array() as $val) {
                    $arr_table[] = $val['t_name'];
                }
            }

            data_log_message('debug', __LINE__ . ' ' . $this->db_sy->last_query());
        }

        return $arr_table;
    }


    /*
     *
    DROP TABLE IF EXISTS lslr_cron_data;
    CREATE TABLE IF NOT EXISTS `lslr_cron_data` (
      `data_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据同步ID',
      `source_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '来源：1、定时任务 2、pc 3、app',
      `start_date`bigint(20) NOT NULL DEFAULT '0' COMMENT '开始日期',
      `end_date`bigint(20) NOT NULL DEFAULT '0' COMMENT '结束日期',
      `job_shops` longtext COMMENT '店铺（英文逗号分隔）',
      `job_types` longtext COMMENT '类别：1、基础信息 2、VIP资料 3、库存 4、销售目标 5、销售明细 6、数据计算 7、属性同步（英文逗号分隔）',
      `user_id` bigint(20) NOT NULL COMMENT '用户id（定时任务 默认0）',
      `user_name` varchar(50) DEFAULT NULL COMMENT '姓名（定时任务 默认0）',
      `user_mobile` varchar(50) DEFAULT NULL COMMENT '手机号（定时任务 默认0）',
      `add_time`bigint(20) NOT NULL DEFAULT '0' COMMENT '执行时间',
      `end_time`bigint(20) NOT NULL DEFAULT '0' COMMENT '同步结束时间',
      `success_time`bigint(20) NOT NULL DEFAULT '0' COMMENT '执行成功时间',
      `state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '执行状态：1.同步进行中 2.同步结束 3.执行成功',
      PRIMARY KEY (`data_id`),
      INDEX `user_id` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据同步表（报表）';
     *
     *
     * */
    public function insertCronDataInfo($source_type, $start_date, $end_date, $job_shops='all', $job_types='1,2,3,4,5,6', $user_info = array())
    {
        $insertData = array(
            'source_type' => $source_type,
            'start_date' => strtotime($start_date.' 00:00:00'),
            'end_date' => strtotime($end_date.' 23:59:59'),
            'job_shops' => $job_shops,
            'job_types' => $job_types,
            'user_id' => isset($user_info['user_id']) ? $user_info['user_id'] : 0,
            'user_name' => isset($user_info['user_name']) ? $user_info['user_name'] : 0,
            'user_mobile' => isset($user_info['user_mobile']) ? $user_info['user_mobile'] : 0,
            'add_time' => time(),
            'end_time' => 0,
            'success_time' => 0,
            'state' => 1,
        );

        $this->db_sy->insert('lslr_cron_data', $insertData);
        data_log_message('debug', __LINE__ . ' ' . $this->db_sy->last_query());

        $insertId = $this->db_sy->insert_id();

        return $insertId;
    }


    //更新：数据同步表 $end_time = 'end_time' | 'success_time'
    public function updateCronDataInfo($data_id, $state, $fields_time)
    {
        $updateData['state'] = $state;
        $updateData[$fields_time] = time();

        $this->db_sy->where('data_id', $data_id)->update('lslr_cron_data', $updateData);
        data_log_message('debug', __LINE__ . ' ' . $this->db_sy->last_query());

        return true;
    }


    /*
     *
        DROP TABLE IF EXISTS lslr_cron_data_job;
        CREATE TABLE IF NOT EXISTS `lslr_cron_data_job` (
          `job_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据同步任务ID',
          `data_id` int(11) NOT NULL COMMENT '数据同步ID',
          `job_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '类别：1、基础信息 2、VIP资料 3、库存 4、销售目标 5、销售明细 6、数据计算 7、属性同步',
          `job_state` tinyint(2) NOT NULL DEFAULT '1' COMMENT '类别状态：1、数据抽取(计算)开始 2、数据抽取(计算)进行中 3、数据抽取(计算)结束 4、数据同步开始 5、数据同步进行中 6、数据同步结束 7、清除缓存',
          `job_desc` longtext COMMENT '类别描述',
          `job_time`bigint(20) NOT NULL DEFAULT '0' COMMENT '执行时间',
          `job_par` longtext COMMENT '参数信息',
            PRIMARY KEY (`job_id`),
            INDEX `data_id` (`data_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据同步任务表（报表）';
     *
     * */
    public function insertCronDataJobInfo($data_id, $job_type=1, $job_state=1, $job_desc='', $job_par=array())
    {
        $insertData = array(
            'data_id' => $data_id,
            'job_type' => $job_type,
            'job_state' => $job_state,
            'job_desc' => $job_desc,
            'job_time' => time(),
            'job_par' => $job_par ? json_encode($job_par) : '',
        );

        $this->db_sy->insert('lslr_cron_data_job', $insertData);
        data_log_message('debug', __LINE__ . ' ' . $this->db_sy->last_query());

        return true;
    }


    //获取：信息
    public function getCronDataInfo($data_id)
    {
        $data = array();

        $query = $this->db_sy->select('source_type, start_date, end_date, job_shops, job_types, user_id, user_name, user_mobile, add_time, end_time, state')
            ->where('data_id', $data_id)
            ->get('lslr_cron_data');

        if ($query->num_rows() == 1) {
            $data = $query->row_array();
        }
        return $data;
    }


    //获取：查询表 与 备份表
    public function init_operation_tables($arr_operation_table = array())
    {
        $arr_data_table = array();
        foreach ($arr_operation_table as $val) {
            $val_name = 'data_'.$val;
            $arr_data_table[] = $val_name;
        }

        //查询表
        $this->arr_table = $arr_operation_table;

        //备份表
        $this->arr_data_table = $arr_data_table;

        return $arr_data_table;
    }


    //获取：所有的数据库信息
    public function to_load_databases($db_res, $config_file)
    {
        data_log_message('debug', 'db_res '. print_r($db_res, true));
        data_log_message('debug', 'config_file '. print_r($config_file, true));

        $this->db_resource = $this->load->database($db_res, true);
        //data_log_message('debug', 'db_res '. $this->db_resource->database);
        $this->load->config($config_file, true);

        //提前执行的sql语句，config的数据库文件的参数
        $run_first = $this->config->item('run_first', $config_file);
        if (!empty($run_first)) {
            $this->db_resource->query($run_first);
        }
    }


    //插入：数据超过100000条，就自动分页处理  is_delete是否删除数据 1是 2否
    public function insert_into_datatable($data_id, $type, $table, $r_config, $config_file, $str_shop, $is_delete = 1)
    {
        data_log_message('debug', date('Y-m-d H:i:s')  . ' user_id = ' .$this->job_user_id . "\n");

        //获取：备份表
        $data_table = 'data_'.$table;

        //创建：备份表
        $sql_create = 'CREATE TABLE IF NOT EXISTS '.$data_table.' LIKE '.$table.';';
        $this->db_sy->query($sql_create);

        if ($this->job_user_id > 0) {
            //获取：备份表
            $data_table_user = 'data_'.$table.'_'.$this->job_user_id;
            //创建：备份表
            $sql_create_user = 'CREATE TABLE IF NOT EXISTS '.$data_table_user.' LIKE '.$data_table.';';
            $this->db_sy->query($sql_create_user);

            $data_table = $data_table_user;
        }

        //if($is_delete == 1){ //多库多表情况下，必须清空data表，不然会数据重复
        //删除：数据
        $this->delete_datatable($data_table, $config_file, $str_shop);

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 2, $data_table.'表数据抽取进行中，删除数据结束', array());
        //}


        //插入：销售目标
        if ($table == 'sy_v_plani') {
            $this->insert_into_plani($data_id, $config_file, $str_shop, $is_delete);
            return;
        }

        $str_where = (empty($r_config['where'])||strlen($r_config['where'])==0)?'':$r_config['where'];

        if ($str_shop == '') {
            //全部店铺
        } else {
            //分店抽取
            if (isset($r_config['shop_code']) && $r_config['shop_code']) {
                $shop_code = $r_config['shop_code'];
                $str_where .= strlen($str_where)?' and ':'';
                $str_where .= $shop_code. $str_shop;
            }
        }

        if ($r_config['date_key'] && strlen($r_config['date_key']) > 0) {
            $str_where .= strlen($str_where)?' and ':'';
            $str_where .= $r_config['date_key']. " >= '". $this->deal_with_resource_data_from_date. "' and ". $r_config['date_key']. " < '". $this->deal_with_resource_data_to_date. "'";
        }

        $arr_columns = array();
        foreach ($r_config['columns'] as $sy => $res) {
            array_push($arr_columns, $res. ' as '. $sy);
        }

        $arr_order = array();
        foreach ($r_config['sy_key'] as $ss) {
            array_push($arr_order, $r_config['columns'][$ss]);
        }

        if ($this->db_resource->dbdriver == 'mssql') {
            $str_query = ' SELECT ROW_NUMBER() OVER(Order by '. implode(', ', $arr_order). ' ) AS RowNumber,'. implode(', ', $arr_columns). ' FROM '. $r_config['table'];
            if (count($r_config['join'])) {
                foreach ($r_config['join'] as $join_table => $on) {
                    $str_query .= ' LEFT JOIN '. $join_table. ' ON '. $on;
                }
            }
        } elseif ($this->db_resource->dbdriver == 'oci8') {
            $str_query = ' SELECT '. implode(', ', $arr_columns). ' FROM '. $r_config['table'];
            if (count($r_config['join'])) {
                foreach ($r_config['join'] as $join_table => $on) {
                    $str_query .= ' LEFT JOIN '. $join_table. ' ON '. $on;
                }
            }
        } else {
            //$this->db_resource->dbdriver == 'mysqli' || $this->db_resource->dbdriver == 'mysql'
            $str_query = ' SELECT '. implode(', ', $arr_columns). ' FROM '. $r_config['table'];
            if (count($r_config['join'])) {
                foreach ($r_config['join'] as $join_table => $on) {
                    $str_query .= ' LEFT JOIN '. $join_table. ' ON '. $on;
                }
            }
        }

        if (strlen($str_where)) {
            $str_query .= ' WHERE '. $str_where;
        }
        if (!empty($r_config['group_by'])) {
            $str_query .= ' GROUP BY '. (is_array($r_config['group_by'])?implode(', ', $r_config['group_by']):$r_config['group_by']);
        }

        if (!empty($r_config['having'])) {
            $str_query .= ' HAVING '. $r_config['having'];
        }
//        if($this->db_resource->dbdriver == 'oci8'){
//            $str_query_c = ' select b.*, ROWNUM as ROWNO from ('.$str_query.') b ORDER BY ROWNUM ASC ';
//            $str_query .= ' ORDER BY ROWNUM ASC ';
//        }else{
//            $str_query_c = $str_query;
//        }

        //获取：数据总数
        $str_query_count = "select count(*) as C from ($str_query) a";
        $obj_get_count = $this->db_resource->query($str_query_count);
        $arr_get_count = $obj_get_count->result_array();
        data_log_message('debug', '---------- count data by db_resource :'. $this->db_resource->last_query());
        data_log_message('debug', '---------- arr_get_count ----------'. print_r($arr_get_count, true));

        $total_count = $arr_get_count[0]['C'];
        if ($total_count > 0) {
            $l = $this->config->item('page_limit', $config_file); //self::PAGE_LIMIT;
            $r = $total_count/$l + 1;
            $t = ($total_count%$l? $r + 1 : $r);
            $c = 0;
            $s = 0;    //数据开始位置

            if ($this->db_resource->dbdriver == 'mssql') {
                while ($s < $total_count) {
                    $e = $l*($c+1);
                    $str_query_limit = "select top ". $l. " * from ($str_query) as b where RowNumber > ". $l*$c. " order by RowNumber";
                    data_log_message('debug', '---------- str_query_limit :'. $str_query_limit);
                    $obj_get = $this->db_resource->query($str_query_limit);
			log_message('debug', 'mssql query limit error number :'. $this->db_resource->_error_number());   //获取错误码
        log_message('debug', 'mssql query limit error message :'. $this->db_resource->_error_message()); //获取错误信息描述
                    if ($obj_get->num_rows()) {
                        $arr_get = $obj_get->result_array();
                        $arr_new = array();
                        foreach ($arr_get as $arr_g) {
                            unset($arr_g['RowNumber']);
                            array_push($arr_new, $arr_g);
                        }
                        $this->db_sy->insert_batch($data_table, $arr_new, 1);
                        data_log_message('debug', '---------- insert data by mssql :'. $this->db_sy->last_query());
                    }
                    $s = ($c + 1)*$l;
                    $c++;
                }
            } elseif ($this->db_resource->dbdriver == 'oci8') {
                while ($s < $total_count) {
                    $e = $l*($c+1);
                    $str_query_limit = "select * from ( select a.*, ROWNUM as ROWNO from ($str_query) a where ROWNUM <= ". $e. " ORDER BY ROWNUM ASC) b where ROWNO > ". $s;
                    data_log_message('debug', '---------- str_query_limit :'. $str_query_limit);
                    $obj_get = $this->db_resource->query($str_query_limit);
                    if ($obj_get->num_rows()) {
                        $arr_get = $obj_get->result_array();
                        $arr_new = array();
                        foreach ($arr_get as $arr_g) {
                            unset($arr_g['ROWNO']);
                            array_push($arr_new, $arr_g);
                        }
                        $this->db_sy->insert_batch($data_table, $arr_new, 1);
                        data_log_message('debug', '---------- insert data by oci8 :'. $this->db_sy->last_query());
                    }
                    $s = $e;
                    $c++;
                }
            } else {
                $str_order = '';
                if (!empty($r_config['group_by'])) {
                    $str_order = ' ORDER BY '. (is_array($r_config['group_by'])?implode(', ', $r_config['group_by']):$r_config['group_by']);
                }

                while ($s < $total_count) {
                    $e = $l*($c+1);
                    $str_query_limit = "select * from ($str_query $str_order ) a limit $s, $l";
                    data_log_message('debug', '---------- str_query_limit sql:'. $str_query_limit);
                    $obj_get = $this->db_resource->query($str_query_limit);
                    if ($obj_get->num_rows()) {
                        $arr_get = $obj_get->result_array();

                        $this->db_sy->insert_batch($data_table, $arr_get, 1);
                        data_log_message('debug', '---------- insert data by mysql :'. $this->db_sy->last_query());
                    }
                    $s = $e;
                    $c++;
                }
            }
        }

        //处理：备份表数据
        $this->update_datatable($table, $data_table, $config_file, $str_shop);

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 2, $data_table.'表数据抽取进行中，处理数据结束', array());

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 3, $data_table.'表数据抽取结束', array());

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 4, $table.'表数据同步开始', array());

        if ($is_delete == 1) {
            //删除：查询表数据
            $this->delete_searchtable($table, $str_shop);

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, $type, 5, $table.'表数据同步进行中，删除数据结束', array());
        }

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 5, $table.'表数据同步进行中', array());

        //同步：数据到查询表
        $this->insert_data_to_searchtable($data_table, $table, $config_file, $str_shop);

        if ($table == 'sy_v_item' && $this->update_item_acc_by_category) {
            $update_item_acc_by_category = $this->config->item('update_item_acc_by_category', 'config_report_v3');
            if(strlen($update_item_acc_by_category) || $update_item_acc_by_category == 'N'){
                $this->update_item_acc_by_category = false;
            }
            //$this->item_acc_update();    //根据品类表更新商品表的‘是否配件’字段
            $this->item_add_acc_update();    //根据品类表更新商品表的‘是否附加’、‘是否配件’字段
        }

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 6, $table.'表数据同步结束', array());
    }


    //删除：数据 是否data备份表的数据全部可以清空？
    public function delete_datatable($data_table, $config_file, $str_shop)
    {
        //清空 data备份表
        $this->db_sy->truncate($data_table);

        data_log_message('debug', '---------- delete data by mysql :'. $this->db_sy->last_query());
    }


    //处理：备份表数据
    public function update_datatable($table, $data_table, $config_file, $str_shop)
    {
        if ($table == 'sy_v_category') {

            //$this->db_sy->query("INSERT INTO `data_sy_v_category` (`ic_id`, `ic_name`, `ic_code`, `class_id`, `ic_prop`, `ic_order`, `ic_state`, `time_stamp`) VALUES ('9999', '外购', '', NULL, '1', '100', '1', CURRENT_TIMESTAMP);");

            $insert_data_sy_v_category = $this->config->item('insert_data_sy_v_category');
            if (method_exists($this, $insert_data_sy_v_category)) {
                data_log_message('debug', "---------- customer sy_v_category method ". $insert_data_sy_v_category. "\n");
                $this->{$insert_data_sy_v_category}($config_file, $data_table);
            }
        } elseif ($table == 'sy_v_item') {

            //$this->db_sy->query("INSERT INTO `data_sy_v_item`(`item_id`, `brand_id`, `is_add`, `is_included`, `year_id`, `sea_id`, `sea_name`, `cost`, `sub_icid`, `wave_no`, `ic_id`, `ic_name`, `class_id`, `class_name`, `sc_id`, `sc_name`, `is_acc`, `sale_price`, `item_desc`, `sales_point`, `item_file`, `info_url`, `style_id`, `s_date`, `e_date`, `pub_date`, `time_stamp`) VALUES ('9999999999999', 'HJH', 1, 1, 2018, '88', '全季', 0.00, '', '', '9999', '外购', '', '', '', '', 'Y', 0.00, '外购品', '', '', '', NULL, '0000-00-00', '0000-00-00', 0, CURRENT_TIMESTAMP);");

            $insert_data_sy_v_item = $this->config->item('insert_data_sy_v_item');
            if (method_exists($this, $insert_data_sy_v_item)) {
                data_log_message('debug', "---------- customer sy_v_item method ". $insert_data_sy_v_item. "\n");
                $this->{$insert_data_sy_v_item}($config_file, $data_table);
            }

            //更新商品表日期
            //$this->update_item_date($config_file);
            //$update_item_date = $this->config->item('update_item_date', $config_file);  //放在 config_report_ipos_v3.php 等其他文件的读取写法
            $update_item_date = $this->config->item('update_item_date');   //放在 config_report_v3.php 的读取写法
            if (method_exists($this, $update_item_date)) {
                data_log_message('debug', "---------- customer sy_v_item method ". $update_item_date. "\n");
                $this->{$update_item_date}($config_file, $data_table);
            }
        }

        data_log_message('debug', '---------- update data by mysql :'. $this->db_sy->last_query());
    }


    public function insert_data_sy_v_category($config_file, $data_table)
    {
        $this->db_sy->query("INSERT INTO `".$data_table."` (`ic_id`, `ic_name`, `ic_code`, `class_id`, `ic_prop`, `ic_order`, `ic_state`, `time_stamp`) VALUES ('9999', '外购', '', NULL, '1', '100', '1', CURRENT_TIMESTAMP);");
        data_log_message('debug', '---------- insert_data_sy_v_category by mysql :'. $this->db_sy->last_query());
    }


    public function insert_data_sy_v_item($config_file, $data_table)
    {
        $this->db_sy->query("INSERT INTO `".$data_table."`(`item_id`, `brand_id`, `is_add`, `is_included`, `year_id`, `sea_id`, `sea_name`, `cost`, `sub_icid`, `wave_no`, `ic_id`, `ic_name`, `class_id`, `class_name`, `sc_id`, `sc_name`, `is_acc`, `sale_price`, `item_desc`, `sales_point`, `item_file`, `info_url`, `style_id`, `s_date`, `e_date`, `pub_date`, `time_stamp`) VALUES ('9999999999999', 'HJH', 1, 1, 2018, '88', '全季', 0.00, '', '', '9999', '外购', '', '', '', '', 'Y', 0.00, '外购品', '', '', '', NULL, '0000-00-00', '0000-00-00', 0, CURRENT_TIMESTAMP);");
        data_log_message('debug', '---------- insert_data_sy_v_item by mysql :'. $this->db_sy->last_query());
    }


    public function update_sum_sales_info($sday, $eday)
    {
        $this->db_sy->query("UPDATE sales_info a, (SELECT b.inv_no ,sum(b.inv_qtys) AS inv_qtys, sum(b.inv_money) AS inv_money, sum(b.rec_money) AS rec_money, sum(b.arr_money) AS arr_money, sum(b.inv_qtys*b.tag_price) AS tag_money FROM sales_info a,sales_detail b WHERE a.inv_no=b.inv_no AND a.sale_date >='".$sday. "' AND a.sale_date<'". $eday."' GROUP BY b.inv_no) b SET a.inv_qtys=b.inv_qtys,a.tag_money=b.tag_money,a.inv_money=b.inv_money WHERE a.inv_no=b.inv_no;");
        data_log_message('debug', '---------- update_sum_sales_info by mysql :'. $this->db_sy->last_query());
    }


    //根据配置更新商品新品日期
    public function update_item_date($config_file, $data_table)
    {
        log_message('debug', 'update_item_date config_file :'. $config_file);
        $sea_span = $this->config->item('sea_span', $config_file);
        //             $arr_sea = $this->config->item('sea_list');
        $year_offset = $this->config->item('year_offset', $config_file);
        $sea_from_to = $this->config->item('sea_from_to', $config_file);
        log_message('debug', 'sea_from_to :'. print_r($sea_from_to, true));
        $sea_whole = array();

        $year_key = 'year_id';
        $sea_key = 'sea_id';
        $str_when_from = '';
        $str_when_to = '';
        foreach ($sea_from_to as $sea_id => $from_to) {
            $to = str_pad($from_to['to'], 2, "0", STR_PAD_LEFT) + 1;
            //                 echo "to +1 ->". $to. "\n";
            $to = str_pad($to, 2, "0", STR_PAD_LEFT);
            //                 echo "to +0 ->". $to. "\n";
            if ($from_to['to'] == 12) {
                array_unshift($sea_whole, $sea_id);
//                $sea_whole = $sea_id;
                $to = '01';
                //                     echo "to 01 ->". $sea_whole. "\n";
            }
            $str_when_from .= " when ". $sea_id. " then '". $from_to['from']. "'";
            $str_when_to .= " when ". $sea_id. " then '". $to. "'";
        }
        log_message('debug', 'str_when_from :'. $str_when_from);
        log_message('debug', 'str_when_to :'. $str_when_to);
        $s_mon = "(case ". $sea_key. $str_when_from. " else '' end )";
        $e_mon = "(case ". $sea_key. $str_when_to. " else '' end )";
        //             echo "s_mon : ". $s_mon. "\n";
        //             echo "e_mon : ". $e_mon. "\n";

        $s_year = "(case when ". $sea_key. " = ". $sea_span. " then ". $year_key. "+". $year_offset['s']. " else ". $year_key. " end)";
        $e_year = "(case when ". $sea_key. " = ". $sea_span. " then ". $year_key. "+". $year_offset['e'];
        if (count($sea_whole)) {
            $str_whole = "(". implode(",", $sea_whole). ")";
            $e_year .= " when ". $sea_key. " in ". $str_whole. " then ". $year_key. "+1";
        }
        $e_year .= " else ". $year_key. " end)";
        //             echo "s_year : ". $s_year. "\n";
        //             echo "e_year : ". $e_year. "\n";

        $date_change_s = "CONCAT(". $year_key. ", '-', ". $s_mon. ", '-01')";
        $date_change_e = "from_unixtime(UNIX_TIMESTAMP(CONCAT(". $e_year. ", '-', ". $e_mon. ", '-01 00:00:00')) - 1)";
        $s_date = "(case when ". $year_key. " is null then '' else (case when ". $sea_key. " is null then '' when ". $sea_key. " ='' then '' else ". $date_change_s. " end) end)";
        $e_date = "(case when ". $year_key. " is null then '' else (case when ". $sea_key. " is null then '' when ". $sea_key. " ='' then '' else ". $date_change_e. " end) end)";
        $sql_item = "update `".$data_table."` set s_date = ". $s_date. ", e_date = ". $e_date. "  where sea_id is not null and sea_id <> ''
            and year_id > 0 and s_date = '0000-00-00' and e_date = '0000-00-00'";

        $this->db_sy->query($sql_item);
        log_message('debug', 'update_item_date query :'. $this->db_sy->last_query());
    }

    //根据品类表更新商品表的‘是否配件’字段
    public function item_acc_update()
    {
        if($this->update_item_acc_by_category){
            $this->db_sy->query("update sy_v_item set is_acc = 'Y' where ic_id in ( select ic_id from sy_v_category where ic_prop = 1 or ic_prop = 3)");
        }
    }


    /*
        2020-07-16 品类属性ic_prop新定义状态如下，is_add 是否计算数量、金额、附加，is_acc 是否配件 Y是 N否，更新逻辑如下：
        1. 配件：计算销售数量，计算销售金额，计算销售附加。  (对应sy_v_item  is_add=1  ,is_acc='Y')
        2. 正品：计算销售数量，计算销售金额，计算销售附加。  (对应sy_v_item  is_add=1  ,is_acc='N')
        3. 其他：不计算销售数量，不计算销售金额，不计算附加。(对应sy_v_item  is_add=2  ,is_acc='Y')
        4. 特殊：计算销售数量，计算销售金额，不算销售附加。  (对应sy_v_item  is_add=3  ,is_acc='Y')
     * */
    public function item_add_acc_update()
    {
        $this->db_sy->query("update sy_v_item as i, sy_v_category as c set i.is_add = '1', i.is_acc = 'Y' where i.ic_id = c.ic_id and ic_prop = 1;");
        log_message('debug', 'item_add_acc_update 1 :'. $this->db_sy->last_query());

        $this->db_sy->query("update sy_v_item as i, sy_v_category as c set i.is_add = '1', i.is_acc = 'N' where i.ic_id = c.ic_id and ic_prop = 2;");
        log_message('debug', 'item_add_acc_update 2 :'. $this->db_sy->last_query());

        $this->db_sy->query("update sy_v_item as i, sy_v_category as c set i.is_add = '2', i.is_acc = 'Y' where i.ic_id = c.ic_id and ic_prop = 3;");
        log_message('debug', 'item_add_acc_update 3 :'. $this->db_sy->last_query());

        $this->db_sy->query("update sy_v_item as i, sy_v_category as c set i.is_add = '3', i.is_acc = 'Y' where i.ic_id = c.ic_id and ic_prop = 4;");
        log_message('debug', 'item_add_acc_update 4 :'. $this->db_sy->last_query());

    }


    public function update_item_date_old($config_file, $data_table)
    {
        log_message('debug', 'update_item_date config_file :'. $config_file);
        $sea_span = $this->config->item('sea_span', $config_file);
        //             $arr_sea = $this->config->item('sea_list');
        $year_offset = $this->config->item('year_offset', $config_file);
        $sea_from_to = $this->config->item('sea_from_to', $config_file);
        log_message('debug', 'sea_from_to :'. print_r($sea_from_to, true));
        $sea_whole = '';

        $year_key = 'year_id';
        $sea_key = 'sea_id';
        $str_when_from = '';
        $str_when_to = '';
        foreach ($sea_from_to as $sea_id => $from_to) {
            $to = str_pad($from_to['to'], 2, "0", STR_PAD_LEFT) + 1;
            //                 echo "to +1 ->". $to. "\n";
            $to = str_pad($to, 2, "0", STR_PAD_LEFT);
            //                 echo "to +0 ->". $to. "\n";
            if ($from_to['to'] == 12) {
                $sea_whole = $sea_id;
                $to = '01';
                //                     echo "to 01 ->". $sea_whole. "\n";
            }
            $str_when_from .= " when ". $sea_id. " then '". $from_to['from']. "'";
            $str_when_to .= " when ". $sea_id. " then '". $to. "'";
        }
        log_message('debug', 'str_when_from :'. $str_when_from);
        log_message('debug', 'str_when_to :'. $str_when_to);
        $s_mon = "(case ". $sea_key. $str_when_from. " else '' end )";
        $e_mon = "(case ". $sea_key. $str_when_to. " else '' end )";
        //             echo "s_mon : ". $s_mon. "\n";
        //             echo "e_mon : ". $e_mon. "\n";

        $s_year = "(case when ". $sea_key. " = ". $sea_span. " then ". $year_key. "+". $year_offset['s']. " else ". $year_key. " end)";
        $e_year = "(case when ". $sea_key. " = ". $sea_span. " then ". $year_key. "+". $year_offset['e'];
        if (strlen($sea_whole)) {
            $e_year .= " when ". $sea_key. " = ". $sea_whole. " then ". $year_key. "+1";
        }
        $e_year .= " else ". $year_key. " end)";
        //             echo "s_year : ". $s_year. "\n";
        //             echo "e_year : ". $e_year. "\n";

        $date_change_s = "CONCAT(". $year_key. ", '-', ". $s_mon. ", '-01')";
        $date_change_e = "from_unixtime(UNIX_TIMESTAMP(CONCAT(". $e_year. ", '-', ". $e_mon. ", '-01 00:00:00')) - 1)";
        $s_date = "(case when ". $year_key. " is null then '' else (case when ". $sea_key. " is null then '' when ". $sea_key. " ='' then '' else ". $date_change_s. " end) end)";
        $e_date = "(case when ". $year_key. " is null then '' else (case when ". $sea_key. " is null then '' when ". $sea_key. " ='' then '' else ". $date_change_e. " end) end)";
        $sql_item = "update `".$data_table."` set s_date = ". $s_date. ", e_date = ". $e_date. "  where sea_id is not null and sea_id <> ''
            and year_id > 0 and s_date = '0000-00-00' and e_date = '0000-00-00'";

        $this->db_sy->query($sql_item);
    }


    //同步：数据到查询表
    public function insert_data_to_searchtable($data_table, $table, $config_file, $str_shop = '')
    {
        $r_config = $this->config->item($table, $config_file);

        if ($str_shop == '')
        {
            if ($table == 'sy_v_stock')
            {
                //$this->db_sy->truncate($table);
                $str_insert = 'INSERT INTO sy_v_stock SELECT * FROM data_sy_v_stock;';
                $this->db_sy->query($str_insert);
                data_log_message('debug', '---------- to search table :'. $this->db_sy->last_query());

                return true;
            }elseif($table == 'sy_v_instock') {
                //$this->db_sy->truncate($table);
                $str_insert = 'INSERT INTO sy_v_instock SELECT * FROM data_sy_v_instock;';
                $this->db_sy->query($str_insert);
                data_log_message('debug', '---------- to search table :'. $this->db_sy->last_query());

                return true;
            }elseif($table == 'sy_v_vip') {
                //$this->db_sy->truncate($table);
                $str_insert = 'INSERT INTO sy_v_vip SELECT * FROM data_sy_v_vip;';
                $this->db_sy->query($str_insert);
                data_log_message('debug', '---------- to search table :'. $this->db_sy->last_query());

                return true;
            }
        }

        $where = ''; //data表，每次都清空，就不需要获取时间范围查询了。

        $obj_shops = $this->db_sy->select('count(*) as total')->get($data_table); //->where($where)
        $arr_shops = $obj_shops->result_array();
        $total_count = $arr_shops[0]['total'];
        if ($total_count == 0) {
            return true;
        }
        data_log_message('debug', '---------- total_count for limit : '. $total_count);

        $l = $this->config->item('page_limit', $config_file); //self::PAGE_LIMIT;
        $r = $total_count/$l + 1;
        $t = ($total_count%$l? $r + 1 : $r);
        $c = 0;
        $s = 0;    //数据开始位置
        while ($c <= $t) {
            if (!empty($r_config['duplicate'])) {
                $arr_update = $r_config['duplicate'];
                $str_update = '';
                foreach ($arr_update as $item) {
                    $str_update .= '`'. $item. '` = VALUES(`'. $item. '`), ';
                }
                $str_update = substr($str_update, 0, strlen($str_update)-2);
                //'insert ignore into sy_v_barcode1 select * from sy_v_barcode';
                // 'insert ignore into sy_v_barcode1 select * from sy_v_barcode  ON DUPLICATE KEY UPDATE `item_id` = VALUES(`item_id`), `color_id` = VALUES(`color_id`), `size_id` = VALUES(`size_id`), `color_name` = VALUES(`color_name`)'
                //SELECT * FROM `sy_v_category` WHERE `time_stamp` >= '2019-04-10 00:00:00' AND `time_stamp` <= '2019-04-10 23:59:59' ORDER BY `ic_id` DESC
                //$this->db_sy->query("INSERT IGNORE INTO ". $table. " (select * from ". $data_table. " WHERE time_stamp >= '".$s_day."' AND time_stamp <= '".$e_day."' limit  ". $s. ",". $l. ") ON DUPLICATE KEY UPDATE ". $str_update);
                $str = empty($r_config['sy_key'])?'':$r_config['sy_key'];
                $this->db_sy->query("INSERT  INTO ". $table. " (select * from ". $data_table. " order by ".(is_array($str)?implode(',', $str):$str). " limit ". $s. ",". $l. ") ON DUPLICATE KEY UPDATE ". $str_update); //" WHERE ".$where. IGNORE
            } else {
                if ($table == 'sales_detail') {
                    $str_insert = 'insert IGNORE into '. $table. '(select inv_no, item_id, color_id, size_id, inv_qtys, inv_money, tag_price, time_stamp, rec_money, arr_money from (select * from '. $data_table. ' order by id limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                } else {
                    $str = empty($r_config['sy_key'])?'':$r_config['sy_key'];

                    //$str_insert = 'insert IGNORE into '. $table. '(select * from (select * from '. $data_table. ' WHERE time_stamp >= "'.$s_day.'" AND time_stamp <= "'.$e_day.'" order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)';
                    if ($table == 'sy_v_plani') {
                        $str_insert = 'insert into '. $table. ' (`shop_code`, `plan_date`, `plan_amt`) (select shop_code, plan_date, plan_amt from (select shop_code, plan_date, plan_amt from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    } elseif ($table == 'sy_v_vip') {
                        $str_insert = 'insert into '. $table. ' (`vip_no`,`cust_name`,`vip_level`,`shop_code`,`register_date`,`cust_sex`,`age`,`born_date`,`mobile`,`address`,`staff_id`,`vip_state`,`active_money`,`vip_points`) (select `vip_no`,`cust_name`,`vip_level`,`shop_code`,`register_date`,`cust_sex`,`age`,`born_date`,`mobile`,`address`,`staff_id`,`vip_state`,`active_money`,`vip_points` from (select `vip_no`,`cust_name`,`vip_level`,`shop_code`,`register_date`,`cust_sex`,`age`,`born_date`,`mobile`,`address`,`staff_id`,`vip_state`,`active_money`,`vip_points` from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    } elseif ($table == 'sy_v_staff_sales') {
                        $str_insert = 'insert into '. $table. ' (`sale_date`,`shop_code`,`staff_id`,`sale_amt`) (select `sale_date`,`shop_code`,`staff_id`,`sale_amt` from (select `sale_date`,`shop_code`,`staff_id`,`sale_amt` from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    } elseif ($table == 'sy_v_shopstock') {
                        $str_insert = 'insert into '. $table. ' (`stock_date`,`shop_code`,`item_id`,`color_id`,`size_id`,`stock_qtys`) (select `stock_date`,`shop_code`,`item_id`,`color_id`,`size_id`,`stock_qtys` from (select `stock_date`,`shop_code`,`item_id`,`color_id`,`size_id`,`stock_qtys` from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    } elseif ($table == 'sy_v_senddata') {
                        $str_insert = 'insert into '. $table. ' (`send_date`,`shop_code`,`send_amt`) (select `send_date`,`shop_code`,`send_amt` from (select `send_date`,`shop_code`,`send_amt` from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    } else {
                        $str_insert = 'insert IGNORE into '. $table. '(select * from (select * from '. $data_table. ' order by '. (is_array($str)?implode(',', $str):$str). ' limit  '. $s. ','. $l. ') a)'; //' WHERE '.$where.
                    }
                }

                $this->db_sy->query($str_insert);
            }
            data_log_message('debug', '---------- to search table :'. $this->db_sy->last_query());
            $s = ($c + 1)*$l;
            $c++;
        }
    }


    //用户创建的表要删除
    public function deleteDataTables($user_id)
    {
        if ($user_id > 0) {
            $query_all = $this->db_sy->select('t_name, t_type')
                ->where('t_state', 1)
                ->order_by('order_seq', 'asc')
                ->get('data_tables');

            if ($query_all->num_rows() > 0) {
                foreach ($query_all->result_array() as $val) {
                    $data_table_user = 'data_'.$val['t_name'].'_'.$user_id;
                    //删除：备份表
                    $this->db_sy->query('DROP TABLE IF EXISTS '.$data_table_user.';');
                    data_log_message('debug', '---------- to drop table :'. $this->db_sy->last_query());
                }
            }
        }
    }

    //插入：销售目标
    public function insert_into_plani($data_id, $config_file, $str_shop, $is_delete)
    {
        $arr_plani = $this->config->item('sy_v_plani', $config_file);

        if (!empty($arr_plani['table'])) {
            if ($this->job_user_id > 0) {
                //获取：备份表
                $data_table = 'data_sy_v_plani_'.$this->job_user_id;
            } else {
                //获取：备份表
                $data_table = 'data_sy_v_plani';
            }

            $this->new_deal_with_data_from_date = $this->deal_with_data_from_date;
            $this->new_deal_with_data_to_date = $this->deal_with_data_to_date;

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 4, 2, 'data_sy_v_plani表数据抽取进行中', array());

            $plani_type = $arr_plani['type'];
            if (method_exists($this, $plani_type)) {
                $this->{$plani_type}($arr_plani, $config_file, $data_table);
            }

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 4, 3, 'data_sy_v_plani表数据抽取结束', array());

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 4, 4, 'sy_v_plani表数据同步开始', array());

            if ($is_delete == 1) {
                //删除：查询表数据
                $this->delete_searchtable('sy_v_plani', $str_shop);
            }

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 4, 5, 'sy_v_plani表数据同步进行中', array());

            //同步：数据到查询表
            $this->insert_data_to_searchtable($data_table, 'sy_v_plani', $config_file, $str_shop);

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 4, 6, 'sy_v_plani表数据同步结束', array());

            $this->deal_with_data_from_date = $this->new_deal_with_data_from_date;
            $this->deal_with_data_to_date = $this->new_deal_with_data_to_date;
        }
    }


    //销售目标抽取，适用于数据源表中一行记录代表一个月数据的情况
    public function plani_with_type1($arr_plani, $config_file, $data_table)
    {
        $arr_col = $arr_plani['columns'];
        $arr_insert = array();
        $res_shop_code = $arr_col['shop_code'];
        $res_year = $arr_col['plan_date']['year'];
        $res_month = $arr_col['plan_date']['month'];
        $res_day = $arr_col['plan_date']['day'];
        //             $arr_insert['plan_amt'] = $arr_col['plan_amt'];

        $str_join = '';
        if (!empty($arr_plani['join']) && count($arr_plani['join'])) {
            foreach ($arr_plani['join'] as $t => $k) {
                $str_join .= ' LEFT JOIN '. $t. ' ON '. $k;
            }
            //                 $str_join = substr($str_join, 0, strlen($str_join)- strlen('and '));
        }

        //  if(strlen($str_join) == strlen(' LEFT JOIN ')){
        //                 $str_join = '';
        //             }

        $str_where = $arr_plani['where'];

        if (!empty($arr_plani['date_key'])) {
            $str_where .= strlen($str_where)?' and':'';
            $str_where .= $arr_plani['date_key']. " >= '". $this->deal_with_resource_data_from_date. "'";
            if ($this->deal_with_data_with_limited_date) {
                $str_where .= ' and '. $arr_plani['date_key']. " < '". $this->deal_with_resource_data_to_date. "'";
            }
        }
        $str_where = strlen($str_where)?' where '. $str_where:'';
        if (!empty($arr_plani['group_by'])) {
            $str_where .= ' GROUP BY '. (is_array($arr_plani['group_by'])?implode(', ', $arr_plani['group_by']):$arr_plani['group_by']);
        }
        //             echo "zsiiiii :". $str_where;
        $obj_plan = $this->db_resource->query("SELECT ". $res_shop_code.", ". $res_year. ", ". $res_month. ", ". implode(', ', $res_day). " FROM ". $arr_plani['table']. $str_join. $str_where);

        data_log_message('debug', "---------- get plani query". $this->db_resource->last_query());
        //echo " get plani query : ". $this->db_resource->last_query(). "\n";
        if ($obj_plan->num_rows()) {
            $arr_plan = $obj_plan->result_array();

            $str_value = '';
            $all_date = array();
            foreach ($arr_plan as $plan) {
                $shop_code = $plan[$res_shop_code];
                $nd_mn = $plan[$res_year]. '-'. str_pad($plan[$res_month], 2, "0", STR_PAD_LEFT);
                $day_count = $this->days_in_month($plan[$res_month], $plan[$res_year]);

                for ($i = 1; $i <= $day_count; $i++) {
                    if ($i < 10) {
                        $plan_date = $nd_mn. '-0'. $i;
                    } else {
                        $plan_date = $nd_mn. '-'. $i;
                    }
                    //                        echo "plan_date: ". $plan_date. "\n";
                    $plan_amt = $plan[$res_day[$i-1]];
                    $str_value .= "('". $shop_code. "', ". strtotime($plan_date). ", ". $plan_amt. "), ";

                    $all_date[] = strtotime($plan_date);
                }
            }

            //获取最新时间，删除这段时间的数据
            $this->deal_with_data_from_date = date('Y-m-d', min($all_date));
            $this->deal_with_data_to_date = date('Y-m-d', strtotime('+1 day', max($all_date)));
            data_log_message('debug', '----------new deal date:'. $this->deal_with_data_from_date. ' to date:'. $this->deal_with_data_to_date);

            //                echo "str values :". $str_value. "\n";
            if (strlen($str_value) > 0) {
                $str_value = substr($str_value, 0, strlen($str_value)-2);
                $this->db_sy->query("INSERT IGNORE INTO ".$data_table." (shop_code, plan_date, plan_amt) VALUES". $str_value. " ON DUPLICATE KEY UPDATE `plan_amt` = VALUES (`plan_amt`)");
            }

            //                 echo " insert plani query : ". $this->db_sy->last_query(). "\n";
            data_log_message('debug', '---------- to data_sy_v_plani table :'. $this->db_sy->last_query());
        }
    }


    //销售目标抽取，适用于数据源表中一行记录代表一天数据的情况
    public function plani_with_type2($arr_plani, $config_file, $data_table)
    {
        $arr_col = $arr_plani['columns'];
        $arr_col['plan_date'] = $arr_col['plan_date']['day'];

        $arr_column = array();
        foreach ($arr_col as $sy => $res) {
            array_push($arr_column, $res. ' as "'. $sy. '"');
        }
        $str_query = ' SELECT '. implode(', ', $arr_column). ' FROM '. $arr_plani['table'];
        if (count($arr_plani['join'])) {
            foreach ($arr_plani['join'] as $join_table => $on) {
                $str_query .= ' LEFT JOIN '. $join_table. ' ON '. $on;
            }
        }

        $str_where = $arr_plani['where'];
        if (!empty($arr_plani['date_key'])) {
            $str_where .= strlen($str_where)?' and':'';
            $str_where .= $arr_plani['date_key']. " >= '". $this->deal_with_resource_data_from_date. "'";
            if ($this->deal_with_data_with_limited_date) {
                $str_where .= ' and '. $arr_plani['date_key']. " < '". $this->deal_with_resource_data_to_date. "'";
            }
        }
        if (strlen($str_where)) {
            $str_query .= ' WHERE '. $str_where;
        }
        if (!empty($arr_plani['group_by'])) {
            $str_query .= ' GROUP BY '. (is_array($arr_plani['group_by'])?implode(', ', $arr_plani['group_by']):$arr_plani['group_by']);
        }
        data_log_message('debug', "---------- get plani ". $str_query);
        $obj_plan = $this->db_resource->query($str_query);
        if ($obj_plan->num_rows()) {
            $arr_plan = $obj_plan->result_array();
            $total_count = $obj_plan->num_rows();
            $size = $this->config->item('page_limit', $config_file) / 10 ; //self::PAGE_LIMIT;
            $page_total = ceil($total_count / $size);

            data_log_message('debug', "---------- get total_count ". $total_count);
            data_log_message('debug', "---------- get size ". $size);
            data_log_message('debug', "---------- get page_total ". $page_total);

			$all_date = array();
            for ($page=1; $page<=$page_total; $page++) {
                $arr_plan_new = array_slice($arr_plan, ($page - 1)*$size, $size);

                $str_value = '';
                foreach ($arr_plan_new as $plan) {
                    $str_value .= "('". $plan['shop_code']. "', ". strtotime($plan['plan_date']). ", ". $plan['plan_amt']. "), ";
					$all_date[] = strtotime($plan['plan_date']);
                }
                //                echo "str values :". $str_value. "\n";


                if (strlen($str_value) > 0) {
                    $str_value = substr($str_value, 0, strlen($str_value)-2);
                    $this->db_sy->query("INSERT IGNORE INTO ".$data_table." (shop_code, plan_date, plan_amt) VALUES". $str_value. " ON DUPLICATE KEY UPDATE `plan_amt` = VALUES (`plan_amt`)");
                }

                //$this->db_sy->insert_batch($data_table, $arr_plan_new, 1);

                data_log_message('debug', '---------- to data_sy_v_plani table :'. $this->db_sy->last_query());
            }

            //获取最新时间，删除这段时间的数据
            $this->deal_with_data_from_date = date('Y-m-d', min($all_date));
            $this->deal_with_data_to_date = date('Y-m-d', strtotime('+1 day', max($all_date)));
            data_log_message('debug', '----------new deal date:'. $this->deal_with_data_from_date. ' to date:'. $this->deal_with_data_to_date);
            //echo " insert plani query : ". $this->db_sy->last_query(). "\n";
            //data_log_message('debug', '---------- to data_sy_v_plani table :'. $this->db_sy->last_query());
        }
    }


    //销售目标抽取，适用于数据源表中一行记录代表自定义时间段内数据的情况
    public function plani_with_type9($arr_plani, $config_file, $data_table)
    {
        //             echo "arr_plani ". print_r($arr_plani, true);
        $arr_col = $arr_plani['columns'];
        $arr_insert = array();
        $arr_insert['shop_code'] = $arr_col['shop_code'];
        $arr_insert['from_date'] = $arr_col['plan_date']['from_date'];
        $arr_insert['to_date'] = $arr_col['plan_date']['to_date'];
        $arr_insert['plan_amt'] = $arr_col['plan_amt'];

        $str_where = $arr_col['plan_date']['from_date']. " >= '". $this->deal_with_resource_data_from_date. "'";
        if ($this->deal_with_data_with_limited_date) {
            $str_where .= ' and '. $arr_plani['date_key']. " < '". $this->deal_with_resource_data_to_date. "'";
        }
        $arr_insert_select = array();
        foreach ($arr_insert as $sy => $res) {
            array_push($arr_insert_select, $res. ' as '. $sy);
        }
        $this->db_resource->select(implode(', ', $arr_insert_select), false);
        foreach ($arr_plani['join'] as $table => $on) {
            $this->db_resource->join($table, $on, 'left');
        }
        $this->db_resource->where($str_where);
        $obj_plan = $this->db_resource->get($arr_plani['table']);
        data_log_message('debug', "---------- get plani ". $this->db_resource->last_query());
        //             echo " select plani query : ". $this->db_resource->last_query(). "\n";
        $str_value = "('";
        if ($obj_plan->num_rows()) {
            $num=$obj_plan->num_rows();
            //                 echo "num :". print_r($num, true). "\n";
            $arr_plan = $obj_plan->result_array();
            // echo "arr plan :". print_r($arr_plan, true). "\n";

            foreach ($arr_plan as $plan) {
                $shop_code = $plan['shop_code'];
                $from_date = strtotime($plan['from_date']);
                $to_date = strtotime($plan['to_date']);
                $plan_amt = $plan['plan_amt'];
                if ($from_date == $to_date) {
                    $str_value .= $plan['shop_code']. "', ". strtotime($plan['from_date']). ", ". $plan['plan_amt']. "),('";
                } elseif ($from_date < $to_date) {
                    $day_count = ceil(($to_date - $from_date)/86400);
                    $plan_amt = $plan_amt/$day_count;
                    for ($i = 0; $i < $day_count; $i++) {
                        $plan_date = $from_date + $i * 86400;
                        $str_value .= "('". $shop_code. "', ". $plan_date. ", ". $plan_amt. "), ('";
                    }
                }
            }
        }
        //                echo "str values :". $str_value. "\n";
        if (strlen($str_value) > strlen("('")) {
            $str_value = substr($str_value, 0, strlen($str_value)-3);
            // echo "str value :". $str_value. "\n";

            $this->db_sy->query("INSERT IGNORE INTO ".$data_table." (shop_code, plan_date, plan_amt) VALUES ". $str_value. " ON DUPLICATE KEY UPDATE `plan_amt` = VALUES (`plan_amt`)");
            // echo " insert plani query : ". $this->db_sy->last_query(). "\n";
            data_log_message('debug', '---------- to data_sy_v_plani table :'. $this->db_sy->last_query());
        }
    }


    //销售目标抽取
    public function plani_weekday_weekend($arr_plani, $config_file, $data_table)
    {
        $arr_col = $arr_plani['columns'];
        $arr_insert = array();
        $res_shop_code = $arr_col['shop_code'];
        $res_year = $arr_col['plan_date']['year'];
        $res_month = $arr_col['plan_date']['month'];
        $res_day = $arr_col['plan_date']['day'];
        $res_amt = $arr_col['plan_amt'];
        //             $arr_insert['plan_amt'] = $arr_col['plan_amt'];

        $str_join = '';
        if (!empty($arr_plani['join']) && count($arr_plani['join'])) {
            foreach ($arr_plani['join'] as $t => $k) {
                $str_join .= ' LEFT JOIN '. $t. ' ON '. $k;
            }
            //                 $str_join = substr($str_join, 0, strlen($str_join)- strlen('and '));
        }

        //  if(strlen($str_join) == strlen(' LEFT JOIN ')){
        //                 $str_join = '';
        //             }

        $str_where = $arr_plani['where'];

        if (!empty($arr_plani['date_key'])) {
            $str_where .= strlen($str_where)?' and':'';
            $str_where .= $arr_plani['date_key']. " >= '". $this->deal_with_resource_data_from_date. "'";
            if ($this->deal_with_data_with_limited_date) {
                $str_where .= ' and '. $arr_plani['date_key']. " < '". $this->deal_with_resource_data_to_date. "'";
            }
        }
        $str_where = strlen($str_where)?' where '. $str_where:'';
        //             echo "zsiiiii :". $str_where;
        $obj_plan = $this->db_resource->query("SELECT ". $res_shop_code." as shop_code, ". $res_year. " as year_id, ". $res_month. " as mon_id, ". $res_amt. " as plan_amt FROM ". $arr_plani['table']. $str_join. $str_where);
        //echo " get plani query : ". $this->db_resource->last_query(). "\n";
        data_log_message('debug', "---------- get plani ". $this->db_resource->last_query());
        if ($obj_plan->num_rows()) {
            $arr_plan = $obj_plan->result_array();
            $last_on = $this->config->item('v_last_plani_add_on', $config_file);

            foreach ($arr_plan as $plan) {
                $str_value = '';
                $shop_code = $plan['shop_code'];
                $nd_mn = $plan['year_id']. '-'. str_pad($plan['mon_id'], 2, "0", STR_PAD_LEFT);
                $day_count = $this->days_in_month($plan['mon_id'], $plan['year_id']);

                $v_plani_ww = $this->config->item('v_plani_ww', $config_file);
                $arr_pro = $this->proportion_for_everyday_in_mon($plan['mon_id'], $plan['year_id'], $v_plani_ww['wd'], $v_plani_ww['wk']);
                for ($i = 1; $i <= $day_count; $i++) {
                    $plan_date = $nd_mn. '-'. str_pad($i, 2, "0", STR_PAD_LEFT);

                    //echo "plan_date: ". $plan_date. "\n";
                    $plan_amt = $arr_pro[$i-1]*$plan['plan_amt'];
                    $str_value .= "('". $shop_code. "', ". strtotime($plan_date). ", ". $plan_amt. "), ";
                }
                if (strlen($str_value) > 0) {
                    $str_value = substr($str_value, 0, strlen($str_value)-2);
                    $this->db_sy->query("INSERT IGNORE INTO ".$data_table." (shop_code, plan_date, plan_amt) VALUES". $str_value. " ON DUPLICATE KEY UPDATE `plan_amt` = VALUES (`plan_amt`)");
                    //echo "insert query: ". $this->db_sy->last_query(). "\n";
                    $sql_amt_sum = "select sum(plan_amt) as plan_amt from ".$data_table." where shop_code = '". $shop_code. "' and FROM_UNIXTIME(plan_date) >= '". $nd_mn. '-01'. "' and FROM_UNIXTIME(plan_date) <= '". $nd_mn. '-'. $day_count. "'";
                    $obj_sum = $this->db_sy->query($sql_amt_sum);
                    //echo "get query: ". $this->db_sy->last_query(). "\n";
                    $arr_sum = $obj_sum->result_array();
                    $sum = $arr_sum[0]['plan_amt'];

                    //echo "last on date: ". $nd_mn. '-'. str_pad($last_on, 2, "0", STR_PAD_LEFT);
                    $last = $plan['plan_amt'] - $sum;
                    $sql_update = "update ".$data_table." set plan_amt = plan_amt+". $last. " where shop_code = '". $shop_code. "' and plan_date = ". strtotime($nd_mn. '-'. str_pad($last_on, 2, "0", STR_PAD_LEFT));
                    $this->db_sy->query($sql_update);
                    //echo "update query: ". $this->db_sy->last_query(). "\n";
                }
            }
        }
    }


    //销售目标抽取
    public function proportion_for_everyday_in_mon($mon, $year, $wd, $wk)
    {
        $day_count = $this->days_in_month($mon, $year);
        $nd_mn = $year. '-'. str_pad($mon, 2, "0", STR_PAD_LEFT);

        $total_pro = 0;
        $arr_pro = array();
        for ($i = 1; $i <= $day_count; $i++) {
            $plan_date = $nd_mn. '-'. str_pad($i, 2, "0", STR_PAD_LEFT);
            if (date("w", strtotime($plan_date)) == 0 || date("w", strtotime($plan_date)) == 6) {
                array_push($arr_pro, $wk);
                //                        echo 'plan_date: '. $plan_date. ' w: '. date("w",strtotime($plan_date)). ' pro: '. $wk. "\n";
                $total_pro += $wk;
            } else {
                array_push($arr_pro, $wd);
                $total_pro += $wd;
                //                        echo 'plan_date: '. $plan_date. ' w: '. date("w",strtotime($plan_date)). ' pro: '. $wd. "\n";
            }
        }

        $arr_pro_new = array();
        $total_pro_new = 0;
        for ($i = 0; $i < $day_count; $i++) {
            $div = round(bcdiv($arr_pro[$i], $total_pro, 6), 4);
            $arr_pro_new[$i] = $div;
            $total_pro_new += $div;
        }

        return $arr_pro_new;
    }


    //抽数辅助函数，用于计算某个月的天数
    public function days_in_month($month, $year)
    {
        // calculate number of days in a month
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }


    //插入：查询销售目标表
    public function insert_to_searche_plani($str_shop)
    {
        $where = "where plan_date >= ". strtotime($this->deal_with_data_from_date." 00:00:00")." and plan_date < ". strtotime($this->deal_with_data_to_date." 00:00:00");
        if (!empty($str_shop) && strlen($str_shop)) {
            $where .= ' and shop_code '. $str_shop;
        }

        $sql_date = "select distinct(plan_date) from data_sy_v_plani " . $where;
        $obj_date = $this->db_sy->query($sql_date);

        if (!($obj_date->num_rows())) {
            return;
        }
        data_log_message('debug', '---------- plani data by mysql :'. $this->db_sy->last_query());

        $arr_date = $obj_date->result_array();
        foreach ($arr_date as $ad) {
            $sql_update = 'delete from sy_v_plani where plan_date = '. $ad['plan_date'];
            $this->db_sy->query($sql_update);
            $this->db_sy->query("INSERT  INTO sy_v_plani(`shop_code`,`plan_date`,`plan_amt`) (select shop_code, ". $ad['plan_date']. " as plan_date, 0 as plan_amt from sy_v_shop where shop_code not in (select shop_code from data_sy_v_plani where plan_date = ". $ad['plan_date']. "))"); //IGNORE
        }
    }


    //销售统计到data表
    public function genReportData($data_id, $s_day = '', $e_day = '', $arr_table, $str_shop)
    {
        //data_log_message('info', __FUNCTION__ . ' exec start');
        $sql_shop = '';
        if (!empty($str_shop) && strlen($str_shop)) {
            $sql_shop = ' and shop_code '. $str_shop;
        }

        //设置开始时间
        //$this->job_s_date = date('Y-m-d H:i:s', time());
        //重新计算，汇总销售主表的数据
        $update_sum_sales_info = $this->config->item('update_sum_sales_info');
        if (method_exists($this, $update_sum_sales_info)) {
            data_log_message('debug', "---------- customer sales_info method ". $update_sum_sales_info. "\n");
            $this->{$update_sum_sales_info}($s_day, $e_day);
        }

        data_log_message('debug', date('Y-m-d H:i:s')  . " s_day = $s_day ; e_day = $e_day" . "\n");
        data_log_message('debug', date('Y-m-d H:i:s')  . ' start' . "\n");
        data_log_message('debug', date('Y-m-d H:i:s')  . ' user_id = ' .$this->job_user_id . "\n");
        ini_set('max_execution_time', 1800);
        ini_set('memory_limit', '1024M');

        //创建：临时表
        $tempTableDetail = uniqid('std');
        $this->db_sy->query('DROP TABLE IF EXISTS '.$tempTableDetail.';');
        $this->db_sy->query('CREATE TABLE '.$tempTableDetail.' LIKE sales_t_detail;');

        /*
        $sql_detail = 'CREATE TEMPORARY TABLE '.$tempTableDetail.' ( shop_code varchar(200) NOT NULL, sale_date int(10) unsigned NOT NULL, inv_no varchar(20) NOT NULL, item_id varchar(20) NOT NULL, color_id char(6) NOT NULL, size_id char(6) NOT NULL, sale_qtys int(11) NOT NULL default 0, sale_amt decimal(14,2) NOT NULL default 0.00, tag_price decimal(14,2) NOT NULL default 0.00, is_new tinyint(3) unsigned NOT NULL, is_add tinyint(3) unsigned NOT NULL, time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP )';
        $this->db_sy->query($sql_detail);
        */
        //$this->db_sy->query('TRUNCATE TABLE sales_t_detail');

        if (strlen($s_day)) {
            $startDay = $s_day;
        } else {
            $timeValue = 0;

            $query = $this->db_sy->select('id, int_value')
                ->where('type', Gdata_model_v2::REPORT_CONFIG_LAST_TIME_TYPE)
                ->limit(1)
                ->get('sy_v_config');

            if ($query->num_rows() != 1) {
                $initData = array('type' => Gdata_model_v2::REPORT_CONFIG_LAST_TIME_TYPE, 'int_value' => $timeValue);

                $this->db_sy->insert('sy_v_config', $initData);
            } else {
                $rowData = $query->row_array();

                $timeValue = $rowData['int_value'];
            }

            $r_days = $this->config->item('r_days');

            if ($timeValue > 0) {
                $timeValue -= $r_days * 86400;
            }

            $startDay = date('Y-m-d H:i:s', $timeValue);
        }

        if (strlen($e_day)) {
            $endDay = $e_day;
        } else {
            $endDay = date('Y-m-d 0:0:0', time() + 86400);
        }

        $vip_sale = $this->config->item('vip_sale');
        if (method_exists($this, $vip_sale)) {
            log_message('debug', "customer vip_sale method ". $vip_sale. "\n");
            $this->{$vip_sale}($startDay, $endDay);
        }

        $timeValue = strtotime($startDay);
        $day_Value = strtotime(date('Y-m-d 0:0:0', time()));
        $newValue = strtotime($endDay);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //$item_id_index = TRUE;
        //$sale_date_index = TRUE;

        if ($this->job_user_id > 0) {
            //获取：备份表
            $data_sy_v_sales = 'data_sy_v_sales_'.$this->job_user_id;
            $data_sy_v_salesdet = 'data_sy_v_salesdet_'.$this->job_user_id;
        //$data_sales_detail = 'data_sales_detail_'.$this->job_user_id;
        } else {
            //获取：备份表
            $data_sy_v_sales = 'data_sy_v_sales';
            $data_sy_v_salesdet = 'data_sy_v_salesdet';
            //$data_sales_detail = 'data_sales_detail';
        }

        //创建：备份表
        $sql_create_sales = 'CREATE TABLE IF NOT EXISTS '.$data_sy_v_sales.' LIKE sy_v_sales;';
        $this->db_sy->query($sql_create_sales);

        //创建：备份表
        $sql_create_salesdet = 'CREATE TABLE IF NOT EXISTS '.$data_sy_v_salesdet.' LIKE sy_v_salesdet;';
        $this->db_sy->query($sql_create_salesdet);

        //清空：data备份表
        $this->db_sy->query('truncate table '.$data_sy_v_sales.';');
        data_log_message('debug', "---------- to truncate data_sy_v_sales \n");

        //清空：data备份表
        $this->db_sy->query('truncate table '.$data_sy_v_salesdet.';');
        data_log_message('debug', "---------- to truncate data_sy_v_salesdet \n");

        //if ($newValue - $timeValue > 86400 * 90)
        //{
        $query = $this->db_sy->query('SHOW INDEX FROM '.$data_sy_v_salesdet.'  WHERE KEY_NAME = "item_id"');
        if ($query->num_rows() > 0) {
            //$item_id_index = TRUE;
            $this->db_sy->query('ALTER TABLE `'.$data_sy_v_salesdet.'` drop KEY `item_id`');
        }

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        $query = $this->db_sy->query('SHOW INDEX FROM '.$data_sy_v_salesdet.'  WHERE KEY_NAME = "sale_date"');
        if ($query->num_rows() > 0) {
            //$sale_date_index = TRUE;
            $this->db_sy->query('ALTER TABLE `'.$data_sy_v_salesdet.'` drop KEY `sale_date`');
        }
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        $query = $this->db_sy->query('SHOW INDEX FROM sy_v_salesdet  WHERE KEY_NAME = "item_id"');
        if ($query->num_rows() > 0) {
            //$item_id_index = TRUE;
            //$this->db_sy->query('ALTER TABLE `sy_v_salesdet` drop KEY `item_id`');
        } else {
            $this->db_sy->query('ALTER TABLE `sy_v_salesdet` ADD KEY `item_id` (`item_id`,`color_id`,`shop_code`,`sale_date`);');
        }

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        $query = $this->db_sy->query('SHOW INDEX FROM sy_v_salesdet  WHERE KEY_NAME = "sale_date"');
        if ($query->num_rows() > 0) {
            //$sale_date_index = TRUE;
            //$this->db_sy->query('ALTER TABLE `sy_v_salesdet` drop KEY `sale_date`');
        } else {
            $this->db_sy->query('ALTER TABLE `sy_v_salesdet` ADD KEY `sale_date` (`shop_code`,`item_id`,`sale_date`);');
        }
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //}

        //创建临时表
        $tempTableInfo = uniqid('sti');
        $this->db_sy->query('CREATE TABLE IF NOT EXISTS sales_t_info (shop_code varchar(200) not null, vip_state tinyint(3) not null, sale_date_t int(11) not null, staff_id varchar(200) not null, vip_no varchar(200) not null, inv_no varchar(200) not null, sale_amt_t decimal(12,2) not null, rec_amt_t decimal(12,2) not null, arr_amt_t decimal(12,2) not null, sale_sheets int(11) not null, inv_type tinyint(3) not null, pay_type tinyint(3) not null, KEY inv_no (inv_no)) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->db_sy->query('DROP TABLE IF EXISTS '.$tempTableInfo.';');
        $this->db_sy->query('CREATE TABLE '.$tempTableInfo.' LIKE sales_t_info;');
        /*
        $sql_info = 'CREATE TEMPORARY TABLE '.$tempTableInfo.' (shop_code varchar(200) not null, vip_state tinyint not null, sale_date_t int not null, staff_id varchar(200) not null, vip_no varchar(200) not null, inv_no varchar(200) not null, sale_amt_t decimal(12,2) not null, sale_sheets int not null)';
        $this->db_sy->query($sql_info);
        */
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        $sales_t_info_insert_method = $this->config->item('sales_t_info_insert_method');
        data_log_message('debug', "---------- customer sales_t_info method ". $sales_t_info_insert_method. "\n");

        if (method_exists($this, $sales_t_info_insert_method)) {
            data_log_message('debug', "---------- customer sales_t_info method ". $sales_t_info_insert_method. "\n");
            $this->{$sales_t_info_insert_method}($tempTableInfo, $sql_shop, $startDay, $endDay);
        } else {
            //根据sales_info填充sales_t_info，其中sale_sheets强制为1
            $sql0 = 'insert into '.$tempTableInfo.' (shop_code, vip_state, sale_date_t, staff_id, vip_no, inv_no, sale_amt_t, rec_amt_t, arr_amt_t, sale_sheets, inv_type, pay_type) select shop_code, vip_state, UNIX_TIMESTAMP(date(sale_date)) as sale_date_t, staff_id, vip_no, inv_no, inv_money, rec_money, arr_money, 1 as sale_sheets, inv_type, pay_type from sales_info where sale_date >= '."'$startDay'". ' and sale_date < '."'$endDay'".$sql_shop;

            $this->db_sy->query($sql0);
        }

        data_log_message('debug', date('Y-m-d H:i:s') .' ' . $this->db_sy->last_query() . "\n");

        //根据sales_t_info和sales_detail填充表sales_t_detail
        $sql1 = 'insert into '.$tempTableDetail.' (inv_no, shop_code, sale_date, item_id, color_id, size_id, sale_qtys, sale_amt, rec_amt, arr_amt, tag_price) '.
            '(select a.inv_no, a.shop_code, a.sale_date_t, b.item_id, b.color_id, b.size_id, ifnull(sum(b.inv_qtys), 0) as sale_qtys, '.
            'ifnull(sum(b.inv_money), 0) as sale_amt, ifnull(sum(b.rec_money), 0) as rec_amt, ifnull(sum(b.arr_money), 0) as arr_amt, ifnull(sum(b.inv_qtys * b.tag_price), 0) as tag_price from '.$tempTableInfo.' a, sales_detail b '.
            'where a.inv_no = b.inv_no group by a.inv_no, a.shop_code, a.sale_date_t, b.item_id, b.color_id, b.size_id)';

        $this->db_sy->query($sql1);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
        $sales_t_detail_insert_method = $this->config->item('sales_t_detail_insert_method');
        if (method_exists($this, $sales_t_detail_insert_method)) {
            data_log_message('debug', "---------- customer sales_t_info method ". $sales_t_detail_insert_method. "\n");
            $this->{$sales_t_detail_insert_method}();
        } else {
            //根据sy_v_item更新sales_t_detail的是否新货及是否附加
            //$sql2 = 'UPDATE '.$tempTableDetail.' a inner join sy_v_item b on a.item_id = b.item_id set is_new = if ((a.sale_date >= UNIX_TIMESTAMP(b.s_date)) && (a.sale_date < (UNIX_TIMESTAMP(b.e_date) + 86400)), 1, 0), a.is_add = if(b.is_add = 1, 1, 0)';
            $sql2 = 'UPDATE '.$tempTableDetail.' a inner join sy_v_item b on a.item_id = b.item_id set is_new = if ((a.sale_date >= UNIX_TIMESTAMP(b.s_date)) && (a.sale_date < (UNIX_TIMESTAMP(b.e_date) + 86400)), 1, 0), a.is_add = b.is_add ';
            $this->db_sy->query($sql2);
        }

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
        $sy_v_sales_insert_method = $this->config->item('sy_v_sales_insert_method');
        if (method_exists($this, $sy_v_sales_insert_method)) {
            data_log_message('debug', "---------- customer sy_v_sales method ". $sy_v_sales_insert_method. "\n");
            $this->{$sy_v_sales_insert_method}();
        } else {
            //根据sales_t_info填充sy_v_sales
            $sql4 = 'insert into '.$data_sy_v_sales.' (shop_code, sale_date, sale_sheets, vip_sheets) select shop_code, sale_date_t, ifnull(sum(sale_sheets), 0) as sale_sheets, ifnull(sum(if(vip_state = 1, 1, 0)), 0) as vip_sheets from '.$tempTableInfo.' group by shop_code, sale_date_t';
            $this->db_sy->query($sql4);
        }

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        $sy_v_sales_update_method = $this->config->item('sy_v_sales_update_method');
        if (method_exists($this, $sy_v_sales_update_method)) {
            data_log_message('debug', "---------- customer sy_v_sales_update_method ". $sy_v_sales_update_method. "\n");
            $this->{$sy_v_sales_update_method}($data_sy_v_sales, $tempTableDetail, $tempTableInfo);
        } else {
            //根据sales_t_detail和sales_t_info更新sy_v_sales的总吊牌价、总新货吊牌价、总新货金额、附加数量
            /*
                2020-07-16 品类属性ic_prop新定义状态如下，is_add 是否计算数量、金额、附加，is_acc 是否配件 Y是 N否，更新逻辑如下：
                1. 配件：计算销售数量，计算销售金额，计算销售附加。  (对应sy_v_item  is_add=1  ,is_acc='Y')
                2. 正品：计算销售数量，计算销售金额，计算销售附加。  (对应sy_v_item  is_add=1  ,is_acc='N')
                3. 其他：不计算销售数量，不计算销售金额，不计算附加。(对应sy_v_item  is_add=2  ,is_acc='Y')
                4. 特殊：计算销售数量，计算销售金额，不算销售附加。  (对应sy_v_item  is_add=3  ,is_acc='Y')
             * */
            //$sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(b.sale_qtys), 0) as sale_qtys, ifnull(sum(b.tag_price), 0) as tag_amt, ifnull(sum(if(b.is_new = 0, 0, b.tag_price)), 0) as tag_amt2, ifnull(sum(if(b.is_new = 0, 0, b.sale_amt)), 0) as new_amt, ifnull(sum(if(b.is_add = 0, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c where b.inv_no = c.inv_no group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
            //$sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(if(b.is_add = 2, 0, b.sale_qtys)), 0) as sale_qtys, ifnull(sum(b.tag_price), 0) as tag_amt, ifnull(sum(if(b.is_new = 0, 0, b.tag_price)), 0) as tag_amt2, ifnull(sum(if(b.is_add = 2, 0, b.sale_amt)), 0) as sale_amt, ifnull(sum(if(b.is_new = 0, 0, b.sale_amt)), 0) as new_amt, ifnull(sum(if(b.is_add > 1, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c where b.inv_no = c.inv_no group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.sale_amt = d.sale_amt, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
            //, ifnull(sum(case when b.is_new = 0 and b.is_add = 2 then 0 else b.sale_amt end), 0) as new_amt
            //, ifnull(sum(case when b.is_new = 0 and b.is_add = 2 then 0 else b.tag_price end), 0) as tag_amt2
            //$sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(if(b.is_add = 2, 0, b.sale_qtys)), 0) as sale_qtys, ifnull(sum(if(b.is_add = 2, 0, b.tag_price)), 0) as tag_amt, ifnull(sum(if(b.is_new = 0, 0, b.tag_price)), 0) as tag_amt2, ifnull(sum(if(b.is_add = 2, 0, b.sale_amt)), 0) as sale_amt, ifnull(sum(if(b.is_new = 0, 0, b.sale_amt)), 0) as new_amt, ifnull(sum(if(b.is_add > 1, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c where b.inv_no = c.inv_no group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.sale_amt = d.sale_amt, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
            //$sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(if(b.is_add = 2, 0, b.sale_qtys)), 0) as sale_qtys, ifnull(sum(if(b.is_add = 2, 0, b.tag_price)), 0) as tag_amt, ifnull(sum(case when b.is_new = 0 and b.is_add = 2 then 0 else b.tag_price end), 0) as tag_amt2, ifnull(sum(if(b.is_add = 2, 0, b.sale_amt)), 0) as sale_amt, ifnull(sum(case when b.is_new = 0 and b.is_add = 2 then 0 else b.sale_amt end), 0) as new_amt, ifnull(sum(if(b.is_add > 1, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c where b.inv_no = c.inv_no group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.sale_amt = d.sale_amt, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
            $sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(if(b.is_add = 1, b.sale_qtys, 0)), 0) as sale_qtys, ifnull(sum(if(b.is_add = 2, 0, b.tag_price)), 0) as tag_amt, ifnull(sum(case when b.is_new = 1 and b.is_add = 1 then b.tag_price else 0 end), 0) as tag_amt2, ifnull(sum(if(b.is_add = 1, b.sale_amt, 0)), 0) as sale_amt, ifnull(sum(case when b.is_new = 1 and b.is_add = 1 then b.sale_amt else 0 end), 0) as new_amt, ifnull(sum(if(b.is_add > 1, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c where b.inv_no = c.inv_no group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.sale_amt = d.sale_amt, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
            $this->db_sy->query($sql6);
            data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

            //根据sales_t_info更新sy_v_sales的销售金额、VIP金额 rec_amt arr_amt
            //$sql61 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date_t as sale_date, ifnull(sum(b.sale_amt_t), 0) as sale_amt , ifnull(sum(b.rec_amt_t), 0) as rec_amt , ifnull(sum(b.arr_amt_t), 0) as arr_amt ,ifnull(sum(if(b.vip_state != 1, 0, b.sale_amt_t)), 0) as vip_amt from '.$tempTableInfo.' b group by b.shop_code, b.sale_date_t) c set a.sale_amt = c.sale_amt , a.rec_amt = c.rec_amt , a.arr_amt = c.arr_amt , a.vip_amt = c.vip_amt where a.shop_code = c.shop_code and a.sale_date = c.sale_date';
            $sql61 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date_t as sale_date , ifnull(sum(b.rec_amt_t), 0) as rec_amt , ifnull(sum(b.arr_amt_t), 0) as arr_amt ,ifnull(sum(if(b.vip_state != 1, 0, b.sale_amt_t)), 0) as vip_amt from '.$tempTableInfo.' b group by b.shop_code, b.sale_date_t) c set a.rec_amt = c.rec_amt , a.arr_amt = c.arr_amt , a.vip_amt = c.vip_amt where a.shop_code = c.shop_code and a.sale_date = c.sale_date';

            $this->db_sy->query($sql61);

            data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
        }


        //创建：临时表
        $tempTableVip = uniqid('svv');
        $this->db_sy->query('DROP TABLE IF EXISTS '.$tempTableVip.';');
        $this->db_sy->query('CREATE TABLE '.$tempTableVip.' LIKE sy_v_vipi;');
        /*
        $sql_vip = 'CREATE TEMPORARY TABLE '.$tempTableVip.' ( `vip_no` varchar(100) NOT NULL, `vip_level` varchar(20) NOT NULL, `shop_code` varchar(200) NOT NULL, `register_date` int(10) unsigned NOT NULL, `cust_sex` tinyint(3) unsigned NOT NULL, `born_date` date NOT NULL, `mobile` varchar(20) NOT NULL, `address` varchar(200) NOT NULL, `staff_id` varchar(100) NOT NULL) ';
        $this->db_sy->query($sql_vip);
        */
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //根据sy_v_vip填充sy_v_vipi
        $syncvip_sql = 'insert into '.$tempTableVip.' (vip_no, vip_level, shop_code, register_date, cust_sex, born_date, staff_id) select vip_no, vip_level, shop_code, UNIX_TIMESTAMP(DATE(register_date)) as sale_date_t, cust_sex, born_date, staff_id FROM sy_v_vip where vip_state>0 and register_date >= '."'$startDay'".$sql_shop. ' and register_date < '."'$endDay'".' ON DUPLICATE KEY UPDATE register_date=values(register_date)';

        $this->db_sy->query($syncvip_sql);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //根据sy_v_vipi插入或更新sy_v_sales
        $sql7 = 'insert into '.$data_sy_v_sales.' (shop_code, sale_date, new_vips) select * from (select shop_code, register_date, count(*) as new_vips from '.$tempTableVip.' where register_date >= '.$timeValue.' group by shop_code, register_date) b ON DUPLICATE KEY UPDATE '.$data_sy_v_sales.'.new_vips = b.new_vips';

        $this->db_sy->query($sql7);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //根据sales_t_detail填充sy_v_salesdet
        $sy_v_salesdet_update_method = $this->config->item('sy_v_salesdet_update_method');
        if (method_exists($this, $sy_v_salesdet_update_method)) {
            data_log_message('debug', "---------- customer sy_v_salesdet_update_method ". $sy_v_salesdet_update_method. "\n");
            $this->{$sy_v_salesdet_update_method}($data_sy_v_salesdet, $tempTableDetail, $tempTableInfo);
        } else {

            //根据sales_t_detail填充sy_v_salesdet
            $sql8 = 'INSERT INTO '.$data_sy_v_salesdet.' (shop_code,sale_date,item_id,color_id,size_id,sale_qtys,sale_amt,rec_amt,arr_amt, tag_amt) SELECT shop_code,sale_date,item_id,color_id,size_id,sum(sale_qtys) as sale_qtys,sum(sale_amt) as sale_amt,sum(rec_amt) as rec_amt,sum(arr_amt) as arr_amt, sum(tag_price) as tag_amt from '.$tempTableDetail.' group by shop_code,sale_date,item_id,color_id,size_id';

            $this->db_sy->query($sql8);
        }
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        if (!strlen($e_day)) {
            $this->db_sy->where('type', Gdata_model_v2::REPORT_CONFIG_LAST_TIME_TYPE);
            $this->db_sy->update('sy_v_config', array('int_value' => $day_Value));
        }
        // data_log_message('debug', date ('Y-m-d H:i:s') . "\n" ."\n");
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //data表不需要外健索引
        //$this->db_sy->query('ALTER TABLE `data_sy_v_salesdet` ADD KEY `item_id` (`item_id`,`color_id`,`shop_code`,`sale_date`);');
        //$this->db_sy->query('ALTER TABLE `data_sy_v_salesdet` ADD KEY `sale_date` (`shop_code`,`item_id`,`sale_date`);');
        //查询表外健不用删除
        //$this->db_sy->query('ALTER TABLE `sy_v_salesdet` ADD KEY `item_id` (`item_id`,`color_id`,`shop_code`,`sale_date`);');
        //$this->db_sy->query('ALTER TABLE `sy_v_salesdet` ADD KEY `sale_date` (`shop_code`,`item_id`,`sale_date`);');

        data_log_message('debug', date('Y-m-d H:i:s')  . ' end' . "\n");

        //data_log_message('info', __FUNCTION__ . ' exec finished');

        //其他计算
        $this->genReportOtherData($this->cid, $data_sy_v_sales, $tempTableInfo, $sql_shop);


        //删除：临时表
        $this->db_sy->query('drop table '.$tempTableInfo);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //删除：临时表
        $this->db_sy->query('drop table '. $tempTableDetail);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //删除：临时表
        $this->db_sy->query('drop table '. $tempTableVip);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, 6, 3, '数据计算结束', array());


        foreach ($arr_table as $table) {
            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 6, 4, $table.'数据同步开始', array());

            //获取：备份表
            if ($this->job_user_id > 0) {
                //获取：备份表
                $data_table = 'data_'.$table.'_'.$this->job_user_id;
            } else {
                //获取：备份表
                $data_table = 'data_'.$table;
            }

            if ($table == 'sy_v_sales') {
                $this->db_sy->where(array('sale_date >=' => $timeValue, 'sale_date < ' => $newValue));
                $this->db_sy->delete('sy_v_sales');
            }
            if ($table == 'sy_v_salesdet') {
                $this->db_sy->where(array('sale_date >=' => $timeValue, 'sale_date < ' => $newValue));
                $this->db_sy->delete('sy_v_salesdet');
            }
            data_log_message('debug', "---------- delete ".$table." from $timeValue to $newValue \n");

            //同步：数据到查询表
            $this->insert_data_to_search_total_table($data_id, $data_table, $table);

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, 6, 6, $table.'数据同步结束', array());
        }
    }


    //销售"开单数"，只计算收款类型（1全款 2订金），退货"开单数"减1，如客户先买再退，开单数=0，其余不计入销售单数
    public function insert_into_sales_t_info_mjs($tempTableInfo, $sql_shop, $startDay, $endDay)
    {
        //inv_type	tinyint(4)	单据类型（1销售 2退货）pay_type	tinyint(4)	收款类型（1全款 2订金 3余款）
        $sql = "insert into ".$tempTableInfo." (shop_code, vip_state, sale_date_t, staff_id, vip_no, inv_no, sale_amt_t,  rec_amt_t, arr_amt_t, sale_sheets, inv_type, pay_type) select * from (select shop_code, vip_state, UNIX_TIMESTAMP(date(sale_date)) as sale_date_t, staff_id, vip_no, inv_no, inv_money, rec_money, arr_money, 1 as sale_sheets, inv_type, pay_type from sales_info where sale_date >= '$startDay' and sale_date < '$endDay' and inv_type = 1 and pay_type < 3 ".$sql_shop."
union all
select shop_code, vip_state, UNIX_TIMESTAMP(date(sale_date)) as sale_date_t, staff_id, vip_no, inv_no, inv_money, rec_money, arr_money, -1 as sale_sheets, inv_type, pay_type from sales_info where sale_date >= '$startDay' and sale_date < '$endDay' and inv_type = 2 ".$sql_shop."
union all
select shop_code, vip_state, UNIX_TIMESTAMP(date(sale_date)) as sale_date_t, staff_id, vip_no, inv_no, inv_money, rec_money, arr_money, 0 as sale_sheets, inv_type, pay_type from sales_info where sale_date >= '$startDay' and sale_date < '$endDay' and inv_type = 1 and pay_type = 3 ".$sql_shop." ) a
";
        $this->db_sy->query($sql);
    }


    //品类标记为其它的，不能计算销售附加，不能计算销售数量。还有，取货单，就是不计算里面的货品数量的。
    public function sy_v_sales_update_method_mjs($data_sy_v_sales, $tempTableDetail, $tempTableInfo)
    {
        //        $sql1 = 'insert into '.$tempTableDetail.' (inv_no, shop_code, sale_date, item_id, color_id, size_id, sale_qtys, sale_amt, rec_amt, arr_amt, tag_price) '.
        //根据sales_t_detail和sales_t_info更新sy_v_sales的总吊牌价、总新货吊牌价、总新货金额、附加数量   取货单，不计算里面的货品数量的。and cc.ic_prop != 3
        //首款：要加到销售金额里，也要加到应收金额里
        $sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(b.sale_qtys), 0) as sale_qtys, ifnull(sum(b.tag_price), 0) as tag_amt, ifnull(sum(if(b.is_new = 0, 0, b.tag_price)), 0) as tag_amt2, ifnull(sum(if(b.is_new = 0, 0, b.sale_amt)), 0) as new_amt, ifnull(sum(if(b.is_add = 0, 0, b.sale_qtys)), 0) as add_qtys, ifnull(sum(b.sale_amt), 0) as sale_amt , ifnull(sum(b.rec_amt), 0) as rec_amt , ifnull(sum(b.arr_amt), 0) as arr_amt ,ifnull(sum(if(c.vip_state = 0, 0, b.sale_amt)), 0) as vip_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and c.pay_type != 3 and cc.ic_prop != 3 group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys, a.sale_amt = d.sale_amt , a.rec_amt = d.rec_amt , a.arr_amt = d.arr_amt , a.vip_amt = d.vip_amt where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
        $this->db_sy->query($sql6);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' mjs6 ' .$this->db_sy->last_query() . "\n");

        //尾款：要加到销售金额里，不能加到应收金额里
        $sql6_1 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(b.sale_amt), 0) as sale_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and cc.ic_prop != 3 group by c.shop_code, c.sale_date_t) d set a.sale_amt = d.sale_amt where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
        $this->db_sy->query($sql6_1);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' mjs61 ' .$this->db_sy->last_query() . "\n");

        /*
        //根据sales_t_detail和sales_t_info更新sy_v_sales的总吊牌价、总新货吊牌价、总新货金额、附加数量   取货单，不计算里面的货品数量的。and cc.ic_prop != 3
        $sql6 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date, ifnull(sum(b.sale_qtys), 0) as sale_qtys, ifnull(sum(b.tag_price), 0) as tag_amt, ifnull(sum(if(b.is_new = 0, 0, b.tag_price)), 0) as tag_amt2, ifnull(sum(if(b.is_new = 0, 0, b.sale_amt)), 0) as new_amt, ifnull(sum(if(b.is_add = 0, 0, b.sale_qtys)), 0) as add_qtys from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and c.pay_type != 3 group by c.shop_code, c.sale_date_t) d set a.sale_qtys = d.sale_qtys, a.tag_amt = d.tag_amt, a.tag_amt2 = d.tag_amt2, a.new_amt = d.new_amt, a.add_qtys = d.add_qtys where a.shop_code = d.shop_code and a.sale_date = d.sale_date';
        $this->db_sy->query($sql6);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");

        //根据sales_t_info更新sy_v_sales的销售金额、VIP金额 rec_amt arr_amt
        $sql61 = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date_t as sale_date, ifnull(sum(b.rec_amt_t) - sum(b.arr_amt_t), 0) as sale_amt , ifnull(sum(b.rec_amt_t), 0) as rec_amt , ifnull(sum(b.arr_amt_t), 0) as arr_amt ,ifnull(sum(if(b.vip_state = 0, 0, b.sale_amt_t)), 0) as vip_amt from '.$tempTableInfo.' b where b.pay_type != 3 group by b.shop_code, b.sale_date_t) c set a.sale_amt = c.sale_amt , a.rec_amt = c.rec_amt , a.arr_amt = c.arr_amt , a.vip_amt = c.vip_amt where a.shop_code = c.shop_code and a.sale_date = c.sale_date';

        $this->db_sy->query($sql61);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
        */
    }


    //品类标记为其它的，不能计算销售附加，不能计算销售数量。还有，取货单，就是不计算里面的货品数量的。
    public function sy_v_salesdet_update_method_mjs($data_sy_v_salesdet, $tempTableDetail, $tempTableInfo)
    {

        //根据sales_t_detail填充sy_v_salesdet     and c.pay_type != 3 and cc.ic_prop != 3
        //首款：要加到销售金额里，也要加到应收金额里；先汇总所有金额
        $sql8 = 'INSERT INTO '.$data_sy_v_salesdet.' (shop_code,sale_date,item_id,color_id,size_id,sale_qtys,sale_amt,rec_amt,arr_amt, tag_amt) SELECT b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id, sum(b.sale_qtys) as sale_qtys, ifnull(sum(b.sale_amt), 0) as sale_amt, sum(b.rec_amt) as rec_amt, sum(b.arr_amt) as arr_amt, sum(b.tag_price) as tag_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and cc.ic_prop != 3 group by b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id';
        $this->db_sy->query($sql8);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' mjs8 ' .$this->db_sy->last_query() . "\n");

        //尾款：要加到销售金额里，不能加到应收金额里；再汇总应收金额、销售数量
        $sql8_1 = 'update '.$data_sy_v_salesdet.' as a, (select b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id, ifnull(sum(b.sale_qtys), 0) as sale_qtys, ifnull(sum(b.rec_amt), 0) as rec_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and c.pay_type != 3 and cc.ic_prop != 3 group by b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id) d set a.sale_qtys = d.sale_qtys, a.rec_amt = d.rec_amt  where a.shop_code = d.shop_code and a.sale_date = d.sale_date and a.item_id = d.item_id and a.color_id = d.color_id and a.size_id = d.size_id';
        $this->db_sy->query($sql8_1);
        data_log_message('debug', date('Y-m-d H:i:s')  . ' mjs81 ' .$this->db_sy->last_query() . "\n");


        //根据sales_t_detail填充sy_v_salesdet
        //$sql8_1 = 'update '.$data_sy_v_salesdet.' as a, (select b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id, sum(b.rec_amt) as rec_amt, sum(b.arr_amt) as arr_amt, ifnull(sum(b.rec_amt) - sum(b.arr_amt), 0) as sale_amt from '.$tempTableDetail.' b group by b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id) c set a.rec_amt = c.rec_amt, a.arr_amt = c.arr_amt, a.sale_amt = c.sale_amt where a.shop_code = c.shop_code and a.sale_date = c.sale_date and a.item_id = c.item_id and a.color_id = c.color_id and a.size_id = c.size_id';  and c.pay_type != 3 and cc.ic_prop != 3

        //$sql8_1 = 'update '.$data_sy_v_salesdet.' as a, (select b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id, sum(b.rec_amt) as rec_amt, sum(b.arr_amt) as arr_amt, ifnull(sum(b.rec_amt) - sum(b.arr_amt), 0) as sale_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and c.pay_type != 3 group by b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id) d set a.rec_amt = d.rec_amt, a.arr_amt = d.arr_amt, a.sale_amt = d.sale_amt where a.shop_code = d.shop_code and a.sale_date = d.sale_date and a.item_id = d.item_id and a.color_id = d.color_id and a.size_id = d.size_id';

        //$this->db_sy->query($sql8_1);

        //$sql8_2 = 'INSERT IGNORE INTO '.$data_sy_v_salesdet.' (shop_code,sale_date,item_id,color_id,size_id,sale_qtys,sale_amt,rec_amt,arr_amt, tag_amt) SELECT shop_code,sale_date,item_id,color_id,size_id, 0, ifnull(sum(rec_amt) - sum(arr_amt), 0) as sale_amt,sum(rec_amt) as rec_amt,sum(arr_amt) as arr_amt, 0 from '.$tempTableDetail.' group by shop_code,sale_date,item_id,color_id,size_id';  and c.pay_type != 3 and cc.ic_prop != 3

        //$sql8_2 = 'INSERT IGNORE INTO '.$data_sy_v_salesdet.' (shop_code,sale_date,item_id,color_id,size_id,sale_qtys,sale_amt,rec_amt,arr_amt, tag_amt) SELECT b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id, sum(b.sale_qtys) as sale_qtys, ifnull(sum(b.rec_amt) - sum(b.arr_amt), 0) as sale_amt, sum(b.rec_amt) as rec_amt, sum(b.arr_amt) as arr_amt, sum(b.tag_price) as tag_amt from '.$tempTableDetail.' b, '.$tempTableInfo.' c , sy_v_item i, sy_v_category cc where b.inv_no = c.inv_no and b.item_id = i.item_id and i.ic_id = cc.ic_id and c.pay_type != 3 group by b.shop_code, b.sale_date, b.item_id, b.color_id, b.size_id';

        //$this->db_sy->query($sql8_2);
    }


    //其他计算
    public function genReportOtherData($cid, $data_sy_v_sales, $tempTableInfo, $sql_shop)
    {
        //销售额（已收） = 应收 - 未收
        if ($cid == 15) {
            //$sql_saleamt = 'update '.$data_sy_v_sales.' as a, (select b.shop_code, b.sale_date_t as sale_date, ifnull(sum(b.rec_amt_t) - sum(b.arr_amt_t), 0) as sale_amt from '.$tempTableInfo.' b group by b.shop_code, b.sale_date_t) c set a.sale_amt = c.sale_amt where a.shop_code = c.shop_code and a.sale_date = c.sale_date';

            //$this->db_sy->query($sql_saleamt);
            //data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
        }
    }


    //同步：数据到查询表
    public function insert_data_to_search_total_table($data_id, $data_table, $table)
    {
        $this->insertCronDataJobInfo($data_id, 6, 5, $table.'数据同步进行中', array());

        if ($table == 'sy_v_sales') {
            $arr_update = array('sale_qtys', 'sale_amt', 'tag_amt', 'new_amt', 'tag_amt2', 'add_qtys', 'sale_sheets', 'new_vips', 'vip_sheets', 'vip_amt', 'rec_amt', 'arr_amt');
        } else {
            $arr_update = array('sale_qtys', 'sale_amt', 'tag_amt', 'rec_amt', 'arr_amt');
        }

        $str_update = '';
        foreach ($arr_update as $item) {
            $str_update .= '`'. $item. '` = VALUES(`'. $item. '`), ';
        }
        $str_update = substr($str_update, 0, strlen($str_update)-2);

        // 'insert ignore into sy_v_barcode1 select * from sy_v_barcode  ON DUPLICATE KEY UPDATE `item_id` = VALUES(`item_id`), `color_id` = VALUES(`color_id`), `size_id` = VALUES(`size_id`), `color_name` = VALUES(`color_name`)'
        //SELECT * FROM `sy_v_category` WHERE `time_stamp` >= '2019-04-10 00:00:00' AND `time_stamp` <= '2019-04-10 23:59:59' ORDER BY `ic_id` DESC
        //$this->db_sy->query("INSERT IGNORE INTO ". $table. " (select * from ". $data_table. " WHERE time_stamp >= '".$s_day."' AND time_stamp <= '".$e_day."' "." and sale_date >= ". strtotime($this->deal_with_data_from_date.' 00:00:00'). " and sale_date < ". strtotime($this->deal_with_data_to_date.' 00:00:00'). " ) ON DUPLICATE KEY UPDATE ". $str_update);
        $this->db_sy->query("INSERT IGNORE INTO ". $table. " (select * from ". $data_table. " WHERE sale_date >= ". strtotime($this->deal_with_data_from_date.' 00:00:00'). " and sale_date < ". strtotime($this->deal_with_data_to_date.' 00:00:00'). " ) ON DUPLICATE KEY UPDATE ". $str_update);

        data_log_message('debug', date('Y-m-d H:i:s')  . ' ' .$this->db_sy->last_query() . "\n");
    }


    //库存全清 销售单及明细仅部分清
    public function delete_searchtable($table, $str_shop = '')
    {
        if ($table == 'sy_v_stock' ||  $table == 'sy_v_instock' || $table == 'sy_v_vip') {
            if ($str_shop == '') {
                //所有先清空表，再插入新数据。
                $this->db_sy->truncate($table);
            } else {
                $where = 'shop_code '. $str_shop;
                $this->db_sy->where($where)->delete($table);
            }
        } elseif ($table == 'sales_info' || $table == 'sales_detail' || $table == 'sales_info2' || $table == 'sales_detail2' || $table == 'sy_v_plani' || $table == 'sy_v_staff_sales' || $table == 'sy_v_shopstock' || $table == 'sy_v_senddata') {

            //销售目标及明细，先删除查询时间范围内的数据，再插入新数据。
            $where = "";
            if ($table == 'sy_v_plani') {
                $where = "plan_date >= ". strtotime($this->deal_with_data_from_date." 00:00:00")." and plan_date < ". strtotime($this->deal_with_data_to_date." 00:00:00");
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
            } elseif ($table == 'sales_info' || $table == 'sales_info2' || $table == 'sy_v_staff_sales') {
                $where = "sale_date >= '". $this->deal_with_data_from_date. " 00:00:00' and sale_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
            } elseif ($table == 'sy_v_shopstock') {
                $where = "stock_date >= '". $this->deal_with_data_from_date. " 00:00:00' and stock_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
            } elseif ($table == 'sy_v_senddata') {
                $where = "send_date >= '". $this->deal_with_data_from_date. " 00:00:00' and send_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
            } elseif ($table == 'sales_detail') {
                //$where = "inv_no in (SELECT inv_no FROM `data_sales_info` where sale_date >= '". $this->deal_with_data_from_date. " 00:00:00' and sale_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                $where = "inv_no in (SELECT inv_no FROM `sales_info` where sale_date >= '". $this->deal_with_data_from_date. " 00:00:00' and sale_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
                $where .= ")";
            } elseif ($table == 'sales_detail2') {
                //$where = "inv_no in (SELECT inv_no FROM `data_sales_info2` where sale_date >= '". $this->deal_with_data_from_date. " 00:00:00' and sale_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                $where = "inv_no in (SELECT inv_no FROM `sales_info2` where sale_date >= '". $this->deal_with_data_from_date. " 00:00:00' and sale_date < '". $this->deal_with_data_to_date. " 00:00:00'";
                if (!empty($str_shop) && strlen($str_shop)) {
                    $where .= ' and shop_code '. $str_shop;
                }
                $where .= ")";
            }

            $this->db_sy->where($where)->delete($table);

        }

        data_log_message('debug', '---------- delete search data by mysql :'. $this->db_sy->last_query());
    }


    /*
     * 任务前执行的sql
    $config['query_before_sync_to_1'] = '';
    $config['query_before_sync_to_2'] = '';
    $config['query_before_sync_to_3'] = '';
    $config['query_before_sync_to_4'] = '';
    $config['query_before_sync_to_5'] = '';
    $config['query_before_sync_to_6'] = '';
    $config['query_before_sync_to_7'] = '';
    $config['query_before_sync_to_8'] = '';
     * */
    public function queryBeforeData($type = 0)
    {
        if (in_array($type, array(1, 2, 3, 4, 5, 6, 7, 8))) {
            $key = 'query_before_sync_to_'.$type;
            $sql = $this->config->item($key);
            data_log_message('debug', "---------- queryBeforeData key: ".$key." , sql: ".$sql);

            if ($sql) {
                $this->db_sy->query($sql);
                data_log_message('debug', '---------- queryBeforeData by mysql :'. $this->db_sy->last_query());
            }
        }
    }


    /*
     * 任务后执行的sql
    $config['query_after_sync_to_1'] = '';
    $config['query_after_sync_to_2'] = '';
    $config['query_after_sync_to_3'] = '';
    $config['query_after_sync_to_4'] = '';
    $config['query_after_sync_to_5'] = '';
    $config['query_after_sync_to_6'] = '';
    $config['query_after_sync_to_7'] = '';
    $config['query_after_sync_to_8'] = '';
     * */
    public function queryAfterData($type = 0)
    {
        if (in_array($type, array(1, 2, 3, 4, 5, 6, 7, 8))) {
            $key = 'query_after_sync_to_'.$type;
            $sql = $this->config->item($key);
            data_log_message('debug', "---------- queryAfterData key: ".$key." , sql: ".$sql);

            if ($sql) {
                $this->db_sy->query($sql);
                data_log_message('debug', '---------- queryAfterData by mysql :'. $this->db_sy->last_query());
            }
        }
    }


    /*
    是否计入VIP销售：
    销售日期小于注册日期，不计入
    销售日期等于注册日期，第一单，不计入
    销售日期等于注册日期，第一单除外部分，计入
    销售日期大于注册日期，计入
    */
    public function vip_sale_1_2($startDay, $endDay)
    {
        if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
            log_message('error', __FUNCTION__. 'error date range -- June');
            echo __FUNCTION__. "error date range -- June \n";
            return;
        }

        $this->vip_sale_1($startDay, $endDay);
        $this->vip_sale_2($startDay, $endDay);
    }


    //销售日期小于注册日期，不计入
    /*       public function vip_sale_1($startDay, $endDay)
           {
               if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
                   log_message('error', __FUNCTION__. 'error date range -- June');
                   echo __FUNCTION__. "error date range -- June \n";
                   return;
               }

               $sql_inv = "select i.inv_no from (
                               select * from sales_info where vip_no is not null and vip_no <> '' and sale_date >= '". $startDay. "' and sale_date < '". $endDay. "'
                           ) i
                           left join (
                               select * from sy_v_vip where register_date >= '". $startDay. "' and register_date < '". $endDay. "'
                           ) v on i.vip_no = v.vip_no
                           where register_date is not null
                           and date_format(v.register_date,'%Y-%m-%d') > sale_date";
               // $obj = $this->db_sy->query($sql_inv);
               // if ($obj->num_rows()) {
               // 	$arr_inv = array();
               // 	$arr = $obj->result_array();
               // 	foreach ($arr as $key => $value) {
               // 		array_push($arr_inv, $value['inv_no']);
               // 	}
               // 	log_message('debug', 'vip_sale_1 invs '. print_r($arr_inv, true));
               // }

               $sql_vip_sale = "update sales_info set vip_state = 0
                           where inv_no in ($sql_inv)";
               $this->db_sy->query($sql_vip_sale);
               log_message('debug', __FUNCTION__. ' query '. $this->db_sy->last_query());
           }
   */
    public function vip_sale_1($startDay, $endDay)
    {
        if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
            log_message('error', __FUNCTION__. 'error date range -- June');
            echo __FUNCTION__. "error date range -- June \n";
            return;
        }

        $sql_vip_sale = "update  sales_info	a,sy_v_vip b set a.vip_state = 0 
			            where a.sale_date >= '". $startDay. "' and a.sale_date < '". $endDay. "'
    					and a.vip_no=b.vip_no and date_format(b.register_date,'%Y-%m-%d')> a.sale_date";
        $this->db_sy->query($sql_vip_sale);
        log_message('debug', __FUNCTION__. ' query '. $this->db_sy->last_query());
    }


    /*
    是否计入VIP销售：
    销售日期等于注册日期，第一单，不计入
    销售日期等于注册日期，第一单除外部分，计入
    */
    /*        public function vip_sale_2($startDay, $endDay)
            {
                if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
                    log_message('error', __FUNCTION__. 'error date range -- June');
                    echo __FUNCTION__. "error date range -- June \n";
                    return;
                }

                $sql_inv = "select min(i.inv_no) as inv_no from (
                                select * from sales_info where vip_no is not null and vip_no <> '' and sale_date >= '". $startDay. "' and sale_date < '". $endDay. "'
                            ) i
                            left join (
                                select * from sy_v_vip where register_date >= '". $startDay. "' and register_date < '". $endDay. "'
                            ) v on i.vip_no = v.vip_no and date_format(v.register_date,'%Y-%m-%d') = sale_date
                            where register_date is not null
                            group by i.vip_no ";
                // $obj = $this->db_sy->query($sql_inv);
                // if ($obj->num_rows()) {
                // 	$arr_inv = array();
                // 	$arr = $obj->result_array();
                // 	foreach ($arr as $key => $value) {
                // 		array_push($arr_inv, $value['inv_no']);
                // 	}
                // 	log_message('debug', 'vip_sale_2 invs '. print_r($arr_inv, true));
                // }

                $sql_vip_sale = "update sales_info set vip_state = 0
                                where inv_no in ($sql_inv)";
                $this->db_sy->query($sql_vip_sale);
                log_message('debug', __FUNCTION__. ' query '. $this->db_sy->last_query());
            }
    */

    public function vip_sale_2($startDay, $endDay)
    {
        if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
            log_message('error', __FUNCTION__. 'error date range -- June');
            echo __FUNCTION__. "error date range -- June \n";
            return;
        }

        $sql_vip_sale = "update sales_info c,
			                (select min(a.inv_no) as inv_no from  sales_info a,sy_v_vip b
							where a.sale_date >= '". $startDay. "' and a.sale_date < '". $endDay. "'
							and a.vip_no=b.vip_no and date_format(b.register_date,'%Y-%m-%d')= a.sale_date
							group by a.vip_no) d
							set c.vip_state=0
    						where c.inv_no =d.inv_no";
        $this->db_sy->query($sql_vip_sale);
        log_message('debug', __FUNCTION__. ' query '. $this->db_sy->last_query());
    }

    /*
    销售日期小于等于注册日期，不计入VIP销售
    */
    /*        public function vip_sale_3($startDay, $endDay)
            {
                if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
                log_message('error', __FUNCTION__. 'error date range -- June');
                echo __FUNCTION__. "error date range -- June \n";
                return;
            }

            $sql_inv = "select i.inv_no from (
                            select * from sales_info where vip_no is not null and vip_no <> '' and sale_date >= '". $startDay. "' and sale_date < '". $endDay. "'
                        ) i
                        left join (
                            select * from sy_v_vip where register_date >= '". $startDay. "' and register_date < '". $endDay. "'
                        ) v on i.vip_no = v.vip_no
                        where register_date is not null
                        and date_format(v.register_date,'%Y-%m-%d') >= sale_date";
             $obj = $this->db_sy->query($sql_inv);
             if ($obj->num_rows()){

                 $arr_inv = array();
                 $arr = $obj->result_array();
                 foreach ($arr as $key => $value) {
                     //array_push($arr_inv, $value['inv_no']);
                 //log_message('debug', 'vip_sale_2 invs '. print_r($arr_inv, true));
    $sql_vip_sale = "update sales_info set vip_state = 0
                            where inv_no in ('". $value['inv_no']. "')";
            $this->db_sy->query($sql_vip_sale);
                 }
             }

    //        $sql_vip_sale = "update sales_info set vip_state = 0
    //						where inv_no in ($sql_inv)";
    //        $this->db_sy->query($sql_vip_sale);
    //        log_message('debug', __FUNCTION__. ' query '. $this->db_sy->last_query());
            }
    */

    public function vip_sale_3($startDay, $endDay)
    {
        if (empty($startDay) || !strlen($startDay) || empty($endDay) || !strlen($endDay)) {
            log_message('error', __FUNCTION__. 'error date range -- June');
            echo __FUNCTION__. "error date range -- June \n";
            return;
        }

        $sql_vip_sale = "update  sales_info	a,sy_v_vip b set a.vip_state = 0 
			            where a.sale_date >= '". $startDay. "' and a.sale_date < '". $endDay. "'
    					and a.vip_no=b.vip_no and date_format(b.register_date,'%Y-%m-%d')>= a.sale_date";
        $this->db_sy->query($sql_vip_sale);


    }


    /*
     获取本地店铺总数
     */
    private function get_total_shop_count(){
        $obj_shops = $this->db_sy->select('count(*) as total')->where("shop_code <> ''")->get('sy_v_shop');
        $arr_shops = $obj_shops->result_array();
        $total_count = $arr_shops[0]['total'];
        //         log_message('debug', 'shop total count :'. $total_count);
        //         return 100;
        return $total_count;
    }


    /*
     获取本地店铺代号
     先将本地店铺表中的数据按店铺代号排序，然后根据s及l取指定数目的店铺代号，以逗号串联返回
     */
    public function get_shop_code_in_search($s, $l){
        $obj_shops = $this->db_sy->query("SELECT GROUP_CONCAT(shop_code SEPARATOR ',') as shops from (SELECT * FROM `sy_v_shop` where shop_code <> '' order by shop_code limit ". $s. ",". $l. ") a");
        $arr_shops = $obj_shops->result_array();
        $shops = $arr_shops[0]['shops'];
        //         log_message('debug', 'get_shop_code_in_sy :'. $shops);
        return $shops;
    }


    public function insert_into_datatable_by_shop($data_id, $type, $table, $r_config, $config_file, $str_shop, $is_delete = 1)
    {
        data_log_message('debug', date('Y-m-d H:i:s')  . ' user_id = ' .$this->job_user_id . "\n");

        //获取：备份表
        $data_table = 'data_'.$table;

        //创建：备份表
        $sql_create = 'CREATE TABLE IF NOT EXISTS '.$data_table.' LIKE '.$table.';';
        $this->db_sy->query($sql_create);

        if ($this->job_user_id > 0) {
            //获取：备份表
            $data_table_user = 'data_'.$table.'_'.$this->job_user_id;
            //创建：备份表
            $sql_create_user = 'CREATE TABLE IF NOT EXISTS '.$data_table_user.' LIKE '.$data_table.';';
            $this->db_sy->query($sql_create_user);

            $data_table = $data_table_user;
        }

        //if($is_delete == 1){ //多库多表情况下，必须清空data表，不然会数据重复
        //删除：数据
        $this->delete_datatable($data_table, $config_file, $str_shop);

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 2, $data_table.'表数据抽取进行中，删除数据结束', array());
        //}
        //插入：销售目标
        if ($table == 'sy_v_plani') {
            $this->insert_into_plani($data_id, $config_file, $str_shop, $is_delete);
            return;
        }

        $str_where = (empty($r_config['where'])||strlen($r_config['where'])==0)?'':$r_config['where'];

        if ($str_shop == '') {
            //全部店铺
            $total_count = $this->get_total_shop_count();
            if($total_count > 0){
                $l = $this->config->item('shop_count', $config_file);    //每一次抽取的店铺个数
                $r = $total_count/$l + 1;
                $t = ($total_count%$l? $r + 1 : $r);
                $c = 0;
                $s = 0;    //数据开始位置
                while($c <= $t){
                    $shops = $this->get_shop_code_in_search($s, $l);
                    if(!strlen($shops)) break;

                    $str_shop = " in ('". str_replace(",", "','", $shops). "') ";;
                    data_log_message('debug', 'shops '. $str_shop);

                    $this->insert_into_datatable_by_shop_detail($table, $config_file, $str_shop, $data_table);

                    $s = ($c + 1)*$l;
                    $c++;
                }
                $str_shop = '';
            }
        } else {
            //分店抽取
            if (isset($r_config['shop_code']) && $r_config['shop_code']) {
                $shop_code = $r_config['shop_code'];
                $str_where .= strlen($str_where)?' and ':'';
                $str_where .= $shop_code. $str_shop;
            }

            $this->insert_into_datatable_by_shop_detail($table, $config_file, $str_shop, $data_table);
        }

        //处理：备份表数据
        $this->update_datatable($table, $data_table, $config_file, $str_shop);

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 2, $data_table.'表数据抽取进行中，处理数据结束', array());

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 3, $data_table.'表数据抽取结束', array());

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 4, $table.'表数据同步开始', array());

        if ($is_delete == 1) {
            //删除：查询表数据
            $this->delete_searchtable($table, $str_shop);

            //插入：定时任务表
            $this->insertCronDataJobInfo($data_id, $type, 5, $table.'表数据同步进行中，删除数据结束', array());
        }

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 5, $table.'表数据同步进行中', array());

        //同步：数据到查询表
        $this->insert_data_to_searchtable($data_table, $table, $config_file, $str_shop);

        //插入：定时任务表
        $this->insertCronDataJobInfo($data_id, $type, 6, $table.'表数据同步结束', array());

    }


    private function insert_into_datatable_by_shop_detail($table, $config_file, $shops = '', $data_table){
        if(!strlen($shops)) return;
        $r_config = $this->config->item($table, $config_file);
        $arr_col = $r_config['columns'];
        $str_where = (empty($r_config['where'])||strlen($r_config['where'])==0)?'':$r_config['where'];
        $str_where .= strlen($str_where)?' and ':'';
        $shop_code = isset($arr_col['shop_code'])?$arr_col['shop_code']:$r_config['shop_code'];

        //$str_where_shop = " in ('". str_replace(",", "','", $shops). "') ";
        $str_where .= $shop_code. $shops;
        if($r_config['date_key'] && strlen($r_config['date_key']) > 0){
            $str_where .= ' and '. $r_config['date_key']. " >= '". $this->deal_with_data_from_date. "' and ". $r_config['date_key']. " < '". $this->deal_with_data_to_date. "'";
        }

        $arr_columns = array();
        foreach($r_config['columns'] as $sy => $res){
            array_push($arr_columns, $res. ' as '. $sy);
        }
        $str_query = ' SELECT '. implode(', ', $arr_columns). ' FROM '. $r_config['table'];
        if(count($r_config['join'])){
            foreach($r_config['join'] as $join_table => $on){
                $str_query .= ' LEFT JOIN '. $join_table. ' ON '. $on;
            }
        }

        if(strlen($str_where)){
            $str_query .= ' WHERE '. $str_where;
        }
        if(!empty($r_config['group_by'])){
            $str_query .= ' GROUP BY '. (is_array($r_config['group_by'])?implode(', ', $r_config['group_by']):$r_config['group_by']);
        }

        if(!empty($r_config['having'])){
            $str_query .= ' HAVING '. $r_config['having'];
        }
        log_message('debug', "get ". $str_query);
        $obj_get = $this->db_resource->query($str_query);
        $arr_data = array();
        if($obj_get->num_rows()){
            $arr_data = $obj_get->result_array();
        }

        if(count($arr_data)){

            /* if($this->config->item('delete_sale_first')){

                if($table == 'sales_info'){
                    $this->delete_sale_info($str_where_shop);
                }
                if($table == 'sales_detail'){
                    $this->delete_sale_detail($str_where_shop);
                }
            } */


            $this->db_sy->insert_batch($data_table, $arr_data, 1);
            //                         log_message('debug', 'insert_batch by shop query :'. $this->db_sy->last_query());
            //$this->after_insert_into_temp($table, $config_file);

        }
    }


    //根据sy_v_sea更新商品新品日期
    public function update_item_date_by_sea_table($config_file, $data_table)
    {
        data_log_message('debug', 'update_item_date_by_sea_table config_file :'. $config_file);
        $sql_change = "select year_id, i.sea_id, s_mon, e_mon, concat(if(span_offset<0, year_id+span_offset, year_id), '-', LPAD(s_mon,2,'0'), '-01') as s, LAST_DAY(concat(if(span_offset>0, year_id+span_offset, year_id), '-', LPAD(e_mon,2,'0'), '-01')) as e from sy_v_item i left join sy_v_sea s on s.sea_id = i.sea_id where i.sea_id is not null and i.sea_id <> '' and year_id > 0 GROUP BY year_id, i.sea_id";//and s_date = '0000-00-00' and e_date = '0000-00-00'

        $sql_item = "update `".$data_table."` a left join (". $sql_change. ")b on a.sea_id = b.sea_id and a.year_id = b.year_id set s_date = s, e_date = e ";

        $this->db_sy->query($sql_item);
        log_message('debug', 'update_item_date_by_sea_table query :'. $this->db_sy->last_query());
    }






}
