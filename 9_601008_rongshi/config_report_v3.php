<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['database_resource'] = array(
	'all' => array(
		'report_resource' => 'config_report_ipos_v3',
		'resource_stock' => 'config_report_stock'
	),
	'report_resource' => array('report_resource' => 'config_report_ipos_v3'),
	'stock' => array('resource_stock' => 'config_report_stock')
);

$config['delete_sale_first'] = true;

$config['base_admin_ip'] = '120.25.60.138';
// $config['base_relative_path'] = 'syweb/api/';
$config['base_admin_port'] = 80;

//建模服务器到app服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_base'] = true;

//建模服务器到app服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_base'] = array(
	'sy_shop' => 1,		//店铺
	'sy_brand' => 1,	//品牌
 	'sy_spclass' => 1,	//品类
	'sy_item' => 1,		//单品
	'sy_itemsize' => 1,	//尺码
	'sy_itemcolor' => 1,//颜色
	'sy_sizeitem' => 1,	//单品尺码
	'sy_sitem' => 1,	//款色
	'sy_group' => 1,	//分组
	'sy_shopgroup' => 1	//店铺分组
);

$config['com_no'] = '601008';//零售链授权号
$config['lsl_admin_ip'] = 'lsla.sunyunsoft.cn';
$config['lsl_admin_port'] = 80;
//建模服务器到零售链服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_lsl'] = true;
//建模服务器到零售链服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_lsl'] = array(
// 	'lsla_title' => 0,
// 	'lsla_dept' => 0,
	'lsla_groupshop' => 1,
	'lsla_shop' => 1,
	'lsla_group' => 1,
 	'lsla_category' => 0,
	'lsla_item' => 1,
	'lsla_color' => 1,
	'lsla_size' => 1,
	'lsla_itemcolor' => 1,
// 	'lsla_config' => 0,
// 	'lsla_user' => 0,
// 	'lsla_user_leader' => 0,
	'lsla_sea' => 1
);


$config['update_item_date'] = 'update_item_date_by_sea_table';
$config['update_sum_sales_info'] = 'update_sum_sales_info';

$group_id = '9999';
$group_name = '未分组';

$config['query_after_sync_to_lsl'] = "insert into `lsla_group`(`group_code`, `group_name`, `group_level`, `parent_group_code`, `group_state`, `group_order_seq`) values ('$group_id', '$group_name', '1', '$group_id', '1', '999');
delete from lsla_groupshop where group_code = '$group_id';
insert into lsla_groupshop(`shop_code`, `group_code`, `gs_order_seq`) (
select shop_code, '$group_id' as group_code, '999' as gs_order_seq from lsla_shop
where shop_code not in (
select shop_code from lsla_groupshop
)
)";

    /*vip销售计算，
    vip_sale_1 为销售日期小于注册日期不计入。
    vip_sale_2 销售日期等于注册日期，第一单，不计入；销售日期等于注册日期，第一单除外部分，计入
    vip_sale_1_2 综合以上两种
    vip_sale_3 为销售日期小于等于注册日期不计入。
    */
    $config['vip_sale'] = 'vip_sale_3';

/* End of file config.php */
/* Location: ./application/config/config.php */
