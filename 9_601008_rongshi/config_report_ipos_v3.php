<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
$config['cid'] = 9;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;
$config['shop_count'] = 10;
$config['update_item_date'] = 'update_item_date';

//大类
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
	'table' => '',
	'columns' => array(
		'class_id' => "' '",
		'class_name' => "' '",
		'class_code' => "' '",
		'class_prop' => "' '",
		'class_state' => "'1'",
		'class_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('class_id'),
	'duplicate' => array('class_name'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

//品类
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
	'table' => 'com_base_dalei',
	'columns' => array(
		'ic_id' => "dldm",
		'ic_name' => "dlmc",
		'ic_code' => "dldm",
		'class_id' => "' '",
		'ic_prop' => "'2'",
		'ic_order' => "'999'"
	),
	'join' => array(),
	'where' => "dldm <> '000' and char_length(dldm) = 3",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('ic_id'),
	'duplicate' => array('ic_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//小小类
$sy_v_smallcategory = 'sy_v_smallcategory';
$config[$sy_v_smallcategory] = array(
	'table' => '',
	'columns' => array(
		'sc_id' => "' '",
		'sc_name' => "' '",
		'sc_code' => "' '",
		'sc_prop' => "'2'",
		'sc_state' => '1',
		'ic_id' => "' '",
		'sc_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sc_id'),
	'duplicate' => array('sc_name'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
	'table' => 'com_base_guige2',
	'columns' => array(
		'size_group' => 'ggwz1',
		'size_id' => "ggdm",
		'size_desc' => "ggmc",
		'size_order' => "ggwz2"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('size_id', 'size_group'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
	'table' => 'com_base_pinpai',
	'columns' => array(
// 		'id' => 'id',
		'brand_id' => "ppdm",
		'brand_name' => "ppmc",
		'sort_order' => "999",
		'active' => '1'
	),
	'join' => array(),
	'where' => "`ppdm` <> '000'",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('brand_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
	'table' => 'com_base_tmdzb',
	'columns' => array(
		'bar_no' => "com_base_tmdzb.sptm",
		'item_id' => "com_base_tmdzb.spdm",
		'color_id' => "com_base_tmdzb.gg1dm",
		'color_name' => "com_base_guige1.ggmc",
		'size_id' => "com_base_guige2.ggmc"
	),
	'join' => array(
		'com_base_guige1' => 'com_base_guige1.id = com_base_tmdzb.gg1_id',
		'com_base_guige2' => 'com_base_guige2.id = com_base_tmdzb.gg2_id'),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('bar_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
	'table' => 'com_base_guige1',
	'columns' => array(
		'color_id' => "ggdm",
		'color_desc' => "ggmc"
	),
	'join' => array(),
	'where' => '',
	'group_by' => '',
	'date_key' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
	'table' => 'com_base_spgg2',
	'columns' => array(
		'item_id' => "spdm",
		'size_group' => "com_base_guige2.ggwz1"
	),
	'join' => array(
		'com_base_guige2' => 'com_base_guige2.ggdm = com_base_spgg2.ggdm'),
	'where' => "",
	'date_key' => '',
	'group_by' => array('spdm'),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
	'table' => 'com_base_spgg1',
	'columns' => array(
		'item_id' => "com_base_spgg1.spdm",
		'color_id' => "ggdm",//gg_id
		'item_picture' => "concat(com_base_spgg1.spdm, '-', ggdm, '.jpg')"
	),
	'join' => array(
		'com_base_shangpin' => 'com_base_shangpin.spdm = com_base_spgg1.spdm'),
	'where' => "sx9dm in ('002', '004', '005') and com_base_shangpin.dldm <> '000' and char_length(com_base_shangpin.dldm) = 3",
	'date_key' => '',
	'group_by' => array(),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
	'table' => 'com_base_fjsx9',
	'columns' => array(
		'sea_id' => 'sxdm',
		'sea_name' => "sxmc",
		'sea_erp_code' => 'sxdm',
		'sea_erp_name' => "sxmc",
		'sea_order' => 'sxdm',
		's_mon' => '(case sxdm when 001 then 03 when 002 then 03 when 003 then 09 when 004 then 09 when 005 then 01 else 01 end)',
		'e_mon' => '(case sxdm when 001 then 05 when 002 then 08 when 003 then 11 when 004 then 02 when 005 then 12 else 12 end)',
		'is_span_sea' => '(case sxdm when 004 then 1 else 0 end)',
		'span_offset' => '(case sxdm when 004 then 1 else 0 end)'
	),
	'join' => array(),
	'where' => "",
	'date_key' => '',
	'group_by' => array(),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sea_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//商品资料
$config['sea_span'] ="'004'";    	//跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');	//跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(		//季度具体时间段
// 	"'01'" => array('from' => '02', 'to' => '04'),
	"'002'" => array('from' => '03', 'to' => '08'),
	"'005'" => array('from' => '01', 'to' => '12'),
	"'004'" => array('from' => '09', 'to' => '02')
);
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
	'table' => 'com_base_shangpin',
	'columns' => array(
		'item_id' => "spdm",
		'brand_id' => "''",
		'is_add' => '1',
		'is_included' => "' '",
		'year_id' => "nddm",
		'sea_id' => 'sx9dm',
		'sea_name' => "com_base_fjsx9.sxmc",
		'cost' => "0",
		'sub_icid' => "' '",
		'wave_no' => "' '",
		'class_id' => "' '",
		'class_name' => "' '",
		'ic_id' => 'com_base_shangpin.dldm',
		'ic_name' => 'com_base_dalei.dlmc',
		'sc_id' => "' '",
		'sc_name' => "' '",
		'is_acc' => "'N'",
		'sale_price' => "sj2",
		'item_desc' => "spmc",
		'sales_point' => "' '",
		'item_file' => "concat(spdm, '.jpg')",
		'info_url' => "' '",
		'style_id' => "' '",
		's_date' => "''",
		'e_date' => "''"
	),
	'join' => array(
		'com_base_fjsx9' => 'com_base_fjsx9.sxdm = com_base_shangpin.sx9dm',
		'com_base_dalei' => 'com_base_dalei.dldm = com_base_shangpin.dldm'),
	'where' => "",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id'),
	'duplicate' => array('year_id', 'sea_id', 'sea_name', 'ic_id', 'ic_name', 'sale_price', 'item_desc', 's_date', 'e_date'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
	'table' => 'com_base_kehu',
	'columns' => array(
		'shop_code' => "khdm",
		'shop_name' => "khmc",
		'model_no' => "' '",
		'ref_shopcode' => "' '",
		'jm_code' => "' '",
		'shop_level' => "' '",
		'branch_id' => "' '",
		'open_date' => "IF(jdrq is null, '', FROM_UNIXTIME(jdrq))",
		'close_date' => "' '",
		'shop_area' => "dpmj",
		'stand_staffs' => "0",
		'real_staff' => "ygrs",
		'max_sku' => "0",
		'min_sku' => "0",
		'max_qtys' => "0",
		'min_qtys' => "0",
		'tel_no' => "phone",
		'addr' => "IF(dz1 is null, '', dz1)",
		'contact' => "' '"
	),
	'join' => array(),
	'where' => '`xzdm` in (4,5)',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code'),
	'duplicate' => array('shop_name', 'tel_no', 'addr', 'shop_area', 'open_date', 'real_staff'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sales_info = 'sales_info';
$config[$sales_info] = array(
	'table' => 'ipos_qtlsd',
	'columns' => array(
		'inv_no' => "djbh",
		'shop_code' => "zddm",
		'sale_date' => 'from_unixtime(yyrq, "%Y-%m-%d %H:%i:%s")',
		'vip_state' => "IF(vpdm = '', 0, 1)",
		'staff_id' => "IF(dgy_list_dm is null, '', SUBSTRING_INDEX(dgy_list_dm,rtrim(','),1))",
		'vip_no' => "vpdm",
		'inv_qtys' => '0',
		'inv_money' => "je",
		'tag_money' => "0"
	),
	'join' => array(),
	'where' => 'gd = 0 and zf = 0',
	'date_key' => 'from_unixtime(yyrq, "%Y-%m-%d %H:%i:%s")',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
	'table' => 'ipos_qtlsdmx',
	'columns' => array(
		'inv_no' => 'djbh',
		'item_id' => 'spdm',
		'color_id' => 'gg1dm',
		'size_id' => 'com_base_guige2.ggmc',
		'inv_qtys' => 'ipos_qtlsdmx.sl',
		'inv_money' => 'ipos_qtlsdmx.je',
		'tag_price' => 'ckj'
	),
	'join' => array('ipos_qtlsd' => 'ipos_qtlsd.id = ipos_qtlsdmx.pid', 'com_base_guige2' => 'com_base_guige2.ggdm = ipos_qtlsdmx.gg2dm'),
	'where' => 'ipos_qtlsd.gd = 0 and ipos_qtlsd.zf = 0 ',//and ipos_qtlsdmx.je <> 0',
	'date_key' => 'from_unixtime(ipos_qtlsd.yyrq, "%Y-%m-%d %H:%i:%s")',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => 'ipos_qtlsd.zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//销售目标
$plani_type_one_month_a_line = 'plani_with_type1';	//数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';		//数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';	//数据源表中一行记录代表的时间为某个时间段的情况
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
	'table' => 'ipos_rizbdmx',
	'columns' => array(
		'shop_code' => 'zddm',
		'plan_date' => array(
			'year' => 'nd',
			'month' => 'mn',
			'day' => array('je1', 'je2', 'je3', 'je4', 'je5', 'je6', 'je7', 'je8', 'je9', 'je10', 'je11', 'je12', 'je13', 'je14', 'je15', 'je16', 'je17', 'je18', 'je19', 'je20', 'je21', 'je22', 'je23', 'je24', 'je25', 'je26', 'je27', 'je28', 'je29', 'je30', 'je31')
			)
	),
	'join' => array(
		'ipos_zdjbb' => 'ipos_zdjbb.pid = ipos_rizbdmx.zd_id',
		'ipos_rizbd' => 'ipos_rizbd.id = ipos_rizbdmx.pid'),
	'where' => 'nd > year(now()) or (nd = year(now()) and mn >= month(now())-1) or (nd = year(DATE_ADD(now(),INTERVAL -1 month)) and mn = month(DATE_ADD(now(),INTERVAL -1 month)))',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code', 'plan_date'),
	'duplicate' => '',//array('plan_amt'),
	'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
	'type' => $plani_type_one_month_a_line
);

$sy_v_shiftsales = 'sy_v_shiftsales';
$config[$sy_v_shiftsales] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => '',
		'new_amt' => '',
		'tag_amt2' => '',
		'add_qtys' => '',
		'sale_sheets' => '',
		'new_vips' => '',
		'vip_sheets' => '',
		'vip_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shiftsalesdet = 'sy_v_shiftsalesdet';
$config[$sy_v_shiftsalesdet] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'item_id' => '',
		'color_id' => '',
		'size_id' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
	'table' => 'ipos_spkcb',
	'columns' => array(
		'shop_code' => 'zddm',
		'item_id' => 'spdm',
		'color_id' => 'gg1dm',
		'size_id' => 'gg2mc',
		'stock_qtys' => 'sum(sl)'
	),
	'join' => array('ipos_zdjbb' => 'ipos_zdjbb.pid = ipos_spkcb.zd_id'),
	'where' => 'zddm is not null and sl <> 0',
	'group_by' => array('zddm', 'spdm', 'gg1dm', 'gg2mc'),
	'date_key' => "",
	'by_shop' => true,
	'shop_code' => 'zddm',
	'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shoparea = 'sy_v_shoparea';
$config[$sy_v_shoparea] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'class_id' => '',
		'sa_area' => ""
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'class_id'),
	'is_multi' => true
);

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
	'table' => 'ipos_vip',
	'columns' => array(
		'vip_no' => "ipos_vip.vpdm",
		'cust_name' => "ipos_vip.vpmc",
		'shop_code' => "ipos_zdjbb.zddm",
		'cust_sex' => "ipos_customer.xb+1",
		'age' => "0",
		'born_date' => 'if(from_unixtime(ipos_customer.sr1, "%Y-%m-%d") is null, "", from_unixtime(ipos_customer.sr1, "%Y-%m-%d"))',			//格式转换，暂时需要加上
		'register_date' => 'from_unixtime(ipos_vip.qyrq, "%Y-%m-%d %H:%i:%S")',		//格式转换，暂时需要加上
		'vip_level' => "ipos_viplb.lbdm",
		'mobile' => "ipos_customer.sj",
		'address' => "ipos_customer.dz",
		'staff_id' => "ifnull(ipos_dianyuan.dydm,0)",
		'vip_state' => "(CASE WHEN ipos_vip.status = 1 THEN 1 ELSE 2 END)",
		'active_money' => '0',
		'vip_points'=>'ipos_vip.dqjf'
	),
	'join' => array(
		'ipos_customer' => 'ipos_customer.gkdm = ipos_vip.gkdm',
		'ipos_viplb' => 'ipos_viplb.id = ipos_vip.lb_id',
		'ipos_zdjbb' => 'ipos_zdjbb.pid = ipos_vip.zd_id',
		'ipos_dianyuan' => 'ipos_dianyuan.id = ipos_vip.jsdy'),
	'where' => "",
	'shop_code' => 'ipos_zdjbb.zddm',
	'date_key' => '',
	'sy_key' => array('vip_no'),
	'by_shop' => true,
	'by_limit' => false,
	'is_multi' => true,
);

$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => 'ipos_zdjhdmx',
	'columns' => array(
	    'inv_no' => 'ipos_zdjhd.djbh',
		'shop_code' => 'trim(ipos_zdjhd.zddm)',
		'stock_date' => 'from_unixtime(ipos_zdjhd.qrrq, "%Y-%m-%d %H:%i:%S")',
		'item_id' => 'ipos_zdjhdmx.spdm',
		'color_id' => 'ipos_zdjhdmx.gg1dm',
		'size_id' => 'ipos_zdjhdmx.gg2mc',
		'stock_qtys' => 'sum(ipos_zdjhdmx.sl)'
	),
	'join' => array('ipos_zdjhd' => 'ipos_zdjhd.id=ipos_zdjhdmx.dj_id'),
	'where' => "zddm is not null and ipos_zdjhdmx.sl > 0 and ipos_zdjhd.qr='1'",
	'group_by' => array('trim(ipos_zdjhd.zddm)', 'ipos_zdjhdmx.spdm', 'ipos_zdjhdmx.gg1dm', 'ipos_zdjhdmx.gg2mc'),
	'date_key' => 'from_unixtime(ipos_zdjhd.qrrq, "%Y-%m-%d %H:%i:%S")',
	'by_shop' => true,
	'shop_code' => 'zddm',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 在途库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_instock
 * */
$sy_v_instock = 'sy_v_instock';
$config[$sy_v_instock] = array(
    'table' => 'ipos_zdjhdmx',
    'columns' => array(
        'shop_code' => 'ipos_zdjhd.zddm',
        'item_id' => "ipos_zdjhdmx.spdm",
        'color_id' => "ipos_zdjhdmx.gg1dm",
        'size_id' => "ipos_zdjhdmx.gg2mc ",
        'instock_qtys' => 'sum(ipos_zdjhdmx.sl)'
    ),
    'join' => array('ipos_zdjhd' => 'ipos_zdjhd.id=ipos_zdjhdmx.dj_id'),
    'where' => "ipos_zdjhd.qr='0'",
    'group_by' => "ipos_zdjhd.zddm,ipos_zdjhdmx.spdm,ipos_zdjhdmx.gg1dm,ipos_zdjhdmx.gg2mc",
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/* End of file config.php */
/* Location: ./application/config/config.php */
