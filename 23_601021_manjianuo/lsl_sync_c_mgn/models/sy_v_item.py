'''
Author: philipdeng
Date: 2021-03-15 23:08:04
LastEditors: philipdeng
LastEditTime: 2021-03-29 19:23:05
Description: file content
'''

from sqlalchemy import Column, DECIMAL, Date, Integer, SmallInteger, TIMESTAMP, text,VARCHAR,CHAR, func
import sqlalchemy.exc as sqlalchemy_exc

from models.base import BaseMixin
from libs.read_conf import ReadConf
from libs.dynamic_db import DbConnect




class SyVItem(BaseMixin):
    __tablename__ = 'sy_v_item'
    __table_args__ = {'comment': '商品款式详情表'}

    item_id = Column(VARCHAR(20), primary_key=True, comment='商品货号')
    item_key = Column(VARCHAR(200), comment='助记码')
    brand_id = Column(VARCHAR(20), nullable=False, comment='品牌代码')
    is_add = Column(Integer, nullable=False, server_default=text("'1'"), comment='是否销售附加')
    is_included = Column(Integer, nullable=False, server_default=text("'1'"))
    year_id = Column(SmallInteger, nullable=False, comment='年份')
    sea_id = Column(VARCHAR(200), nullable=False, comment='季节代码')
    sea_name = Column(VARCHAR(200), nullable=False, comment='季节名称')
    cost = Column(DECIMAL(12, 2), nullable=False, comment='成本')
    sub_icid = Column(CHAR(6), nullable=False)
    wave_no = Column(CHAR(3), nullable=False)
    ic_id = Column(VARCHAR(200), nullable=False, comment='品类代码')
    ic_name = Column(VARCHAR(200), nullable=False, comment='品类名称')
    class_id = Column(VARCHAR(40), nullable=False, comment='大类id')
    class_name = Column(VARCHAR(40), nullable=False, comment='大类名称')
    sc_id = Column(VARCHAR(40), nullable=False, comment='小小类id')
    sc_name = Column(VARCHAR(40), nullable=False, comment='小小类名称')
    is_acc = Column(VARCHAR(8), nullable=False, server_default=text("'N'"), comment='是否配件')
    sale_price = Column(DECIMAL(18, 2), nullable=False, comment='销售单价')
    item_desc = Column(VARCHAR(128), nullable=False, comment='货品描述')
    sales_point = Column(VARCHAR(8192), nullable=False, comment='货品买点')
    item_file = Column(VARCHAR(256), nullable=False, comment='图片路径')
    info_url = Column(VARCHAR(256), nullable=False, comment='页面链接')
    style_id = Column(VARCHAR(6))
    s_date = Column(Date, nullable=False, comment='销售开始日期')
    e_date = Column(Date, nullable=False, comment='销售结束日期')
    pub_date = Column(Integer, nullable=False, server_default=text("'0'"), comment='商品发布日期')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


    @staticmethod
    def get_item(page_index,page_size):

        db_session = DbConnect()
        session = db_session.set_session()
        config = ReadConf().read_conf()
        # page_size = config.getint("DB_SET","PIC_PAGE")
        page_size = page_size
        item_list = session.query(SyVItem.item_id).filter().order_by(SyVItem.item_id)\
        .limit(page_size).offset((page_index-1)*page_size).all()
        session.close()

        return item_list

    @staticmethod
    def update_pic_url(pic_url_list):
        db_session = DbConnect()
        session = db_session.set_session()
        table_name = SyVItem.__tablename__ 
        table_comment = SyVItem.__table_args__.get('comment')
        if pic_url_list:
            update_count = len(pic_url_list)
            try:
                session.bulk_update_mappings(SyVItem,pic_url_list)
                session.commit()
            except (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
                session.rollback()
                session.close()
                log_txt = f'发生数据库操作错误！{table_comment}[{table_name}]更新失败！{e}'
                print(f'发生数据库操作错误！{table_comment}[{table_name}]更新失败！{e}')
                return {'log_txt':log_txt, 'update_count':0}
            else:
                session.close()
                log_txt = f'{table_comment}[{table_name}]资料更新成功！'
                return {'log_txt':log_txt, 'update_count':update_count}

    @staticmethod
    def get_row():
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            rows = session.query(SyVItem).count()
        except (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            return None
        else:
            session.close()
            return rows