<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
$config['cid'] = 14;
$config['resource_data_date_type'] = 2; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['plan_select_days'] = '2'; //1为默认，2为当月
$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';


/*
 * 条码资料
 * select sy_v_barcode.bao_no,sy_v_barcode.item_id,sy_v_barcode.color_id,sy_v_barcode.color_name,sy_v_size.size_desc as size_id
 from sy_v_barcode left join sy_v_size on sy_v_barcode.size_id=sy_v_size.size_id
 left join sy_v_itemsize on sy_v_size.size_group=sy_v_itemsize.size_group
 where ROWNUM<100000
 * 上面的语句，产生重复数据
 * select sy_v_barcode.bao_no,sy_v_barcode.item_id,sy_v_barcode.color_id,sy_v_barcode.color_name,sy_v_size.size_desc as size_id from sy_v_barcode
  left join sy_v_itemsize on sy_v_barcode.item_id=sy_v_itemsize.item_id
 left join sy_v_size on sy_v_itemsize.size_group=sy_v_size.size_group where sy_v_barcode.size_id = sy_v_size.size_id
 */
$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
    'table' => 'sy_v_barcode',
    'columns' => array(
        'bar_no' => 'sy_v_barcode.bao_no',	//有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
        'item_id' => 'sy_v_barcode.item_id',
        'color_id' => 'sy_v_barcode.color_id',
        'size_id' => 'sy_v_size.size_desc',
        'color_name' => 'sy_v_barcode.color_name'
    ),
    'join' => array(
        //'sy_v_size' => 'sy_v_barcode.size_id=sy_v_size.size_id',
        //'sy_v_itemsize' => 'sy_v_size.size_group=sy_v_itemsize.size_group',
        'sy_v_itemsize' => 'sy_v_barcode.item_id=sy_v_itemsize.item_id',
        'sy_v_size' => 'sy_v_itemsize.size_group=sy_v_size.size_group',
    ),
    'where' => 'sy_v_barcode.size_id = sy_v_size.size_id',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('bar_no'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 品类资料
 * select ic_id,ic_name from sy_v_category
 */
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
    'table' => 'sy_v_category',
    'columns' => array(
        'ic_id' => 'ic_id',
        'ic_name' => "ic_name",
        'class_id' => 'NVL(class_id, 0)',
        'ic_prop' => 'ic_prop', //品类属性：1.配件；2.正品；3.其他
        'ic_order' => '1'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('ic_id'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('ic_name', 'class_id', 'ic_prop'),
    //'another_resource' => 'category_qt'	//同库其它数据源
);


/*
 * 大类资料
 * SELECT * FROM sy_v_item_class
 */
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
    'table' => 'sy_v_item_class',
    'columns' => array(
        'class_id' => 'class_id',
        'class_name' => 'class_name',
        'class_order' => 'class_order',
        'class_state' => "'1'",
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('class_id'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('class_name', 'class_order'),
    //'another_resource' => 'category_qt'	//同库其它数据源
);


/*
 * 尺码资料
 * select size_group,size_id,size_desc,ic_order from sy_v_size
 */
$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
    'table' => 'sy_v_size',
    'columns' => array(
        'size_group' => 'size_group',
        'size_id' => 'size_id',
        'size_desc' => 'size_desc',
        'size_order' => 'ic_order'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('size_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 颜色资料
 * select VALUE, NAME from sy_v_color
 */
$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
    'table' => 'sy_v_color',
    'columns' => array(
        'color_id' => 'VALUE',
        'color_desc' => 'NAME'
    ),
    'join' => array(),
    'where' => '',
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 季节资料
 * select sea_id,sea_name, (case sy_v_sea.sea_name
				when '春装' then '02'
				when '夏装' then '05'
				when '秋装' then '08'
				when '冬装' then '11'
				when '年货' then '01'
				else '01' end
			) as s_mon, (case sy_v_sea.sea_name
				when '春装' then '04'
				when '夏装' then '07'
				when '秋装' then '10'
				when '冬装' then '01'
				when '年货' then '03'
				else '12' end
			) as e_mon, (case sy_v_sea.sea_name
			when '秋春' then '1'
			when '秋冬' then '1'
			when '冬装' then '1'
			when '冬季' then '1'
			when '年货' then '0'
			else '0' end) as is_span_sea from sy_v_sea
 */
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'sy_v_sea',//M_PRODUCT
    'columns' => array(
        'sea_id'=> 'sea_id',
        'sea_name' =>'sea_name',//char_length
        'sea_desc'=>'sea_name',
        's_mon' => "(case sy_v_sea.sea_id
				when '1' then '02'
				when '2' then '05'
				when '3' then '08'
				when '4' then '11'
				else '01' end
			)",
        'e_mon' => "(case sy_v_sea.sea_id
				when '1' then '04'
				when '2' then '07'
				when '3' then '10'
				when '4' then '01'
				else '12' end
			)",
        'is_span_sea' => "(case sy_v_sea.sea_id
			when '4' then '1'
			else '0' end)",
        'span_offset' => "(case sy_v_sea.sea_id
            when '4' then '1'
            else '0' end)",
        'sea_order' => 'sea_id'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    //'group_by' => array(''),
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('sea_id'),
    'duplicate' => array('s_mon', 'e_mon', 'is_span_sea', 'span_offset'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);
//商品资料
$config['sea_span'] ="'4' ";    	//跨年季度id or sea_id = '5' or sea_id = '6' or sea_id = '7'
$config['year_offset'] = array('s' => 0, 'e' => '1');	//跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(		//季度具体时间段
    "'1'" => array('from' => '02', 'to' => '04'),
    "'2'" => array('from' => '05', 'to' => '07'),
    "'3'" => array('from' => '08', 'to' => '10'),
    "'4'" => array('from' => '11', 'to' => '01'),
    "'5'" => array('from' => '01', 'to' => '03'),
    "'6'" => array('from' => '01', 'to' => '03')
);


/*
 * 商品信息
 * select item_id,year_id,sea_id,sea_name,cost,ic_id,ic_name,sale_price,item_desc,pub_date from sy_v_item
 * */
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
    'table' => 'sy_v_item',
    'columns' => array(
        'item_id' => 'item_id',
        'item_key' => 'item_key', //item_key
        'brand_id' => "'SYJ'",
        'year_id' => 'year_id',
        'sea_id' => 'sea_id',
        'sea_name' => 'sea_name',
        'cost' => 'cost',
        'ic_id' => 'ic_id',
        'ic_name' =>'ic_name',
        'is_acc' => "'N'",
        'sale_price' => 'sale_price',
        'item_desc' => 'item_desc',
        'sales_point' => "' '",
        'item_file' => "item_id||'.jpg'",
        'info_url' => "' '",
        'style_id' => "' '",
        's_date' => "' '",
        'e_date' => "' '",
        'pub_date' => 'pub_date',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id'),
    'duplicate' => array('item_key', 'year_id', 'sea_id', 'sea_name', 'cost', 'ic_id', 'ic_name', 'sale_price', 'item_desc', 'pub_date', 's_date', 'e_date'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 款号尺码
 * select item_id,size_group from sy_v_itemsize
 * */
$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
    'table' => 'sy_v_itemsize',
    'columns' => array(
        'item_id' => 'item_id',
        'size_group' => 'size_group'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 款号颜色
 * select item_id,value1_code  as color_id from sy_v_itemcolor
 * */
$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
    'table' => 'sy_v_itemcolor',
    'columns' => array(
        'item_id' => 'item_id',
        'color_id' => 'value1_code',
        'item_picture' => "item_id||'_'||value1_code||'.jpg'"
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'color_id'),
    'duplicate' => array('item_picture'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 店铺类型
 * sy_v_shop_type
 *
DROP TABLE IF EXISTS sy_v_shop_type;
CREATE TABLE IF NOT EXISTS `sy_v_shop_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `st_name` varchar(200) DEFAULT NULL COMMENT '类型名称',
  `st_type_code` varchar(200) DEFAULT NULL COMMENT '所属类型代号',
  `st_type_name` varchar(200) DEFAULT NULL COMMENT '所属类型名称',
  `st_order_seq` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序号',
  `st_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否有效：0.无效；1.有效',
  PRIMARY KEY (`st_id`),
  INDEX `st_code` (`st_code`),
  INDEX `st_type_code` (`st_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型基础表';
 * */
$sy_v_shop_type = 'sy_v_shop_type';
$config[$sy_v_shop_type] = array(
    'table' => 'sy_v_shop_type',
    'columns' => array(
        'st_code' => 'st_code',
        'st_name' => 'st_name',
        'st_type_code' => "'ALL'",
        'st_type_name' => "'全部'",
        'st_order_seq' => 'st_code',
        'st_state' => 'st_state',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型明细
 * sy_v_typeshop
 *
DROP TABLE IF EXISTS sy_v_typeshop;
CREATE TABLE IF NOT EXISTS `sy_v_typeshop` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `shop_code` varchar(200) DEFAULT NULL COMMENT '店铺代号',
  PRIMARY KEY (`ts_id`),
  INDEX `st_code` (`st_code`),
  INDEX `shop_code` (`shop_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型明细表';
 * */
$sy_v_typeshop = 'sy_v_typeshop';
$config[$sy_v_typeshop] = array(
    'table' => 'sy_v_typeshop',
    'columns' => array(
        'st_code' => 'st_code',
        'shop_code' => 'shop_code',
    ),
    'join' => array(),
    'where' => 'st_code is not null ',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code', 'shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);



/*
 * 店铺资料
 * select shop_code,shop_name,shop_level,open_date,shop_area,tel_no,addr, case when shop_state='营业' then '1' else '2' end,contact from sy_v_shop
 */
$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
    'table' => 'sy_v_shop',
    'columns' => array(
        'shop_code' => 'shop_code',
        'shop_name' => 'shop_name',
        'model_no' => "''",
        'ref_shopcode' => 'shop_code',
        'shop_level' => 'shop_level',
        'open_date' => 'open_date',
        'shop_area' => 'shop_area',//'rb.area',
        'stand_staffs' => 'stand_staffs',
        'real_staff' => 'real_staff',
        'max_sku' => 'max_sku',
        'min_sku' => 'min_sku',
        'max_qtys' => 'max_qtys',
        'min_qtys' => 'min_qtys',
        'tel_no' => 'tel_no',
        'addr' => 'addr',
        'contact' => 'contact',
        'shop_state' => "(case when shop_state='营业' then '1' else '2' end)"
    ),
    'join' => array(
    ),
    'where' => '',//"rcd.ZG_status <> '已撤店'",
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),
    'duplicate' => array('shop_name', 'shop_level', 'open_date', 'shop_area', 'stand_staffs', 'real_staff', 'max_sku', 'min_sku', 'max_qtys', 'min_qtys', 'tel_no', 'tel_no', 'addr', 'contact', 'shop_state'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'shop_people_count'
);


/*
 * 库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_stock
 * */
$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
    'table' => 'sy_v_stock',
    'columns' => array(
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'stock_qtys' => 'stock_qtys'
    ),
    'join' => array(),
    'where' => 'stock_qtys > 0',
    'group_by' => '',
    'date_key' => '',
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 销售主表
 * select inv_no,shop_code, to_char(to_date(sale_date,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss') as sale_date,vip_state,staff_id,vip_no,inv_qtys,inv_money,tag_money from sales_info
where ROWNUM<100000 and sales_info.sale_date >= '20190101' and sales_info.sale_date < '20190201'
 */
$sales_info = 'sales_info';
$config[$sales_info] = array(
    'table' => 'sales_info',
    'columns' => array(
        'inv_no' => 'inv_no',
        'shop_code' => 'shop_code',
        'sale_date' => "to_char(to_date(sale_date,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')",
        'vip_state' => "(case when vip_state='Y' then '1' else '0' end)",
        'staff_id' => 'staff_id',
        'vip_no' => 'vip_no',
        'inv_qtys' => 'inv_qtys',
        'inv_money' => 'inv_money',
        'tag_money' => 'tag_money'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => 'sale_date', //TO_CHAR(TO_DATE(M_RETAIL.BILLDATE,'YYYY-MM-DD'), 'YYYY-MM-DD')
    'group_by' => '',
    //'by_shop' => true,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'sales_info_o2o'
);



/*
 * 销售明细
 * select sales_detail.inv_no, sales_detail.item_id, sales_detail.color_id, sales_detail.size_id, sales_detail.inv_qtys, sales_detail.inv_money, sales_detail.tag_price from sales_detail
left join sales_info on sales_info.inv_no = sales_detail.inv_no
where ROWNUM<100000 and sales_info.sale_date >= '20190101' and sales_info.sale_date < '20190201'
 */
$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
    'table' => 'sales_detail',
    'columns' => array(
        'inv_no' => 'sales_detail.inv_no',
        'item_id' => 'sales_detail.item_id',
        'color_id' => 'sales_detail.color_id',
        'size_id' => 'sales_detail.size_id',
        'inv_qtys' => 'sales_detail.inv_qtys',
        'inv_money' => 'sales_detail.inv_money',
        'tag_price' => 'sales_detail.tag_price'
    ),
    'join' => array(
        'sales_info' => 'sales_detail.inv_no=sales_info.inv_no',
    ),
    'where' => '',
    'date_key' => 'sales_info.sale_date',
    'group_by' => '',
    //'by_shop' => true,
    'shop_code' => 'sales_info.shop_code',
    'by_limit' => true,
    'sy_key' => array('inv_no', 'item_id', 'color_id', 'size_id'), //, 'color_id', 'size_id'
    'duplicate' => '',//array('inv_qtys', 'inv_money', 'tag_price'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'sales_detail_o2o'
);


/*
 * 导购销售明细
 *
 * */
$sy_v_staff_sales = 'sy_v_staff_sales';
$config[$sy_v_staff_sales] = array(
    'table' => 'sy_v_staff_sales',
    'columns' => array(
        'sale_date' => "to_char(to_date(sale_date,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')",
        'shop_code' => 'shop_code',
        'staff_id' => 'staff_id',
        'sale_amt' => 'sale_amt'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => 'sale_date', //TO_CHAR(TO_DATE(M_RETAIL.BILLDATE,'YYYY-MM-DD'), 'YYYY-MM-DD')
    'group_by' => '',
    //'by_shop' => true,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('sale_date', 'staff_id'),
    'duplicate' => '',
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'sales_info_o2o'
);


/*
 * 销售目标
 * select shop_code,plan_date, TO_NUMBER((TO_DATE(plan_date,'YYYY-MM-DD HH24:mi:ss') - TO_DATE('19700101 08:00:00', 'YYYY-MM-DD HH24:mi:ss')) * 86400), plan_amt from sy_v_plani
where ROWNUM<100000  and plan_date >= '20190101' and plan_date < '20190201'
 * */
$plani_type_one_month_a_line = 'plani_with_type1';    //数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';        //数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';    //数据源表中一行记录代表的时间为某个时间段的情况'plan_date' =>'C_STOREMARKITEM.MONTHDATE',
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
    'table' => 'sy_v_plani',
    'columns' => array(
        'shop_code' => 'shop_code',
        //'plan_date' =>  "TO_NUMBER((TO_DATE(plan_date,'YYYY-MM-DD HH24:mi:ss') - TO_DATE('19700101 08:00:00', 'YYYY-MM-DD HH24:mi:ss')) * 86400)",
        'plan_date' => array(
            'year' => 'YEAR',
            'month' => 'MONTH',
            //'day' => "TO_NUMBER((TO_DATE(plan_date,'YYYY-MM-DD HH24:mi:ss') - TO_DATE('19700101 08:00:00', 'YYYY-MM-DD HH24:mi:ss')) * 86400)"
            'day' => "to_char(to_date(plan_date,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')"
        ),
        'plan_amt' => 'decode(plan_amt,NULL, 0, plan_amt)'
    ),
    'join' => array(),
    'where' => "", //"plan_date >= '20170101'", //plan_date = trunc(sysdate-1)  plan_date >= to_char(trunc(sysdate, 'mm'), 'yyyymmdd')
    'date_key' => 'plan_date',
    'group_by' => '',
    'by_shop' => true,
    'shop_code' => 'shop_code',
    'sy_key' => array('shop_code', 'plan_date'),
    'duplicate' => '',
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'type' => $plani_type_one_day_a_line
);


/*
 * 会员资料
 * select vip_no,cust_name,vip_level,shop_code,register_date,case when cust_sex='W' then '2' else '1' end,born_date,mobile,
address,staff_id,vip_points,vip_state
from sy_v_vip
where ROWNUM<100000
 * */
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
    'table' => 'sy_v_vip',
    'columns' => array(
        'vip_no' => 'vip_no',
        'cust_name' => 'cust_name',
        'shop_code' => 'shop_code',
        'cust_sex' => "case when cust_sex='W' then '2' else '1' end",
        'age' => "' '",
        'born_date' => "to_char(to_date(born_date, 'yyyy-mm-dd'),'yyyy-mm-dd')", //格式转换，暂时需要加上
        'register_date' => "to_char(to_date(register_date,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')",    //格式转换，暂时需要加上 C_VIP.OPENCARDDATE
        'vip_level' => 'vip_level',
        'mobile' => 'mobile',
        'address' => 'address',
        'staff_id' => 'staff_id',
        'vip_state' => '1',
        'active_money' => '0',
        'vip_points' => 'vip_points'
    ),
    'join' => array(),
    'where' => '',
    'shop_code' => 'shop_code',
    'date_key' => '',
    'sy_key' => array('vip_no'),
    //'duplicate' => array('cust_name', 'shop_code', 'cust_sex', 'born_date', 'register_date', 'vip_level', 'mobile', 'address', 'staff_id', 'vip_points'),    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'duplicate' => '',    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'by_shop' => false,
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 导购资料
 * select staff_id,staff_name,staff_shop_code,staff_shop_name
 from sy_v_staff
 * */
$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
    'table' => 'sy_v_staff',
    'columns' => array(
        'staff_id' => 'staff_id',
        'staff_name' => 'staff_name',
        'staff_phone' => 'staff_name', //staff_phone
        'staff_shop_code' => 'staff_shop_code',
        'staff_shop_name' => 'staff_shop_name',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('staff_id'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('staff_name', 'staff_phone', 'staff_shop_code','staff_shop_name')
);


$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
    'table' => 'sy_v_shopstock',
    'columns' => array(
        'stock_date' => 'stock_date',
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'stock_qtys' => 'stock_qtys'
    ),
    'join' => array(),
    'where' => '',
    'group_by' => '',
    'date_key' => 'stock_date',
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('stock_date', 'shop_code', 'item_id', 'color_id', 'size_id'),
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 在途库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_instock
 * */
$sy_v_instock = 'sy_v_instock';
$config[$sy_v_instock] = array(
    'table' => 'sy_v_instock',
    'columns' => array(
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'instock_qtys' => 'instock_qtys'
    ),
    'join' => array(),
    'where' => 'instock_qtys > 0',
    'group_by' => '',
    'date_key' => '',
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);



//处理：备份表数据
$config['insert_data_sy_v_category'] = '';
$config['insert_data_sy_v_item'] = '';
$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = '';

/* End of file config.php */
/* Location: ./application/config/config.php */
