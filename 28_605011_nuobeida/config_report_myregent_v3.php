<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

$config['cid'] = 28;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['shop_count'] = 50;
$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';

//会员资料(单独抽数,已成功)
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
    'table' => 'CustomerVIP',    //对方数据表名
    'columns' => array(            //字段匹配，匹配不到的时候，以"''"代替
        'vip_no' => 'rtrim(VIP)',
        'cust_name' => "isnull(Name,'')",
        'shop_code' => 'rtrim(Customer_ID)',
        'cust_sex' => "Case Sex When '女' Then '2' When '男' Then '1' ELSE '4' END ",
        'age' => "''",
        'born_date' => 'convert(varchar(10), BirthDate, 120)',            //格式转换，必要时加上
        'register_date' => 'convert(varchar(10), input_date, 120)',    //格式转换，必要时加上
        'vip_level' => "''",
        'mobile' => "isnull(MobileTel,'')",//"(Case isnull(MobileTel,'') when '' then isnull(Tel,'') else MobileTel end)",
        'address' => "isnull(Address,'')",
        'staff_id' => "isnull(BusinessManID,'')",//导购id
        'vip_state' => "1",
        'active_money' => '0'
    ),
    'join' => array(),    //关联表，格式翻看下面有关联表的配置
    'where' => "Customer_ID <> ''",
    'by_shop' => false,    //是否根据分店抽取，一般表数据过多且可以关联店铺表的时候，可设置值为true以表示分店抽取，值为true时，需配置下面的shop_code，
    'shop_code' => 'rtrim(Customer_ID)',
    'date_key' => '',    //若客户表中有纪录数据更新的时间戳字段，可以将该字段配置到这里，加快数据同步
    'sy_key' => array('vip_no'),    //不用动
    'duplicate' => array('mobile'),    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'by_limit' => true,//分页
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//大类
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
	'table' => 'dictcategory',
	'columns' => array(
		'class_id' => "CategoryCode",
		'class_name' => "Category",
		'class_code' => "CategoryCode",
		'class_prop' => "' '",
		'class_state' => "'1'",
		'class_order' => "100"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('class_id'),
	'duplicate' => array('class_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


//品类表(单独抽数,已成功)
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
    'table' => 'DictPattern',
    'columns' => array(
        'ic_id' => 'patterncode', //'ISNULL(CategoryCode, Category)',
        'ic_name' => "pattern",
		'class_id' => "left(patterncode,1)",
        'ic_prop' => "'2'",
        'ic_order' => '99'
    ),
    'join' => array(),
    'where' => '',//"CategoryCode is not null",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('ic_id'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('ic_name')
);

//尺码表(单独抽数,已成功)
$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
    'table' => 'DictSize',
    'columns' => array(
        'size_group' => 'Size_Class',
        'size_id' => 'Size',
        'size_desc' => 'Size',
        'size_order' => 'DisplayIndex'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('size_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


////品牌资料，无则各项内容为空(单独抽数,已成功)
$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
    'table' => '',
    'columns' => array(
// 		'id' => 'id',
        'brand_id' => 'BrandCode',
        'brand_name' => 'Brand',
        'sort_order' => "''",
        'active' => '1'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('brand_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);
//条码表(单独抽数,已成功)
$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
    'table' => 'BarCode',
    'columns' => array(
        'bar_no' => 'BarCode.BarCode',    //有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
        'item_id' => 'rtrim(BarCode.Goods_no)',
        'color_id' => 'BarCode.ColorID',
        'size_id' => 'BarCode.Size',
        'color_name' => 'Dictcolor.color'
    ),
    'join' => array('DictColor' => 'DictColor.ColorID = BarCode.ColorID'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('bar_no'),
    'duplicate' => '',
    'by_limit' => true,//分页
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//颜色表(单独抽数,已成功)
$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
    'table' => 'DictColor',
    'columns' => array(
        'color_id' => 'rtrim(ColorID)',
        'color_desc' => 'color'
    ),
    'join' => array(),
    'where' => "ColorID is not null and Color is not null",
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//商品尺码对照表(单独抽数,已成功)
$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
    'table' => 'Goods',
    'columns' => array(
        'item_id' => 'rtrim(Goods_no)',
        'size_group' => 'Size_class'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//商品颜色对照表(单独抽数,已成功)
$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
    'table' => 'GoodsColor',
    'columns' => array(
        'color_id' => 'ColorID',
        'item_id' => 'rtrim(Goods_no)',
        'item_picture' => "''"//"'spc/'+rtrim(Goods_No)+'_'+rtrim(ColorID)+'.jpg'"
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'color_id'),
    'duplicate' => array('item_picture'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//季度表(单独抽数,已成功)
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'DictSeason',
    'columns' => array(
        'sea_id' => "case when Season='0' then '未定义' else Season end",
        'sea_name' => "case when Season='0' then '未定义' else Season end",//char_length
        's_mon' => "(case Season
					when '春季' then 02
					when '夏季' then 05
					when '秋季' then 08
					when '冬季' then 11
					when '春夏' then 02
					when '秋冬' then 08
					else 02 end)",
        'e_mon' => "(case Season
					when '春季' then 04
					when '夏季' then 07
					when '秋季' then 10
					when '冬季' then 01
					when '春夏' then 07
					when '秋冬' then 01
					else 01 end)",
        'is_span_sea' => "(case Season
					when '秋冬' then 1 
					when '未定义' then 1
					else 0 end)"
    ),
    'join' => array(),
    'where' => "",
    'date_key' => '',
    'group_by' => array('Season'),
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('sea_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$config['sea_span'] = "'冬季' or sea_id = '秋冬' or sea_id = '未定义'";        //跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');    //跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(        //季度具体时间段
    "'春季'" => array('from' => '02', 'to' => '04'),
    "'夏季'" => array('from' => '05', 'to' => '07'),
    "'秋季'" => array('from' => '08', 'to' => '10'),
    "'冬季'" => array('from' => '11', 'to' => '01'),
    "'春夏'" => array('from' => '02', 'to' => '07'),
	"'秋冬'" => array('from' => '08', 'to' => '01'),
    "'未定义'" => array('from' => '02', 'to' => '01'),

);

//商品资料(单独抽数,已成功)
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
    'table' => 'Goods',
    'columns' => array(
        'item_id' => 'rtrim(Goods.Goods_no)',
        'brand_id' => "' '",
        'year_id' => 'case when len(Goods.Year)<>4  then datename(year,input_Date) else Goods.Year end',
        'sea_id' => "case when len(Goods.season)>1 then season else '未定义' end",
        'sea_name' => "case when len(Goods.season)>1 then season else '未定义' end",
		'class_id' => "case when len(dictcategory.Category)>=1 then dictcategory.CategoryCode else '9999' end",
		'class_name' => "case when len(dictcategory.Category)>=1 then dictcategory.Category else '未分类' end",
        'ic_id' => "case when len(DictPattern.pattern)>=1 then DictPattern.patterncode else '9999' end ",//CASE WHEN DictCategory.CategoryCode >= 20 THEN 'qt' WHEN DictCategory.CategoryCode='zw' THEN '99' ELSE DictCategory.CategoryCode END
        'ic_name' => "case when len(DictPattern.pattern)>=1 then DictPattern.pattern else '未分类' end",
        'is_acc' => "'N'",
        'sale_price' => 'Goods.dpprice2',
        'item_desc' => 'Goods.Goods_name',
        'sales_point' => "''",
        'item_file' => "rtrim(Goods.Goods_no)+'.jpg'",
        'info_url' => "''",
        'style_id' => "''",
        's_date' => "''",
        'e_date' => "''",
    	'pub_date' => "''"//"convert(varchar(10), newold, 120)"
    ),
    'join' => array(
        'dictcategory' => 'dictcategory.Category = Goods.Category',
		'DictPattern' => 'DictPattern.pattern = Goods.pattern'
		),

    'where' => '',//"DictCategory.Category not in ('样衣') and Goods.Brand not in ('MORUO')",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id'),
    'duplicate' => array('item_desc', 'sale_price', 'ic_id', 'ic_name','class_id','class_name','sea_id','sea_name', 's_date', 'e_date'),
    'by_limit' => false,//分页
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//店铺表(单独抽数,已成功)
$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
    'table' => 'Customer c',
    'columns' => array(
        'shop_code' => 'rtrim(c.Customer_id)',
        'shop_name' => 'c.Customer_na',
        'model_no' => "''",
        'ref_shopcode' => 'c.Customer_id',
        'shop_level' => 'c.Level',
        'open_date' => 'convert(varchar(10), c.PractiseDate, 120)',
        'close_date'=>'convert(varchar(10), c.ShutOutDate, 120)',
        'shop_area' => 'c.Area',
        'stand_staffs' => "0",
        'real_staff' => '0',//'peo',//实际店铺人数
        'max_sku' => "''",
        'min_sku' => "''",
        'max_qtys' => "''",
        'min_qtys' => "''",
        'tel_no' => 'c.Tel1',
        'addr' => 'c.Address',
        'contact' => '""',//"(case c.Region when null then '空' when '' then '空' else c.Region end)",
        'jm_code'=> '""',//'cast(c.ShutOut as varchar)'
    ),
    'join' => array(
//         '(select customerid, count(*) as peo from BuisnessMan where ShutOut=0 group by customerid) as shop_people' => 'shop_people.customerid = c.Customer_id'
        //'reportcenter.dbo.vw_customer rb' => "(case rb.cmp_id when 'YS' then '' ELSE rb.cmp_id+'_' end)+rtrim(rb.Customer_id) = rtrim(c.Customer_id)"
    ),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),
    'duplicate' => array('shop_name', 'shop_level', 'ref_shopcode', 'shop_area', 'tel_no', 'addr', 'contact','jm_code','close_date','open_date'),//指定更新的字段
    'is_multi' => true   //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型
 * sy_v_shop_type
 *
DROP TABLE IF EXISTS sy_v_shop_type;
CREATE TABLE IF NOT EXISTS `sy_v_shop_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `st_name` varchar(200) DEFAULT NULL COMMENT '类型名称',
  `st_type_code` varchar(200) DEFAULT NULL COMMENT '所属类型代号',
  `st_type_name` varchar(200) DEFAULT NULL COMMENT '所属类型名称',
  `st_order_seq` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序号',
  `st_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否有效：0.无效；1.有效',
  PRIMARY KEY (`st_id`),
  INDEX `st_code` (`st_code`),
  INDEX `st_type_code` (`st_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型基础表';
 * */
$sy_v_shop_type = 'sy_v_shop_type';
$config[$sy_v_shop_type] = array(
    'table' => 'DictMathod',
    'columns' => array(
        'st_code' => 'Mathod',
        'st_name' => 'Mathod',
        'st_type_code' => "'ALL'",
        'st_type_name' => "'全部'",
        'st_order_seq' => '999',
        'st_state' => "'1'",
    ),
    'join' => array(),
    'where' => "",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型明细
 * sy_v_typeshop
 *
DROP TABLE IF EXISTS sy_v_typeshop;
CREATE TABLE IF NOT EXISTS `sy_v_typeshop` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `shop_code` varchar(200) DEFAULT NULL COMMENT '店铺代号',
  PRIMARY KEY (`ts_id`),
  INDEX `st_code` (`st_code`),
  INDEX `shop_code` (`shop_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型明细表';
 * */
$sy_v_typeshop = 'sy_v_typeshop';
$config[$sy_v_typeshop] = array(
    'table' => 'Customer',
    'columns' => array(
        'st_code' => "Mathod",
        'shop_code' => 'Customer_id',
    ),
    'join' => array(),
    'where' => "",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code', 'shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//销售主表(单独抽数,已成功)
$sales_info = 'sales_info';
$config[$sales_info] = array(
    'table' => '(select CheckID,Customer_ID,CheckDate,VIP_Card from [Check] where saleflow <>2
				union all
				select  CheckID,Customer_ID,CheckDate,VIP_Card from [CheckL] where saleflow <>2) as vnv_Check',
    'columns' => array(
        'inv_no' => "vnv_Check.CheckID",
        'shop_code' => 'rtrim(vnv_Check.Customer_ID)',
        'sale_date' => 'convert(varchar(10), vnv_Check.CheckDate, 120)',
        'vip_state' => "(case isnull(VIP_Card, '') when '' then 0  else 1 end)",
        'staff_id' => "isnull(max(vn_CheckGoods.BusiManID), ' ')",
        'vip_no' => "isnull(VIP_Card, ' ')",
        'inv_money' => "''"
    ),
    'join' => array(
        'vn_CheckGoods' => 'vnv_Check.CheckID = vn_CheckGoods.CheckID',
        'CustomerVIP' => 'vnv_Check.VIP_Card = CustomerVIP.VIP'
    ),
    'where' => "",
    'date_key' => "convert(varchar(10), [vnv_Check].CheckDate, 120)",
    'group_by' => 'vnv_Check.CheckID,vnv_Check.Customer_ID,vnv_Check.CheckDate,vnv_Check.VIP_Card',
    'by_shop' => false,
    'by_limit' => false,//true 表示分页
    'shop_code' => 'rtrim([vnv_Check].Customer_ID)',
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'by_limit' => true,//分页
    'is_multi' => true   //多数据库情况下是否这个数据库这个表要抽
);


//订单详情表(单独抽数,已成功)
$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
    'table' => '(select  AutoID, CheckGoodsID, ColorID, Long,FieldName,FieldName_value from (
					select AutoID, CheckGoodsID, ColorID, Long, cast(S1 as numeric(6,0)) as S1, S2, S3, S4, S5, S6, S7, S8, S9 from vn_CheckDetail
 				) as AAA
 				UNPIVOT
 				(FieldName_value for FieldName IN (S1, S2, S3, S4, S5, S6, S7, S8, S9)) AS BBB
 				where FieldName_value is not null and FieldName_value<>0
 				) as vnv_CheckDetail',
    'columns' => array(
        'inv_no' => "vn_CheckGoods.CheckID",
        'item_id' => 'rtrim(vn_CheckGoods.Goods_No)',
        'color_id' => 'vnv_CheckDetail.ColorID',
        'size_id' => 'DictSize.SIZE',
        'inv_qtys' => 'vnv_CheckDetail.FieldName_value',
        'inv_money' => 'vn_CheckGoods.DiscountPrice*vnv_CheckDetail.FieldName_value',
        'tag_price' => "Goods.DpPrice2"

    ),
    'join' => array(
        'vn_CheckGoods' => 'vnv_CheckDetail.CheckGoodsID = vn_CheckGoods.CheckGoodsID',
        '(select CheckID,Customer_ID,CheckDate,VIP_Card,saleflow from [Check]
		union all
		select  CheckID,Customer_ID,CheckDate,VIP_Card,saleflow from [CheckL]) as vnv_Check' => 'vn_CheckGoods.CheckID = vnv_Check.CheckID',
        'Goods' => 'vn_CheckGoods.Goods_No = goods.Goods_No',
        'DictSize' => 'Goods.Size_Class = DictSize.Size_Class AND vnv_CheckDetail.FieldName = DictSize.FieldName'
    ),
    'where' => "vnv_Check.saleflow <>2 and goods.Goods_No is not null",
    'date_key' => "convert(varchar(10), vnv_Check.CheckDate, 120)",
    'group_by' => '',//array('[vn_Check].CheckID', 'rtrim(vn_CheckGoods.Goods_No)', 'CCC.ColorID', 'DictSize.SIZE','[vn_Check].Customer_ID'),
    'by_shop' => false,
    'by_limit' => true,
    'shop_code' => '',
    'sy_key' => array('inv_no', 'item_id', 'color_id', 'size_id'),
    'duplicate' => '',//array('inv_qtys', 'inv_money', 'tag_price'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//销售目标(丽金的数据库里没有数据)
$plani_type_one_month_a_line = 'plani_with_type1';    //数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';        //数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';    //数据源表中一行记录代表的时间为某个时间段的情况
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
    'table' => '',
    'columns' => array(
        'shop_code' => 'rtrim(SalesTarget.Customer_ID)',
        'plan_date' => array(
            'year' => '',    //年度字段名
            'month' => '',    //月份字段名
            'day' => 'convert(varchar(10), SalesTargetDetail.TargetDate, 120)'
        ),
        'plan_amt' => 'SalesTargetDetail.AmountLevel1'
    ),
    'join' => array('SalesTarget' => 'SalesTargetDetail.TargetID = SalesTarget.TargetID'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code', 'plan_date'),
    'duplicate' => array('plan_amt'),
    'is_multi' => false,    //多数据库情况下是否这个数据库这个表要抽
    'type' => $plani_type_one_day_a_line
);

//库存信息表
$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
    'table' => '(select AutoId, StockCode, ColorId, Long,FieldName,FieldName_value
				 from
				 (select AutoId, StockCode, ColorId, Long, cast(S1 as numeric(8,0)) as S1,S2,S3,S4,S5,S6,S7,S8,S9
				 from Cstockdetail
				 ) as AAA
				 UNPIVOT 
				 (FieldName_value for FieldName IN (S1,S2,S3,S4,S5,S6,S7,S8,S9)) AS BBB) as vnv_Cstockdetail',
    'columns' => array(
        'shop_code' => 'rtrim(Cstock.Customer_id)',
        'item_id' => 'rtrim(Cstock.Goods_no)',
        'color_id' => 'vnv_Cstockdetail.ColorID',
        'size_id' => 'DictSize.Size',
        'stock_qtys' => 'sum(vnv_Cstockdetail.FieldName_value)'
    ),
    'join' => array(
        'Cstock' => 'vnv_Cstockdetail.StockCode = Cstock.StockCode',
        'Goods' => 'Goods.Goods_no = Cstock.Goods_no',
        'DictSize' => 'DictSize.Size_class = Goods.Size_class AND vnv_Cstockdetail.FieldName=DictSize.FieldName'
    ),
    'where' => "Goods.Goods_no is not null and rtrim(Cstock.Customer_id) is not null and rtrim(Cstock.Customer_id) <>'' 
 and FieldName_value is not null and FieldName_value<>0",
    'group_by' => 'rtrim(Cstock.Customer_id),Cstock.Goods_no, vnv_Cstockdetail.ColorID, DictSize.Size',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),//, 'item_id', 'color_id', 'size_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => '(select AutoId, ShopTinGoodsID, ColorId, Long,FieldName,FieldName_value
 from
 (select AutoId, ShopTinGoodsID, ColorId, Long, S1,S2,S3,S4,S5,S6,S7,S8,S9
 from ShopTinDetail
 ) as AAA
 UNPIVOT
 (FieldName_value for FieldName IN (S1,S2,S3,S4,S5,S6,S7,S8,S9)) AS BBB) as ShopTinDetail',
	'columns' => array(
		'stock_date' => "convert(varchar(10), ShopTin.tin_date, 120)",
		'shop_code' => 'rtrim(ShopTin.tinshop)',
		'item_id' => 'rtrim(ShopTinGoods.goods_no)',
		'color_id' => 'rtrim(ShopTinDetail.colorid)',
		'size_id' => 'DictSize.Size',
		'stock_qtys' => 'sum(ShopTinDetail.FieldName_value)'
	),
	'join' => array(
					'ShopTinGoods' => 'ShopTinGoods.shoptingoodsid=ShopTinDetail.shoptingoodsid',
                    'ShopTin' => 'ShopTin.ShopTinID = ShopTinGoods.shoptinid',
					'Goods' => 'rtrim(Goods.Goods_no) = rtrim(ShopTinGoods.Goods_no)',
					'DictSize' => 'DictSize.Size_class = Goods.Size_class AND ShopTinDetail .FieldName=DictSize.FieldName'),
	'where' => "Goods.Goods_no is not null and FieldName_value is not null and FieldName_value<>0 and ShopTin.Posted=1
 and FieldName_value is not null and FieldName_value<>0",
	'group_by' => 'ShopTin.tin_date,ShopTin.tinshop,ShopTinGoods.goods_no,ShopTinDetail.colorid,DictSize.Size',
	'date_key' => "convert(varchar(10), ShopTin.tin_date, 120)",
	'by_shop' => false,
	'shop_code' => 'rtrim(ShopTin.tinshop)',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'),
	'by_limit' => true,
	'duplicate' => '',//array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
 * 在途库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_instock
 * */
$sy_v_instock = 'sy_v_instock';
$config[$sy_v_instock] = array(
    'table' => '(select AutoId, ShopTinGoodsID, ColorId, Long,FieldName,FieldName_value
				 from
				 (select AutoId, ShopTinGoodsID, ColorId, Long, S1,S2,S3,S4,S5,S6,S7,S8,S9
				 from ShopTinDetail
				 ) as AAA
				 UNPIVOT 
				 (FieldName_value for FieldName IN (S1,S2,S3,S4,S5,S6,S7,S8,S9)) AS BBB) as ShopTinDetail ',
    'columns' => array(
        'shop_code' => 'ShopTin.tinshop',
        'item_id' => "ShopTinGoods.goods_no",
        'color_id' => "ShopTinDetail.colorid",
        'size_id' => "DictSize.Size",
        'instock_qtys' => 'sum(ShopTinDetail.FieldName_value)'
    ),
    'join' => array('ShopTinGoods' => 'ShopTinGoods.shoptingoodsid=ShopTinDetail.shoptingoodsid',
                    'ShopTin' => 'ShopTin.ShopTinID = ShopTinGoods.shoptinid',
					'Goods' => 'rtrim(Goods.Goods_no) = rtrim(ShopTinGoods.Goods_no)',
					'DictSize' => 'DictSize.Size_class = Goods.Size_class AND ShopTinDetail .FieldName=DictSize.FieldName'),
    'where' => "Goods.Goods_no is not null and FieldName_value is not null and FieldName_value<>0 and ShopTin.Posted=0
 and FieldName_value is not null and FieldName_value<>0",
    'group_by' => "ShopTin.tinshop,ShopTinGoods.goods_no,ShopTinDetail.colorid,DictSize.Size",
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
    'table' => 'BuisnessMan',
    'columns' => array(
        'staff_id' => 'rtrim(BuisnessMan.BuisnessManID)',
        'staff_name' => 'BuisnessMan.Name',
        'staff_shop_code' => "BuisnessMan.CustomerID",
        'staff_shop_name' => "Customer.Customer_na"
    ),
    'join' => array(
    	'Customer' => 'BuisnessMan.CustomerID=Customer.Customer_id'),
    'where' => "",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('staff_id'),
    'duplicate' => array('staff_name', 'staff_shop_code', 'staff_shop_name'),
    'is_multi' => true //多数据库情况下是否这个数据库这个表要抽
);

/* End of file config.php */
/* Location: ./application/config/config.php */
