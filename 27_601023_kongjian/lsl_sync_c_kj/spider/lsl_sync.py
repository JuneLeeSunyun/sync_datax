'''
Author: philipdeng
Date: 2021-03-23 19:55:34
LastEditors: philipdeng
LastEditTime: 2021-03-26 12:12:21
Description: file content
'''
'''
Author: philipdeng
Date: 2021-03-17 22:46:25
LastEditors: philipdeng
LastEditTime: 2021-03-26 10:03:33
Description: 零售链资料同步
'''
import requests,json,math

from libs.read_conf import ReadConf
from libs.message_logs import MessageLogs
from libs.sqlalchemy_tools import SuperEncoder
from models.sync_data import SyncData
from spider.login import LoginSys
# from cyberbrain import trace


class LslSync():
    '''
    零售链数据同步请求主体类库
    '''
    def __init__(self):
        self.logs = MessageLogs()
    

    def get_sync_info(self):
        '''
        获取需同步的表资料和api名称
        '''
 
        config = ReadConf().read_conf()
        sync_tables = [sync_table[0] for sync_table in config.items('SYNC_TABLE') if sync_table[1] == 'True']
        sync_info = [{'table_name':sync_table,'api_name':config.get('API_NAME',sync_table)} for sync_table in sync_tables]
        return sync_info

    # @trace
    def __date_sync(self,api_url,token,api_name,data):
        url = f"{api_url}{api_name}"
        headers = {'Content-Type':'application/json','SY-ACCESS-TOKEN':token}
        data = data
        # self.logs.message_logs(json.dumps(data1, ensure_ascii=False, cls = SuperEncoder),'shop_type_list')
        try:
            get_data = requests.post(url=url, data=json.dumps(data, cls = SuperEncoder),headers=headers)
            
            data_list = get_data.json()
            if get_data.status_code == 201:
                log_txt = f"{data_list.get('error_code')},{data_list.get('msg')},{data_list.get('request')}"
                # self.logs.message_logs(log_txt,'shop_type_list')
                return data_list
            else:
                log_txt = f"{data_list.get('error_code')},{data_list.get('msg')},{data_list.get('request')}"
                # self.logs.message_logs(log_txt,'shop_type_list')
                return data_list
        except Exception as e:
            log_txt = e
            print(log_txt)
            self.logs.message_logs(log_txt,'lsl_sync_c')
            return None

        
    # @trace
    def sync_job(self,api_url,token,com_id):
        '''
        同步任务调度
        '''
        sync_model = SyncData()
        sync_infos = self.get_sync_info()
        config = ReadConf().read_conf()
        page_size = config.getint("DB_SET","PAGE_SIZE")
        # to_dict = QueryToDict()
        for sync_info in sync_infos:
            table_name = sync_info.get('table_name')
            api_name = sync_info.get('api_name')
            rows_info = sync_model.get_rows(table_name)
            rows = rows_info.get('rows')
            if rows != 0:
                page_indexs = math.ceil(rows/page_size) + 1
                log_txt_star = f"表[{table_name}]共{page_indexs -1}批次数据"
                all_rows = 0
                for page_index in range(1,page_indexs):
                    log_txt = f"{log_txt_star},正在处理第{page_index}批次数据"
                    data_get = sync_model.sync_go(table_name,page_index,com_id)
                    data_rows = data_get.get('data_count')
                    # data_rows2 = len(data_get.get('data_list'))
                    # self.logs.message_logs(data_rows2,'item_log')
                    data_list = {"data_info":data_get.get('data_list')}
                    if data_rows != 0:
                        data = data_list
                        post_result = self.__date_sync(api_url,token,api_name,data)
                        #需要判断post_result是否为空
                        if post_result.get('error_code') == 0:
                            all_rows +=  data_rows
                            log_txt = f"{log_txt},{post_result.get('msg')}{post_result.get('request')}.表[{table_name}]合共传输{all_rows}行数据。"
                            print(log_txt)
                            self.logs.message_logs(log_txt,'lsl_sync_c')
                        else:
                            all_rows +=  0
                            log_txt = f"{log_txt},{post_result.get('msg')}{post_result.get('request')}.表[{table_name}]合共传输{all_rows}行数据。"
                            print(log_txt)
                            self.logs.message_logs(log_txt,'lsl_sync_c') 
                    else:
                        log_txt = f"{log_txt},{data_get.get('log_txt')}"
                        print(log_txt)
                        self.logs.message_logs(log_txt,'lsl_sync_c')
            else:
                log_txt = rows_info.get('log_txt')
                print(log_txt)
                self.logs.message_logs(log_txt,'lsl_sync_c')


    
    def sync_go(self):
        login_info = LoginSys().login_info()
        api_url = login_info.get('api_url')
        token = login_info.get('token')
        com_id = login_info.get('com_id')
        self.sync_job(api_url,token,com_id)

        
