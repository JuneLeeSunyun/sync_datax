<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

$config['cid'] = 27;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['shop_count'] = 10;

//大类
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
	'table' => 'g_othertype10',
	'columns' => array(
		'class_id' => "code",
		'class_name' => "name",
		'class_code' => "code",
		'class_prop' => "' '",
		'class_state' => "'1'",
		'class_order' => "999"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('class_id'),
	'duplicate' => array('class_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//品类
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
	'table' => 'g_othertype11',
	'columns' => array(
		'ic_id' => "code",
		'ic_name' => "name",
		'ic_code' => "code",
		'class_id' => "left(code,1)",
		'ic_prop' => "'2'",
		'ic_order' => "999"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('ic_id'),
	'duplicate' => array('ic_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//小小类
$sy_v_smallcategory = 'sy_v_smallcategory';
$config[$sy_v_smallcategory] = array(
	'table' => '',
	'columns' => array(
		'sc_id' => "' '",
		'sc_name' => "' '",
		'sc_code' => "' '",
		'sc_prop' => "'2'",
		'sc_state' => '1',
		'ic_id' => "' '",
		'sc_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sc_id'),
	'duplicate' => array('sc_name'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
	'table' => 'g_level3',
	'columns' => array(
		'size_group' => 'brand',
		'size_id' => "code",
		'size_desc' => "name",
		'size_order' => "999"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('size_id', 'size_group'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
	'table' => '',
	'columns' => array(
// 		'id' => 'id',
		'brand_id' => "",
		'brand_name' => "",
		'sort_order' => "",
		'active' => '1'
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('brand_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
	'table' => 'goodsbarcode',
	'columns' => array(
		'bar_no' => "goodsbarcode.barcode",
		'item_id' => "goodsdtl.code",
		'color_id' => "case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end",
		'color_name' => "case when goodsdtl.L2code='' then '未定义' else g_level2.name end",
		'size_id' => "case when goodsdtl.l3code='' then 'F' else goodsdtl.l3code end",
		'size_code' => "case when goodsdtl.l3code='' then 'F' else goodsdtl.l3code end"
	),
	'join' => array(
	    'goodsdtl' => 'goodsbarcode.sku=goodsdtl.sku',
		'goods' => 'goodsdtl.code=goods.code',
		'g_level2' => 'goodsdtl.L2code=g_level2.code and goods.brand=g_level2.brand',
		'g_level3' => 'goodsdtl.l3code=g_level3.code and goods.lvlcode=g_level3.brand'),
	'where' => "len(goods.season)='3' and left(goods.season,2)>='17'",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('bar_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
	'table' => 'g_level2',
	'columns' => array(
		'color_id' => "case when left(brand,1) ='k' then code+'_'+left(brand,1)  else code+'_'+brand end",
		'color_desc' => "name"
	),
	'join' => array(),
	'where' => '',
	'group_by' => '',
	'date_key' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
	'table' => 'goods',
	'columns' => array(
		'item_id' => "code",
		'size_group' => "lvlcode"
	),
	'join' => array(),
	'where' => "",
	'date_key' => '',
	'group_by' => array(),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'size_group'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
	'table' => "(select distinct  goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,
goodsdtl.code+goodsdtl.L2code + '.jpg'as item_picture from goodsdtl
left join goods on goodsdtl.code=goods.code) as  goodsdtl",
	'columns' => array(
		'item_id' => "item_id",
		'color_id' => "color_id",
		'item_picture' => "item_picture",
	),
	'join' => array(),
	'where' => "",
	'date_key' => '',
	'group_by' => array(),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
	'table' => 'goods',
	'columns' => array(
		'sea_id' => 'apply_spr||apply_sum||apply_aut||apply_win',
		'sea_name' => "(case apply_spr||apply_sum||apply_aut||apply_win when 'YYYY' THEN '全季度' when 'YYNY' THEN '春夏冬' when 'YNNN' THEN '春季' when 'NYNY' THEN '夏冬' when 'NYNN' THEN '夏季' when 'NNYN' THEN '秋季' when 'NNNY' THEN '冬季' when 'NNNN' THEN '无季度' ELSE '暂无' end)",
		'sea_erp_code' => 'apply_spr||apply_sum||apply_aut||apply_win',
		'sea_erp_name' => "(case apply_spr||apply_sum||apply_aut||apply_win when 'YYYY' THEN '全季度' when 'YYNY' THEN '春夏冬' when 'YNNN' THEN '春季' when 'NYNY' THEN '夏冬' when 'NYNN' THEN '夏季' when 'NNYN' THEN '秋季' when 'NNNY' THEN '冬季' when 'NNNN' THEN '无季度' ELSE '暂无' end)",
	),
	'join' => array(),
	'where' => "isstyle='N'",
	'date_key' => '',
	'group_by' => array('pid', 'colorid'),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);*/
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
	'table' => 'g_season',
	'columns' => array(
		'sea_id' => "right(code,1)",
		'sea_name' => "case right(code,1) when '1' then '春' when '2' then '夏' when '3' then '秋' when '4' then '冬' else '常年款' end",
		's_mon' => "case right(code,1) when '1' then '12' when '2' then '03' when '3' then '07' when '4' then '09' else '01' end",
		'e_mon' => "case right(code,1) when '1' then '04' when '2' then '09' when '3' then '12' when '4' then '02' else '12' end",
		'is_span_sea' => "case right(code,1) when '1' then '1' when '4' then '1' else '0' end",
		'span_offset' => "case right(code,1) when '1' then '-1' when '4' then '1' else '0' end"
	),
	'join' => array(),
	'where' => "len(code)='3' and left(code,2)>='17'",
	'date_key' => '',
	'group_by' => array("right(code,1)","case right(code,1) when '1' then '春' when '2' then '夏' when '3' then '秋' when '4' then '冬' else '常年款' end"),
	'duplicate' => array('s_mon', 'e_mon', 'sea_order', 'is_span_sea', 'span_offset'),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sea_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
//商品资料
$config['sea_span'] ="'1' or '4'";    	//跨年季度id,$config['sea_span'] ="'Q4' or sea_id = 'Q0'"
$config['year_offset'] = array('s' => 0, 'e' => '1');	//跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(		//季度具体时间段
	"'1'" => array('from' => '12', 'to' => '04'),
	"'2'" => array('from' => '03', 'to' => '09'),
	"'3'" => array('from' => '07', 'to' => '12'),
	"'4'" => array('from' => '09', 'to' => '02')	
);

$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
	'table' => 'goods',
	'columns' => array(
		'item_id' => "goods.code",
		'brand_id' => "goods.brand",
		'is_add' => '1',
		'is_included' => "' '",
		'year_id' => "'20'+left(g_season.code,2)",
		'sea_id' => "right(g_season.code,1)",
		'sea_name' => "case right(g_season.code,1) when '1' then '春' when '2' then '夏' when '3' then '秋' when '4' then '冬' else '常年款' end",
		'cost' => "cast(isnull(soPrice,0) as decimal(12,2))",
		'sub_icid' => "' '",
		'wave_no' => "' '",
		'class_id' => "case  when goods.othertype10 is null then '9999' else goods.othertype10 end",
		'class_name' => "case  when goods.othertype10 is null then '未分类' else g_othertype10.name end",
		'ic_id' => "case when goods.othertype11 is null then '9999' else goods.othertype11 end",
		'ic_name' => "case when goods.othertype11 is null then '未分类' else g_othertype11.name end",
		'sc_id' => "' '",
		'sc_name' => "' '",
		'is_acc' => "'N'",
		'sale_price' => "cast(price as decimal(10,2))",
		'item_desc' => "goods.name",
		'sales_point' => "' '",
		'item_file' => "goods.code+'.jpg'",
		'pub_date' => "DATEDIFF(s, '19700101',GETDATE())",
		'info_url' => "' '",
		'style_id' => "' '",
		's_date' => "' '",
		'e_date' => "' '"
	),
	'join' => array(
		'g_season' => 'goods.season=g_season.code',
		'g_othertype10' => 'goods.othertype10=g_othertype10.code',
		'g_othertype11' =>  'goods.othertype11=g_othertype11.code'),
	'where' => "len(g_season.code)='3' and left(g_season.code,2)>='18'",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id'),
    'duplicate' => array('year_id', 'sea_id', 'sea_name', 'class_id','class_name','ic_id', 'ic_name','sale_price', 'item_desc','pub_date', 's_date', 'e_date'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
	'table' => 'shop',
	'columns' => array(
		'shop_code' => "code",
		'shop_name' => "name",
		'model_no' => "' '",
		'ref_shopcode' => "' '",
		'jm_code' => "' '",
		'shop_level' => "account",
		'branch_id' => "commu",
		'open_date' => "convert(char(10),begdate,120)",
		'close_date' => "convert(char(10),enddate,120)",
		'shop_area' => "leftamt",
		'stand_staffs' => "point",
		'real_staff' => "point",
		'max_sku' => "0",
		'min_sku' => "0",
		'max_qtys' => "0",
		'min_qtys' => "0",
		'tel_no' => "tel2",
		'addr' => "addr",
		'contact' => "' '"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code'),
	'duplicate' => array('shop_name','shop_level', 'tel_no', 'addr','open_date','shop_area','stand_staffs', 'real_staff'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型
 * sy_v_shop_type
 *
DROP TABLE IF EXISTS sy_v_shop_type;
CREATE TABLE IF NOT EXISTS `sy_v_shop_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `st_name` varchar(200) DEFAULT NULL COMMENT '类型名称',
  `st_type_code` varchar(200) DEFAULT NULL COMMENT '所属类型代号',
  `st_type_name` varchar(200) DEFAULT NULL COMMENT '所属类型名称',
  `st_order_seq` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序号',
  `st_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否有效：0.无效；1.有效',
  PRIMARY KEY (`st_id`),
  INDEX `st_code` (`st_code`),
  INDEX `st_type_code` (`st_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型基础表';
 * */
$sy_v_shop_type = 'sy_v_shop_type';
$config[$sy_v_shop_type] = array(
    'table' => "(select code as st_code,name as st_name,'49' as st_type_code,'门店等级' as st_type_name from SYS_CSB where brand='49'
                  union all
                 select code as st_code,name as st_name,brand as st_type_code,'门店分类' as st_type_name from s_shoptype) as xxx",
    'columns' => array(
        'st_code' => 'st_code',
        'st_name' => 'st_name',
        'st_type_code' => "st_type_code",
        'st_type_name' => "st_type_name",
        'st_order_seq' => '999',
        'st_state' => "'1'",
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型明细
 * sy_v_typeshop
 *
DROP TABLE IF EXISTS sy_v_typeshop;
CREATE TABLE IF NOT EXISTS `sy_v_typeshop` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `shop_code` varchar(200) DEFAULT NULL COMMENT '店铺代号',
  PRIMARY KEY (`ts_id`),
  INDEX `st_code` (`st_code`),
  INDEX `shop_code` (`shop_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型明细表';
 * */
$sy_v_typeshop = 'sy_v_typeshop';
$config[$sy_v_typeshop] = array(
    'table' => "(select account as st_code,code as shop_code from SHOP
                 union all
                 select shoptype as st_code,code as shop_code from SHOP ) as xxx ",
    'columns' => array(
        'st_code' => "st_code",
        'shop_code' => 'shop_code',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code', 'shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

$sales_info = 'sales_info';
$config[$sales_info] = array(
	'table' => 'pos_sale',
	'columns' => array(
		'inv_no' => "pos_sale.code",
		'shop_code' => "pos_sale.dhcodefrm",
		'sale_date' => "convert(char(19),pos_sale.gsdate,120)",
		'vip_state' => "case len(pos_sale.dhcodeto) when 0 then 0 else 1 end",
		'staff_id' => "upper(pos_sale.empcodegs)",
		'vip_no' => "pos_sale.dhcodeto",
		'inv_qtys' => 'pos_sale.totqty',
		'inv_money' => "pos_sale.totamount-isnull(pos_paydtl.amount,0)",
		'tag_money' => "pos_sale.totstdamt"
	),
	'join' => array("(select code,sum(amount) as amount from pos_paydtl where sku in ('ZSM','YHQ') group by code) as pos_paydtl" => 'pos_sale.code=pos_paydtl.code'),
	'where' => '',
	'date_key' => 'pos_sale.gsdate',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
	'table' => 'pos_saledtl',
	'columns' => array(
		'inv_no' => 'pos_saledtl.code',
		'item_id' => "goodsdtl.code",
		'color_id' => "case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end",
		'size_id' => "case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end",
		'inv_qtys' => 'sum(pos_saledtl.qty)',
		'inv_money' => "sum(cast (case pos_sale.totamount when 0 then pos_saledtl.amount else pos_saledtl.amount*(1-(isnull(pos_paydtl.amount,0))/pos_sale.totamount) end as decimal(12,2)))",
		'tag_price' => 'cast(pos_saledtl.stdprice as decimal(18,2))'
	),
	'join' => array('goodsdtl' => "pos_saledtl.sku=goodsdtl.sku",
	    'goods' => "goodsdtl.code=goods.code",
		'pos_sale' => 'pos_saledtl.code=pos_sale.code',
		"(select code,sum(amount) as amount from pos_paydtl where sku in ('ZSM','YHQ') group by code) as pos_paydtl" => 'pos_sale.code=pos_paydtl.code'),
	'where' => '',
	'date_key' => 'convert(char(19),pos_sale.gsdate,120)',
	'group_by' => array("pos_saledtl.code","goodsdtl.code","case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end","case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end","cast(pos_saledtl.stdprice as decimal(18,2))"),
	'by_shop' => false,
	'shop_code' => 'ipos_qtlsd.zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//销售目标
/*
select ipos_zdjbb.zddm, nd, mn, je1, je2, je3, je4, je5, je6, je7, je8, je9, je10, je11, je12, je13, je14, je15, je16, je17, je18, je19, je20, je21, je22, je23, je24, je25, je26, je27, je28, je29, je30, je31 from 
(
select ipos_rizbdmx.zd_id, ipos_rizbd.nd, ipos_rizbd.mn, max(ipos_rizbd.id) as id from ipos_rizbdmx 
left join ipos_rizbd on ipos_rizbd.id = ipos_rizbdmx.pid 
where  (nd > year(now()) or (nd = year(now()) and mn >= month(now())))
group by ipos_rizbdmx.zd_id , ipos_rizbd.nd, ipos_rizbd.mn
) a
left join ipos_rizbdmx on ipos_rizbdmx.pid = a.id and ipos_rizbdmx.zd_id = a.zd_id
left join ipos_zdjbb on ipos_zdjbb.pid = a.zd_id
*/
$plani_type_one_month_a_line = 'plani_with_type1';	//数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';		//数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';	//数据源表中一行记录代表的时间为某个时间段的情况
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'plan_date' => array(
			'year' => '',
			'month' => '',
			'day' => array('je1', 'je2', 'je3', 'je4', 'je5', 'je6', 'je7', 'je8', 'je9', 'je10', 'je11', 'je12', 'je13', 'je14', 'je15', 'je16', 'je17', 'je18', 'je19', 'je20', 'je21', 'je22', 'je23', 'je24', 'je25', 'je26', 'je27', 'je28', 'je29', 'je30', 'je31')
			)
	),
	'join' => array(''),
	'where' => '',
	'date_key' => '',//'ipos_rizbdmx.lastchanged',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code', 'plan_date'),
// 	'duplicate' => array('plan_amt'),
	'is_multi' => false,	//多数据库情况下是否这个数据库这个表要抽
	'type' => $plani_type_one_month_a_line
);

$sy_v_shiftsales = 'sy_v_shiftsales';
$config[$sy_v_shiftsales] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => '',
		'new_amt' => '',
		'tag_amt2' => '',
		'add_qtys' => '',
		'sale_sheets' => '',
		'new_vips' => '',
		'vip_sheets' => '',
		'vip_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shiftsalesdet = 'sy_v_shiftsalesdet';
$config[$sy_v_shiftsalesdet] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'item_id' => '',
		'color_id' => '',
		'size_id' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
	'table' => "(select  shopinv.shop as shop_code, goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end as size_id,shopinv.id_qty as  stock_qtys
from shopinv 
left join goodsdtl on shopinv.id_sku=goodsdtl.sku
left join goods on goodsdtl.code=goods.code
where shopinv.id_qty<>0 and shopinv.shop<>'GZ001'
union all
select yw_sptz_out.dhcodefrm as shop_code,goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end as size_id,
-1*yw_sptz_outdtl.qty2 as stock_qtys
from yw_sptz_outdtl 
left join yw_sptz_out on yw_sptz_outdtl.code=yw_sptz_out.code
left join goodsdtl on yw_sptz_outdtl.sku=goodsdtl.sku
left join goods on goodsdtl.code=goods.code
where yw_sptz_out.codestatus='2' and yw_sptz_out.dhcodefrm<>'GZ001'
union all
select  VIEW_inv_use.shop as shop_code, goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end as size_id,VIEW_inv_use.useqty as  stock_qtys
from VIEW_inv_use 
left join goodsdtl on VIEW_inv_use.id_sku=goodsdtl.sku
left join goods on goodsdtl.code=goods.code
where VIEW_inv_use.shop='GZ001') as xxx
",
	'columns' => array(
		'shop_code' => 'shop_code',
		'item_id' => 'item_id',
		'color_id' => 'color_id',
		'size_id' => 'size_id',
		'stock_qtys' => 'sum(stock_qtys)'
	),
	'join' => array(),
	'where' => 'item_id is not null',
	'group_by' => array('shop_code', 'item_id', 'color_id', 'size_id'),
	'date_key' => "",
	'by_shop' => false,
	'shop_code' => 'shop_code',
	'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), 
	'by_limit' => true,
// 	'duplicate' => array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shoparea = 'sy_v_shoparea';
$config[$sy_v_shoparea] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'class_id' => '',
		'sa_area' => ""
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'class_id'),
	'is_multi' => true
);

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
	'table' => 'vip',
	'columns' => array(
		'vip_no' => "vip.code",
		'cust_name' => "vip.name",
		'shop_code' => "vip.shop",
		'cust_sex' => "case vip.emp when '0' then '1' else '2' end",
		'age' => "0",
		'born_date' => 'convert(char(10),vip.birth,120)',			//格式转换，暂时需要加上
		'register_date' => 'convert(char(10),begdate,120)',		//格式转换，暂时需要加上
		'vip_level' => "case  when vipdengji.name is null or vipdengji.name='' then '无等级' else vipdengji.name end",
		'mobile' => "vip.mobile",
		'address' => "vip.addr",
		'staff_id' => "vip.openemp",
		'vip_state' => "case vip.status when '0' then '1' else '2' end",
		'active_money' => 'isnull(vip.openamt,0)',
		'vip_points'=>'isnull(vip.point,0)'
	),
	'join' => array('vipdengji' => "vip.vipdengji=vipdengji.code"),
	'where' => "",
	'shop_code' => 'shop',
	'date_key' => '',
	'sy_key' => array('vip_no'),
	'by_shop' => true,
	'by_limit' => false,
	'is_multi' => true,
);

//员工信息
$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
	'table' => 'employee',
	'columns' => array(
		'staff_id' => "employee.code",
		'staff_name' => "employee.name",
		'staff_phone' => "employee.mobile",
		'staff_shop_code' => "employee.shop",
		'staff_shop_name' => "shop.name "
	),
	'join' => array(
		'shop' => 'employee.shop=shop.code'
		),
	'where' => "",
	'shop_code' => 'shop',
	'date_key' => '',
	'sy_key' => array('staff_id'),
	'by_shop' => false,
	'by_limit' => false,
	'is_multi' => true,
);


$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => "(select yw_sp_in.code as inv_no, convert(char(10),yw_sp_in.shdate,120)  as stock_date,yw_sp_in.dhcodefrm as shop_code,goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end as size_id,yw_sp_indtl.qty2 as stock_qtys,yw_sp_indtl.amount as stock_money 
 from yw_sp_indtl
left join yw_sp_in on yw_sp_in.code=yw_sp_indtl.code
left join goodsdtl on yw_sp_indtl.sku=goodsdtl.sku
left join goods on goodsdtl.code=goods.code
where yw_sp_in.codestatus=1
union all
select  yw_vd_in.code as inv_no, convert(char(10),yw_vd_in.shdate,120)  as stock_date,yw_vd_in.dhcodefrm as shop_code,goodsdtl.code as item_id,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end as color_id,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end as size_id,yw_vd_indtl.qty2 as stock_qtys,yw_vd_indtl.amount as stock_money 
 from yw_vd_indtl
left join yw_vd_in on yw_vd_in.code=yw_vd_indtl.code
left join goodsdtl on yw_vd_indtl.sku=goodsdtl.sku
left join goods on goodsdtl.code=goods.code
where yw_vd_in.codestatus=1) as a",
	'columns' => array(
		'stock_date' => "stock_date",
		'shop_code' => 'shop_code',
		'item_id' => 'item_id',
		'color_id' => "color_id",
		'size_id' => "size_id",
		'stock_qtys' => "stock_qtys"
	),
	'join' => array(),
	'where' => '',
	'group_by' => array(),
	'date_key' => "stock_date",
	'by_shop' => false,
	'shop_code' => 'shop_code',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'), 
	'by_limit' => true,
// 	'duplicate' => array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
 * 在途库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_instock
 * */
$sy_v_instock = 'sy_v_instock';
$config[$sy_v_instock] = array(
    'table' => 'inv_indtl',
    'columns' => array(
        'shop_code' => 'inv_in.dhcodefrm',
        'item_id' => "goodsdtl.code",
        'color_id' => "case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end",
        'size_id' => "case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end",
        'instock_qtys' => 'sum(inv_indtl.qty2)'
    ),
    'join' => array('inv_in' => 'inv_in.code=inv_indtl.code',
					'goodsdtl' => 'inv_indtl.sku=goodsdtl.sku',
					'goods' => 'goodsdtl.code=goods.code'),
    'where' => 'inv_in.codestatus<>1',
    'group_by' => "inv_in.dhcodefrm,goodsdtl.code,case when goodsdtl.L2code ='' or goodsdtl.L2code is null then '9999' when left(goods.brand,1) ='k' then goodsdtl.L2code+'_'+left(goods.brand,1)  else  goodsdtl.L2code+'_'+goods.brand end,case goodsdtl.l3code when '' then 'F' else goodsdtl.l3code end",
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/* End of file config.php */
/* Location: ./application/config/config.php */
