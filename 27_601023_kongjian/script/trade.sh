#!/bin/bash

# 根据所输入日期抽取交易资料

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date +%Y-%m-%d`
else
    s_day=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date -d "+1 days" +%Y-%m-%d`
else
    e_day=$ed
fi

Location='/opt/sydata/datax/bin'


cd $Location;

python datax.py ../job/kj/ms2my/kj_s2m_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/kj/ms2my/kj_s2m_stock.json;
cd /opt/sydata/lsl_sync_c_kj;python3 clean_cache.py
