'''
Author: philipdeng
Date: 2021-03-16 21:22:36
LastEditors: philipdeng
LastEditTime: 2021-04-30 15:00:09
Description: 日志记录模块
'''
import datetime,sys,os,stat

from libs.read_conf import ReadConf

class MessageLogs():
    '''
    日志记录
    '''
    
    def __init__(self):
        config = ReadConf().read_conf()
        self.log_time_mode = config.get("BASE_PARAM","LOG_TIME_MODE")

    
    def __log_save(self,log_txt,log_file_name):
        '''
        log_txt:日志信息
        log_file_name：日志文件名
        log_time：日志文件存放方式，year为按年存放，month为按月存放，day为按天存放
        '''
        log_txt = log_txt
        log_file_name = log_file_name
        log_time_mode = self.log_time_mode
        log_time = datetime.datetime.now()
        log_time_str = log_time.__format__('%Y-%m-%d %H:%M:%S')
        if log_time_mode == 'month':
            log_nametime = log_time.__format__('%Y%m')
        elif log_time_mode == 'day':
            log_nametime = log_time.__format__('%Y%m%d')
        else:
            log_nametime = log_time.__format__('%Y')
        program_path=sys.path[0]
        log_path = f"{program_path}/logs/{log_file_name}_{log_nametime}.txt"
        log_txt = f"[{log_time_str}]:{log_txt}"
        f=open(log_path,'a')
        f.write(log_txt)
        f.write("\n")
        f.close()
        try:
            os.chmod(log_path, stat.S_IRUSR + stat.S_IWUSR + stat.S_IRGRP + stat.S_IWGRP + stat.S_IROTH + stat.S_IWOTH)
        except Exception as e:
            print(e)

    def message_logs(self,log_txt,log_file_name):
        log_txt = log_txt
        log_file_name = log_file_name
        self.__log_save(log_txt,log_file_name)
