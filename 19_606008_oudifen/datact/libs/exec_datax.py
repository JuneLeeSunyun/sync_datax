'''
Author: philipdeng
Date: 2021-07-04 09:12:35
LastEditors: philipdeng
LastEditTime: 2021-07-04 15:10:18
FilePath: /datact/libs/exec_datax.py
Description: 执行datax
'''

from libs.read_conf import ReadConf
from libs.message_logs import MessageLogs
import datetime
import os, subprocess

from pymysql import connect

import cx_Oracle


class Exec_Datax():
    '''
    执行datax并更新执行状态
    '''

    def __init__(self, log_no):
        config = ReadConf().read_conf()
        self.logs = MessageLogs()
        self.x_shell = config.get("BASE_PARAM", "DSHELL")
        self.x_shell_stock = config.get("BASE_PARAM", "DSHELL_STOCK")
        self.shell_path = config.get("BASE_PARAM", "SHELL_PATH")
        self.m_db_type = config.get("MDB", "DB_TYPE")
        self.m_db_addr = config.get("MDB", "DB_ADDR")
        self.m_db_user = config.get("MDB", "DB_USER")
        self.m_db_pwd = config.get("MDB", "DB_PWD")
        self.m_db_port = config.getint("MDB", "DB_PORT")
        self.m_db_name = config.get("MDB", "DB_NAME")
        self.m_db_charset = config.get("MDB", "DB_CHARSET")
        self.log_no = log_no

    def update_smdate_sales(self):
        '''
        销售取数开始时间更新
        '''

        smdate = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                cur.execute(
                    "UPDATE SYADM.SY_V_CRON_DATE SET CHECK_DATE = TO_DATE(:smdate,'YYYY-MM-DD HH24:mi:ss') WHERE ID=:ID",
                    smdate=smdate, ID=1)
                connection.commit()
                cur.close()
                connection.close()
                log_txt = f"[{self.log_no}]销售取数开始时间已更新为{smdate}"
                print(log_txt)
                self.logs.message_logs(log_txt, 'datact')
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "update sy_v_cron_date set check_date = now() where ID = %s"
                cur.execute(sql, 1)
                conn.commit()
                cur.close()
                conn.close()

        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误,取数开始时间更新失败！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def update_smdate_stock(self):
        '''
        库存取数开始时间更新
        '''

        smdate = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                cur.execute(
                    "UPDATE SYADM.SY_V_CRON_DATE SET CHECK_DATE = TO_DATE(:smdate,'YYYY-MM-DD HH24:mi:ss') WHERE ID=:ID",
                    smdate=smdate, ID=3)
                connection.commit()
                cur.close()
                connection.close()
                log_txt = f"[{self.log_no}]库存取数开始时间已更新为{smdate}"
                print(log_txt)
                self.logs.message_logs(log_txt, 'datact')
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "update sy_v_cron_date set check_date = now() where ID = %s"
                cur.execute(sql, 3)
                conn.commit()
                cur.close()
                conn.close()

        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误,取数开始时间更新失败！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def update_emdate_sales(self):
        '''
        销售取数结束时间更新
        '''

        emdate = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                cur.execute(
                    "UPDATE SYADM.SY_V_CRON_DATE SET CHECK_DATE = TO_DATE(:emdate,'YYYY-MM-DD HH24:mi:ss') WHERE ID=:ID",
                    emdate=emdate, ID=2)
                connection.commit()
                cur.close()
                connection.close()
                log_txt = f"[{self.log_no}]销售取数结束时间已更新为{emdate}"
                print(log_txt)
                self.logs.message_logs(log_txt, 'datact')
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "update sy_v_cron_date set check_date = now() where ID = %s"
                cur.execute(sql, 2)
                conn.commit()
                cur.close()
                conn.close()

        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误,取数结束时间更新失败！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def update_emdate_stock(self):
        '''
        库存取数结束时间更新
        '''

        emdate = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                cur.execute(
                    "UPDATE SYADM.SY_V_CRON_DATE SET CHECK_DATE = TO_DATE(:emdate,'YYYY-MM-DD HH24:mi:ss') WHERE ID=:ID",
                    emdate=emdate, ID=4)
                connection.commit()
                cur.close()
                connection.close()
                log_txt = f"[{self.log_no}]库存取数结束时间已更新为{emdate}"
                print(log_txt)
                self.logs.message_logs(log_txt, 'datact')
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "update sy_v_cron_date set check_date = now() where ID = %s"
                cur.execute(sql, 4)
                conn.commit()
                cur.close()
                conn.close()

        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误,取数结束时间更新失败！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def exec_script(self, script_file):
        '''
        datax取数脚本执行
        '''
        cmd = os.path.join(self.shell_path, script_file)
        try:
            cmd_exec = subprocess.run(cmd)
            log_txt = f"[{self.log_no}]命令{cmd}执行成功！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
        except Exception as e:
            log_txt = f"[{self.log_no}]命令执行出错！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def exec_go(self, exec_job):
        if exec_job.get('sales'):
            self.update_smdate_sales()
            self.exec_script(self.x_shell)
            self.update_emdate_sales()
            log_txt = f"[{self.log_no}]销售数据取数完毕！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
        else:
            log_txt = f"[{self.log_no}]销售数据取数条件不满足，无需取销售数据！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
        if exec_job.get('stock'):
            self.update_smdate_stock()
            self.exec_script(self.x_shell_stock)
            self.update_emdate_stock()
            log_txt = f"[{self.log_no}]库存数据取数完毕！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
        else:
            log_txt = f"[{self.log_no}]库存数据取数条件不满足，无需取库存数据！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
