'''
Author: philipdeng
Date: 2021-06-22 15:28:36
LastEditors: philipdeng
LastEditTime: 2021-07-05 00:05:55
FilePath: /datact/libs/judgex.py
Description: 判断是否需执行datax
'''
import psutil
from libs.read_conf import ReadConf
from libs.message_logs import MessageLogs
import datetime
import pymysql
from pymysql import connect

import cx_Oracle


class JudgeX():

    def __init__(self, log_no):
        config = ReadConf().read_conf()
        self.x_process = config.get("BASE_PARAM", "XPROCESS")
        self.x_cmd = config.get("BASE_PARAM", "EXECMD")
        self.x_shell = config.get("BASE_PARAM", "DSHELL")
        self.x_shell_stock = config.get("BASE_PARAM", "DSHELL_STOCK")
        self.m_db_type = config.get("MDB", "DB_TYPE")
        self.m_db_addr = config.get("MDB", "DB_ADDR")
        self.m_db_user = config.get("MDB", "DB_USER")
        self.m_db_pwd = config.get("MDB", "DB_PWD")
        self.m_db_port = config.getint("MDB", "DB_PORT")
        self.m_db_name = config.get("MDB", "DB_NAME")
        self.m_db_charset = config.get("MDB", "DB_CHARSET")
        self.c_db_type = config.get("CDB", "DB_TYPE")
        self.c_db_addr = config.get("CDB", "DB_ADDR")
        self.c_db_user = config.get("CDB", "DB_USER")
        self.c_db_pwd = config.get("CDB", "DB_PWD")
        self.c_db_port = config.getint("CDB", "DB_PORT")
        self.c_db_name = config.get("CDB", "DB_NAME")
        self.m_db_charset = config.get("CDB", "DB_CHARSET")
        self.logs = MessageLogs()
        self.log_no = log_no

    def checkshell(self):
        """
        判断取数脚本是否执行中
        """
        try:
            shl = psutil.pids()
            for pid in shl:
                shl_run = psutil.Process(pid)
                if shl_run.name() == self.x_shell:
                    log_txt = f"[{self.log_no}]脚本{shl_run.name()}正在执行，检测不通过！"
                    print(log_txt)
                    self.logs.message_logs(log_txt, 'datact')
                    return False
                if shl_run.name() == self.x_shell_stock:
                    log_txt = f"[{self.log_no}]脚本{shl_run.name()}正在执行，检测不通过！"
                    print(log_txt)
                    self.logs.message_logs(log_txt, 'datact')
                    return False
            log_txt = f"[{self.log_no}]脚本{self.x_shell}或{self.x_shell_stock}当前未执行，检测通过！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
            return True
        except Exception as e:
            log_txt = f"[{self.log_no}]脚本执行进程判断错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def checkprocess(self):
        """
        判断进程是否执行中
        """
        try:
            pl = psutil.pids()
            for pid in pl:
                pl_run = psutil.Process(pid)
                # self.logs.message_logs(pl_run.name(),'datact')
                if pl_run.name() == self.x_process:
                    pl_cmd = pl_run.cmdline()
                    for pl_x in pl_cmd:
                        if pl_x == self.x_cmd:
                            log_txt = f"[{self.log_no}]正在执行的命令{pl_cmd}中包含{pl_x},检测不通过！"
                            print(log_txt)
                            self.logs.message_logs(log_txt, 'datact')
                            return False
            log_txt = f"[{self.log_no}]datax当前没有执行,检测通过！"
            print(log_txt)
            self.logs.message_logs(log_txt, 'datact')
            return True
        except Exception as e:
            log_txt = f"[{self.log_no}]datax执行进程判断错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def checkupdate_sales(self):
        """
        判断是否执行过销售数据抽取
        """
        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                result = cur.execute("SELECT CHECK_DATE FROM SYADM.SY_V_CRON_DATE WHERE ID = :ID", ID=2)
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "SELECT check_date FROM sy_v_cron_date svcd WHERE ID = %s"
                cur.execute(sql, 2)
                result = cur.fetchall()
                cur.close()
                conn.close()

            for row in result:
                for update_date in row:
                    if update_date.date() < datetime.datetime.now().date():
                        log_txt = f"[{self.log_no}]最近销售抽取日期{update_date.date()},小于当前日期{datetime.datetime.now().date()},销售取数尚未执行，检测通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return True
                    else:
                        log_txt = f"[{self.log_no}]最近销售抽取日期{update_date.date()},大于或等于当前日期{datetime.datetime.now().date()},销售取数已执行，检测不通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return False
        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def checkupdate_stock(self):
        """
        判断是否执行过库存数据抽取
        """
        try:
            if self.m_db_type == 'cx_Oracle':
                connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.m_db_user, self.m_db_pwd, self.m_db_addr,
                                                           self.m_db_port, self.m_db_name)
                connection = cx_Oracle.connect(connect_url)

                cur = connection.cursor()
                result = cur.execute("SELECT CHECK_DATE FROM SYADM.SY_V_CRON_DATE WHERE ID = :ID", ID=4)
            else:
                dbconfig = {
                    "host": self.m_db_addr,
                    "port": self.m_db_port,
                    "user": self.m_db_user,
                    "passwd": self.m_db_pwd,
                    "database": self.m_db_name
                }
                conn = connect(**dbconfig)
                cur = conn.cursor()
                sql = "SELECT check_date FROM sy_v_cron_date WHERE ID = %s"
                cur.execute(sql, 4)
                result = cur.fetchall()
                cur.close()
                conn.close()

            for row in result:
                for update_date in row:
                    if update_date.date() < datetime.datetime.now().date():
                        log_txt = f"[{self.log_no}]最近库存抽取日期{update_date.date()},小于当前日期{datetime.datetime.now().date()},库存取数尚未执行，检测通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return True
                    else:
                        log_txt = f"[{self.log_no}]最近库存抽取日期{update_date.date()},大于或等于当前日期{datetime.datetime.now().date()},库存取数已执行，检测不通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return False
        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def checkdcenter_sales(self):
        """
        判断数据中心昨天销售数据是否准备完成
        """
        try:
            connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.c_db_user, self.c_db_pwd, self.c_db_addr, self.c_db_port,
                                                       self.c_db_name)
            connection = cx_Oracle.connect(connect_url)

            cur = connection.cursor()
            result = cur.execute("SELECT MAX(UP_DATE) FROM qpms.DM_P_JOB_LOG WHERE TAG_TABLE = :TAG_TABLE",
                                 TAG_TABLE='DM_F_SALES_DETAIL_D')
            for row in result:
                for up_date in row:
                    if up_date.date() < datetime.datetime.now().date():
                        log_txt = f"[{self.log_no}]最近销售数据准备日期{up_date.date()},小于当前日期{datetime.datetime.now().date()},昨日数据尚未准备完成，检测不通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return False
                    else:
                        log_txt = f"[{self.log_no}]最近销售数据准备日期{up_date.date()},大于或等于当前日期{datetime.datetime.now().date()},昨日数据准备完成，检测通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return True
        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def checkdcenter_stock(self):
        """
        判断数据中心昨天库存数据是否准备完成
        """
        try:
            connect_url = "{0}/{1}@{2}:{3}/{4}".format(self.c_db_user, self.c_db_pwd, self.c_db_addr, self.c_db_port,
                                                       self.c_db_name)
            connection = cx_Oracle.connect(connect_url)

            cur = connection.cursor()
            result = cur.execute("SELECT MAX(UP_DATE) FROM qpms.DM_P_JOB_LOG WHERE TAG_TABLE = :TAG_TABLE",
                                 TAG_TABLE='DM_F_STOCK_DETAIL_M')
            for row in result:
                for up_date in row:
                    if up_date.date() < datetime.datetime.now().date():
                        log_txt = f"[{self.log_no}]最近库存数据准备日期{up_date.date()},小于当前日期{datetime.datetime.now().date()},昨日库存尚未准备完成，检测不通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return False
                    else:
                        log_txt = f"[{self.log_no}]最近库存数据准备日期{up_date.date()},大于或等于当前日期{datetime.datetime.now().date()},昨日库存准备完成，检测通过！"
                        print(log_txt)
                        self.logs.message_logs(log_txt, 'datact')
                        return True
        except Exception as e:
            log_txt = f"[{self.log_no}]发生数据库操作错误！{e}"
            self.logs.message_logs(log_txt, 'datact')
            raise Exception(log_txt)

    def judge_go(self):
        judge_update_sales = self.checkupdate_sales()
        judge_update_stock = self.checkupdate_stock()
        judge_dcenter_sales = self.checkdcenter_sales()
        judge_dcenter_stock = self.checkdcenter_stock()
        judge_sales = judge_update_sales and judge_dcenter_sales
        judge_stock = judge_update_stock and judge_dcenter_stock
        judge_sales_stock = judge_sales or judge_stock
        judge_shell = self.checkshell()
        judge_process = self.checkprocess()
        # judge_result = judge_shell and judge_process and judge_update and judge_dcenter
        judge_result = judge_shell and judge_process and judge_sales_stock
        if judge_result:
            return {'sales': judge_sales, 'stock': judge_stock}
        else:
            return None
