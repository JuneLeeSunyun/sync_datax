'''
Author: philipdeng
Date: 2021-06-22 15:27:33
LastEditors: philipdeng
LastEditTime: 2021-07-04 15:28:51
FilePath: /datact/datact.py
Description: 
'''
from libs.judgex import JudgeX
from libs.message_logs import MessageLogs
from libs.exec_datax import Exec_Datax
import datetime

log_no = datetime.datetime.now().strftime("%H%M%S")
judgex = JudgeX(log_no)
logs = MessageLogs()
exec_datax = Exec_Datax(log_no)
exec_job = judgex.judge_go()
print(exec_job)

if exec_job:
    # log_txt = f"[{log_no}]所有条件检测通过，符合取数要求！"
    # logs.message_logs(log_txt,'datact')
    # print(log_txt)
    exec_datax.exec_go(exec_job)

else:
    log_txt = f"[{log_no}]存在检测不通过的取数条件，不符合取数要求！"
    logs.message_logs(log_txt,'datact')
    print(log_txt)