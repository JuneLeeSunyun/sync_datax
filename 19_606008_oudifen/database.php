<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;


$db[$active_group]['hostname'] = '10.10.20.26';
$db[$active_group]['username'] = 'rpdbuser';
$db[$active_group]['password'] = 'DScjEVa83l';
$db[$active_group]['database'] = 'report_db';
$db[$active_group]['dbdriver'] = 'mysqli';
$db[$active_group]['dbprefix'] = '';
$db[$active_group]['pconnect'] = FALSE;
$db[$active_group]['db_debug'] = FALSE;
$db[$active_group]['cache_on'] = FALSE;
$db[$active_group]['cachedir'] = '';
$db[$active_group]['char_set'] = 'utf8';
$db[$active_group]['dbcollat'] = 'utf8_general_ci';
$db[$active_group]['swap_pre'] = '';
$db[$active_group]['autoinit'] = TRUE;
$db[$active_group]['stricton'] = FALSE;
// $db[$active_group]['port'] = 1433;

/*
 * report_db账号 : rpdbuser
   report_de账号 : DScjEVa83l
 */
$report_db = 'report_db';

$db[$report_db]['hostname'] = '10.10.20.26';
$db[$report_db]['username'] = 'rpdbuser';
$db[$report_db]['password'] = 'DScjEVa83l';
$db[$report_db]['database'] = 'report_db';
$db[$report_db]['dbdriver'] = 'mysqli';
$db[$report_db]['dbprefix'] = '';
$db[$report_db]['pconnect'] = FALSE;
$db[$report_db]['db_debug'] = TRUE;
$db[$report_db]['cache_on'] = FALSE;
$db[$report_db]['cachedir'] = '';
$db[$report_db]['char_set'] = 'utf8';
$db[$report_db]['dbcollat'] = 'utf8_general_ci';
$db[$report_db]['swap_pre'] = '';
$db[$report_db]['autoinit'] = TRUE;


/*
ORACLE 数据库信息
10.10.20.45  :  1521
sanyun/sanyun2019
dslrqas2
 * */
$report_resource = 'report_resource';
$db[$report_resource]['hostname'] = '10.10.20.245/dslrqas2';
$db[$report_resource]['username'] = 'sanyun';
$db[$report_resource]['password'] = 'sanyun2019';
$db[$report_resource]['database'] = '';
$db[$report_resource]['dbdriver'] = 'oci8';
$db[$report_resource]['dbprefix'] = '';
$db[$report_resource]['pconnect'] = FALSE;
$db[$report_resource]['db_debug'] = TRUE;
$db[$report_resource]['cache_on'] = FALSE;
$db[$report_resource]['cachedir'] = '';
$db[$report_resource]['char_set'] = 'utf8';
$db[$report_resource]['dbcollat'] = 'utf8_general_ci';
$db[$report_resource]['swap_pre'] = '';
$db[$report_resource]['autoinit'] = TRUE;
$db[$report_resource]['stricton'] = FALSE;
//$db[$report_resource]['port'] = 1521;


/* End of file database.php */
/* Location: ./application/config/database.php */
