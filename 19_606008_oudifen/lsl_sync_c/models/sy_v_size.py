'''
Author: philipdeng
Date: 2021-03-20 17:37:36
LastEditors: philipdeng
LastEditTime: 2021-03-25 15:53:39
Description: file content
'''
# coding: utf-8
from sqlalchemy import Column, TIMESTAMP, VARCHAR, Integer, text

from models.base import BaseMixin



class SyVSize(BaseMixin):
    __tablename__ = 'sy_v_size'

    size_group = Column(VARCHAR(200), primary_key=True, nullable=False, comment='尺码组代号')
    size_id = Column(VARCHAR(200), primary_key=True, nullable=False, comment='尺码代码')
    size_desc = Column(VARCHAR(200), nullable=False, comment='尺码描述')
    size_order = Column(Integer, nullable=False, server_default=text("'100'"), comment='序号')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"), comment='时间戳')
