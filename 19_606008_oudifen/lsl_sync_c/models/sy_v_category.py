'''
Author: philipdeng
Date: 2021-03-18 16:52:34
LastEditors: philipdeng
LastEditTime: 2021-03-18 16:56:47
Description: 品类数据模型
'''

from sqlalchemy import Column, SmallInteger, TIMESTAMP, VARCHAR, text
from sqlalchemy.dialects.mysql import TINYINT


from models.base import BaseMixin




class SyVCategory(BaseMixin):
    __tablename__ = 'sy_v_category'

    ic_id = Column(VARCHAR(200), primary_key=True, comment='品类代号')
    ic_name = Column(VARCHAR(200), nullable=False, comment='品类名称')
    ic_code = Column(VARCHAR(200), nullable=False, comment='品类代码')
    class_id = Column(VARCHAR(40), comment='大类id')
    ic_prop = Column(TINYINT, nullable=False, server_default=text("'2'"), comment='品类属性：1配件,2正品,3其他')
    ic_order = Column(SmallInteger, nullable=False, server_default=text("'100'"), comment='序号')
    ic_state = Column(TINYINT, nullable=False, server_default=text("'1'"), comment='状态：0停止、1正常')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"), comment='时间戳')
