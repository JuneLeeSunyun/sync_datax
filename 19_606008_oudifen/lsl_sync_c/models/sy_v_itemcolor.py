'''
Author: philipdeng
Date: 2021-03-20 17:39:08
LastEditors: philipdeng
LastEditTime: 2021-03-24 16:39:14
Description: file content
'''
# coding: utf-8
from sqlalchemy import Column, TIMESTAMP, VARCHAR, text

from models.base import BaseMixin



class SyVItemcolor(BaseMixin):
    __tablename__ = 'sy_v_itemcolor'
    __table_args__ = {'comment': '款号颜色匹配信息表'}

    item_id = Column(VARCHAR(200), primary_key=True, nullable=False, comment='商品货号')
    color_id = Column(VARCHAR(200), primary_key=True, nullable=False, comment='颜色代号')
    item_picture = Column(VARCHAR(256), nullable=False, comment='图片地址')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
