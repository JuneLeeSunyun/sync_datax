'''
Author: philipdeng
Date: 2021-03-20 17:37:05
LastEditors: philipdeng
LastEditTime: 2021-03-25 15:48:51
Description: file content
'''
# coding: utf-8
from sqlalchemy import Column, Date, Integer, SmallInteger, VARCHAR, TIMESTAMP, text


from models.base import BaseMixin


class SyVShop(BaseMixin):
    __tablename__ = 'sy_v_shop'
    __table_args__ = {'comment': '店铺资料表'}

    shop_code = Column(VARCHAR(16), primary_key=True, comment='店铺代号')
    shop_name = Column(VARCHAR(200), nullable=False, comment='店铺名称')
    model_no = Column(VARCHAR(16), nullable=False, server_default=text("''"))
    ref_shopcode = Column(VARCHAR(200), nullable=False, server_default=text("''"), comment='参考店铺')
    jm_code = Column(VARCHAR(200), nullable=False, server_default=text("''"))
    shop_level = Column(VARCHAR(200), nullable=False, server_default=text("''"), comment='店铺等级')
    branch_id = Column(VARCHAR(200), nullable=False, server_default=text("''"), comment='品牌代号')
    open_date = Column(Date, nullable=False, server_default=text("'1970-01-01'"), comment='开店日期')
    close_date = Column(Date, nullable=False, server_default=text("'2099-12-31'"), comment='闭店日期')
    shop_area = Column(Integer, nullable=False, server_default=text("'0'"), comment='店铺面积')
    stand_staffs = Column(Integer, nullable=False, server_default=text("'0'"), comment='店铺编制人数')
    real_staff = Column(Integer, nullable=False, server_default=text("'0'"), comment='实际人数')
    max_sku = Column(SmallInteger, nullable=False, server_default=text("'0'"), comment='sku上限')
    min_sku = Column(SmallInteger, nullable=False, server_default=text("'0'"), comment='sku下限')
    max_qtys = Column(Integer, nullable=False, server_default=text("'0'"), comment='铺场数上限')
    min_qtys = Column(Integer, nullable=False, server_default=text("'0'"), comment='铺场数下限')
    tel_no = Column(VARCHAR(50), nullable=False, server_default=text("''"), comment='联系电话')
    addr = Column(VARCHAR(64), nullable=False, server_default=text("''"), comment='店铺地址')
    contact = Column(VARCHAR(16), nullable=False, server_default=text("''"), comment='联系人')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"), comment='时间戳')
    shop_state = Column(Integer, nullable=False, server_default=text("'1'"), comment='营业状态：1营业中 2闭店 3整顿')
