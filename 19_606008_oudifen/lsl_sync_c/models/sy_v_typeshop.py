'''
Author: philipdeng
Date: 2021-03-26 10:20:35
LastEditors: philipdeng
LastEditTime: 2021-03-26 10:28:29
Description: 店铺类型明细模型
'''

from sqlalchemy import Column, Integer, VARCHAR


from models.base import BaseMixin


class SyVTypeshop(BaseMixin):
    __tablename__ = 'sy_v_typeshop'
    __table_args__ = {'comment': '店铺类型明细表'}

    ts_id = Column(Integer, primary_key=True, comment='ID')
    st_code = Column(VARCHAR(200), index=True, comment='类型代号')
    shop_code = Column(VARCHAR(16), index=True)
