'''
Author: philipdeng
Date: 2021-02-26 17:25:25
LastEditors: philipdeng
LastEditTime: 2021-03-16 16:33:04
Description: SQLAlchemy基类
'''
from sqlalchemy.ext.declarative import declarative_base

from libs.read_conf import ReadConf

Base = declarative_base()


class BaseMixin(Base):
    __abstract__ = True
