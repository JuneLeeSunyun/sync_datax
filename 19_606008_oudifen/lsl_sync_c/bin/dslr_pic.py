'''
Author: philipdeng
Date: 2021-03-26 17:02:23
LastEditors: philipdeng
LastEditTime: 2021-03-29 19:23:31
Description: 获取都市丽人图片并写入数据库主体程序
'''
import math

from libs.read_conf import ReadConf
from libs.dynamic_db import DbConnect
from models.sy_v_item import SyVItem
from spider.get_pic_url import GetPicUrl
from libs.message_logs import MessageLogs

class DslrPic():
    def dslr_pic(self):
        config = ReadConf().read_conf()
        pic_url = GetPicUrl()
        logs = MessageLogs()
        page_size = config.getint("DB_SET","PIC_PAGE")
        rows = SyVItem().get_row()
        if rows:
            page_indexs = math.ceil(rows/page_size) + 1
            log_txt_star = f"共{page_indexs -1}批次数据"
            all_update = 0
            for page_index in range(1,page_indexs):
                log_txt = f"{log_txt_star},正在处理第{page_index}批次数据"
                item_lists = SyVItem().get_item(page_index,page_size)
                if item_lists:
                    item_list = [item_list.item_id for item_list in item_lists]
                    pic_url_list = pic_url.get_pic_url(item_list)
                    if pic_url_list:
                        update_result = SyVItem().update_pic_url(pic_url_list)
                        all_update = all_update + update_result.get('update_count')
                        log_txt = f"{log_txt}:{update_result.get('log_txt')},本批次共更新{update_result.get('update_count')}行，总更新{all_update}"
                        print(log_txt)
                        logs.message_logs(log_txt,'lsl_sync_c')
                    else:
                        log_txt = f"{log_txt}:所返回数据为空!"
                        logs.message_logs(log_txt,'lsl_sync_c')
                else:
                    log_txt = log_txt = f"{log_txt}:无款号资料!"
                    logs.message_logs(log_txt,'lsl_sync_c')
