'''
Author: philipdeng
Date: 2021-03-26 14:50:11
LastEditors: philipdeng
LastEditTime: 2021-03-26 14:52:38
Description: 清理缓存
'''
from spider.delete_cache import DeleteCache

delete_cache = DeleteCache()
delete_cache.clean_cache()