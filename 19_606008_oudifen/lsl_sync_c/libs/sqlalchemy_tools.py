'''
Author: philipdeng
Date: 2021-02-28 22:35:32
LastEditors: philipdeng
LastEditTime: 2021-03-22 17:23:04
Description: SQLAlchemy处理工具
'''
import datetime,json,decimal

class QueryToDict():
    '''
    SQLAlchemy处理工具：

    '''

    
    def column_all(self,all_one_row):
        '''
        将SQLAlchemy查询所有字段，一行记录的结果转换为字典
        '''
        dict_column_all = all_one_row.__dict__
        if "_sa_instance_state" in dict_column_all:
            del dict_column_all["_sa_instance_state"]
        return dict_column_all

    def column_part(self,part_one_row):
        '''
        将SQLAlchemy查询的部分字段，一行记录的结果转换为字典
        '''
        part_one_row = part_one_row
        dict_column_part = dict([(key,getattr(part_one_row,key)) for key in part_one_row.keys()])
        return dict_column_part
            
    def list_to_dict(self,result_list):
        '''
        将SQLAlchemy查询多行记录的结果转换为字典
        '''
        result_list = result_list
        result_list_dict =[]
        for resutl_object in result_list:
            if hasattr(resutl_object, 'keys'): 
                result_list_dict.append(self.column_part(resutl_object))
            else:
                result_list_dict.append(self.column_all(resutl_object))
        return  result_list_dict

    
    def query2dict(self,query_result):
        '''
        自动将SQLAlchemy查询结果转换为字典
        '''
        if isinstance(query_result,list):
            data_dict = self.list_to_dict(query_result)
        elif hasattr(query_result, 'keys'):
            data_dict = self.column_part(query_result)
        else:
            data_dict = self.column_all(query_result)
        return data_dict



class SuperEncoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, decimal.Decimal):
                return float(o)
            if isinstance(o, datetime.datetime):
                return o.strftime('%Y-%m-%d %H:%M:%S')  
            elif isinstance(o, datetime.date):
                return o.strftime('%Y-%m-%d')          
            else:
                return json.JSONEncoder.default(self,o)
