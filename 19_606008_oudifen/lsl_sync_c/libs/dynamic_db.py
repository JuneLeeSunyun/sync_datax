'''
Author: philipdeng
Date: 2021-03-15 21:15:06
LastEditors: philipdeng
LastEditTime: 2021-03-16 16:13:00
Description: 动态配置数据库链接
'''

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sqlalchemy.exc as sqlalchemy_exc

from libs.read_conf import ReadConf

class DbConnect():
    '''
    生成动态数据链接
    '''
    
    def __init__(self):
        config = ReadConf().read_conf()
        self.db_type = config.get("DB","DB_TYPE")
        self.db_addr = config.get("DB","DB_ADDR")
        self.db_user = config.get("DB","DB_USER")
        self.db_pwd = config.get("DB","DB_PWD")
        self.db_port = config.get("DB","DB_PORT")
        self.db_name = config.get("DB","DB_NAME")
        self.db_charset = config.get("DB","DB_CHARSET")
        self.sql_scripts = config.getboolean("DB_SET","SQL_SCRIPTS")

            
    def set_session(self, **param):
        '''
        创建session，用于SQLAlchemy
        '''
        for key,value in param.items():
            if hasattr(self, key):
                setattr(self, key, value)
        connect_url = "{0}://{1}:{2}@{3}:{4}/{5}{6}".format(self.db_type, self.db_user, self.db_pwd, self.db_addr, self.db_port, self.db_name, self.db_charset)
        try:
            engine = create_engine(connect_url, echo=self.sql_scripts)
            db_Session = sessionmaker(bind=engine)
            session = db_Session()
            # result = session.execute("select 1") 
            result = session.connection()
        except sqlalchemy_exc.OperationalError as e:
            raise (f"数据库链接错误，请检查数据库配置是否正确.错误信息：{e}")
        
        return session