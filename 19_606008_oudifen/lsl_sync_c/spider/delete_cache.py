'''
Author: philipdeng
Date: 2021-03-26 11:05:12
LastEditors: philipdeng
LastEditTime: 2021-03-26 15:07:28
Description: 缓存删除
'''
import requests,json

from libs.read_conf import ReadConf
from spider.login import LoginSys
from libs.message_logs import MessageLogs

# from cyberbrain import trace

class DeleteCache():
    '''
    删除客户缓存
    '''
    def __init__(self):
        self.logs = MessageLogs()

    # @trace
    def delete_cache(self,api_url,token,com_id):
        url = f"{api_url}/v1/receive/delete_cache"
        headers = {'Content-Type':'application/json','SY-ACCESS-TOKEN':token}
        cache_code = f"cc{str(com_id)}"
        data = {'cache_code':cache_code}
        try:
            get_data = requests.post(url=url, data=json.dumps(data),headers=headers)
            
            data_list = get_data.json()
            log_txt = f"{data_list.get('msg')},{data_list.get('request')}"
            print(log_txt)
            self.logs.message_logs(log_txt,'lsl_sync_c')
            return data_list

        except Exception as e:
            log_txt = e
            print(log_txt)
            self.logs.message_logs(log_txt,'lsl_sync_c')

    def clean_cache(self):
        login_info = LoginSys().login_info()
        api_url = login_info.get('api_url')
        token = login_info.get('token')
        com_id = login_info.get('com_id')
        self.delete_cache(api_url,token,com_id)
    