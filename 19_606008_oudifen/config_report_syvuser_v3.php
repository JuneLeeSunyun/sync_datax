<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
$config['cid'] = 19;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['plan_select_days'] = '2'; //1为默认，2为当月


/*
 * 条码资料
 * SELECT BAO_NO, ITEM_ID, COLOR_ID, COLOR_NAME, SIZE_ID, CUP_ID FROM SY_V_BARCODE;
 * */
$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
    'table' => 'qpms.sy_v_barcode',
    'columns' => array(
        'bar_no' => 'bao_no',	//有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'color_name' => 'color_name',
        'size_id' => 'size_id',
        'cup_id' => 'cup_id',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('bar_no'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 品类资料
 * select ic_id,ic_name, NVL(class_id, 0) ,substr(ic_id, 4, 6) from sy_v_category order by class_id , ic_id ;
 */
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
    'table' => 'qpms.sy_v_category',
    'columns' => array(
        'ic_id' => 'ic_id',
        'ic_name' => "ic_name",
        'ic_code' => "ic_id",
        'class_id' => 'NVL(class_id, 0)',
        'ic_prop' => '2', //品类属性：1.配件；2.正品；3.其他
        'ic_order' => "substr(ic_id, 4, 6)"
    ),
    'join' => array(),
    'where' => 'ic_id is not null ',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('ic_id'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('ic_name', 'ic_code', 'class_id'),
    //'another_resource' => 'category_qt'	//同库其它数据源
);


/*
 * 大类资料
 * SELECT class_id, class_name, substr(class_id, 3, 3) FROM sy_v_item_class
 */
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
    'table' => 'qpms.sy_v_item_class',
    'columns' => array(
        'class_id' => 'class_id',
        'class_name' => 'class_name',
        'class_code' => 'class_id',
        'class_order' => "substr(class_id, 3, 3)",
        'class_state' => "'1'",
    ),
    'join' => array(),
    'where' => 'class_id is not null ',
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('class_id'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('class_name', 'class_code'),
    //'another_resource' => 'category_qt'	//同库其它数据源
);


/*
 * 尺码资料
 * select size_group,size_id,size_desc from sy_v_size
 */
$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
    'table' => 'qpms.sy_v_size',
    'columns' => array(
        'size_group' => 'size_group',
        'size_id' => 'size_id',
        'size_desc' => 'size_desc',
        'size_order' => '1'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('size_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 颜色资料
 * select color_id, color_desc from sy_v_color
 */
$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
    'table' => 'qpms.sy_v_color',
    'columns' => array(
        'color_id' => 'color_id',
        'color_desc' => 'color_desc'
    ),
    'join' => array(),
    'where' => '',
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 罩杯资料
 * select cup_id, cup_desc from sy_v_cup
 */
$sy_v_color = 'sy_v_cup';
$config[$sy_v_color] = array(
    'table' => 'qpms.sy_v_cup',
    'columns' => array(
        'cup_id' => 'cup_id',
        'cup_desc' => 'cup_desc'
    ),
    'join' => array(),
    'where' => 'cup_id is not null ',
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('cup_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 季节资料
 * select sea_id,sea_name, (case sy_v_sea.sea_name
				when '四季' then '01'
				when '春夏' then '02'
				when '秋冬' then '08'
				when '夏' then '05'
				else '01' end
			) as s_mon, (case sy_v_sea.sea_name
				when '四季' then '12'
				when '春夏' then '07'
				when '秋冬' then '01'
				when '夏' then '07'
				else '12' end
			) as e_mon, (case sy_v_sea.sea_name
			when '四季' then '1'
			when '春夏' then '1'
			when '秋冬' then '1'
			when '夏' then '1'
			else '0' end) as is_span_sea from sy_v_sea
 */
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'qpms.sy_v_sea',//M_PRODUCT
    'columns' => array(
        'sea_id'=> 'sea_id',
        'sea_name' =>'sea_name',//char_length
        'sea_desc'=>'sea_name',
        'sea_order'=>'1',
        's_mon' => "(case sy_v_sea.sea_name
				when '四季' then '01'
				when '春夏' then '02'
				when '秋冬' then '08'
				when '夏' then '05'
				else '01' end
			)",
        'e_mon' => "(case sy_v_sea.sea_name
				when '四季' then '12'
				when '春夏' then '07'
				when '秋冬' then '01'
				when '夏' then '07'
				else '12' end
			)",
        'is_span_sea' => "(case sy_v_sea.sea_name
			when '四季' then '1'
			when '春夏' then '1'
			when '秋冬' then '1'
			when '夏' then '1'
			else '0' end)"
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    //'group_by' => array(''),
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('sea_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);
//商品资料
$config['sea_span'] ="'102' ";    	//跨年季度id or sea_id = '5' or sea_id = '6' or sea_id = '7'
$config['year_offset'] = array('s' => 0, 'e' => '1');	//跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(		//季度具体时间段
    "'100'" => array('from' => '01', 'to' => '12'),
    "'101'" => array('from' => '02', 'to' => '07'),
    "'102'" => array('from' => '08', 'to' => '01'),
    "'104'" => array('from' => '02', 'to' => '05'),
);


/*
 * 商品信息
 * select item_id,year_id,sea_id,sea_name,cost,ic_id,ic_name,sale_price,item_desc,pub_date from sy_v_item
 *  SELECT item_id,year_id,sea_id,sea_name,ic_id,ic_name,sale_price,item_desc, substr(pub_date, 0, 4) as y, substr(pub_date, 5, 2) as m, substr(pub_date, 7, 2) as d FROM sy_v_item
 */
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
    'table' => 'qpms.sy_v_item',
    'columns' => array(
        'item_id' => 'item_id',
        'item_key' => 'item_id', //item_key
        'brand_id' => "'Ordifen'",
        'year_id' => 'year_id',
        'sea_id' => 'sea_id',
        'sea_name' => 'sea_name',
        'cost' => "'0'",
        'ic_id' => 'ic_id',
        'ic_name' =>'ic_name',
        'is_acc' => "'N'",
        'sale_price' => 'sale_price',
        'item_desc' => 'item_desc',
        'sales_point' => "' '",
        'item_file' => "item_id||'.jpg'",
        'info_url' => "' '",
        'style_id' => "' '",
        's_date' => "' '",
        'e_date' => "' '",
        'pub_date' => "(case when pub_date='00000000' then 0 else TO_NUMBER((to_date(pub_date, 'YYYY-MM-DD HH24:mi:ss') - TO_DATE('19700101 08:00:00', 'YYYY-MM-DD HH24:mi:ss')) * 86400) end)",
        //'pub_date' => "substr(pub_date, 0, 4)||'-'||substr(pub_date, 5, 2)||'-'||substr(pub_date, 7, 2)",  //'_'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id'),
    'duplicate' => array('item_key', 'year_id', 'sea_id', 'sea_name', 'ic_id', 'ic_name', 'sale_price', 'item_desc', 's_date', 'e_date', 'pub_date'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 款号颜色
 * select item_id, color_id from sy_v_itemcolor
 * */
$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
    'table' => 'qpms.sy_v_itemcolor',
    'columns' => array(
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'item_picture' => "item_id||'_'||color_id||'.jpg'"
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'color_id'),
    'duplicate' => array('item_picture'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 店铺类型
 * select * from sy_v_shop_type
 * */
$sy_v_shop_type = 'sy_v_shop_type';
$config[$sy_v_shop_type] = array(
    'table' => 'qpms.sy_v_shop_type',
    'columns' => array(
        'st_code' => 'st_code',
        'st_name' => 'st_name',
        'st_type_code' => 'st_type_code',
        'st_type_name' => 'st_type_name',
        'st_order_seq' => 'substr(st_code, 4, 6)',
        'st_state' => '1',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型明细
 * select * from sy_v_typeshop
 */
$sy_v_typeshop = 'sy_v_typeshop';
$config[$sy_v_typeshop] = array(
    'table' => 'qpms.sy_v_typeshop',
    'columns' => array(
        'st_code' => 'st_code',
        'shop_code' => 'shop_code',
    ),
    'join' => array(),
    'where' => 'st_code is not null ',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code', 'shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);



/*
 * 店铺资料
 * select shop_code, shop_name, open_date, close_date, shop_area, tel_no, addr, contact, shop_state from sy_v_shop
 */
$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
    'table' => 'qpms.sy_v_shop',
    'columns' => array(
        'shop_code' => 'shop_code',
        'shop_name' => 'shop_name',
        'model_no' => "''",
        'ref_shopcode' => 'shop_code',
        'shop_level' => "''",
        'open_date' => "to_char(open_date, 'yyyy-mm-dd')",
        'close_date' => "to_char(close_date, 'yyyy-mm-dd')",
        'shop_area' => 'shop_area',//'rb.area',
        'stand_staffs' => '1',
        'real_staff' => '1',
        'max_sku' => "''",
        'min_sku' => "''",
        'max_qtys' => "''",
        'min_qtys' => "''",
        'tel_no' => 'tel_no',
        'addr' => 'addr',
        'contact' => 'contact',
        'shop_state' => 'shop_state'
    ),
    'join' => array(
    ),
    'where' => '',//"rcd.ZG_status <> '已撤店'",
    'date_key' => '',
    'group_by' => '',
    //'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),
    'duplicate' => array('shop_name', 'open_date', 'close_date', 'shop_area', 'tel_no', 'tel_no', 'addr', 'contact', 'shop_state'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'shop_people_count'
);


/*
 * 库存信息
 * select shop_code,item_id,color_id,size_id,stock_qtys from sy_v_stock
 * */
$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
    'table' => 'qpms.sy_v_stock',
    'columns' => array(
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'stock_qtys' => 'stock_qtys'
    ),
    'join' => array(),
    'where' => '', //stock_qtys > 0
    'group_by' => '',
    'date_key' => '',
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);


/*
 * 销售主表
 *
  select distinct inv_no, shop_code, to_char(sale_date,'yyyy-mm-dd'), listagg(staff_id,',') within group(order by staff_id) , vip_state, vip_no , INV_TYPE,
	sum(INV_QTYS) as INV_QTYS , sum(INV_MONEY) as INV_MONEY , sum(TAG_MONEY) as TAG_MONEY ,
	SUBSTR (listagg(staff_id,',') within group(order by staff_id), 1, INSTR (listagg(staff_id,',') within group(order by staff_id), ',', 1, 1) - 1)
	from SY_V_SALES_INFO where
	-- inv_no in ('710ZD28710ZD280002130905',  '200YA39200YA390003129691' , '200YA39200YA390003129693')
	 to_char(SALE_DATE, 'yyyy-mm-dd') >= '2019-11-01' and to_char(SALE_DATE, 'yyyy-mm-dd') < '2019-11-16'
	GROUP BY inv_no, shop_code, to_char(sale_date,'yyyy-mm-dd'), vip_state, vip_no , INV_TYPE;
 *
 * select inv_no, inv_type, shop_code, sale_date, vip_state, staff_id, vip_no, inv_qtys, inv_money, tag_money, to_char(sale_date, 'yyyy-mm-dd hh24:mi:ss') from sy_v_sales_info
where ROWNUM<2 and to_char(sale_date, 'yyyy-mm-dd') >= '2018-01-01' and to_char(sale_date, 'yyyy-mm-dd') < '2019-10-01'
 */
$sales_info = 'sales_info';
$config[$sales_info] = array(
    'table' => 'qpms.sy_v_sales_info',
    'columns' => array(
        'inv_no' => 'distinct inv_no',
        'inv_type' => 'inv_type',
        'shop_code' => 'shop_code',
        'sale_date' => "to_char(sale_date,'yyyy-mm-dd')",
        'vip_state' => 'vip_state',
        'staff_id' => "	NVL( SUBSTR(listagg(staff_id,',') within group(order by staff_id), 1, INSTR (listagg(staff_id,',') within group(order by staff_id), ',', 1, 1) - 1), listagg(staff_id,',') within group(order by staff_id))",
        'vip_no' => 'vip_no',
        'inv_qtys' => 'sum(INV_QTYS)',
        'inv_money' => 'sum(INV_MONEY)',
        'tag_money' => 'sum(TAG_MONEY)'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => "to_char(sale_date, 'yyyy-mm-dd')", //TO_CHAR(TO_DATE(M_RETAIL.BILLDATE,'YYYY-MM-DD'), 'YYYY-MM-DD')
    'group_by' => "inv_no, shop_code, to_char(sale_date,'yyyy-mm-dd'), vip_state, vip_no , inv_type",
    //'by_shop' => true,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'sales_info_o2o'
);



/*
 * 销售明细
 * select inv_no, item_id, color_id, size_id, inv_qtys, inv_money, tag_price from sy_v_sales_detail
where ROWNUM<100000 and to_char(sale_date, 'yyyy-mm-dd') >= '2019-10-01' and to_char(sale_date, 'yyyy-mm-dd') < '2019-11-01'

 * select sy_v_sales_detail.inv_no, sy_v_sales_detail.item_id, sy_v_sales_detail.color_id, sy_v_sales_detail.size_id, sy_v_sales_detail.inv_qtys, sy_v_sales_detail.inv_money, sy_v_sales_detail.tag_price from sy_v_sales_detail
left join sy_v_sales_info on sy_v_sales_info.inv_no = sy_v_sales_detail.inv_no
where ROWNUM<100000 and to_char(sy_v_sales_info.sale_date, 'yyyy-mm-dd') >= '2019-10-01' and to_char(sy_v_sales_info.sale_date, 'yyyy-mm-dd') < '2019-11-01'
 */
$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
    'table' => 'qpms.sy_v_sales_detail',
    'columns' => array(
        'inv_no' => 'inv_no',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'inv_qtys' => 'inv_qtys',
        'inv_money' => 'inv_money',
        'tag_price' => 'tag_price'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => "to_char(sale_date, 'yyyy-mm-dd')",
    'group_by' => '',
    //'by_shop' => true,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('inv_no', 'item_id', 'color_id', 'size_id'), //, 'color_id', 'size_id'
    'duplicate' => '',//array('inv_qtys', 'inv_money', 'tag_price'),
    'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
    //'another_resource' => 'sales_detail_o2o'
);



/*
 * 会员资料
 * select vip_no,cust_name,vip_level,shop_code,register_date,cust_sex,born_date,mobile,vip_state, substr(born_date, 0, 4)||'-'||substr(born_date, 5, 2)||'-'||substr(born_date, 7, 2)
from sy_v_vip
where ROWNUM<100000
 * */
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
    'table' => 'qpms.sy_v_vip',
    'columns' => array(
        'vip_no' => 'vip_no',
        'cust_name' => 'cust_name',
        'shop_code' => 'shop_code',
        'cust_sex' => 'cust_sex',
        'age' => "' '",
        'born_date' => 'born_date', //格式转换，暂时需要加上
        'register_date' => "to_char(register_date, 'YYYY-MM-DD HH24:mi:ss')",    //格式转换，暂时需要加上 C_VIP.OPENCARDDATE
        'vip_level' => 'vip_level',
        'mobile' => 'mobile',
        'address' => "''",
        'staff_id' => "''",
        'vip_state' => 'vip_state',
        'active_money' => '0',
        'vip_points' => '0'
    ),
    'join' => array(),
    'where' => '',
    'shop_code' => 'shop_code',
    'date_key' => '',
    'sy_key' => array('vip_no'),
    //'duplicate' => array('cust_name', 'shop_code', 'cust_sex', 'born_date', 'register_date', 'vip_level', 'mobile', 'address', 'staff_id', 'vip_points'),    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'duplicate' => '',    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'by_shop' => false,
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


/*
 * 导购资料
 * select staff_id,staff_name,staff_shop_code,staff_shop_name
 from sy_v_staff
 *
$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
    'table' => 'qpms.sy_v_staff',
    'columns' => array(
        'staff_id' => 'staff_id',
        'staff_name' => 'staff_name',
        'staff_phone' => 'staff_name', //staff_phone
        'staff_shop_code' => 'staff_shop_code',
        'staff_shop_name' => 'staff_shop_name',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('staff_id'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('staff_name', 'staff_phone', 'staff_shop_code','staff_shop_name')
);
 */

/*
 * 店铺商品调拨表
 *  select stock_date,shop_code,item_id,color_id,size_id,stock_qtys,stock_money
 from sy_v_shopstock
 where ROWNUM<100000 and to_char(stock_date, 'yyyy-mm-dd hh24:mi:ss') >= '2019-11-01' and to_char(stock_date, 'yyyy-mm-dd hh24:mi:ss') < '2019-11-08'
 * */
$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
    'table' => 'qpms.sy_v_shopstock',
    'columns' => array(
		'inv_no' => 'inv_no',
        'stock_date' => "to_char(stock_date, 'yyyy-mm-dd')",
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'stock_qtys' => 'stock_qtys',
        'stock_money' => 'stock_money'
    ),
    'join' => array(),
    'where' => "SUBSTR(inv_no,0,1) in ('Q','S')",
    'group_by' => '',
    'date_key' => "to_char(stock_date, 'yyyy-mm-dd')",
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('inv_no', 'stock_date', 'shop_code', 'item_id', 'color_id', 'size_id'),
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
 * 在途库存信息
 * select shop_code,item_id,color_id,size_id,instock_qtys from qpms.sy_v_instock
 * */
$sy_v_instock = 'sy_v_instock';
$config[$sy_v_instock] = array(
    'table' => 'qpms.sy_v_instock',
    'columns' => array(
        'shop_code' => 'shop_code',
        'item_id' => 'item_id',
        'color_id' => 'color_id',
        'size_id' => 'size_id',
        'instock_qtys' => 'instock_qtys'
    ),
    'join' => array(),
    'where' => 'instock_qtys > 0',
    'group_by' => '',
    'date_key' => '',
    //'by_shop' => false,
    'shop_code' => 'shop_code',
    'by_limit' => true,
    'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), //array('shop_code', 'sy_v_item.item_id', 'color_id', 'size_id')
    'duplicate' => '', //array('stock_qtys'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//处理：备份表数据
$config['insert_data_sy_v_category'] = '';
$config['insert_data_sy_v_item'] = '';
$config['update_item_date'] = 'update_item_date';  //update_item_date
$config['update_sum_sales_info'] = '';

$config['query_before_sync_to_1'] = '';
$config['query_before_sync_to_2'] = '';
$config['query_before_sync_to_3'] = '';
$config['query_before_sync_to_4'] = '';
$config['query_before_sync_to_5'] = '';
$config['query_before_sync_to_6'] = '';
$config['query_before_sync_to_7'] = '';
$config['query_before_sync_to_8'] = '';

//$config['query_after_sync_to_1'] = 'insert IGNORE into sy_v_color (color_id, color_desc) SELECT CONCAT(color_id, cup_id) as color_id, CONCAT(color_desc, cup_desc) as color_desc FROM data_sy_v_color, sy_v_cup;'; //insert IGNORE into sy_v_color (color_id, color_desc) SELECT CONCAT(color_id, cup_id) as color_id, CONCAT(color_desc, cup_desc) as color_desc FROM data_sy_v_color, sy_v_cup
$config['query_after_sync_to_1'] = '';
$config['query_after_sync_to_2'] = '';
$config['query_after_sync_to_3'] = '';
$config['query_after_sync_to_4'] = '';
$config['query_after_sync_to_5'] = '';
$config['query_after_sync_to_6'] = '';
$config['query_after_sync_to_7'] = '';
$config['query_after_sync_to_8'] = '';

/* End of file config.php */
/* Location: ./application/config/config.php */
