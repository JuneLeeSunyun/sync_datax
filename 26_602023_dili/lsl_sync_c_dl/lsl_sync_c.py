'''
Author: philipdeng
Date: 2021-03-15 11:48:57
LastEditors: philipdeng
LastEditTime: 2021-03-26 14:59:04
Description: file content
'''
from spider.lsl_sync import LslSync
from spider.default_group import DefaultGroup
from spider.delete_cache import DeleteCache


com_info = LslSync()
com_info.sync_go()
default_group = DefaultGroup()
default_group.sync_dg()
delete_cache = DeleteCache()
delete_cache.clean_cache()
