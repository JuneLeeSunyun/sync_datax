'''
Author: philipdeng
Date: 2021-03-15 23:08:04
LastEditors: philipdeng
LastEditTime: 2021-03-17 11:07:34
Description: file content
'''

from sqlalchemy import Column, DECIMAL, Date, Integer, SmallInteger, TIMESTAMP, text,VARCHAR,CHAR
from sqlalchemy.dialects.mysql import VARCHAR
import sqlalchemy.exc as sqlalchemy_exc


from models.base import BaseMixin
from libs.read_conf import ReadConf
from libs.dynamic_db import DbConnect




class SyVItemTemp(BaseMixin):
    __tablename__ = 'sy_v_item_temp'
    __table_args__ = {'prefixes': ['TEMPORARY']}

    item_id = Column(VARCHAR(20), primary_key=True, comment='商品货号')
    item_file = Column(VARCHAR(256), primary_key=True,  comment='图片路径')

