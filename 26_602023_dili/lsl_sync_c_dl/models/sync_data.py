'''
Author: philipdeng
Date: 2021-03-18 16:57:20
LastEditors: philipdeng
LastEditTime: 2021-05-30 16:20:23
Description: 数据同步模型
'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import literal,func

import sqlalchemy.exc as sqlalchemy_exc
import datetime,time

from models.sy_v_category import SyVCategory
from models.sy_v_sea import SyVSea
from models.sy_v_item import SyVItem
from models.sy_v_size import SyVSize
from models.sy_v_color import SyVColor
from models.sy_v_itemcolor import SyVItemcolor
from models.sy_v_shop import SyVShop
from models.sy_v_shop_type import SyVShopType
from models.sy_v_typeshop import SyVTypeshop 
from libs.dynamic_db import DbConnect
from libs.read_conf import ReadConf
from libs.sqlalchemy_tools import QueryToDict

# from cyberbrain import trace



class SyncData():

    def __init__(self):
        self.config = ReadConf().read_conf()
        self.page_size = self.config.getint("DB_SET","PAGE_SIZE")
        self.object_table = self.config.getint("DB_SET","PAGE_SIZE")
        self.to_dict = QueryToDict()


    def get_model(self,table_name):
        model_list = {'sy_v_category' : SyVCategory,'sy_v_sea': SyVSea,'sy_v_item' : SyVItem,
                      'sy_v_size':SyVSize,'sy_v_color':SyVColor,'sy_v_itemcolor':SyVItemcolor,
                      'sy_v_shop':SyVShop, 'sy_v_shop_type':SyVShopType, 'sy_v_typeshop':SyVTypeshop}
        model = model_list.get(table_name)
        return(model)

    def get_rows(self,table_name):
        '''
        获取页数
        '''
        model = self.get_model(table_name)
        db_session = DbConnect()
        session = db_session.set_session()

        try:
            rows = session.query(model).count()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'获取行数失败！{e}'
            return {'log_txt':log_txt,'rows':0}
        else:
            session.close()
            log_txt = f'共获取{rows}行资料。'
            return {'log_txt':log_txt,'rows':rows}


    def get_category(self,page_index,com_id):
        '''
        品类资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVCategory.ic_id.label('cat_code'),SyVCategory.ic_name.label('cat_name'),
                        SyVCategory.ic_order.label('cat_order_seq'))\
                    .filter().order_by(SyVCategory.ic_id)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}


    def get_sea(self,page_index,com_id):
        '''
        季度资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVSea.sea_id,SyVSea.sea_name,
                        SyVSea.sea_order.label('sea_order_seq'))\
                    .filter().order_by(SyVSea.sea_id)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}

    # @trace
    def get_item(self,page_index,com_id):
        '''
        货品资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVItem.item_id,SyVItem.ic_id.label('cat_code'),SyVItem.ic_name.label('cat_name'),literal(' ').label('ser_id'),
                                      literal('').label('ser_name'),SyVItem.sale_price,SyVItem.item_key,SyVItem.item_desc,
                                      SyVItem.item_desc.label('item_text'),SyVItem.s_date.label('pub_date'),SyVItem.year_id.label('item_year'),
                                      SyVItem.sea_id,SyVItem.sea_name,literal(1).label('use_state'),SyVItem.item_file,
                                      literal(999).label('item_order'),literal(' ').label('item_url'),func.now().label('create_time'))\
                                     .filter().order_by(SyVItem.item_id)\
                                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'货品资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            # data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            result_list = []
            for data in data_list:
                try:
                    pub_date_t = datetime.datetime.combine(data.get('pub_date'),datetime.time(00,00,00))
                except  (Exception) as e:
                    str_date = '1970-01-01'
                    pub_date_t = datetime.datetime.strptime(str_date, '%Y-%m-%d')
                data['pub_date'] = int(time.mktime(pub_date_t.timetuple()))
                if data.get('item_file')[0:4] != 'http':
                    data['item_file'] = f"c{str(com_id)}/spc/{data.get('item_file')}"
                result_list.append(data)
            data_list = result_list
            data_count = len(result_list)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}


    def get_size(self,page_index,com_id):
        '''
        尺码资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVSize.size_desc.label('size_name'),SyVSize.size_id.label('size_code'),
                    SyVSize.size_group,SyVSize.size_order.label('size_order_seq'))\
                    .filter().order_by(SyVSize.size_id)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'尺码资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}


    def get_color(self,page_index,com_id):
        '''
        颜色资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVColor.color_id,SyVColor.color_desc.label('color_name'),
                    literal(999).label('color_order_seq'))\
                    .filter().order_by(SyVColor.color_id)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}

    def get_itemcolor(self,page_index,com_id):
        '''
        款色资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVItemcolor.item_id,SyVItemcolor.color_id,
                    SyVItemcolor.item_picture)\
                    .filter().order_by(SyVItemcolor.item_id,SyVItemcolor.color_id)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()

            data_list = self.to_dict.query2dict(data_query)
            result_list = []
            for data in data_list:
                data['item_picture'] = f"c{str(com_id)}/spc/{data.get('item_picture')}"
                result_list.append(data)
            data_list = result_list
            data_count = len(result_list)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}


    def get_shop(self,page_index,com_id):
        '''
        店铺资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVShop.shop_code,SyVShop.shop_name,
                    SyVShop.shop_state)\
                    .filter().order_by(SyVShop.shop_code)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}

    def get_shop_type(self,page_index,com_id):
        '''
        店铺类型基础资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVShopType.st_code,SyVShopType.st_name,SyVShopType.st_type_code,
                    SyVShopType.st_type_name, SyVShopType.st_order_seq,SyVShopType.st_state)\
                    .filter().order_by(SyVShopType.st_code)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}

    def get_typeshop(self,page_index,com_id):
        '''
        店铺类型明细资料获取
        '''
        db_session = DbConnect()
        session = db_session.set_session()
        try:
            data_query = session.query(SyVTypeshop.st_code,SyVTypeshop.shop_code)\
                    .filter().order_by(SyVTypeshop.st_code, SyVTypeshop.shop_code)\
                    .limit(self.page_size).offset((page_index-1)*self.page_size).all()
        except  (sqlalchemy_exc.IntegrityError,sqlalchemy_exc.OperationalError,sqlalchemy_exc.InvalidRequestError,Exception) as e:
            session.close()
            log_txt = f'品类资料获取失败！{e}'
            return {'log_txt':log_txt, 'data_count':0}
        else:
            session.close()
            data_count = len(data_query)
            data_list = self.to_dict.query2dict(data_query)
            log_txt = f'本批次获取{data_count}行资料'
            return{'log_txt':log_txt,'data_count':data_count,'data_list':data_list}

            

    def sync_go(self,table_name,page_index,com_id):
        if table_name == 'sy_v_category':
            data = self.get_category(page_index,com_id)
        if table_name == 'sy_v_sea':
            data = self.get_sea(page_index,com_id)
        if table_name == 'sy_v_item':
            data = self.get_item(page_index,com_id)
        if table_name == 'sy_v_size':
            data = self.get_size(page_index,com_id)
        if table_name == 'sy_v_color':
            data = self.get_color(page_index,com_id)
        if table_name == 'sy_v_itemcolor':
            data = self.get_itemcolor(page_index,com_id)
        if table_name == 'sy_v_shop':
            data = self.get_shop(page_index,com_id)
        if table_name == 'sy_v_shop_type':
            data = self.get_shop_type(page_index,com_id)
        if table_name == 'sy_v_typeshop':
            data = self.get_typeshop(page_index,com_id)
        return data
            

