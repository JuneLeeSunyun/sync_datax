'''
Author: philipdeng
Date: 2021-03-20 17:36:22
LastEditors: philipdeng
LastEditTime: 2021-03-26 16:33:26
Description: file content
'''
from sqlalchemy import Column, Integer, TIMESTAMP, VARCHAR, text
from sqlalchemy.dialects.mysql import TINYINT

from models.base import BaseMixin



class SyVSea(BaseMixin):
    __tablename__ = 'sy_v_sea'

    sea_id = Column(VARCHAR(200), primary_key=True, comment='季度代码')
    sea_name = Column(VARCHAR(200), comment='季度名称')
    s_mon = Column(TINYINT, comment='季度开始月份')
    e_mon = Column(TINYINT, comment='季度结束月份')
    sea_desc = Column(VARCHAR(40), comment='季度描述')
    sea_order = Column(TINYINT, comment='季度序号')
    sea_erp_code = Column(VARCHAR(4), comment='季度ERP代号')
    sea_erp_name = Column(VARCHAR(20), comment='季度ERP名称')
    is_span_sea = Column(TINYINT, nullable=False, server_default=text("'0'"), comment=' 是否跨年季度')
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"), comment='时间戳')
