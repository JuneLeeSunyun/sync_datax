'''
Author: philipdeng
Date: 2021-03-15 14:27:08
LastEditors: philipdeng
LastEditTime: 2021-03-16 21:32:59
Description: 读取配置文件
'''
import configparser
import os,sys


class ReadConf():
    '''
    读取配置文件信息
    '''

    
    def __init__(self):
        # self.this_path = os.path.dirname(__file__)
        # self.parent_path = os.path.dirname(self.this_path)
        self.parent_path=sys.path[0]
        self.conf_path = os.path.join(self.parent_path , 'config/')


    def get_file(self,file_ext):
        config_list = []
        for root,dirs,files in os.walk(self.conf_path):
            for name in files:
                if file_ext in name:
                    config_list.append(os.path.join(self.conf_path,name))
        return config_list
        
    
    def read_conf(self):
        config = configparser.ConfigParser()
        config_list = self.get_file('.conf')
        config.read(config_list)
        return config




    