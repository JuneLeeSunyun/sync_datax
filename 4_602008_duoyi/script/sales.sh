#!/bin/bash

# 根据所输入日期抽取交易资料

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "3 day ago" +%Y-%m-%d`
	s_mday=`date +%Y-%m-01`
else
    s_day=$sd
	s_mday=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
	e_mday=`date -d "1 month" +%Y-%m-01`
else
    e_day=$ed
	e_mday=$ed
fi


Location='/opt/sydata/datax/bin'


cd $Location;

python datax.py ../job/ms2my/ms2my_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
