#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "3 day ago" +%Y-%m-%d`
	s_mday=`date +%Y-%m-01`
else
    s_day=$sd
    s_mday=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
	e_mday=`date -d "1 month" +%Y-%m-01`
else
    e_day=$ed
    e_mday=$ed
fi


Location='/opt/sydata/datax/bin'
#s_day=`date -d "1 day ago" +%Y-%m-%d`
#e_day=`date +%Y-%m-%d`

cd $Location;
python datax.py ../job/ms2my/ms2my_category.json;
python datax.py ../job/ms2my/ms2my_size.json;
python datax.py ../job/ms2my/ms2my_color.json;
python datax.py ../job/ms2my/ms2my_sea.json;
python datax.py ../job/ms2my/ms2my_item.json;
python datax.py ../job/ms2my/ms2my_barcode.json;
python datax.py ../job/ms2my/ms2my_itemcolor.json;
python datax.py ../job/ms2my/ms2my_itemsize.json;
python datax.py ../job/ms2my/ms2my_shop.json;
python datax.py ../job/ms2my/ms2my_vip.json;
python datax.py ../job/ms2my/ms2my_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_stock.json;
python datax.py ../job/ms2my/ms2my_staff.json;
python datax.py ../job/ms2my/ms2my_plani_d.json -p "-Ds_day=$s_mday -De_day=$e_mday"
python datax.py ../job/ms2my/ms2my_staffplani_d.json -p "-Ds_day=$s_mday -De_day=$e_mday"
#cd /opt/lampp/htdocs/ci;/opt/lampp/bin/php index.php gdata_v5 sync_all 1 all N today tomorrow all 7;
cd /opt/sydata/lsl_sync_c;python3 lsl_sync_c.py
