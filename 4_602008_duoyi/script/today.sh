#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date +%Y-%m-%d`
	s_mday=`date +%Y-%m-01`
else
    s_day=$sd
	s_mday=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date -d "1 day" +%Y-%m-%d`
	e_mday=`date -d "1 month" +%Y-%m-01`
else
    e_day=$ed
	e_mday=$ed
fi


Location='/opt/sydata/datax/bin'
#s_day=`date -d "1 day ago" +%Y-%m-%d`
#e_day=`date +%Y-%m-%d`

cd $Location;
python datax.py ../job/ms2my/ms2my_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/ms2my/ms2my_stock.json;
python datax.py ../job/ms2my/ms2my_staff.json;
python datax.py ../job/ms2my/ms2my_plani_d.json -p "-Ds_day=$s_mday -De_day=$e_mday"
cd /opt/lampp/htdocs/ci;/opt/lampp/bin/php index.php lsldata clean_cache_in_lsl;
