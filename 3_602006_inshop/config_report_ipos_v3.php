<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

$config['cid'] = 3;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000; //PAGE_LIMIT
$config['shop_count'] = 10;

//大类
$sy_v_item_class = 'sy_v_item_class';
$config[$sy_v_item_class] = array(
	'table' => '',
	'columns' => array(
		'class_id' => "' '",
		'class_name' => "' '",
		'class_code' => "' '",
		'class_prop' => "' '",
		'class_state' => "'1'",
		'class_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('class_id'),
	'duplicate' => array('class_name'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

//品类
$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
	'table' => 'com_base_dalei',
	'columns' => array(
		'ic_id' => "dldm",
		'ic_name' => "dlmc",
		'ic_code' => "dldm",
		'class_id' => "' '",
		'ic_prop' => "'2'",
		'ic_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('ic_id'),
	'duplicate' => array('ic_name'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//小小类
$sy_v_smallcategory = 'sy_v_smallcategory';
$config[$sy_v_smallcategory] = array(
	'table' => '',
	'columns' => array(
		'sc_id' => "' '",
		'sc_name' => "' '",
		'sc_code' => "' '",
		'sc_prop' => "'2'",
		'sc_state' => '1',
		'ic_id' => "' '",
		'sc_order' => "'999'"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sc_id'),
	'duplicate' => array('sc_name'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
	'table' => 'com_base_guige2',
	'columns' => array(
		'size_group' => 'ggwz1',
		'size_id' => "ggdm",
		'size_desc' => "ggmc",
		'size_order' => "ggwz2"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('size_id', 'size_group'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
	'table' => 'com_base_pinpai',
	'columns' => array(
// 		'id' => 'id',
		'brand_id' => "ppdm",
		'brand_name' => "ppmc",
		'sort_order' => "999",
		'active' => '1'
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('brand_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
	'table' => 'com_base_tmdzb',
	'columns' => array(
		'bar_no' => "com_base_tmdzb.sptm",
		'item_id' => "com_base_tmdzb.spdm",
		'color_id' => "com_base_tmdzb.gg1dm",
		'color_name' => "com_base_guige1.ggmc",
		'size_id' => "com_base_guige2.ggmc"
	),
	'join' => array(
		'com_base_guige1' => 'com_base_guige1.id = com_base_tmdzb.gg1_id',
		'com_base_guige2' => 'com_base_guige2.id = com_base_tmdzb.gg2_id'),
	'where' => "com_base_tmdzb.sptm is not null and com_base_tmdzb.sptm <> ''",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('bar_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
	'table' => 'com_base_guige1',
	'columns' => array(
		'color_id' => "ggdm",
		'color_desc' => "ggmc"
	),
	'join' => array(),
	'where' => '',
	'group_by' => '',
	'date_key' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
	'table' => 'com_base_spgg2',
	'columns' => array(
		'item_id' => "spdm",
		'size_group' => "com_base_guige2.ggwz1"
	),
	'join' => array(
		'com_base_guige2' => 'com_base_guige2.ggdm = com_base_spgg2.ggdm'),
	'where' => "",
	'date_key' => '',
	'group_by' => array('spdm'),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'size_group'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
	'table' => 'com_base_spgg1',
	'columns' => array(
		'item_id' => "spdm",
		'color_id' => "ggdm",
		'item_picture' => "concat(substring(spdm, 3), '.jpg')",
	),
	'join' => array(),
	'where' => "",
	'date_key' => '',
	'group_by' => array(),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
	'table' => 'goods',
	'columns' => array(
		'sea_id' => 'apply_spr||apply_sum||apply_aut||apply_win',
		'sea_name' => "(case apply_spr||apply_sum||apply_aut||apply_win when 'YYYY' THEN '全季度' when 'YYNY' THEN '春夏冬' when 'YNNN' THEN '春季' when 'NYNY' THEN '夏冬' when 'NYNN' THEN '夏季' when 'NNYN' THEN '秋季' when 'NNNY' THEN '冬季' when 'NNNN' THEN '无季度' ELSE '暂无' end)",
		'sea_erp_code' => 'apply_spr||apply_sum||apply_aut||apply_win',
		'sea_erp_name' => "(case apply_spr||apply_sum||apply_aut||apply_win when 'YYYY' THEN '全季度' when 'YYNY' THEN '春夏冬' when 'YNNN' THEN '春季' when 'NYNY' THEN '夏冬' when 'NYNN' THEN '夏季' when 'NNYN' THEN '秋季' when 'NNNY' THEN '冬季' when 'NNNN' THEN '无季度' ELSE '暂无' end)",
	),
	'join' => array(),
	'where' => "isstyle='N'",
	'date_key' => '',
	'group_by' => array('pid', 'colorid'),
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id', 'color_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);*/
$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
	'table' => 'com_base_jijie',
	'columns' => array(
		'sea_id' => 'jjdm',
		'sea_name' => "jjmc"
	),
	'join' => array(),
	'where' => "",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('sea_id'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
//商品资料
$config['sea_span'] ="'04'";    	//跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');	//跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(		//季度具体时间段
	"'01'" => array('from' => '02', 'to' => '04'),
	"'02'" => array('from' => '05', 'to' => '07'),
	"'03'" => array('from' => '08', 'to' => '10'),
	"'04'" => array('from' => '11', 'to' => '01')	
);
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
	'table' => 'com_base_shangpin',
	'columns' => array(
		'item_id' => "spdm",
		'brand_id' => "''",
		'is_add' => '1',
		'is_included' => "' '",
		'year_id' => "nddm",
		'sea_id' => 'com_base_jijie.jjdm',
		'sea_name' => "com_base_jijie.jjmc",
		'cost' => "0",
		'sub_icid' => "' '",
		'wave_no' => "' '",
		'class_id' => "' '",
		'class_name' => "' '",
		'ic_id' => 'com_base_shangpin.dldm',
		'ic_name' => 'com_base_dalei.dlmc',
		'sc_id' => "' '",
		'sc_name' => "' '",
		'is_acc' => "'N'",
		'sale_price' => "bzsj",
		'item_desc' => "spmc",
		'sales_point' => "' '",
		'item_file' => "concat(substring(spdm, 3), '.jpg')",
		'info_url' => "' '",
		'style_id' => "' '",
		's_date' => 'from_unixtime(ssrq, "%Y-%m-%d")',
		'e_date' => 'from_unixtime(UNIX_TIMESTAMP(timestampadd(day, 19, from_unixtime(ssrq))), "%Y-%m-%d")'
	),
	'join' => array(
		'com_base_jijie' => 'com_base_jijie.id = com_base_shangpin.jj_id',
		'com_base_dalei' => 'com_base_dalei.dldm = com_base_shangpin.dldm'),
	'where' => "com_base_shangpin.pp_id <> 10995",
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('item_id'),
    'duplicate' => array('year_id', 'sea_id', 'sea_name', 'ic_id', 'ic_name', 's_date', 'e_date'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
	'table' => 'com_base_kehu',
	'columns' => array(
		'shop_code' => "khdm",
		'shop_name' => "khmc",
		'model_no' => "' '",
		'ref_shopcode' => "' '",
		'jm_code' => "' '",
		'shop_level' => "' '",
		'branch_id' => "' '",
		'open_date' => "IF(jdrq is null, '', FROM_UNIXTIME(jdrq))",
		'close_date' => "' '",
		'shop_area' => "dpmj",
		'stand_staffs' => "0",
		'real_staff' => "ygrs",
		'max_sku' => "0",
		'min_sku' => "0",
		'max_qtys' => "0",
		'min_qtys' => "0",
		'tel_no' => "phone",
		'addr' => "IF(dz1 is null, '', dz1)",
		'contact' => "' '"
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code'),
	'duplicate' => array('shop_name', 'tel_no', 'addr','open_date', 'close_date', 'shop_area', 'real_staff'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型
 * sy_v_shop_type
 *
DROP TABLE IF EXISTS sy_v_shop_type;
CREATE TABLE IF NOT EXISTS `sy_v_shop_type` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `st_name` varchar(200) DEFAULT NULL COMMENT '类型名称',
  `st_type_code` varchar(200) DEFAULT NULL COMMENT '所属类型代号',
  `st_type_name` varchar(200) DEFAULT NULL COMMENT '所属类型名称',
  `st_order_seq` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序号',
  `st_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否有效：0.无效；1.有效',
  PRIMARY KEY (`st_id`),
  INDEX `st_code` (`st_code`),
  INDEX `st_type_code` (`st_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型基础表';
 * */
$sy_v_shop_type = 'sy_v_shop_type';
$config[$sy_v_shop_type] = array(
    'table' => 'com_base_khsx4',
    'columns' => array(
        'st_code' => 'sxdm',
        'st_name' => 'sxmc',
        'st_type_code' => "'ALL'",
        'st_type_name' => "'全部'",
        'st_order_seq' => 'sxdm',
        'st_state' => "'1'",
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

/*
 * 店铺类型明细
 * sy_v_typeshop
 *
DROP TABLE IF EXISTS sy_v_typeshop;
CREATE TABLE IF NOT EXISTS `sy_v_typeshop` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `st_code` varchar(200) DEFAULT NULL COMMENT '类型代号',
  `shop_code` varchar(200) DEFAULT NULL COMMENT '店铺代号',
  PRIMARY KEY (`ts_id`),
  INDEX `st_code` (`st_code`),
  INDEX `shop_code` (`shop_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺类型明细表';
 * */
$sy_v_typeshop = 'sy_v_typeshop';
$config[$sy_v_typeshop] = array(
    'table' => 'com_base_kehu',
    'columns' => array(
        'st_code' => "ifnull(sx4dm,'000')",
        'shop_code' => 'khdm',
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('st_code', 'shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);




$sales_info = 'sales_info';
$config[$sales_info] = array(
	'table' => 'ipos_qtlsd',
	'columns' => array(
		'inv_no' => "djbh",
		'shop_code' => "zddm",
		'sale_date' => 'from_unixtime(yyrq, "%Y-%m-%d %H:%i:%s")',
		'vip_state' => "IF(vpdm = '', 0, 1)",
		'staff_id' => "SUBSTRING_INDEX(dgy_list_dm,rtrim(','),1)",
		'vip_no' => "vpdm",
		'inv_qtys' => '0',
		'inv_money' => "je",
		'tag_money' => "0"
	),
	'join' => array(),
	'where' => 'zf=0 and gd=0',
	'date_key' => 'from_unixtime(yyrq, "%Y-%m-%d %H:%i:%s")',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
	'table' => 'ipos_qtlsdmx',
	'columns' => array(
		'inv_no' => 'djbh',
		'item_id' => "IF(spdm is null, ' ', spdm)",
		'color_id' => "IF(gg1dm is null, ' ', gg1dm)",
		'size_id' => "IF(com_base_guige2.ggmc is null, ' ', com_base_guige2.ggmc)",
		'inv_qtys' => 'ipos_qtlsdmx.sl',
		'inv_money' => 'ipos_qtlsdmx.je',
		'tag_price' => 'ckj'
	),
	'join' => array('ipos_qtlsd' => 'ipos_qtlsd.id = ipos_qtlsdmx.pid',
		'com_base_guige2' => 'com_base_guige2.ggdm = ipos_qtlsdmx.gg2dm'),
	'where' => 'ipos_qtlsd.gd=0 and ipos_qtlsd.zf=0',
	'date_key' => 'from_unixtime(ipos_qtlsd.yyrq, "%Y-%m-%d %H:%i:%s")',
	'group_by' => '',
	'by_shop' => true,
	'shop_code' => 'ipos_qtlsd.zddm',
	'sy_key' => array('inv_no'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

//销售目标
/*
select ipos_zdjbb.zddm, nd, mn, je1, je2, je3, je4, je5, je6, je7, je8, je9, je10, je11, je12, je13, je14, je15, je16, je17, je18, je19, je20, je21, je22, je23, je24, je25, je26, je27, je28, je29, je30, je31 from 
(
select ipos_rizbdmx.zd_id, ipos_rizbd.nd, ipos_rizbd.mn, max(ipos_rizbd.id) as id from ipos_rizbdmx 
left join ipos_rizbd on ipos_rizbd.id = ipos_rizbdmx.pid 
where  (nd > year(now()) or (nd = year(now()) and mn >= month(now())))
group by ipos_rizbdmx.zd_id , ipos_rizbd.nd, ipos_rizbd.mn
) a
left join ipos_rizbdmx on ipos_rizbdmx.pid = a.id and ipos_rizbdmx.zd_id = a.zd_id
left join ipos_zdjbb on ipos_zdjbb.pid = a.zd_id
*/
$plani_type_one_month_a_line = 'plani_with_type1';	//数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';		//数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';	//数据源表中一行记录代表的时间为某个时间段的情况
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
	'table' => '(
select ipos_rizbdmx.zd_id, ipos_rizbd.nd, ipos_rizbd.mn, max(ipos_rizbd.id) as id from ipos_rizbdmx 
left join ipos_rizbd on ipos_rizbd.id = ipos_rizbdmx.pid 
where  (nd > year(now()) or (nd = year(now()) and mn >= month(now())-1))
group by ipos_rizbdmx.zd_id , ipos_rizbd.nd, ipos_rizbd.mn
) a',
	'columns' => array(
		'shop_code' => 'zddm',
		'plan_date' => array(
			'year' => 'nd',
			'month' => 'mn',
			'day' => array('je1', 'je2', 'je3', 'je4', 'je5', 'je6', 'je7', 'je8', 'je9', 'je10', 'je11', 'je12', 'je13', 'je14', 'je15', 'je16', 'je17', 'je18', 'je19', 'je20', 'je21', 'je22', 'je23', 'je24', 'je25', 'je26', 'je27', 'je28', 'je29', 'je30', 'je31')
			)
	),
	'join' => array('ipos_rizbdmx' => 'ipos_rizbdmx.pid = a.id and ipos_rizbdmx.zd_id = a.zd_id',
					'ipos_zdjbb' => 'ipos_zdjbb.pid = a.zd_id'),
	'where' => '',
	'date_key' => '',//'ipos_rizbdmx.lastchanged',
	'group_by' => '',
	'by_shop' => false,
	'shop_code' => '',
	'sy_key' => array('shop_code', 'plan_date'),
// 	'duplicate' => array('plan_amt'),
	'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
	'type' => $plani_type_one_month_a_line
);

$sy_v_shiftsales = 'sy_v_shiftsales';
$config[$sy_v_shiftsales] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => '',
		'new_amt' => '',
		'tag_amt2' => '',
		'add_qtys' => '',
		'sale_sheets' => '',
		'new_vips' => '',
		'vip_sheets' => '',
		'vip_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shiftsalesdet = 'sy_v_shiftsalesdet';
$config[$sy_v_shiftsalesdet] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'sale_date' => '',
		'shift_code' => '',
		'item_id' => '',
		'color_id' => '',
		'size_id' => '',
		'sale_qtys' => '',
		'sale_amt' => '',
		'tag_amt' => ''
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'sale_date', 'shift_code'),
	'is_multi' => false	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
	'table' => 'ipos_spkcb',
	'columns' => array(
		'shop_code' => 'zddm',
		'item_id' => 'ipos_spkcb.spdm',
		'color_id' => 'gg1dm',
		'size_id' => 'gg2mc',
		'stock_qtys' => 'sum(sl)'
	),
	'join' => array(
                    'ipos_zdjbb' => 'ipos_zdjbb.pid = ipos_spkcb.zd_id',
                    'com_base_shangpin' => 'ipos_spkcb.spdm = com_base_shangpin.spdm'),
	'where' => 'ipos_zdjbb.zddm is not null and com_base_shangpin.pp_id <> 10995 and ipos_spkcb.sl<>0',
	'group_by' => array('zddm', 'ipos_spkcb.spdm', 'gg1dm', 'gg2mc'),
	'date_key' => "",
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('shop_code', 'item_id', 'color_id', 'size_id'), 
	'by_limit' => true,
// 	'duplicate' => array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shoparea = 'sy_v_shoparea';
$config[$sy_v_shoparea] = array(
	'table' => '',
	'columns' => array(
		'shop_code' => '',
		'class_id' => '',
		'sa_area' => ""
	),
	'join' => array(),
	'where' => '',
	'date_key' => '',
	'by_shop' => false,
	'sy_key' => array('shop_code', 'class_id'),
	'is_multi' => true
);

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
	'table' => 'ipos_vip',
	'columns' => array(
		'vip_no' => "ipos_vip.vpdm",
		'cust_name' => "ipos_vip.vpmc",
		'shop_code' => "ipos_zdjbb.zddm",
		'cust_sex' => "ipos_customer.xb+1",
		'age' => "0",
		'born_date' => 'if(from_unixtime(ipos_customer.sr1, "%Y-%m-%d") is null, "", from_unixtime(ipos_customer.sr1, "%Y-%m-%d"))',			//格式转换，暂时需要加上
		'register_date' => 'from_unixtime(ipos_vip.qyrq, "%Y-%m-%d %H:%i:%S")',		//格式转换，暂时需要加上
		'vip_level' => "ipos_viplb.lbdm",
		'mobile' => "ipos_customer.sj",
		'address' => "ipos_customer.dz",
		'staff_id' => "ifnull(ipos_dianyuan.dydm,0)",
		'vip_state' => "(CASE WHEN ipos_vip.status = 1 THEN 1 ELSE 2 END)",
		'active_money' => '0',
		'vip_points'=>'ipos_vip.dqjf'
	),
	'join' => array(
		'ipos_customer' => 'ipos_customer.gkdm = ipos_vip.gkdm',
		'ipos_viplb' => 'ipos_viplb.id = ipos_vip.lb_id',
		'ipos_zdjbb' => 'ipos_zdjbb.pid = ipos_vip.zd_id',
		'ipos_dianyuan' => 'ipos_dianyuan.id = ipos_vip.jsdy'),
	'where' => "",
	'shop_code' => 'ipos_zdjbb.zddm',
	'date_key' => '',
	'sy_key' => array('vip_no'),
	'by_shop' => true,
	'by_limit' => false,
	'is_multi' => true,
);

//员工信息
$sy_v_staff = 'sy_v_staff';
$config[$sy_v_staff] = array(
	'table' => 'ipos_dianyuan',
	'columns' => array(
		'staff_id' => "dydm",
		'staff_name' => "dymc",
		'staff_shop_code' => "khdm",
		'staff_shop_name' => "khmc"
	),
	'join' => array(
		'com_base_kehu' => 'com_base_kehu.id = ipos_dianyuan.zd_id'
		),
	'where' => "",
	'shop_code' => 'khdm',
	'date_key' => '',
	'sy_key' => array('staff_id'),
	'by_shop' => true,
	'by_limit' => false,
	'is_multi' => true,
);


$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => 'ipos_zdjhd',
	'columns' => array(
		'stock_date' => 'FROM_UNIXTIME(ipos_zdjhd.rq, "%Y-%m-%d")',
		'shop_code' => 'zddm',
		'item_id' => 'ipos_zdjhdmx.spdm',
		'color_id' => 'ipos_zdjhdmx.gg1dm',
		'size_id' => 'ipos_zdjhdmx.gg2mc',
		'stock_qtys' => 'sum(ipos_zdjhdmx.sl)'
	),
	'join' => array(
                    'ipos_zdjhdmx' => 'ipos_zdjhd.id=ipos_zdjhdmx.dj_id'),
	'where' => 'year(FROM_UNIXTIME(ipos_zdjhd.rq, "%Y-%m-%d")) = year(now()) and month(FROM_UNIXTIME(ipos_zdjhd.rq, "%Y-%m-%d")) >= month(now())',
	'group_by' => array('zddm', 'ipos_zdjhd.rq', 'ipos_zdjhdmx.spdm', 'gg1dm', 'gg2mc'),
	'date_key' => "",
	'by_shop' => false,
	'shop_code' => 'zddm',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'), 
	'by_limit' => true,
// 	'duplicate' => array('stock_qtys'),
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
/* End of file config.php */
/* Location: ./application/config/config.php */
