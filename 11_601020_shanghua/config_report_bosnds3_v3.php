<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/
//decode(aa,null, '当空时要填充的内容 '， '非空时内容 ', '默认内容 ')
$config['cid'] = 11;
$config['resource_data_date_type'] = 1; //时间格式：1:2019-01-01   2:20190101
$config['page_limit'] = 100000;
    
$config['shop_count'] = 20;

//会员资料
$sy_v_vip = 'sy_v_vip';
$config[$sy_v_vip] = array(
    'table' => 'C_VIP',
    'columns' => array(
        'vip_no' => "nvl(C_VIP.CARDNO,' ')",//rtrim(C_VIP.CARDNO)'
        'cust_name' => "nvl(C_VIP.VIPNAME,' ')",
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'cust_sex' => "(case when C_VIP.SEX='M' then '1' when C_VIP.SEX='W' then '2' ELSE '3' end)",
        'age' => "' '",
        'born_date' => "to_char(DECODE(LENGTH(to_char(C_VIP.BIRTHDAY)), 8, to_date(to_char(C_VIP.BIRTHDAY), 'yyyy-mm-dd'), sysdate), 'yyyy-mm-dd')",//to_char(to_date(C_VIP.BIRTHDAY, 'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')", //格式转换，暂时需要加上
        'register_date' => "to_char(DECODE(LENGTH(to_char(C_VIP.OPENCARDDATE)), 8, to_date(to_char(OPENCARDDATE), 'yyyy-mm-dd hh24:mi:ss'), sysdate), 'yyyy-mm-dd hh24:mi:ss')",//"to_char(to_date(C_VIP.OPENCARDDATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')",    //格式转换，暂时需要加上 C_VIP.OPENCARDDATE
        'vip_level' => "' '",
        'mobile' => "nvl(C_VIP.MOBIL,' ')",
        'address' => "nvl(C_VIP.ADDRESS,' ')",
        'staff_id' => "nvl(HR_EMPLOYEE.NO,' ')",
        'vip_state' => "(case when C_VIP.ISACTIVE='Y' then '1' else '2' end)",
        'active_money' => "nvl(C_VIP.INTEGRAL, 0)"
    ),
    'join' => array('HR_EMPLOYEE' => 'C_VIP.OPENCARDERID=HR_EMPLOYEE.ID','C_STORE' => 'C_VIP.C_STORE_ID=C_STORE.ID'),
    'where' => '',
    'shop_code' => 'rtrim(C_STORE.CODE)',
    'date_key' => '',
    'sy_key' => array('vip_no'),
    'duplicate' => array('cust_name', 'shop_code', 'cust_sex', 'age', 'born_date', 'register_date', 'vip_level', 'mobile', 'address', 'staff_id', 'vip_state', 'active_money'),    //主键冲突情况下，更新哪些字段，为空时表示抽取数据时全清空重抽
    'by_shop' => true,
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_category = 'sy_v_category';
$config[$sy_v_category] = array(
    'table' => 'M_DIM',
    'columns' => array(
        'ic_id' => 'ATTRIBCODE',
        'ic_name' => "ATTRIBNAME",
        'ic_code' => "ATTRIBCODE",
        'ic_prop' => "'2'",
        'ic_order' => '999'//客户表没有这个字段
    ),
    'join' => array(),
    'where' => "M_DIMDEF_ID =20 and ISACTIVE ='Y'",// and ATTRIBCODE not in ('000','100','101','103','106','107','108','95','96','97','98')",//2018-10-30
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('ic_id'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'duplicate' => array('ic_name')
);


$sy_v_size = 'sy_v_size';
$config[$sy_v_size] = array(
    'table' => 'M_SIZE',
    'columns' => array(
        'size_group' => 'M_ATTRIBUTE_ID',
        'size_id' => 'VALUE',
        'size_desc' => 'NAME',
      'size_order' => 'MARTIXCOL'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('size_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_brand = 'sy_brand';
$config[$sy_brand] = array(
    'table' => 'M_DIM',
    'columns' => array(
// 		'id' => 'id',
        'brand_id' => 'ATTRIBCODE',
        'brand_name' => 'ATTRIBNAME',
        'sort_order' => "' '",
        'active' => '1'
    ),
    'join' => array(),
    'where' => ' M_DIMDEF_ID =1',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('brand_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_barcode = 'sy_v_barcode';
$config[$sy_v_barcode] = array(
    'table' => 'M_PRODUCT_ALIAS',
    'columns' => array(
        'bar_no' => 'M_PRODUCT_ALIAS.NO',    //有时数据源没有bar_no，需要自己生成，注意语法（mysql为concat，mssql为+）
        'item_id' => 'rtrim(M_PRODUCT.NAME)',
        'color_id' => 'M_ATTRIBUTESETINSTANCE.VALUE1_CODE',
        'size_id' => 'M_ATTRIBUTESETINSTANCE.VALUE2_CODE',
        'color_name' => 'M_ATTRIBUTESETINSTANCE.VALUE1'
    ),
    'join' => array('M_PRODUCT' => 'M_PRODUCT_ALIAS.M_PRODUCT_ID=M_PRODUCT.ID', 'M_ATTRIBUTESETINSTANCE' => 'M_PRODUCT_ALIAS.M_ATTRIBUTESETINSTANCE_ID=M_ATTRIBUTESETINSTANCE.ID'),//表的关联
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('bar_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_color = 'sy_v_color';
$config[$sy_v_color] = array(
    'table' => 'M_COlOR',
    'columns' => array(
        'color_id' => 'VALUE',
        'color_desc' => 'NAME'
    ),
    'join' => array(),
    'where' => '',
    'group_by' => '',
    'date_key' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_itemsize = 'sy_v_itemsize';
$config[$sy_v_itemsize] = array(
    'table' => 'M_PRODUCT',
    'columns' => array(
        'item_id' => 'rtrim(NAME)',
        'size_group' => 'M_SIZEGROUP_ID'
    ),
    'join' => array(),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'size_group'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_itemcolor = 'sy_v_itemcolor';
$config[$sy_v_itemcolor] = array(
    'table' => 'M_PRODUCT_ALIAS',
    'columns' => array(
        'item_id' => 'distinct M_PRODUCT.NAME ',
        'color_id' => 'M_ATTRIBUTESETINSTANCE.VALUE1_CODE',
        'item_picture' => "' '"
    ),
    'join' => array('M_PRODUCT' => 'M_PRODUCT_ALIAS.M_PRODUCT_ID=M_PRODUCT.ID', 'M_ATTRIBUTESETINSTANCE' => 'M_PRODUCT_ALIAS.M_ATTRIBUTESETINSTANCE_ID=M_ATTRIBUTESETINSTANCE.ID'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id', 'color_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


$sy_v_sea = 'sy_v_sea';
$config[$sy_v_sea] = array(
    'table' => 'M_DIM',//M_PRODUCT
    'columns' => array(
        'sea_id'=> 'ATTRIBCODE',
        'sea_name' =>'ATTRIBNAME',//char_length
        'sea_desc'=>'ATTRIBNAME',
        's_mon' => "(case M_DIM.ATTRIBNAME
				when '春' then '03'
				when '夏' then '03'
				when '秋' then '09'
				when '冬' then '09'
				else '01' end
			)",
        'e_mon' => "(case M_DIM.ATTRIBNAME
				when '春' then '08'
				when '夏' then '08'
				when '秋' then '02'
				when '冬' then '02'
				else '12' end
			)",
        'is_span_sea' => "(case M_DIM.ATTRIBNAME
			when '春' then '0'
			when '夏' then '0'
			when '秋' then '1'
			when '冬' then '1'
			else '0' end)"
    ),
    'join' => array(),
    'where' => 'M_DIMDEF_ID=3',
    'date_key' => '',
    //'group_by' => array(''),
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('sea_id'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//商品资料
$config['sea_span'] = "'D' or sea_id = 'Q'";        //跨年季度id
$config['year_offset'] = array('s' => 0, 'e' => '1');    //跨年季度开始时间与结束时间 年份的增加值，一般span_sea为春季id时，s为-1，e为0；span_sea为冬季id时，s为0，e为1
$config['sea_from_to'] = array(        //季度具体时间段
    "'X'" => array('from' => '03', 'to' => '08'),//夏
    "'Q'" => array('from' => '09', 'to' => '02'),//秋
    "'D'" => array('from' => '09', 'to' => '02'),//冬
    "'C'" => array('from' => '03', 'to' => '08')//春

);
//商品表
$sy_v_item = 'sy_v_item';
$config[$sy_v_item] = array(
    'table' => 'M_PRODUCT',
    'columns' => array(
        'item_id' => 'rtrim(NAME)',
        'brand_id' => 'M_DIM1_ID',
        'year_id' => '(select decode(ATTRIBCODE,NULL, 0, lpad(ATTRIBCODE, 4 ,20)) from M_DIM where M_DIMDEF_ID=2 and id=M_DIM2_ID)',//尚画本字段长度为2位，与app不匹配，因此不足4位则补齐（出现上世纪商品年份会出错，但目前没有这种情况）
        'sea_id' => '(select ATTRIBCODE from M_DIM where M_DIMDEF_ID=3 and id=M_DIM3_ID)',
        'sea_name' => '(select ATTRIBNAME from M_DIM where M_DIMDEF_ID=3 and id=M_DIM3_ID)',
        'ic_id' => '(select ATTRIBCODE from M_DIM where M_DIMDEF_ID=20 and id=M_DIM20_ID)',
        'ic_name' =>'(select ATTRIBNAME from M_DIM where M_DIMDEF_ID=20 and id=M_DIM20_ID)',
        'is_acc' => "'N'",
        'sale_price' => 'PRICELIST',
        'item_desc' => 'VALUE',
        'sales_point' => "' '",
        'item_file' => "rtrim(NAME)||'.jpg'",
        'info_url' => "' '",
        'style_id' => "' '",
        's_date' => "' '",
        'e_date' => "' '",
    ),
    'join' => array(),
    'where' => "M_DIM20_ID in (select id from M_DIM where ISACTIVE ='Y')",// and ATTRIBCODE not in ('000','100','101','103','106','107','108','95','96','97','98'))",//2018-10-30
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('item_id'),
    'duplicate' => array('year_id', 'sea_id', 'sea_name', 'ic_id', 'ic_name', 'sales_point'),
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//这个单独抽数的时候,已成功
$sy_v_shop = 'sy_v_shop';
$config[$sy_v_shop] = array(
    'table' => 'C_STORE',
    'columns' => array(
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'shop_name' => 'C_STORE.NAME',
        'model_no' => "' '",
        'branch_id' => 'C_STORE.M_DIM1_ID',
        'open_date' => "to_char(DECODE(LENGTH(to_char(RP_STOREINFO.BILLDATE_FRIST)), 8, to_date(to_char(RP_STOREINFO.BILLDATE_FRIST), 'yyyy-mm-dd'), sysdate), 'yyyy-mm-dd')",
        'shop_area' => 'C_STORE.PROPORTION',
        'ref_shopcode' => "' '",//参考代号
        'shop_level' => "' '",
        'stand_staffs' => 'P_COUNT',
        'real_staff' => 'P_COUNT',
        'max_sku' => "' '",
        'min_sku' => "' '",
        'max_qtys' => "' '",
        'min_qtys' => "' '",
        'tel_no' => 'C_STORE.PHONE',
        'addr' => 'C_STORE.ADDRESS',
        'contact' => "C_STORE.CONTACTOR",
        'shop_state' => "(case when C_STORE.ISACTIVE='Y' then '1' else '0' end)"
    ),
    'join' => array(
    	'RP_STOREINFO' => 'RP_STOREINFO.C_STORE_id = C_STORE.id',
    	"(select HR_EMPLOYEE.C_STORE_ID,count(*) as P_COUNT from HR_EMPLOYEE where HR_EMPLOYEE.ISACTIVE ='Y' and HR_EMPLOYEE.ISSALER='Y' group by C_STORE_ID) P" => 'P.C_STORE_ID = C_STORE.id'),
    'where' => '',
    'date_key' => '',
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',
    'sy_key' => array('shop_code'),
    'duplicate' => array('shop_name', 'open_date', 'shop_level', 'ref_shopcode', 'shop_area', 'tel_no', 'addr', 'contact', 'stand_staffs', 'real_staff', 'shop_state'),
    'is_multi' => true //多数据库情况下是否这个数据库这个表要抽
);
//这个单独抽数的时候,已成功
$sales_info = 'sales_info';
$config[$sales_info] = array(
    'table' => 'M_RETAIL',
    'columns' => array(
        'inv_no' => 'M_RETAIL.DOCNO',
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'sale_date' => "to_char(to_date(M_RETAIL.BILLDATE,'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')",
        'vip_state' => "(case when M_RETAIL.C_VIP_ID is not null  then '1' else '0' end)",
        'staff_id' => 'HR_EMPLOYEE.NO',
        'vip_no' => "decode(C_VIP.CARDNO,NULL, ' ', C_VIP.CARDNO)",
        'inv_money' => 'M_RETAIL.TOT_AMT_ACTUAL',
        'inv_qtys' => 'M_RETAIL.TOT_QTY',
        'tag_money' => 'M_RETAIL.TOT_AMT_LIST'
    ),
    'join' => array(
        'C_STORE' => 'M_RETAIL.C_STORE_ID=C_STORE.ID',
        'HR_EMPLOYEE' => 'M_RETAIL.SALESREP_ID=HR_EMPLOYEE.ID',
        'C_VIP' => 'M_RETAIL.C_VIP_ID=C_VIP.ID'
    ),
    'where' => 'M_RETAIL.STATUS=2',
    'date_key' => "TO_CHAR(TO_DATE(M_RETAIL.BILLDATE,'YYYY-MM-DD'), 'YYYY-MM-DD')",
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => 'C_STORE.CODE',
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);


//这个单独抽数的时候,已成功
$sales_detail = 'sales_detail';
$config[$sales_detail] = array(
    'table' => 'M_RETAILITEM',
    'columns' => array(
        'inv_no' => 'M_RETAIL.DOCNO',
        'item_id' => 'rtrim(M_PRODUCT.NAME)',
        'color_id' => 'M_ATTRIBUTESETINSTANCE.VALUE1_CODE',
        'size_id' => 'M_ATTRIBUTESETINSTANCE.VALUE2_CODE',
        'inv_qtys' => 'M_RETAILITEM.QTY',
        'inv_money' => 'M_RETAILITEM.TOT_AMT_ACTUAL',
        'tag_price' => 'M_RETAILITEM.PRICELIST'
    ),
    'join' => array(
        'M_RETAIL' => 'M_RETAILITEM.M_RETAIL_ID =M_RETAIL.ID',
        'M_PRODUCT' => 'M_RETAILITEM.M_PRODUCT_ID=M_PRODUCT.ID',
        'M_ATTRIBUTESETINSTANCE' => 'M_RETAILITEM.M_ATTRIBUTESETINSTANCE_ID=M_ATTRIBUTESETINSTANCE.ID'
    ),
    'where' => 'M_RETAIL.STATUS=2',
    'date_key' => "TO_CHAR(M_RETAILITEM.MODIFIEDDATE, 'yyyy-mm-dd')",
    'group_by' => '',
    'by_shop' => false,
    'shop_code' => '',//rtrim(M_RETAIL.BILLDATE)
    'sy_key' => array('inv_no'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

//这个单独抽数的时候,已成功
//销售目标
$config['plan_select_days'] = '2'; //1为默认，2为当月
$plani_type_one_month_a_line = 'plani_with_type1';    //数据源表中一行记录代表一个月数据的情况
$plani_type_one_day_a_line = 'plani_with_type2';        //数据源表中一行记录代表一天数据的情况
$plani_type_custom_date_a_line = 'plani_with_type9';    //数据源表中一行记录代表的时间为某个时间段的情况'plan_date' =>'C_STOREMARKITEM.MONTHDATE',
$sy_v_plani = 'sy_v_plani';
$config[$sy_v_plani] = array(
    'table' => 'C_STOREMARKITEM',//C_STOREMARKITEM
    'columns' => array(
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'plan_date' =>  array(
            'year' => '', 	//年度字段名
            'month' => '', 	//月份字段名
            'day' => "to_char(to_date(C_STOREMARKITEM.MONTHDATE,'yyyy-mm-dd'),'yyyy-mm-dd')"),
        'plan_amt' => 'C_STOREMARKITEM.TOT_AMT_MARK'
    ),
    'join' => array('C_STOREMARK' => 'C_STOREMARKITEM.C_STOREMARK_ID=C_STOREMARK.ID','C_STORE'=>'C_STOREMARK.C_STORE_ID =C_STORE.ID'),
    'where' => "C_STOREMARKITEM.MONTHDATE >= to_char(trunc(sysdate, 'mm'),'yyyymmdd') and C_STOREMARKITEM.TOT_AMT_MARK > 0",
    'date_key' => '',
    'group_by' => '',
    'by_shop' => true,
    'shop_code' => 'rtrim(C_STORE.CODE)',
    'sy_key' => array('shop_code', 'plan_date'),
    'duplicate' => '',//array('plan_amt'),
    'is_multi' => true,    //多数据库情况下是否这个数据库这个表要抽
    'type' => $plani_type_one_day_a_line
);


$sy_v_stock = 'sy_v_stock';
$config[$sy_v_stock] = array(
    'table' => 'FA_STORAGE',
    'columns' => array(
        'shop_code' => 'rtrim(C_STORE.CODE)',
        'item_id' => 'rtrim(M_PRODUCT.NAME)',
        'color_id' => 'M_ATTRIBUTESETINSTANCE.VALUE1_CODE',
        'size_id' => 'M_ATTRIBUTESETINSTANCE.VALUE2_CODE',
        'stock_qtys' => 'sum(FA_STORAGE.QTY)'
    ),
    'join' => array(
        'C_STORE' => 'FA_STORAGE.C_STORE_ID=C_STORE.ID',
        'M_PRODUCT' => 'FA_STORAGE.M_PRODUCT_ID =M_PRODUCT.ID',
        'M_ATTRIBUTESETINSTANCE' => 'FA_STORAGE.M_ATTRIBUTESETINSTANCE_ID=M_ATTRIBUTESETINSTANCE.ID'
    ),
    'where' => 'FA_STORAGE.QTY<>0',
    'group_by' => array('rtrim(C_STORE.CODE)', 'M_PRODUCT.NAME', 'M_ATTRIBUTESETINSTANCE.VALUE1_CODE', 'M_ATTRIBUTESETINSTANCE.VALUE2_CODE'),
    'date_key' => '',
    'by_shop' => true,
    'shop_code' => 'rtrim(C_STORE.CODE)',
    'sy_key' => array('shop_code'),
    'duplicate' => '',
    'is_multi' => true    //多数据库情况下是否这个数据库这个表要抽
);

$sy_v_shopstock = 'sy_v_shopstock';
$config[$sy_v_shopstock] = array(
	'table' => 'm_transfer a',
	'columns' => array(
		'shop_code' => 'trim(e.code)',
		'stock_date' => "TO_CHAR(TO_DATE(a.datein,'YYYY-MM-DD'), 'YYYY-MM-DD')",
		'item_id' => 'd.name',
		'color_id' => 'c.value1_code',
		'size_id' => 'c.value2_code',
		'stock_qtys' => 'sum(b.qtyin)'
	),
	'join' => array(
		'm_transferitem b' => 'a.id = b.m_transfer_id',
       	'm_attributesetinstance c' => 'c.id = b.m_attributesetinstance_id',
       	'm_product d' => 'd.id = b.m_product_id',
       	'c_store e' => 'a.c_dest_id = e.id'
	),
	'where' => "a.status = 2 AND a.out_status = 2 AND a.in_status = 2 AND a.confirm_status = 2 AND a.isactive = 'Y'",
	'group_by' => array('a.datein, e.code', 'd.name', 'c.value1_code', 'c.value2_code'),
	'date_key' => "TO_CHAR(TO_DATE(a.datein,'YYYY-MM-DD'), 'YYYY-MM-DD')",
	'by_shop' => true,
	'shop_code' => 'trim(e.code)',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'), 
	'is_multi' => true,	//多数据库情况下是否这个数据库这个表要抽
	'another_resource' => 'sy_v_shopstock_jm'
);

$sy_v_shopstock_jm = 'sy_v_shopstock_jm';
$config[$sy_v_shopstock_jm] = array(
	'table' => 'm_sale a',
	'columns' => array(
		'shop_code' => 'trim(e.code)',
		'stock_date' => "TO_CHAR(TO_DATE(a.datein,'YYYY-MM-DD'), 'YYYY-MM-DD')",
		'item_id' => 'd.name',
		'color_id' => 'c.value1_code',
		'size_id' => 'c.value2_code',
		'stock_qtys' => 'sum(b.qtyin)'
	),
	'join' => array(
		'm_saleitem b' => 'a.id = b.m_sale_id',
       	'm_attributesetinstance c' => 'c.id = b.m_attributesetinstance_id',
       	'm_product d' => 'd.id = b.m_product_id',
       	'c_store e' => 'a.c_dest_id = e.id'
	),
	'where' => "a.status = 2 AND a.out_status = 2 AND a.in_status = 2 AND a.confirm_status = 2 AND a.isactive = 'Y'",
	'group_by' => array('a.datein, e.code', 'd.name', 'c.value1_code', 'c.value2_code'),
	'date_key' => "TO_CHAR(TO_DATE(a.datein,'YYYY-MM-DD'), 'YYYY-MM-DD')",
	'by_shop' => true,
	'shop_code' => 'trim(e.code)',
	'sy_key' => array('shop_code', 'stock_date', 'item_id', 'color_id', 'size_id'), 
	'is_multi' => true	//多数据库情况下是否这个数据库这个表要抽
);
/* End of file config.php */
/* Location: ./application/config/config.php */
