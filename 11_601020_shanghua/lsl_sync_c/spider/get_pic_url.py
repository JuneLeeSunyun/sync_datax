'''
Author: philipdeng
Date: 2021-03-15 23:56:35
LastEditors: philipdeng
LastEditTime: 2021-03-26 17:07:09
Description: 都市丽人图片地址获取
'''
import requests,json

from libs.read_conf import ReadConf

class GetPicUrl():
    '''
    获取都市丽人图片url
    '''


    def __init__(self):
        config = ReadConf().read_conf()
        self.pic_url = config.get("SPIDER","PIC_URL")


    def get_pic_url(self,item_list):
        url = self.pic_url
        data = {'goodsCodeList':item_list}
        headers = {'external_key':'data:platform:external:cosmolady', 'Content-Type':'application/json'}
        get_data = requests.get(url=url, data=json.dumps(data),headers=headers)
        if get_data.status_code == 200:
            data_list = get_data.json()
            if data_list.get('data'):
                data_list = data_list.get('data')
                try:
                    pic_url_list = [{'item_id':key,'item_file':value[0].get('externalUrl')} for key,value in data_list.items()]
                    pic_url_list = filter(lambda f_list:True if not f_list.get('item_id').islower() else False,pic_url_list)
                    pic_url_list = list(pic_url_list)
                except Exception as e:
                    raise (f'发生错误！错误原因：{e}')
                else:
                    return(pic_url_list)
            else:
                print('所返回数据为空！')
                return None
        else:
            print('请求失败！')
            return None
