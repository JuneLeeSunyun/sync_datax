'''
Author: philipdeng
Date: 2021-03-26 11:05:12
LastEditors: philipdeng
LastEditTime: 2021-03-27 16:32:40
Description: 未分组店铺处理
'''
import requests,json

from libs.read_conf import ReadConf
from spider.login import LoginSys
from libs.message_logs import MessageLogs

# from cyberbrain import trace

class DefaultGroup():
    '''
    将未分组店铺放入APP服务器的一个默认分组
    '''
    def __init__(self):
        self.logs = MessageLogs()

    def default_info(self):
        config = ReadConf().read_conf()
        group_code = config.get("DEFAULT_GROUP","GROUP_CODE")
        group_name = config.get("DEFAULT_GROUP","GROUP_CODE")
        group_level = config.getint("DEFAULT_GROUP","GROUP_LEVEL")
        parent_group_code = config.get("DEFAULT_GROUP","PARENT_GROUP_CODE")
        group_order_seq = config.getint("DEFAULT_GROUP","GROUP_ORDER_SEQ")
        group_state = config.getint("DEFAULT_GROUP","GROUP_STATE")
        default_data = {'group_code':group_code, 'group_name':group_name, 'group_level':group_level,
                    'parent_group_code':parent_group_code, 'group_order_seq':group_order_seq,'group_state':group_state}
        return default_data

    # @trace
    def sync_group(self,api_url,token,data):
        url = f"{api_url}/v1/receive/default_group"
        headers = {'Content-Type':'application/json','SY-ACCESS-TOKEN':token}
        data = data
        try:
            get_data = requests.post(url=url, data=json.dumps(data),headers=headers)
            
            data_list = get_data.json()
            log_txt = f"{data_list.get('msg')},{data_list.get('request')}"
            print(log_txt)
            self.logs.message_logs(log_txt,'lsl_sync_c')
            return data_list

        except Exception as e:
            log_txt = e
            print(log_txt)
            self.logs.message_logs(log_txt,'lsl_sync_c')

    def sync_dg(self):
        config = ReadConf().read_conf()
        group_code = config.getboolean("DEFAULT_GROUP","GROUP_SYNC")
        if group_code:
            login_info = LoginSys().login_info()
            api_url = login_info.get('api_url')
            token = login_info.get('token')
            data = self.default_info()
            self.sync_group(api_url,token,data)
    