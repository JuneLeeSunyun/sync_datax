'''
Author: philipdeng
Date: 2021-03-20 17:38:11
LastEditors: philipdeng
LastEditTime: 2021-03-24 16:38:14
Description: file content
'''
# coding: utf-8
from sqlalchemy import Column, TIMESTAMP, VARCHAR, text
from models.base import BaseMixin



class SyVColor(BaseMixin):
    __tablename__ = 'sy_v_color'

    color_id = Column(VARCHAR(200), primary_key=True)
    color_desc = Column(VARCHAR(200), nullable=False)
    time_stamp = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))
