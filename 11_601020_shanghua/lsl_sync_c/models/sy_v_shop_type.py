# coding: utf-8
from sqlalchemy import Column, Integer, SmallInteger, VARCHAR, Integer, text


from models.base import BaseMixin


class SyVShopType(BaseMixin):
    __tablename__ = 'sy_v_shop_type'
    __table_args__ = {'comment': '店铺类型基础表'}

    st_id = Column(Integer, primary_key=True, comment='ID')
    st_code = Column(VARCHAR(200), index=True, comment='类型代号')
    st_name = Column(VARCHAR(200), comment='类型名称')
    st_type_code = Column(VARCHAR(200), index=True, comment='所属类型代号')
    st_type_name = Column(VARCHAR(200), comment='所属类型名称')
    st_order_seq = Column(SmallInteger, nullable=False, server_default=text("'0'"), comment='排序号')
    st_state = Column(Integer, nullable=False, server_default=text("'1'"), comment='是否有效：0.无效；1.有效')
