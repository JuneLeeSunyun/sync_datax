<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
  多数据库抽数配置，适用于数据源来自多个数据库，不管各个数据库中数据结构是否一致
*/

/**
	数据库与配置文件对应关系，结构为
	$config['database_resource'] = array(
		'a' => array('b' => 'c'),
		...
	);
	其中，
		a为自定义（需要与报表数据库中sy_v_data_resource表config_key对应）
		b为database.config中抽数数据源库对应的item
		c为数据库b对应的配置文件文件名，文件内容为客户数据源库与三云报表数据库的表字段匹配信息
		
*/

$config['database_resource'] = array(
	'all' => array(
		'report_resource' => 'config_report_bosnds3_v3'
	),
	'bosnds3' => array('report_resource' => 'config_report_bosnds3_v3')
);

//客户erp到建模服务器数据同步前，是否清除对应时间段内的销售数据，true表示清除，false表示不清除
$config['delete_sale_first'] = false;

//客户ERP到建模服务器数据同步后，进行初步数据计算时是否有特殊计算
$config['sales_t_info_insert_method'] = '';
$config['sy_v_sales_insert_method'] = '';

//app服务器地址及端口，用于清除缓存及资料同步
$config['base_admin_ip'] = '120.79.44.91';	
$config['base_admin_port'] = 80;	

//建模服务器到app服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_base'] = true;

//建模服务器到app服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_base'] = array(
	'sy_shop' => 1,		//店铺
	'sy_brand' => 1,	//品牌
 	'sy_spclass' => 1,	//品类
	'sy_item' => 1,		//单品
	'sy_itemsize' => 1,	//尺码
	'sy_itemcolor' => 0,//颜色
	'sy_sizeitem' => 1,	//单品尺码
	'sy_sitem' => 0,	//款色
	'sy_group' => 1,	//分组
	'sy_shopgroup' => 1	//店铺分组
);


$config['com_no'] = '601020';//零售链授权号
$config['lsl_admin_ip'] = 'lsla.sunyunsoft.cn';
$config['lsl_admin_port'] = 80;
//建模服务器到零售链服务器资料同步相关配置，true表示将进行同步，false表示不进行同步，true的情况下，需要结合table_in_base使用
$config['send_data_to_lsl'] = true;
//建模服务器到零售链服务器资料同步相关配置，1表示对应的资料将进行同步，0表示不同步
$config['table_in_lsl'] = array(
// 	'lsla_title' => 0,
// 	'lsla_dept' => 0,
	'lsla_groupshop' => 0,
	'lsla_shop' => 1,
	'lsla_group' => 0,
 	'lsla_category' => 1,
	'lsla_item' => 1,
	'lsla_color' => 1,
	'lsla_size' => 1,
	'lsla_itemcolor' => 0,
// 	'lsla_config' => 0,
// 	'lsla_user' => 0,
// 	'lsla_user_leader' => 0,
	'lsla_sea' => 1
);

$config['update_item_date'] = 'update_item_date';
$config['update_sum_sales_info'] = 'update_sum_sales_info';

$group_id = '9999';
$group_name = '未分组';

$config['query_after_sync_to_lsl'] = "insert into `lsla_group`(`group_code`, `group_name`, `group_level`, `parent_group_code`, `group_state`, `group_order_seq`) values ('$group_id', '$group_name', '1', '$group_id', '1', '999'); 
delete from lsla_groupshop where group_code = '$group_id';
insert into lsla_groupshop(`shop_code`, `group_code`, `gs_order_seq`) (
select shop_code, '$group_id' as group_code, '999' as gs_order_seq from lsla_shop 
where shop_code not in (
select shop_code from lsla_groupshop
)
)";
// $config['sales_t_detail_insert_method'] = 'insert_into_sales_t_detail_ys';
// $config['sy_v_sales_update_method'] = 'update_sy_v_sales_ys';

// $config['base_relative_path'] = 'syweb/api/';

/* End of file config.php */
/* Location: ./application/config/config.php */
