#!/bin/bash

# 根据所输入日期抽取交易资料
LOCK_NAME="/tmp/sales.lock"
if ( set -o noclobber; echo "$$" > "$LOCK_NAME") 2> /dev/null;
then
trap 'rm -f "$LOCK_NAME"; exit $?' INT TERM EXIT

### 开始正常流程


sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "1 day ago" +%Y-%m-%d`
else
    s_day=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
else
    e_day=$ed
fi

Location='/opt/sydata/datax/bin'


cd $Location;

python datax.py ../job/o2o/o2o_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";



### 正常流程结束

### Removing lock
rm -f $LOCK_NAME
trap - INT TERM EXIT
else
echo "Failed to acquire lockfile: $LOCK_NAME."
echo "Held by $(cat $LOCK_NAME)"
exit 1
fi
echo "Done."
