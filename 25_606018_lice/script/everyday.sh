#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "1 day ago" +%Y-%m-%d`
else
    s_day=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
else
    e_day=$ed
fi

Location='/opt/sydata/datax/bin'

cd $Location;
python datax.py ../job/o2o/o2o_item_class.json;
python datax.py ../job/o2o/o2o_category.json;
python datax.py ../job/o2o/o2o_size.json;
python datax.py ../job/o2o/o2o_color.json;
python datax.py ../job/o2o/o2o_sea.json;
python datax.py ../job/o2o/o2o_item.json;
python datax.py ../job/o2o/o2o_item_pic.json;
python datax.py ../job/o2o/o2o_barcode.json;
python datax.py ../job/o2o/o2o_cup.json;
python datax.py ../job/o2o/o2o_itemcolor.json;
python datax.py ../job/o2o/o2o_shop.json;
python datax.py ../job/o2o/o2o_shop_type.json;
python datax.py ../job/o2o/o2o_typeshop.json;
python datax.py ../job/o2o/o2o_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_detail_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_info_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_salesdet_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_salesdet_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_shop_map.json;
python datax.py ../job/o2o/o2o_shopstock_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_shopstock_md_d.json -p "-Ds_day=$s_day -De_day=$e_day";
#python datax.py ../job/o2o/o2o_stock.json;
python datax.py ../job/o2o/o2o_instock.json;
python datax.py ../job/o2o/o2o_staff.json;
cd /opt/sydata/lsl_sync_c;python3 lsl_sync_c.py
