#!/bin/bash

# 每天更新资料内容

sd=$1
ed=$2


if [ ! -n "$sd" ]; then
	s_day=`date -d "1 day ago" +%Y-%m-%d`
else
    s_day=$sd
fi

if [ ! -n "$ed" ]; then
	e_day=`date +%Y-%m-%d`
else
    e_day=$ed
fi

Location='/opt/sydata/datax/bin'

cd $Location;
python datax.py ../job/o2o/o2o_vip_d.json -p "-Ds_day=$s_day -De_day=$e_day";
python datax.py ../job/o2o/o2o_sales_d.json -p "-Ds_day=$s_day -De_day=$e_day";
cd /opt/sydata/lsl_sync_c;python3 clean_cache.py




